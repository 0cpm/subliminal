/* crc16-6sub8-tablegen.c -- Generate the table for CRC-16/6sub8
 *
 * The polynomial used is x^16 + x^8 + x^4 + x^3 + x^1 + x^0
 * The corresponding modulo-value is 0x1011b.
 * 
 * For details, see https://users.ece.cmu.edu/~koopman/crc/6sub8.html
 * Algorithms based on https://stackoverflow.com/questions/44131951/how-to-generate-16-bit-crc-table-from-a-polynomial
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>


int main (int argc, char *argv []) {
	if (argc != 4) {
		fprintf (stderr, "Usage: %s outfile name polynomial   # use 0x prefix for hex\n", argv [0]);
		exit (1);
	}
	char *endptr;
	long poly = strtoul (argv [3], &endptr, 0);
	if (*endptr != '\0') {
		fprintf (stderr, "Usage: %s outfile name polynomial   # use a proper number, possibly 0x...\n", argv [0]);
		exit (1);
	}
	if ((poly & ~ 0x0000ffffL) != 0x10000) {
		fprintf (stderr, "Usage: %s outfile name 0x1????   # Hex polynomial for 16 bits\n", argv [0]);
		exit (1);
	}
	uint16_t table [256];
	for (int i=0; i < 256; i++) {
		uint32_t crc = i << 8;
		for (int b = 0; b < 8; b++) {
			if ((crc & 0x8000) == 0) {
				crc <<= 1;
			} else {
				crc = (crc << 1) ^ poly;
			}
			assert ((crc & ~ 0x0000ffffL) == 0);
		}
		table [i] = crc;
	}
	FILE *fout = fopen (argv [1], "w");
	fprintf (fout, "/* Generated CRC-16 table for\n * - modulus %s\n * - polynomial ", argv [3]);
	char *plus = "";
	for (int bit = 16; bit >= 0; bit--) {
		if ((poly & (1 << bit)) != 0) {
			fprintf (fout, "%sx^%d", plus, bit);
			plus = " + ";
		}
	}
	fprintf (fout, "\n *\n * %s %s %s %s\n */\n\n", argv [0], argv [1], argv [2], argv [3]);
	fprintf (fout, "#include <stdint.h>\n\n\n");
	fprintf (fout, "const uint16_t crc16_%s [256] = {", argv [2]);
	char *comma = "";
	for (int i = 0; i < 256; i++) {
		if (i % 8 == 0) {
			fprintf (fout, "%s\n\t0x%04x", comma, table [i]);
		} else {
			fprintf (fout, "%s 0x%04x", comma, table [i]);
		}
		comma = ",";
	}
	fprintf (fout, "\n};\n");
	fclose (fout);
}
