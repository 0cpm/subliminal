/* keygen.c -- Generate a key for crypto under Subliminal Messaging.
 *
 * This program produces a key for a given algorithm, with additional
 * random and predefined elements.  The key format used below is v0.0
 * of the SubliMe key format, and is usually saved with .0key extension.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <sys/random.h>

#include <arpa/inet.h>

#include <getopt.h>


#define SUBLIME_ATTR_KEY     ((int) 'k')
#define SUBLIME_ATTR_SALT    ((int) 'K')
#define SUBLIME_ATTR_IV_UP   ((int) 'i')
#define SUBLIME_ATTR_IV_DOWN ((int) 'I')


int main (int argc, char *argv []) {
	//
	// Parse arguments
	char *progname = strrchr (argv [0], ',');
	bool error = false;
	bool help = false;
	char *keyfilestr = NULL;
	char *algidstr = NULL;
	if (progname == NULL) {
		progname = argv [0];
	}
	int opt = -1;
	while (opt = getopt (argc, argv, "k:a:h"), opt != -1) {
		switch (opt) {
		case 'a':
			error = error || (algidstr != NULL);
			algidstr = optarg;
			break;
		case 'k':
			error = error || (keyfilestr != NULL);
			keyfilestr = optarg;
			break;
		case 'h':
			help = true;
			break;
		default:
			error = true;
			break;
		}
	}
	//
	// Check if a proper algorithm identifier was supplied
	uint16_t algid = ~1000;
	if (algidstr == NULL) {
		error = true;
	} else {
		char *endptr = NULL;
		long algidl = strtol (algidstr, &endptr, 0);
		error = error || ((endptr != NULL) && (*endptr != '\0'));
		error = error || (endptr == algidstr);
#ifdef DEBUG
		error = error || (algidl < -1);
#else
		error = error || (algidl < 0);
#endif
		error = error || (algidl > 255);
		algid = algidl;
	}
	//
	// Check if the keyfile pre-exists
	if (keyfilestr == NULL) {
		error = true;
	}
	// 
	// Report if an error was encountered
	if (help || error) {
		fprintf (stderr, "Usage: %s -a ALGID -k KEYFILE [[COUNT]TAG...]\nGenerates a key file named KEYFILE for 0cpm Subliminal Messaging\n", argv [0]);
		exit (error ? 1 : 0);
	}
	if (access (keyfilestr, R_OK) == F_OK) {
		fprintf (stderr, "%s: Refusing to overwrite pre-existing key file %s\n", progname, keyfilestr);
		exit (1);
	}
	//
	// Open the keyfile for writing
	int kf = open (keyfilestr, O_WRONLY | O_CREAT | O_EXCL, 0600);
	if (kf < 0) {
		perror ("Failure to open keyfile");
		exit (1);
	}
	//
	// Write the magic code and version of the keyfile (TODO: Maybe do this last)
	errno = 0;
	if (write (kf, "SubliMe1.0", 10) != 10) {
		perror ("Incomplete write of magic code");
		goto close_cleanup;
	}
	uint8_t meta [4];
	//
	// Write the algorithm identity of the keyfile
	* ((uint16_t *) meta) = htons (algid);
	if (write (kf, meta, 2) != 2) {
		perror ("Incomplete write of algorithm identifier");
		goto close_cleanup;
	}
	//
	// Write the attr, length, value, [pad] sequences
	char buf8k [8192 + 4];
	for (char **argi = &argv[optind]; argi < &argv[argc]; argi++) {
		char *endptr = *argi;
#ifdef DEBUG
		off_t here = lseek (kf, 0, SEEK_CUR);
		printf ("0x%08x offset for \"%s\"\n", (int) here, *argi);
#endif
		//
		// Parse the number argument
		//TODO// Support absence of numarg
		long numarg = strtol (endptr, &endptr, 0);
		bool nonum = (endptr == *argi);
		if (nonum) {
			numarg = 0;
		} else if ((numarg < 0) || (numarg > 8192)) {
			fprintf (stderr, "%s: Wrong value in %s\n", progname, *argi);
			error = true;
			break;
		}
		//
		// Parse the attribute code
		if (*endptr == '\0') {
			fprintf (stderr, "%s: Missing attribute name in %s\n", progname, *argi);
			error = true;
			break;
		}
		char attr = *endptr++;
		//
		//TODO// Parse the string argument
		//TODO// Support absense of the string
		char *strarg = endptr;
		int strarglen = 0;
		bool nostr = (*endptr == '\0');
		while (*endptr != '\0') {
			if (*endptr == '\\') {
				switch (*++endptr) {
				case '\0':
					fprintf (stderr, "%s: Attribute string with hanging backslash in %s\n", progname, *argi);
					error = true;
					break;
				case 'r':
					strarg [strarglen++] = '\r';
					break;
				case 'n':
					strarg [strarglen++] = '\n';
					break;
				case 't':
					strarg [strarglen++] = '\t';
					break;
				case '0':
					strarg [strarglen++] = '\0';
					break;
				case '=':
					// Non-standard: explicit empty string
					strarglen++;
					break;
				case 'x':
					//TODO// Process hexadecimal, ...
					fprintf (stderr, "%s: TODO: Hexadecimal not yet supported in %s\n", progname, *argi);
					error = true;
					break;
				default:
					strarg [strarglen++] = *endptr++;
					break;
				}
				endptr++;
			} else {
				strarg [strarglen++] = *endptr++;
			}
		}
		if (nostr) {
			strarg = NULL;
		} else {
			//
			// Add safety NUL termination *after* strarglen
			strarg [strarglen] = '\0';
		}
		;
		//
		// Specifics for the attribute
		switch (attr) {
		case 'k':
		case 's':
			//
			// Add random bytes as key or salt
			bool salt = (attr == 's');
			* (int16_t *) &meta[0] = htons (salt ? SUBLIME_ATTR_SALT : SUBLIME_ATTR_KEY);
			* (int16_t *) &meta[2] = htons (numarg);
			if (write (kf, meta, 4) != 4) {
				fprintf (stderr, "%s: Failed to write metadata for %s\n", progname, *argi);
				error = true;
			}
			//
			// Write random bytes
			assert (numarg <= 8192);
			int gotrnd = 0;
			int style = (attr == 'k') ? GRND_RANDOM : 0;
			while (gotrnd < numarg) {
				int ask = (numarg > gotrnd + 4) ? 4 : (numarg - gotrnd);
				if (getrandom (buf8k + gotrnd, ask, style) != ask) {
					fprintf (stderr, "%s: Random generator failure at %d/%ld for %s\n", progname, gotrnd, numarg, *argi);
					error = true;
					break;
				}
				gotrnd += ask;
			}
			if (error) {
				break;
			}
			if (write (kf, buf8k, numarg) != numarg) {
				fprintf (stderr, "%s: Failed to write random bytes for %s\n", progname, *argi);
				error = true;
				break;
			}
			//
			// Write 1-4 padding bytes while clearing the key bytes
			memset (buf8k, 0, sizeof (buf8k));
			int pad = ((numarg - 1) & 0x0003) + 1;
			if (write (kf, buf8k, pad) != pad) {
				fprintf (stderr, "%s: Failed to write padding for %s\n", progname, *argi);
				error = true;
				break;
			}
			break;
		case 'i':
		case 'I':
			//
			// Add 0x00 or 0xff bytes for use as up/down initialisation vector
			{}
			bool up = (attr == 'i');
			* (int16_t *) &meta[0] = htons (up ? SUBLIME_ATTR_IV_UP : SUBLIME_ATTR_IV_DOWN);
			* (int16_t *) &meta[2] = htons (numarg);
			if (write (kf, meta, 4) != 4) {
				fprintf (stderr, "%s: Failed to write metadata for %s\n", progname, *argi);
				error = true;
			}
			//
			// Fill the IV buf, including padding bytes
			assert (numarg <= 8192);
			uint8_t ivbuf [8192 + 4];
			memset (ivbuf, up ? 0x00 : 0xff, numarg);
			memset (ivbuf + numarg, 4, 0x00);
			int len = (numarg + 4) - (numarg % 4);	/* (numarg+4) - ((numarg+4) % 4) ... */
			//
			// Write the IV and padding out to file
			if (write (kf, ivbuf, len) != len) {
				fprintf (stderr, "%s: Failed to write IV and padding for %s\n", progname, *argi);
				error = true;
			}
			break;
		//TODO// case 'u':
			//
			// Add a key-identifying UUID, the number may be a version number and the string a 0cpm subdomain; with neither use a random generator; only a version number might be used as a clock sequence into a time-based UUID, but note that such is not supported in libuuid, and that does make sense.
		default:
			//
			// Report an error
			fprintf (stderr, "%s: Unknown attribute '%c' -- use k, i, I\n", progname, attr);
			error = true;
			break;
		}
		if (error) {
			break;
		}
	}
#ifdef DEBUG
	off_t here = lseek (kf, 0, SEEK_CUR);
	printf ("0x%08x offset for the end of the keyfile\n", (int) here);
#endif
	//
	// Close and cleanup
close_cleanup:
	if (kf >= 0) {
		close (kf);
	}
	if (error) {
		unlink (keyfilestr);
	}
	//
	// Return in a joyful mood
	exit (error ? 1 : 0);
}
