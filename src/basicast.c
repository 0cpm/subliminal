/* Basicast -- send Basicode as an A-law RTP stream.
 *
 * This program is based on the version in the RetroCast project
 * https://gitlab.com/0cpm/retrocast/-/blob/master/basicast.c
 * The project documents background to this program.  It has
 * been modified below to produce A-law output on stdout.
 *
 * Basicode is a system developed in the Netherlands in the
 * 70's, whereby generalised BASIC programs were broadcasted
 * over radio stations, thus sharing software between the
 * computer hobbyists of the time.  I have vivid recollections
 * of the ZX Spectrum, and a time where real men could program
 * a computer :-)
 *
 * Streaming is done over RTP, of course over IPv6.  We are
 * not going to do this thing over an old-fashioned Internet!
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>


/* The following tables provide 3 periods of "1" and 3 periods of "0".
 * They are copied as 7, 6, 7 samples for a group of 3 bits, thus
 * creating 20 samples or, at a rate of 8000 samples/second, a wave
 * mixture of 1200 Hz and 2400 Hz.
 *
 * The samples are encoded as A-law samples of one byte each.
 */

uint8_t basic0 [20] = { 0xd5, 0xba, 0xa7, 0x8d, 0x33, 0x26, 0x33, 0x8d, 0xa7, 0xba, 0xd5, 0x3a, 0x27, 0x0d, 0xb3, 0xa6, 0xb3, 0x0d, 0x27, 0x3a };
uint8_t basic1 [20] = { 0xd5, 0xa7, 0x33, 0x33, 0xa7, 0xd5, 0x27, 0xb3, 0xb3, 0x27, 0xd5, 0xa7, 0x33, 0x33, 0xa7, 0xd5, 0x27, 0xb3, 0xb3, 0x27 };


/* Following are global variables used for transmission. */

uint16_t shift0, shift1, shift2, shift3;

uint8_t csum;

uint32_t pilot_packets;
uint32_t trailer_packets;

enum txstate {
	PILOT,		// Sending out pilot tone RTP packets until pilot_packets==0
	STARTOFTEXT,	// Want to send STX | 0x80
	FILEDATA,	// Am sending asciifile data until feof() is reached
	ENDOFTEXT,	// Want to send ETX | 0x80
	CHECKSUM,	// Want to send checksum
	TRAILER,	// Sending out pilot tone RTP packets until trailer_packets==0
	EXIT
};

enum txstate txstate;

FILE *asciifile;

struct rtppkt {
	uint16_t V_P_X_CC_M_PT;
	uint16_t seqnr;
	uint32_t tstamp;
	uint32_t syncrsc;
	uint8_t rtp160 [160];
};
struct rtppkt rtppkt;

bool verbose = true;


/* Debugging code: Collect submitted bits, and reconstruct
 * the ASCII codes for the text.  When an ASCII character
 * is recognised, print it out.
 */

#ifdef DEBUG
int bitstate = -1;	// -1 = Await START; 0..6 = Read data; 7..9 = Check STOP
uint16_t shiftreg;
void submitted (bool bit) {
	if (bitstate < 0) {
		if (bit == 0) {
			bitstate = 0;
			shiftreg = 0x0000;
		}
	} else if (bitstate < 7) {
		shiftreg >>= 1;
		if (bit) {
			shiftreg |= 0x40;
		}
		bitstate++;
	} else if (bitstate < 10) {
		if (bit == 0) {
			bitstate = -1;
		}
		bitstate++;
		if (bitstate == 10) {
			fputc ((char) shiftreg, stdout);
			bitstate = -1;
		}
	} else {
		bitstate = -1;
	}
}
#endif


/* Make waves: Encode 3 bits in 20 samples, and write the output to
 * the buffer provided, which always receives 20 bytes.
 */
void make_waves (bool const first, bool const second, bool const third, uint8_t *waves20) {
	memcpy (waves20 +  0, first?  &basic1[0] : &basic0[0] , 7);
	memcpy (waves20 +  7, second? &basic1[7] : &basic0[7] , 6);
	memcpy (waves20 + 13, third?  &basic1[13]: &basic0[13], 7);
}


/* Encode data from a shift buffer provided, and produce a 20 ms RTP
 * packet in rtp160.  This contains 160 samples, or 24 bits of data.
 *
 * The shift buffer shifts down, and the lowest bit is sent as long
 * as the shift buffer is nonzero.  Note that data ends in stopbits
 * valued "1" and that the pilot tone is also made from "1" bits.
 *
 * The next two shift buffers are also provided.  Shift buffers that
 * are zero upon return, are clearly used up, and must be replaced.
 * The following shift buffer(s) may already have been used.
 *
 * The shift buffers are stored in global variables shift0..shift3.
 */
void make_rtpdata (void) {
	int tripples = 8;
	uint8_t *wptr = rtppkt.rtp160;
	bool a, b, c;
	rtppkt.seqnr = htons (ntohs (rtppkt.seqnr) + 1);
	rtppkt.tstamp = htonl ((int32_t) clock ());
	while (tripples-- > 0) {
		if (!shift0) {
			shift0 = shift1;
			shift1 = shift2;
			shift2 = shift3;
			shift3 = 0;
		}
		a = 0x01 & shift0;
		shift0 >>= 1;
		if (!shift0) {
			shift0 = shift1;
			shift1 = shift2;
			shift2 = shift3;
			shift3 = 0;
		}
		b = 0x01 & shift0;
		shift0 >>= 1;
		if (!shift0) {
			shift0 = shift1;
			shift1 = shift2;
			shift2 = shift3;
			shift3 = 0;
		}
		c = 0x01 & shift0;
		shift0 >>= 1;
		make_waves (a, b, c, wptr);
#ifdef DEBUG
		submitted (a);
		submitted (b);
		submitted (c);
#endif
		wptr += 20;
	}
	if (!shift0) {
		shift0 = shift1;
		shift1 = shift2;
		shift2 = shift3;
		shift3 = 0;
	}
}


/* Append new bytes to the given shift buffer from the given
 * file, inasfar as that is needed and possible.  Once the
 * feof() condition is set for the file, the byte stream
 * will be finalised.  This routine updates the csum variable.
 */
void prepare_filebytes (uint16_t *shiftreg) {
	if ((*shiftreg == 0) && !feof (asciifile)) {
		int byte = fgetc (asciifile);
		if (byte != EOF) {
			byte = (byte & 0x7f) | 0x80;
			csum ^= byte;
			byte <<= 1;	// add START bit
			byte |= 0x600;	// add STOP bits
			*shiftreg = byte;
		}
	}
}



/* Append a given byte to the given shift buffer, surrounded
 * with start and stop bits but not modifying their bit 7.
 * This is used for checksum and the special characters STX|0x80
 * and ETX|0x80.
 * Return success on success, or false if there is no room.
 */
bool prepare_specialbyte (uint8_t byte) {
	uint16_t *shiftreg;
	if (shift0) {
		if (shift1) {
			if (shift2) {
				if (shift3) {
					return false;
				} else {
					shiftreg = &shift3;
				}
			} else {
				shiftreg = &shift2;
			}
		} else {
			shiftreg = &shift1;
		}
	} else {
		shiftreg = &shift0;
	}
	*shiftreg = byte;
	*shiftreg <<= 1;	// add START bit
	*shiftreg |= 0x600;	// add STOP bits
	return true;
}


/* Append enough bytes to send an RTP packet, following the current
 * transmission state in the txstate variable.  Assume that the
 * datafile is positioned in the beginning of the content to be sent,
 * and that it will continue until the end of that file.  Assume
 * that there will be no ETX codes in the file.  Ignore any set
 * bit 7 values.
 */
void prepare_packet (void) {
	if (txstate == PILOT) {
		if (pilot_packets == 0) {
			txstate = STARTOFTEXT;
			fprintf (stderr, "Sending STARTOFTEXT\n");
			shift0 = shift1 = shift2 = shift3 = 0;
		} else {
			pilot_packets--;
			shift0 = shift1 = shift2 = shift3 = 0xffff;
		}
	}
	if (txstate == STARTOFTEXT) {
		csum = 0x00;
		if (prepare_specialbyte (130)) {
			txstate = FILEDATA;
			fprintf (stderr, "Sending FILEDATA\n");
		}
	}
	if (txstate == FILEDATA) {
		prepare_filebytes (&shift0);
		prepare_filebytes (&shift1);
		prepare_filebytes (&shift2);
		prepare_filebytes (&shift3);
		if (feof (asciifile)) {
			txstate = ENDOFTEXT;
			fprintf (stderr, "Sending ENDOFTEXT\n");
		}
	}
	if (txstate == ENDOFTEXT) {
		if (prepare_specialbyte (131)) {
			txstate = CHECKSUM;
			fprintf (stderr, "Sending CHECKSUM\n");
		}
	}
	if (txstate == CHECKSUM) {
		if (prepare_specialbyte (csum)) {
			txstate = TRAILER;
			fprintf (stderr, "Sending TRAILER\n");
		}
	}
	if (txstate == TRAILER) {
		if (trailer_packets == 0) {
			txstate = EXIT;
			fprintf (stderr, "Sending EXIT\n");
		} else {
			trailer_packets--;
			if (shift0 == 0) {
				shift0 = 0xffff;
			}
			if (shift1 == 0) {
				shift1 = 0xffff;
			}
			if (shift2 == 0) {
				shift2 = 0xffff;
			}
			if (shift3 == 0) {
				shift3 = 0xffff;
			}
		}
	}
	make_rtpdata ();
}


/* This routine is meant to be started at a regular interval
 * of 20 ms per call.  Before it is called the first time,
 * one invocation of prepare_packet() must have been made.
 *
 * This function submits an RTP packet at the accurate,
 * interval-timed time and then prepare the next one, unless the
 * transmission state already had evolved to EXIT.  In that
 * state, it stops the interval timer and finishes immediately.
 */
void send_packet_prepare (void) {
	ssize_t sent = write (1, &rtppkt.rtp160, sizeof (rtppkt.rtp160));
	if ((sent != sizeof (rtppkt.rtp160)) || (txstate == EXIT)) {
		if (errno == 0) {
			errno = ESTRPIPE;
		}
	} else {
		prepare_packet ();
	}
	//TODO// be willing to pickup incoming packets on the UDP port
}


int main (int argc, char *argv []) {
	if (argc != 2) {
		fprintf (stderr, "Usage: %s FILE.BAS\n", argv [0]);
		exit (1);
	}
	asciifile = fopen (argv [1], "r");
	if (asciifile == NULL) {
		fprintf (stderr, "Failed to open input file \"%s\" for reading\n", argv [1]);
		exit (1);
	}
	txstate = PILOT;
	//
	// Start the generation of sound
	errno = 0;
	int pilot_ms = 3000;
	int trailer_ms = 3000;
	pilot_packets = (pilot_ms + 10) / 20;
	trailer_packets = (trailer_ms + 10) / 20;
	prepare_packet ();
	while ((txstate != EXIT) && (errno == 0)) {
		send_packet_prepare ();
	}
	if (errno != 0) {
		perror ("Error while sending");
	}
	fclose (asciifile);
	asciifile = NULL;
	exit (errno? 1: 0);
}
