/* sendrecv.c -- Commandline filtering program.
 *
 * This is the program "sublime", with variants "sublime-send" and
 * "sublime-recv" for unidirectional operation.  It reads an input
 * stream and produces an output stream.  To support both directions,
 * an uplink must be given.
 *
 * This is a tool that uses libsublime to do the actual work.  The
 * same approach can easily be built into VoIP code, perhaps after
 * an opportunistic attempt to load libsublime.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>

#include <0cpm/subliminal.h>


int main (int argc, char *argv []) {
	char *progname = argv [0];
	//
	// Parse arguments
	int opt;
	char *mediatype = "audio";
	char *codec = "PCMA";
	bool send = false;
	bool recv = false;
	bool help = false;
	bool stxerr = false;
	char *progdash = strrchr (progname, '-');
	if (progdash != NULL) {
		if (strcmp (progdash, "-send") == 0) {
			send = true;
		}
		if (strcmp (progdash, "-recv") == 0) {
			recv = true;
		}
	}
	while (opt = getopt (argc, argv, "m:c:h"), opt != -1) {
		switch (opt) {
		case 'm':
			/* mediatype, other than audio */
			mediatype = optarg;
			break;
		case 'c':
			/* codec, other than PCMA */
			codec = optarg;
			break;
		case 'h':
			/* help wanted */
			help = true;
			break;
		default:
			/* funny option */
			fprintf (stderr, "%s: Unknown option -%c\n", progname, opt);
			stxerr = true;
			break;
		}
	}
	if ((!send) && (!recv)) {
		fprintf (stderr, "%s: You did not choose between -send and -recv mode\n", progname);
		stxerr = true;
	}
	if (stxerr || help) {
		fprintf (stderr, "Usage: %s [-m MEDIA] [-c CODEC] [-h]\n", progname);
		exit (stxerr ? 1 : 0);
	}
	//
	// Initialise for Subliminal Messaging
	sublime_init ();
	struct sublime *recvfilt = NULL;
	struct sublime *sendfilt = NULL;
	if (recv) {
		recvfilt = sublime_open (SUBLIME_RECV, mediatype, codec);
		sublime_detach (recvfilt, true);
	}
	if (send) {
		sendfilt = sublime_open (SUBLIME_SEND, mediatype, codec);
		sublime_detach (sendfilt, true);
	}
	if (recv && send) {
		sublime_attach (recvfilt, sendfilt);
	}
	//
	// Read data and pass it through the applicable filters
	while (true) {
		uint8_t buf [1024];
		//
		// Read a buffer full of data
		ssize_t got = read (0, buf, sizeof (buf));
		if (got <= 0) {
			if (got < 0) {
				perror ("Error reading codec data");
			}
			break;
		}
		//
		// Modify the codec in passing
		sublime_recvmedia (recvfilt, buf, got);
		sublime_sendmedia (sendfilt, buf, got);
		//
		// Send the buffer full of data
		ssize_t sent = write (1, buf, got);
		if (sent < 0) {
			perror ("Error sending codec data");
			break;
		}
		if (sent != got) {
			fprintf (stderr, "%s: Only sent %zd out of %zd codec bytes\n", progname, sent, got);
		}
	}
	//
	// Cleanup after Subliminal Messaging
	sublime_close (sendfilt);
	sublime_close (recvfilt);
	sublime_fini ();
	//
	// Return neutral success
	return 0;
}
