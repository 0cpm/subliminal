/* crc.c -- CRC-16 and CRC-32 checksums over iovec structures for HDLC.
 *
 * This code can be used for generation and checking.  The iovec structure
 * is convenient for a number of reasons:
 *
 *  - Attach bytes for the CRC but do not include them in the iovlen
 *  - Windowing can be supported without scattering
 *
 * The schemes used interact with protocol lengths, and aim to get the
 * highest achievable Hamming distance (number of bit errors) that can
 * be detected with the CRC at those lengths.
 *
 * I-frames can be longer and will use a CRC-32 polynomial, 0xTODO.
 * S-frames and most U-frames are shorter and use a CRC-16, 0xTODO.
 * UI-frames should be short to enjoy an okayish Hamming distance.
 * TODO: We might use CRC-32 for UI frames as well...?
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <sys/types.h>
#include <sys/socket.h>



uint16_t crc16tab [256] = {
	0x0000, 0x0000,
};


uint16_t crc32tab [256] = {
	0x00000000, 0x00000000,
};


void crc16 (struct iovec *iov, uint16_t iovlen, uint8_t *out16_netorder) {
	uint16_t checksum = 0x0000;
	while (iovlen-- > 0) {
		size_t  more = iov->ivo_len;
		uint8_t here = iov->ivo_base;
		iov++;
		while (more-- > 0) {
			checksum = (checksum >> 8) ^ crc16tab [0xff & (checksum ^ *here)];
			here++;
		}
	}
	* (uint16_t *) output_bigendian = htons (checksum);
}


void crc32 (struct iovec *iov, uint16_t iovlen, uint8_t *out32_netorder) {
	uint32_t checksum = 0x00000000;
	while (iovlen-- > 0) {
		size_t  more = iov->ivo_len;
		uint8_t here = iov->ivo_base;
		iov++;
		while (more-- > 0) {
			checksum = (checksum >> 8) ^ crc32tab [0xff & (checksum ^ *here)];
			here++;
		}
	}
	* (uint32_t *) output_bigendian = htons (checksum);
}

