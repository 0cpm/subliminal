/* silence.c -- Generate a given number of seconds of A-law silence.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <math.h>


int main (int argc, char *argv []) {
	if ((argc < 2) || (argc > 3)) {
		fprintf (stderr, "Usage: %s TTT.T [outfile.al]\nGenerates TTT.T seconds of A-law silence on stdout or in outfile.al\n", argv [0]);
		exit (1);
	}
	float time = atof (argv [1]);
	FILE *fout = (argc > 2) ? fopen (argv [2], "w") : stdout;
	if (fout == NULL) {
		fprintf (stderr, "Failed to open \"%s\" for writing\n", argv [2]);
		exit (1);
	}
	int samples = truncl (8000 * time);
	while (samples-- > 0) {
		fputc (0x55, fout);
	}
	fclose (fout);
	return 0;
}
