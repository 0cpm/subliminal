/* wgsip.c -- Session Initiation Protocol for Wireguard.
 *
 * Subcommands:
 * 	wgsip-connect     <profile>...
 * 	wgsip-disconnect  <profile>...
 * 	wgsip-server     [<profile>...]
 *
 * SIP can be used for sessions of many kinds, not just audio or video.
 * Wireguard has UDP endpoints, and this can be setup with SIP like
 * anything else, using SDP lines
 *	c=IN IP6 <ip6address>
 * 	m=application <port> udp vnd.wireguard
 * where <ip6address> and <port> indicate the UDP location of Wireguard.
 * (The example uses IPv6 to avoid NAT traversal and continuous pings.)
 *
 * What would the best solution be for Wireguard over IPv4?  It is not
 * trivial, due to port forwarding requirements, but a port can be
 * kept open, even without reaching (far) beyond the NAT router, so
 * with minimal display.  For how, see
 * https://gitlab.com/arpa2/socketSYNergy
 * It assumes precisely what we have here, namely mutual agreement
 * of the endpoints which then serve to pass traffic.  It remains a
 * mystery whether NAT will change external ports though, so care
 * must be taken to learn the external port -- possibly from the
 * bounce that a SYNergy frame gets back because its TTL runs out.
 * This would be as accurate as ICE, without the software mods.
 * In general however, choosing IPv4 is yet another stagnation in
 * the inevitable switch to IPv6, so it is probably not worth it.
 * I for onw am not backward-compatible with IPv4 in places where
 * it feels like pandering to unreasonable stagnation (like this).
 *
 * It is completely valid to use SIP to initiate a session and quit
 * while the session continues to be in use, and start SIP again to
 * tear down the session.  SIP is stateless, after all, and the only
 # reason that phone calls are managed and eventually torn down is
 * that they cost money per minute.  Not so with Wireguard over UDP.
 *
 * The only requirement is that endpoints continue to use the same
 * "SIP dialog" when they intend to modify the media session.  This
 * means replicating the Call-ID header, the From: tag and To: tag.
 * It is easy enough to store this data between SIP operations.
 * After a system reboots, key material would (should) be dropped
 * but this identifying data will still be around; a re-INVITE with
 * authentication would suffice to negotiate new key material.  The
 * CSeq: number is also functional within a SIP dialog.
 *
 * This is not a hack.  I believe that this is how SIP was intended,
 * but due to per-minute charges never got to this point.  It can be
 * equally useful to sign up with a plethora of protocols.
 *
 * One thing that has yet to be added is key establishment.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <ini.h>
#include <re/re.h>
//NOT_SELFCONTAINED// #include <re/re_sa.h>
//NOT_SELFCONTAINED// #include <re/re_sdp.h>



/* Configuration profiles and variables.
 *
 * Profiles are symmetric; they distinguish local and remote
 * values but not where the initiative lies.  In general, a
 * server will load from /etc/0cpm/wgsip.conf and make
 * sure that anything runs that it can trigger, while clients
 * explicitly indicate a profile to work with, possibly from
 * the central file or their own ~/.config/0cpm/wgsip.conf
 * file.  Both follow the common INI file format.
 *
 * CLIENT, SERVER -- HEADER FILE
 */
struct profile {
	//
	// Next element in a linked list of profiles
	struct profile *next;
	//
	// The section name is used as a profile name that can be
	// quickly referenced from the commandline, and which will
	// be logged to conceal specifics of the connection and
	// its security.
	char *name;
	//
	// The TunnelIfName is the interface name for the tunnel.
	char *TunnelIfName;
	//
	// The LocalName and RemoteName are used to populate and match
	// the From: and To: headers in a SIP message.  When used in
	// Subliminal Messaging they may alternatively be set to an
	// e.164 phone number.  When RemoteName is NULL it will match
	// anything.
	char *LocalName;
	char *RemoteName;
	//
	// InnerAddress is used to configure the tunnel's IPv6 address.
	char *InnerAddress;
	//
	// The InnerPrefix is used to configure the tunnel's IPv6 prefix.
	// The value is sent as part of the SDP message from this profile.
	// IPv6 enables independent allocation of prefixes without clash.
	char *InnerPrefix;
	//
	// The PskMethod ca be set to a method name and possibly parameters
	// for preshared key negotiation.  The value is sent as part of the
	// SDP message from this profile.
	char *PskMethod;
	//
	// The OuterAddress is an IPv6 address port that serves as the
	// Wireguard encrypted service.  The address (and port) may be
	// dynamic, and need not be local to the host setting up the
	// connection over SIP/SDP.  There is no way to pin the remote
	// OuterAddress, because the security does not rely on such fixations.
	// The most important reason to setup with SIP/SDP is to support
	// this dynamicity.  This setting is optional; it defaults to a
	// routable source address.  Setting it is useful on multi-homed
	// networks.
	char *OuterAddress;
	//
	// The optional ListenAddress that waits for incoming SIP requests
	char *ListenAddress;
	//
	// The optional ProxyAddress for routing towards the remote
	char *ProxyAddress;
};



/* Load the INI configuration file as a list under the profiles global.
 * The "user" parameter may be NULL to load everything, or it may be a
 * char *profile to constrain loading to the profile with that name.
 *
 * CLIENT, SERVER
 */
struct config_profiles {
	int profc;
	char **profv;
};
//
static struct profile *profiles = NULL;
//
static int config_cb (void *user, const char *section, const char *name, const char *value) {
	//
	// We may be called for a new section
#if INI_CALL_HANDLER_ON_NEW_SECTION
	if ((name == NULL) || (value == NULL)) {
		return 1;
	}
#endif
	//
	// Maybe drop this profile name (due to a non-empty positive list)
	struct config_profiles *cps = (struct config_profiles *) user;
	if ((cps != NULL) && (cps->profc > 0)) {
		bool drop = true;
		for (int seekc = cps->profc - 1; seekc >= 0; seekc--) {
			drop = drop && (strcasecmp (section, cps->profv [seekc]) != 0);
		}
		if (drop) {
			/* Ignore quietly */
			return 1;
		}
	}
	//
	// Debug
#ifdef DEBUG
	fprintf (stderr, "DEBUG: Config setting: [%s].%s = %s\n", section, name, value);
#endif
	//
	// Check if we need to insert a profile structure
	if ((profiles == NULL) || (strcmp (profiles->name, section) != 0)) {
		//TODO// mem_zalloc()
		struct profile *new_remote = calloc (1, sizeof (struct profile));
		if (new_remote == NULL) {
			return 0;
		}
		new_remote->name = strdup (section);
		if (new_remote->name == NULL) {
			//TODO// mem_zalloc()
			free (new_remote);
			return 0;
		}
		new_remote->next = profiles;
		profiles = new_remote;
	}
	//
	// We can now assume dealing with the *remote entry
	char *duppie = strdup (value);
	if (duppie == NULL) {
		return 0;
	}
	if (strcasecmp (name, "LocalName") == 0) {
		free (profiles->LocalName);
		profiles->LocalName    = duppie;
	} else if (strcasecmp (name, "RemoteName") == 0) {
		free (profiles->RemoteName);
		profiles->RemoteName    = duppie;
	} else if (strcasecmp (name, "TunnelIfName") == 0) {
		free (profiles->TunnelIfName);
		profiles->TunnelIfName  = duppie;
	} else if (strcasecmp (name, "InnerAddress") == 0) {
		free (profiles->InnerAddress);
		profiles->InnerAddress    = duppie;
	} else if (strcasecmp (name, "InnerPrefix") == 0) {
		free (profiles->InnerPrefix);
		profiles->InnerPrefix    = duppie;
	} else if (strcasecmp (name, "PskMethod") == 0) {
		free (profiles->PskMethod);
		profiles->PskMethod    = duppie;
	} else if (strcasecmp (name, "OuterAddress") == 0) {
		free (profiles->OuterAddress);
		profiles->OuterAddress = duppie;
	} else if (strcasecmp (name, "ListenAddress") == 0) {
		free (profiles->ListenAddress);
		profiles->ListenAddress = duppie;
	} else if (strcasecmp (name, "ProxyAddress") == 0) {
		free (profiles->ProxyAddress);
		profiles->ProxyAddress = duppie;
	} else {
		//
		// Report an error and return failure
		fprintf (stderr, "Ignoring unknown name [%s].%s\n", section, name);
		free (duppie);
		return 0;
	}
	//
	// Return OK
	return 1;
}


/* Free a configuration element by replacing a pointer to its pointer.
 *
 * CLIENT, SERVER
 */
void config_free (struct profile **prfp) {
	//
	// Quietly exit under error conditions
	if ((prfp == NULL) || (*prfp == NULL)) {
		return;
	}
	//
	// Stash the goner and take it out of the list
	struct profile *goner = *prfp;
	*prfp = goner->next;
	//
	// Erase any variable fields
	free (goner->name         );
	free (goner->LocalName    );
	free (goner->RemoteName   );
	free (goner->TunnelIfName );
	free (goner->InnerAddress );
	free (goner->InnerPrefix  );
	free (goner->PskMethod    );
	free (goner->OuterAddress );
	free (goner->ListenAddress);
	free (goner->ProxyAddress );
	//
	// Remove the structure itself
	//TODO// mem_zalloc()
	free (goner);
	//
	// Success is a quiet party
	return;
}


/* Prune any unusable configuration elements.  The client and/or server
 * flag may be set.  If neither is set, nothing will be usable and all
 * will be pruned.
 *
 * CLIENT, SERVER
 */
void config_prune (bool client, bool server) {
	//
	// Iterate over the entire list
	struct profile **prfp = &profiles;
	struct profile *prf;
	while (prf = *prfp, prf != NULL) {
		//
		// Start assuming that the element must go
		bool keep = false;
		//
		// Keep proper server elements
		keep = keep || (server /*&& ...*/);
		//
		// Keep proper client elements -- no required variables...
		keep = keep || (client /*&& ...*/);
		//
		// Require at least a profile name and LocalName and InnerPrefix and InnerAddress and TunnelIfName in any case
		keep = keep && (prf->name != NULL) && (prf->LocalName != NULL) && (prf->InnerPrefix != NULL) && (prf->InnerAddress != NULL) && (prf->TunnelIfName != NULL);
		//
		// Now remove the element if that is intended
		if (!keep) {
			//
			// Drop the pointed-at profile element
			config_free (prfp);
			//
			// Reload the now-pointed-at profile element
			continue;
		}
		//
		// Advance to the next pointer-pointer
		prfp = &prf->next;
	}
}


/* Find a configuration entry with a given LocalName and, if set, RemoteName.
 *
 * SERVER
 */
const struct profile *config_find_relation (const struct pl *localname, const struct pl *remotename) {
	//
	// If the content we got is partial, we return failure
	if ((localname->p == NULL) || (remotename->p == NULL)) {
		return NULL;
	}
	//
	// Start search for at least a matching LocalName, maybe also RemoteName
	const struct profile *partial = NULL;
	for (const struct profile *prf = profiles; prf != NULL; prf = prf->next) {
		//
		// We always need to match locally
		if (pl_strcmp (localname, prf->LocalName) == 0) {
			//
			// Partial matches are stored as fallback
			if (prf->RemoteName == NULL) {
				partial = prf;
			//
			// Full matches can be returned completely
			} else if (pl_strcmp (remotename, prf->RemoteName) == 0) {
				return prf;
			}
		}
		//
		// No direct return, so continue the search
		;
	}
	//
	// We did not return a full match; return a partial match (or NULL)
	return partial;
}


/* Find a configuration entry with a given section name.
 *
 * CLIENT
 */
const struct profile *config_find_named (const char *prfname) {
	for (const struct profile *prf = profiles; prf != NULL; prf = prf->next) {
		if (strcasecmp (prf->name, prfname) == 0) {
			return prf;
		}
	}
	return NULL;
}


/* Print a string for libre
 *
 * CLIENT, SERVER
 */
int print4re (const char *str, size_t size, void *arg) {
	fwrite (str, 1, size, (FILE *) arg);
	return 0;
}
//
struct re_printf out4re = { print4re, NULL /* stdout setup in main() */ };
//
struct re_printf err4re = { print4re, NULL /* stderr setup in main() */ };


/* Parse a [v6addr]:port pair into a struct sa.  Have a default.
 * Return 0 on OK, otherwise clear out_sa and return an errno value.
 *
 * CLIENT, SERVER
 */
int parse_sa (struct sa *out_sa, const char *in_str, const char *default_str, const char *profile, const char *varname) {
	int err = 0;
	if (profile == NULL) {
		profile = "commandline";
	}
	if (in_str == NULL) {
		if (default_str == NULL) {
			fprintf (stderr, "[%s].%s has no default value\n", profile, varname);
			err = EINVAL;
			goto ret_error;
		}
		in_str = default_str;
	}
	struct pl addrport_str = { .p = in_str, .l = strlen (in_str) };
	struct pl port_str;
	struct pl host_str;
	err = uri_decode_hostport (&addrport_str, &host_str, &port_str);
	if (err != 0) {
		fprintf (stderr, "[%s].%s = \"%s\" did not decode as host:port\n", profile, varname, in_str);
		goto ret_error;
	}
	uint32_t port_num = pl_u32 (&port_str);
	if ((port_num == 0) || (port_num > 65535)) {
		fprintf (stderr, "[%s].%s = \"%s\" has an invalid port number\n", profile, varname, in_str);
		err = EINVAL;
		goto ret_error;
	}
	sa_init (out_sa, AF_INET6);
	err = sa_set (out_sa, &host_str, (uint16_t) port_num);
	if (err != 0) {
		fprintf (stderr, "[%s].%s = \"%s\" did not work for an IPv6 socket address\n", profile, varname, in_str);
		goto ret_error;
	}
	return 0;
ret_error:
	memset (out_sa, 0, sizeof (*out_sa));
	return err;
}


/* Run a program with optional input/output streams.  Wait for completion.
 * Set chldin to -1 to read from /dev/null, set chldout to -1 to write
 * to stderr.
 *
 * Return 0 on success, otherwise an errno.
 *
 * CLIENT, SERVER.
 */
int runcmd (int chldin, int chldout, char **argv) {
#ifdef DEBUG
	fprintf (stderr, "DEBUG: Running %s <&%d >&%d\n", argv [0], chldin, chldout);
#endif
	pid_t child = fork ();
	int wstatus;
	switch (child) {
	case -1:
		/* An error occurred in fork() */
		perror ("Failure in fork()");
		return errno;
	case 0:
		/* Child program, execute the command */
		if (chldin < 0) {
			chldin = open ("/dev/null", O_RDONLY);
		}
		dup2 (chldin, 0);
		close (chldin);
		if (chldout < 0) {
			chldout = 2;
		}
		dup2 (chldout, 1);
		close (chldout);
		char *envp [] = { NULL };
		execve (argv [0], argv, envp);
		perror ("Failure in exec()");
		return errno;
	default:
		/* Parent program, wait for child completion */
		if (waitpid (child, &wstatus, 0) == -1) {
			perror ("Faillure in waitpid()");
			return errno;
		}
#ifdef DEBUG
		fprintf (stderr, "DEBUG: Rundone %s\n", argv [0]);
#endif
		return 0;
	}
}


/* Run a program to read a string buffer.  Set chldin to -1 to read
 * from /dev/null.  If chop is set to true, a trailing newline will
 * be removed.  The output buffer will always be NUL-terminated and
 * the output size is returned in buflen.  Due to NUL-termination,
 * the output buffer length will always be less than the input.
 *
 * Return 0 on success, otherwise an errno.  Data that does not fit
 * in the buffer (including trailing NUL) results in EMSGSIZE.  An
 * empty output usually signifies error and returns ENODATA.
 *
 * CLIENT, SERVER.
 */
int runstrcmd (int chldin, char **argv, bool chop, struct pl *buf) {
	int err = 0;
	//
	// Create a pipe for output string harvesting
	int chldout [2];
	if (pipe (chldout) == -1) {
		err = errno;
		perror ("Failure in pipe()");
		return err;
	}
	//
	// Run the command while writing to the pipe
	err = runcmd (chldin, chldout [1], argv);
	close (chldout [1]);
	if (err != 0) {
		close (chldout [0]);
		return err;
	}
	//
	// Read the buffer, allowing the full size for now
	ssize_t rdlen = read (chldout [0], (char *) buf->p, buf->l);
	err = errno;
	close (chldout [0]);
	if (rdlen < 0) {
		return err;
	} else if (rdlen == 0) {
		return ENODATA;
	}
	//
	// Optionally chop off a trailing newline
	if (chop && (buf->p [rdlen-1] == '\n')) {
		rdlen--;
	}
	//
	// NUL-terminate, or return EMSGSIZE if we lack space
	if (rdlen >= buf->l) {
		return EMSGSIZE;
	}
	((char *) buf->p) [rdlen] = '\0';
	buf->l = (int) rdlen;
	//
	// Return successfully
	return 0;
}


/* Have a Wireguard interface of the given name.  This may or may not be
 * a fresh interface, it does not matter.  Its listen-port and public-key
 * will be queried for storage.  The public_key needs to be free()d after
 * a successful return.
 *
 * Return 0 on success, otherwise an errno.
 *
 * CLIENT, SERVER.
 */
int wg_have (const struct sa *listen_sa, const char *interface, const char *inner_addr,
				uint16_t *listen_port, char **public_key) {
	int reader [2];
	//
	// Reset the output values
	*listen_port = 0;
	*public_key = NULL;
	//
	// Produce the address string
	char listen_addr [INET6_ADDRSTRLEN];
	int err = sa_ntop (listen_sa, listen_addr, INET6_ADDRSTRLEN);
	if (err != 0) {
		return err;
	}
	//
	// Run the script to have a Wireguard interface, possibly a new one
	char *argv_have [] = {
		"../share/wgsip/have-wg",
		listen_addr, (char *) interface, (char *) inner_addr, NULL };
	err = runcmd (-1, -1, argv_have);
	if (err != 0) {
		return err;
	}
	//
	// Query the port number, no matter if it is old or new
	char portstr [10];
	struct pl portbuf = { .p = portstr, .l = sizeof (portstr) };
	char *argv_port [] = {
		"../share/wgsip/show-wg", (char *) listen_addr, (char *) interface, "listen-port", NULL };
	err = runstrcmd (-1, argv_port, true, &portbuf);
	if (err != 0) {
		return err;
	}
	//
	// Parse the port number
	uint32_t portint = pl_u32 (&portbuf);
	if ((portint == 0) || (portint > 65535)) {
		return EINVAL;
	}
	//
	// Query the public key, no matter if it is old or new
	char keystr [200];
	struct pl keybuf = { .p = keystr, .l = sizeof (keystr) };
	char *argv_pubkey [] = {
		"../share/wgsip/show-wg", (char *) listen_addr, (char *) interface, "public-key", NULL };
	err = runstrcmd (-1, argv_pubkey, true, &keybuf);
	if (err != 0) {
		return err;
	}
	//
	// Allocate the keystr
	char *pubkey = strdup (keystr);
	if (pubkey == NULL) {
		return errno;
	}
	//
	// Return successfully (with a last replication risk)
	*listen_port = (uint16_t) portint;
	*public_key = pubkey;
	return 0;
}


/* Destroy a Wireguard interface, if it exists.
 *
 * Return 0 on success, otherwise an errno.
 *
 * CLIENT, SERVER.
 */
int wg_drop (const struct sa *listen_sa, const char *interface) {
	//
	// Produce the address string
	char listen_addr [INET6_ADDRSTRLEN];
	int err = sa_ntop (listen_sa, listen_addr, INET6_ADDRSTRLEN);
	if (err != 0) {
		return err;
	}
	//
	// Run the script command
	char *argv_drop [] = {
		"../share/wgsip/drop-wg", (char *) listen_addr, (char *) interface, NULL };
	err = runcmd (-1, -1, argv_drop);
	return err;
}


/* Set a peer on a Wireguard interface.  The pinning key identifies the peer.
 * This peer's endpoint, allowed-ip (prefix) and preshared-key will be set.
 *
 * Return 0 on success, otherwise an errno.
 *
 * CLIENT, SERVER.
 */
int wg_setpeer (const struct sa *listen_sa, const char *interface, const struct pl *pinkey,
			const struct sa *remote_sa, const struct pl *prefix, const char *pskey) {
	int err;
	int rdwr [2];
	//
	// Produce the listen_address string
	char listen_addr [INET6_ADDRSTRLEN];
	err = sa_ntop (listen_sa, listen_addr, INET6_ADDRSTRLEN);
	if (err != 0) {
		return err;
	}
	//
	// Produce the remote address string
	char remote_addr [1+INET6_ADDRSTRLEN+1+1+7];
	remote_addr [0] = '[';
	err = sa_ntop (remote_sa, remote_addr+1, INET6_ADDRSTRLEN);
	if (err != 0) {
		return err;
	}
	int raddrlen = strlen (remote_addr);
	remote_addr [raddrlen++] = ']';
	remote_addr [raddrlen++] = ':';
	sprintf (&remote_addr[raddrlen], "%d", sa_port (remote_sa));
	//
	// Rewrite the pinkey as a NUL-terminated string
	char pinkey_cstr [pinkey->l + 1];
	memcpy (pinkey_cstr, pinkey->p, pinkey->l);
	pinkey_cstr [pinkey->l] = '\0';
	//
	// Rewrite the prefix as a NUL-terminated string
	char prefix_cstr [pinkey->l + 1];
	memcpy (prefix_cstr, prefix->p, prefix->l);
	prefix_cstr [prefix->l] = '\0';
	//
	// Decide whether to set or clear the PSK
	if (pskey == NULL) {
		//
		// Setup /dev/null for stdin
		rdwr [0] = -1;
	} else {
		//
		// Open a PSK-pipe into which the pskey will be written
		if (pipe (rdwr) != 0) {
			err = errno;
			perror ("Failure in pipe()");
			return err;
		}
		//
		// Write pskey to the pipe to pop out as stdin
		int pskeylen = strlen (pskey);
		ssize_t wrlen = write (rdwr [1], pskey, pskeylen);
		close (rdwr [1]);
		err = errno;
		if (wrlen != pskeylen) {
			err = (wrlen < 0) ? errno : EDEADLK;
			goto cleanup;
		}
	}
	//
	// Run the script command with stdin from either /dev/zero or a PSK-pipe
	char *argv_setpeer [] = {
		"../share/wgsip/set-peer", listen_addr, (char *) interface,
			pinkey_cstr, remote_addr, prefix_cstr, NULL };
	err = runcmd (rdwr [0], -1, argv_setpeer);
	//
	// Cleanup the PSK-pipe if it was opened
cleanup:
	if (pskey != NULL) {
		close (rdwr [0]);
	}
	//
	// Return success or error, as arrived from runcmd
	return err;
}


/* Remove a peer from a Wireguard interface.
 *
 * Return 0 on success, otherwise an errno.
 *
 * CLIENT, SERVER.
 */
int wg_delpeer (const struct sa *listen_sa, const char *interface, const struct pl *pinkey) {
	//
	// Produce the address string
	char listen_addr [INET6_ADDRSTRLEN];
	int err = sa_ntop (listen_sa, listen_addr, INET6_ADDRSTRLEN);
	if (err != 0) {
		return err;
	}
	//
	// Rewrite the pinkey as a NUL-terminated string
	char pinkey_cstr [pinkey->l + 1];
	memcpy (pinkey_cstr, pinkey->p, pinkey->l);
	pinkey_cstr [pinkey->l] = '\0';
	//
	// Run the script command
	char *argv_delpeer [] = {
		"../share/wgsip/del-peer", listen_addr, (char *) interface,
			pinkey_cstr, NULL };
	err = runcmd (-1, -1, argv_delpeer);
	return err;
}


/* Setup a new SDP session with local information.
 *
 * CLIENT, SERVER
 */
int setup_sdp_session (struct sdp_session **sessp, const struct sa *esa, const char *pubkey, const char *prefix, const char *pskmth) {
	//
	// Setup SDP
	struct sdp_session *sess;
	assert (0 == sdp_session_alloc (&sess, esa));
	//
	// Have a default pskmth
	if (pskmth == NULL) {
		pskmth = "none";
	}
	//
	// Allocate additional media under SDP
	struct sdp_media *sdm;
	struct sdp_format *sdf;
	assert (0 == sdp_media_add (&sdm, sess, "application", sa_port (esa), "udp"));
	assert (0 == sdp_format_add (&sdf, sdm, false, "vnd.wireguard", "VPN", 0, 1, NULL, NULL, NULL, false, "pubkey=%s;prefix=%s;pskmth=%s", pubkey, prefix, pskmth));
	*sessp = sess;
	return 0;
}


/* Mix a remote offer into the SDP session with local information.
 *
 * CLIENT, SERVER
 */
int mixin_sdp_offer (struct sdp_session *sess, struct mbuf *sdp, const struct sa **out_raddr, struct pl *out_pubkey, struct pl *out_prefix, struct pl *out_pskmth) {
	//
	// Outputs to collect (and only return on success)
	const struct sa *ret_raddr = NULL;
	struct pl ret_pubkey = { .p = NULL, .l = 0 };
	struct pl ret_prefix = { .p = NULL, .l = 0 };
	struct pl ret_pskmth = { .p = NULL, .l = 0 };
	//
	// Incorporate the SDP offer to learn media remote address/port
	assert (0 == sdp_decode (sess, sdp, true));
	//
	// Do we have an "m=application 12345 udp vnd.wireguard" line?
	struct sdp_media *sdp_media_find(const struct sdp_session *sess,
			 const struct pl *name,
			 const struct pl *proto,
			 bool update_proto);		/* forgotten export? */
	struct pl app = { .p = "application", .l = 11 };
	struct pl udp = { .p = "udp",         .l =  3 };
	struct sdp_media *ln = sdp_media_find (sess, &app, &udp, false);
	fprintf (stderr, "ln[application]=%p\n", ln);
	if (ln != NULL) {
		fprintf (stderr, "remote port: %d attr: %s\n", (int) sdp_media_rport (ln), sdp_media_rattr (ln, "vnd.wireguard"));
	}
	//
	// Iterate over the media found for the SDP session
	struct le *le;
	LIST_FOREACH (sdp_session_medial (sess, false), le) {
		struct sdp_media *m = le->data;
		fprintf (stderr, "Considering name=%s, proto=%s\n", sdp_media_name (m), sdp_media_proto (m));
		if ((str_cmp (sdp_media_name (m), "application") == 0) &&
				(str_cmp (sdp_media_proto (m), "udp") == 0)) {
			ret_raddr = sdp_media_raddr (m);
#ifdef DEBUG
			fprintf (stderr, "Got it, rport=%u, raddr=", sdp_media_rport (m));
			sa_print_addr (&err4re, ret_raddr);
			fprintf (stderr, "\n");
			// fprintf (stderr, ", format=%p\n", sdp_media_format (m, "VPN" /*"vnd.wireguard"*/));
			// struct pl wgpl = { .p = "vnd.wireguard", .l = 13 };
			// fprintf (stderr, " - vnd.wireguard = %p\n", sdp_format_find (sdp_media_format_lst (m, false), &wgpl));
#endif
			struct le *lef;
			LIST_FOREACH (sdp_media_format_lst (m, false), lef) {
				struct sdp_format * fmt = lef->data;
#ifdef DEBUG
				fprintf (stderr, " - Considering format with params %s\n", fmt->params);
				sdp_format_debug (&err4re, lef->data);
#endif
				//
				// Parse the format so the variables in it may be returned
				// If it does not match, continue with old/empty findings
				if (0 != re_regex (fmt->params, strlen (fmt->params), "pubkey=[^;]+;prefix=[^;]+;pskmth=[^;]+", &ret_pubkey, &ret_prefix, &ret_pskmth)) {
					if (0 == re_regex (fmt->params, strlen (fmt->params), "pubkey=[^;]+;prefix=[^;]+", &ret_pubkey, &ret_prefix)) {
						ret_pskmth.p = "none";
						ret_pskmth.l = 4;
					}
				}
			}
		}
	}
#ifdef DEBUG
	sdp_media_debug (&err4re, ln);
#endif
	//
	// Decide about success and return BADMSG if not
	if ((ret_raddr == NULL) || (ret_pubkey.p == NULL) || (ret_prefix.p == NULL) || (sa_af (ret_raddr) != AF_INET6)) {
		return EBADMSG;
	}
	*out_raddr = ret_raddr;
	memcpy (out_pubkey, &ret_pubkey, sizeof (*out_pubkey));
	memcpy (out_prefix, &ret_prefix, sizeof (*out_prefix));
	memcpy (out_pskmth, &ret_pskmth, sizeof (*out_pskmth));
	return 0;
}


/* Extract the user@domain.name part from a parsed URI.  Return 0 when OK,
 * otherwise reset the userdom_out and return an error code.
 *
 * CLIENT, SERVER
 */
int extract_userdomain (struct pl *userdom_out, const struct uri *uri_in) {
	if ((uri_in == NULL) || (uri_in->user.p == NULL) || (uri_in->host.p == NULL)) {
		goto badmsg;
	}
	if (uri_in->user.p + uri_in->user.l + 1 != uri_in->host.p) {
		goto badmsg;
	}
	userdom_out->p = uri_in->user.p;
	userdom_out->l = uri_in->user.l + 1 + uri_in->host.l;
	return 0;
badmsg:
	memset (userdom_out, 0, sizeof (*userdom_out));
	return EBADMSG;
}


/* Hook for incoming SIP requests.
 *
 * SERVER
 */
struct srvreq_data {
	struct sip *stack;
};
//
bool server_request (const struct sip_msg *req, void *arg) {
	struct srvreq_data *md = (struct srvreq_data *) arg;
#ifdef DEBUG
	fprintf (stderr, "server_request,<sip>\n%.*s</sip>\n", (int) (req->mb->end -           0 ), &req->mb->buf [          0 ]);
#endif
	if (pl_strcmp (&req->ver, "SIP/2.0") != 0) {
		fprintf (stderr, "Wild version %.*s will be ignored\n", (int) req->ver.l, req->ver.p);
		goto send400;
	}
	if (!req->req) {
		fprintf (stderr, "Spurious response will be ignored\n");
		return true;
	}
	bool m_ACK    = (0 == pl_strcmp (&req->met, "ACK"   ));
	bool m_INVITE = (0 == pl_strcmp (&req->met, "INVITE"));
	bool m_BYE    = (0 == pl_strcmp (&req->met, "BYE"   ));
	if (m_ACK) {
		fprintf (stderr, "Quietly swallowing ACK\n");
		return true;
	}
	if (! (m_INVITE || m_BYE)) {
		fprintf (stderr, "Ignoring non-INVITE / non-BYE message\n");
		goto send405;
	}
//TODO// Crudely send a 401 response to the first message received
static int chalctr = 1;
if (chalctr > 0) {
chalctr--;
assert (0 == sip_replyf (md->stack, req, 401, "Unauthorized",
"Content-type: application/sdp\r\nProxy-Authenticate: Digest realm=\"example.com\", nonce=\"SGVsbG8gSG90IFdvcmxkCg==\", qop=\"auth\"\r\nContent-length: 0\r\n\r\n"));
return true;
}
//TODO// Authenticate with SIP-SASL
//TODO// Authorise as a ARPA2 group member: access_comm(remotename, localname, opt_svckey/len, optrule:NULL/0, out:level, optout:actor)) --> true and non-NULL actor, then group_hasmember (testing:actor, opt_svckey/len, optrule:NULL/0, outmust:RW) --> true
//TODO// Declare CommSvcKey, GroupSvcKey (to not derive the domain-without-password defaults)
//TODO// Consider CommSvcRule, GroupSvcRule (to not use the Rules DB)
	if (m_INVITE && !msg_ctype_cmp (&req->ctyp, "application", "sdp")) {
		fprintf (stderr, "Missing Content-Type in INVITE: application/sdp\n");
		goto send415;
	}
	//
	// Determine the target user@domain.name
#if 0
	fprintf (stderr, "Request-URI: <%.*s>\n", (int) req->ruri.l, req->ruri.p);
	struct uri ruri;
	assert (0 == uri_decode (&ruri, &req->ruri));
	if ((ruri.user.p == NULL) || (ruri.host.p == NULL)) {
		fprintf (stderr, "Request-URI lacks user and/or host\n");
		goto send400;
	}
	if (ruri.user.p + ruri.user.l + 1 != ruri.host.p) {
		fprintf (stderr, "Request-URI user and host do not align\n");
		goto send400;
	}
	const struct pl target = { .p = ruri.user.p, .l = ruri.user.l + 1 + ruri.host.l };
	fprintf (stderr, "Request-URI user@domain.name <%.*s>\n", (int) target.l, target.p);
#endif
	//
	// Determine the user@domain.name in From: and To: headers
	struct pl pl_from;
	struct pl pl_to;
	int err = 0;
	err = err || extract_userdomain (&pl_from, &req->from.uri);
	err = err || extract_userdomain (&pl_to  , &req->to  .uri);
	//
	// Lookup server configuration for the target
	const struct profile *prf = config_find_relation (&pl_to, &pl_from);
	if ((prf == NULL) || (prf->LocalName == NULL)) {
#if 0
		fprintf (stderr, "No server setup for target <%.*s>\n", (int) target.l, target.p);
#else
		fprintf (stderr, "No profile serving From: <sip:%.*s> To: <sip:%.*s>\n",
				(int) pl_from.l, pl_from.p,
				(int) pl_to  .l, pl_to  .p);
#endif
		goto send404;
	}
	//
	// Parse the OuterAddress into a Wireguard endpoint
	bool stxerr = false;
	//
	// Parse the OuterAddress into esa
	struct sa esa;
	if (prf->OuterAddress == NULL) {
		stxerr = stxerr || (0 != net_default_source_addr_get (AF_INET6, &esa));
	} else {
		stxerr = stxerr || (0 != sa_pton (prf->OuterAddress, &esa));
	}
	//
	// Be sure to have a Wireguard interface, fetch port and public key
	uint16_t tunnel_port = 0;
	char *tunnel_pubkey = NULL;
	stxerr = stxerr || (0 != wg_have (&esa, prf->TunnelIfName, prf->InnerAddress, &tunnel_port, &tunnel_pubkey));
	//
	// Report any error
	if (stxerr) {
		fprintf (stderr, "Failure producing OuterAddress data\n");
		goto send500;
	}
	//
	// Set the tunnel_port as endpoint address
	sa_set_port (&esa, tunnel_port);
	//
	// Combine the SDP sessions, first the local...
	struct sdp_session *sess;
	setup_sdp_session (&sess, &esa, tunnel_pubkey, prf->InnerPrefix, prf->PskMethod);
	//
	// ...then the remote SDP side
	const struct sa *raddr;
	struct pl pubkey;
	struct pl prefix;
	struct pl pskmth;
	if (0 == mixin_sdp_offer (sess, req->mb, &raddr, &pubkey, &prefix, &pskmth)) {
		printf ("\n\nWG %s ... ADDR=", m_INVITE ? "SETUP" : "TEARDOWN");
		sa_print_addr (&out4re, raddr);
		printf (" PORT=%u", sa_port (raddr));
		printf (" PUBKEY=%.*s", (int) pubkey.l, pubkey.p);
		printf (" PREFIX=%.*s", (int) prefix.l, prefix.p);
		printf (" PSKMTH=%.*s\n\n", (int) pskmth.l, pskmth.p);
		if (m_INVITE) {
			assert (0 == wg_setpeer (&esa, prf->TunnelIfName, &pubkey, raddr, &prefix, NULL));
		} else {
			assert (0 == wg_delpeer (&esa, prf->TunnelIfName, &pubkey));
		}
	}
	//
	// Print SDP session debugging information
#ifdef DEBUG
	assert (0 == sdp_session_debug (&err4re, sess));
#endif
	//
	// Produce an SDP message and print it
	struct mbuf *sdpbuf;
	assert (0 == sdp_encode (&sdpbuf, sess, true));
	printf ("<sdp>\n%.*s</sdp>\n", (int) (sdpbuf->end - sdpbuf->pos), &sdpbuf->buf [sdpbuf->pos]);
	//
	// Respond with the SDP attachment
	assert (0 == sip_replyf (md->stack, req, 200, "OK",
			"Content-type: application/sdp\r\nContent-length: %d\r\n\r\n%b",
			sdpbuf->end,
			sdpbuf->buf, sdpbuf->end));
	//
	// The message was handled, so we return true
	//TODO// Cleanup, globals, bidirectionality
	return true;
	//
	// Send an error response for non-matching recipients
	int   sta;
	char *str;
send400:
	sta = 400;
	str = "Bad Request";
	goto senderr;
send404:
	sta = 404;
	str = "Not Found";
	goto senderr;
send405:
	sta = 405;
	str = "Method Not Allowed";
send415:
	sta = 415;
	str = "Unsupported Media Type";
	goto senderr;
send500:
	sta = 500;
	str = "Internal Server Error";
senderr:
	assert (0 == sip_reply (md->stack, req, sta, str));
	return true;
}


/* Hook for sending the request.  Called from request() in sip/request.c
 *
 * TODO: Unclear what it does! but it seems to supply extra Via: headers and trailing content?
 *
 * UNUSED
 */
#if 0
struct send_data {
	struct sip *stack;
};
//
int send_handler (enum sip_transp tp, struct sa *src, const struct sa *dst, struct mbuf *mb, struct mbuf **mbp, void *arg) {
	printf ("send_handler()\n");
	printf ("<sip>\n%.*s</sip>\n", (int) (mb->end - mb->pos), &mb->buf [mb->pos]);
	return 0;
}
#endif


/* Hook for responses
 *
 * CLIENT
 */
struct cliresp_data {
	struct sip *stack;
	const struct profile *profile;
	struct sip_request *cause;
	struct sip_auth *auth;
	struct sdp_session *sess;
	uint32_t cseq_num;
	bool disconnect;
	uint64_t call_id;
	const char *call_dom;
	uint64_t from_tag;
	struct uri nexthop;
	const char *lname;
	const char *rname;
	const char *ifname;
	struct mbuf *sdpbuf;
	struct sa esa;
};
//
void client_response (int err, const struct sip_msg *rsp, void *arg) {
	//
	// Map the argument type to cliresp_data
	struct cliresp_data *crd = (struct cliresp_data *) arg;
	//
	// Print a debugging message
#ifdef DEBUG
	if (err != 0) {
		fprintf (stderr, "client_response(%d,...)\n", err);
		return;
	} else if (rsp->mb != NULL) {
		fprintf (stderr, "client_response(%d,<sip>\n%.*s</sip>,[arg])\n", err, (int) rsp->mb->end, rsp->mb->buf);
	} else {
		fprintf (stderr, "response-Handler(0,NULL)\n");
	}
#endif
	//
	// Check for timeout errors
	if (err == ETIMEDOUT) {
		sip_request_cancel (crd->cause);
		return;
	}
	//
	// Ignore replies without a To: tag
	if (rsp->to.tag.p == NULL) {
		return;
	}
	//
	// Authenticate if that is requested
	if ((rsp->scode == 401) || (rsp->scode==407)) {
		//
		// Need to resend the request with authentication
		assert (0 == sip_auth_authenticate (crd->auth, rsp));
		//
		// Cancel the old request
		sip_request_cancel (crd->cause);
		//
		//TODO// Test CSeq against authcseq -- to make it step properly
		if (rsp->cseq.num != crd->cseq_num) {
			return;
		}
		//
		// Produce a secondary request
		const char *method = crd->disconnect ? "BYE" : "INVITE";
		char to_uri [4 + strlen (crd->profile->RemoteName) + 1];
		sprintf (to_uri, "sip:%s", crd->profile->RemoteName);
		crd->cause    = NULL;
		crd->cseq_num++;
		assert (0 == sip_requestf (&crd->cause, crd->stack, true,
				method, to_uri,
				&crd->nexthop,
				crd->auth,
				NULL /*send_handler*/, client_response, (void *) crd,
				/* Headers with formatting: */
				"From: Client <sip:%s>;tag=%016llx\r\n"
				"To: Wireguard RAN <sip:%s>;tag=%b\r\n"
				"Call-ID: <%016llx@%s>\r\n"
				"CSeq: %d %s\r\n"
				"Content-type: application/sdp\r\n"
				"Content-length: %d\r\n"
				"\r\n"
				"%b",
					crd->lname, crd->from_tag,
					crd->rname,
					rsp->to.tag.p, rsp->to.tag.l,
					crd->call_id, crd->call_dom,
					crd->cseq_num, method,
					crd->sdpbuf->end,
					crd->sdpbuf->buf, crd->sdpbuf->end));
		return;
	}
	//
	// Clear the authentication data, since we are done with it
	sip_auth_reset (crd->auth);
	//
	// Leave for anything but a 200 OK or 183 Session Progress
	// Note: 183 is an option for 802.1x in the VPN, with SIP accept/reject
	if ((rsp->scode != 200) && (rsp->scode != 183)) {
		goto leave;
	}
	//
	// Leave if there is no application/sdp attachment
	if (!msg_ctype_cmp (&rsp->ctyp, "application", "sdp")) {
		goto leave;
	}
	//
	// We found a 200 OK or 183 Session Progress with SDP attachment
	fprintf (stderr, "Response %03d %.*s to our request offers SDP\n", rsp->scode, (int) rsp->reason.l, rsp->reason.p);
	const struct sa *raddr;
	struct pl pubkey;
	struct pl prefix;
	struct pl pskmth;
	if (0 == mixin_sdp_offer (crd->sess, rsp->mb, &raddr, &pubkey, &prefix, &pskmth)) {
		printf ("\n\nWG %s ... ADDR=", crd->disconnect ? "TEARDOWN" : "SETUP");
		sa_print_addr (&out4re, raddr);
		printf (" PORT=%u", sa_port (raddr));
		printf (" PUBKEY=%.*s", (int) pubkey.l, pubkey.p);
		printf (" PREFIX=%.*s", (int) prefix.l, prefix.p);
		printf (" PSKMTH=%.*s\n\n", (int) pskmth.l, pskmth.p);
		if (crd->disconnect) {
			assert (0 == wg_delpeer (&crd->esa, crd->ifname, &pubkey));
		} else {
			assert (0 == wg_setpeer (&crd->esa, crd->ifname, &pubkey, raddr, &prefix, NULL));
		}
	}
	//
	// Print SDP session debugging information
#ifdef DEBUG
	assert (0 == sdp_session_debug (&err4re, crd->sess));
#endif
	//
	// The client is done
	sip_request_cancel (crd->cause);
	re_cancel ();
	//
	// Print SIP stack debugging information
#ifdef DEBUG
	assert (0 == sip_debug (&err4re, crd->stack));
#endif
	//
	// Free the memory of the client response data
	free (crd);
	//
	// Return in a positive mood
	return;
leave:
	//TODO// find request, then: sip_request_cancel (req);
	fprintf (stderr, "Accepting response %03d %.*s\n", rsp->scode, (int) rsp->reason.l, rsp->reason.p);
	;
	//
	// Finish the transaction after any final responses
	if (rsp->scode >= 200) {
		sip_request_cancel (crd->cause);
		re_cancel ();
	}
}


/* Hook for authentication
 *
 * UNUSED
 */
struct auth_data {
	struct sip *stack;
};
//
int auth_handler (char **user, char **pass, const char *realm, void *_arg) {
	fprintf (stderr, "Authentication handler for realm=%s\n", realm);
	int err = 0;
	err |= str_dup (user, "67890");
	err |= str_dup (pass, "sekreet");
	fprintf (stderr, "Authentication handled --> %d\n", err);
	return err;
}


/* Hook for signals: SIGINT, SIGALRM, SIGTERM
 *
 * CLIENT, SERVER
 */
static void signal_handler (int sig) {
	fprintf (stderr, "\n***signal %d***\n", sig);
	re_cancel ();
}


/* Start a client by sending the request.  Then, callbacks take control.
 * Return true on success, false on failure.
 */
bool start_client (const struct profile *prf, const char *progname, struct sip *stack, bool disconnect) {
	//
	// Parse the OuterAddress, or fallback to the default source address
	// Note: The port is obtained dynamically, using wg_have()
	struct sa esa;
	if (prf->OuterAddress == NULL) {
		if (net_default_source_addr_get (AF_INET6, &esa) != 0) {
			fprintf (stderr, "Failure to replace OuterAddress with default source address\n");
			return false;
		}
	} else {
		if (sa_pton (prf->OuterAddress, &esa) != 0) {
			return false;
		}
	}
	//
	// Try to parse the ProxyAddress
	struct sa rsa;
	struct sa lsa;
	if (prf->ProxyAddress == NULL) {
		//
		// Clear the ProxyAddress
		memset (&rsa, 0, sizeof (rsa));
		//
		// Base LocalAddress on defaults
		net_default_source_addr_get (AF_INET6, &lsa);
	} else {
		//
		// Parse the ProxyAddress
		if (parse_sa (&rsa, prf->ProxyAddress, NULL, prf->name, "ProxyAddress") != 0) {
			return false;
		}
		//
		// Base LocalAddress on RemoteAddress
		net_dst_source_addr_get (&rsa, &lsa);
	}
	//
	// Setup a transport for UDP
	if (sip_transp_add (stack, SIP_TRANSP_UDP, &lsa) != 0) {
		fprintf (stderr, "%s: Failed to add local SIP transport\n", progname);
		return false;
	}
	//
	// Have a Wireguard interface, and set Endpoint port and PublicKey
	uint16_t esa_port = 0;
	char *public_key = NULL;
	int err = wg_have (&esa, prf->TunnelIfName, prf->InnerAddress, &esa_port, &public_key);
	if (err != 0) {
		errno = err;
		perror ("Failed to setup Wireguard interface");
		return false;
	}
	sa_set_port (&esa, esa_port);
	//
	// Setup the local side of an SDP session
	struct sdp_session *sess;
	if (setup_sdp_session (&sess, &esa, public_key, prf->InnerPrefix, NULL) != 0) {
		fprintf (stderr, "%s: Failed setup_sdp_session()\n", progname);
		return false;
	}
	//
	// Produce an SDP message and print it
	struct mbuf *sdpbuf;
	if (sdp_encode (&sdpbuf, sess, true) != 0) {
		fprintf (stderr, "%s: Failed in sdp_encode()\n", progname);
		return false;
	}
	fprintf (stderr, "<sdp>\n%.*s</sdp>\n",
			(int) (sdpbuf->end - sdpbuf->pos),
			&sdpbuf->buf [sdpbuf->pos]);
	//
	// Produce an authentication object
	struct sip_auth *auth;
	if (sip_auth_alloc (&auth, auth_handler, NULL /*arg*/, false /*inc-arg-ref*/) != 0) {
		fprintf (stderr, "%s: Failed sip_auth_alloc()\n", progname);
		return false;
	}
	//
	// Prepare response data
	//TODO// mem_zalloc()
	struct cliresp_data *crd = calloc (1, sizeof (struct cliresp_data));
	if (crd == NULL) {
		fprintf (stderr, "%s: Out of memory\n", progname);
		return false;
	}
	crd->stack      = stack;
	crd->profile    = prf;
	crd->auth       = auth;
	crd->sess       = sess;
	crd->disconnect = disconnect;
	crd->cseq_num   = 1;
	crd->cause      = NULL;
	crd->lname      = prf->LocalName;
	crd->rname      = prf->RemoteName;
	crd->ifname	= prf->TunnelIfName;
	crd->from_tag   = rand_u64 ();
	crd->call_id    = rand_u64 ();
	crd->call_dom   = strrchr (prf->LocalName, '@');
	crd->call_dom   = (crd->call_dom == NULL) ? prf->LocalName : 1 + crd->call_dom;
	crd->sdpbuf     = sdpbuf;
	sa_cpy (&crd->esa, &esa);
	//
	// Configure a URI for the next-hop route and a matching sender
	memset (&crd->nexthop, 0, sizeof (crd->nexthop));
	const char *proxy_colon = (prf->ProxyAddress != NULL) ? strrchr (prf->ProxyAddress, ':') : NULL;
	if (proxy_colon != NULL) {
		crd->nexthop.scheme.p = "sip";
		crd->nexthop.scheme.l = 3;
		crd->nexthop.params.p = "transport=udp";
		crd->nexthop.params.l = 13;
		crd->nexthop.host  .p = prf->ProxyAddress + 1;
		crd->nexthop.host  .l = (proxy_colon - prf->ProxyAddress - 2);
		crd->nexthop.port     = sa_port (&rsa);
	}
	//
	// Send the INIVITE request
	const char *method = disconnect ? "BYE" : "INVITE";
	char to_uri [4 + strlen (prf->RemoteName) + 1];
	sprintf (to_uri, "sip:%s", prf->RemoteName);
	bool sent = (0 == sip_requestf (&crd->cause, stack, true,
			method, to_uri,
			&crd->nexthop,
			NULL /*auth*/,
			NULL /*send_handler*/, client_response, (void *) crd,
			/* Headers with formatting: */
			"From: Client <sip:%s>;tag=%016llx\r\n"
			"To: Wireguard RAN <sip:%s>\r\n"
			"Call-ID: <%016llx@%s>\r\n"
			"CSeq: %d %s\r\n"
			"Content-type: application/sdp\r\n"
			"Content-length: %d\r\n"
			"\r\n"
			"%b",
				prf->LocalName, crd->from_tag,
				prf->RemoteName,
				crd->call_id, crd->call_dom,
				crd->cseq_num, method,
				sdpbuf->end,
				sdpbuf->buf, sdpbuf->end));
	//
	// Print stack debugging information
#ifdef DEBUG
	sip_debug (&err4re, stack);
#endif
	//
	// Return whether the request was sent
	return sent;
}


/* The main program.
 *
 * CLIENT uses many arguments, and probably should continue with that
 * CLIENT would mostly do what is done below
 *
 * SERVER can be called with [-c CONFFILE] [-f] [PROFILE...] only
 * SERVER would pickup only named PROFILEs (or all if none mentioned)
 * SERVER would try to open each ListenAddress, listen and re_main()
 */
int main (int argc, char *argv []) {
	const char *progname = argv [0];
	//
	// Setup constants
	out4re.arg = (void *) stdout;
	err4re.arg = (void *) stderr;
	//
	// Figure out the mode of operation
	int prognamelen = strlen (progname);
	bool client = false;
	bool server = false;
	bool disconnect = false;
	if ((prognamelen > 8) && (strcmp (progname + prognamelen - 8 , "-connect") == 0)) {
		client = true;
	} else if ((prognamelen > 11) && (strcmp (progname + prognamelen - 11, "-disconnect") == 0)) {
		client = true;
		disconnect = true;
	} else if ((prognamelen > 7) && (strcmp (progname + prognamelen - 7, "-server") == 0)) {
		server = true;
	}
	bool stxerr = ! (client || server);
	//
	// Parse arguments
	int opt;
	bool help = false;
	bool foreground = false;
	const char *explicit_cfgfile = NULL;
	while (opt = getopt (argc, argv, "fhc:"), opt != -1) {
		switch (opt) {
		case 'f':
			/* Setup for foreground server operation */
			stxerr = stxerr || foreground || client;
			foreground = true;
			break;
		case 'c':
			/* Set a specific INI configuration file */
			stxerr = stxerr || (explicit_cfgfile != NULL);
			explicit_cfgfile = optarg;
			break;
		case 'h':
			help = true;
			break;
		default:
			stxerr = true;
			break;
		}
	}
	//
	// Determine the PROFILE... (1+ for clients, 0+ for servers)
	struct config_profiles confprofs = {
		.profc = argc - optind,
		.profv = argv + optind,
	};
	bool have_profiles = (confprofs.profc > 0);
	stxerr = stxerr || (client && !have_profiles);
	//
	// Report any errors and possibly quit
	if (stxerr || help) {
		if (client) {
			fprintf (stderr, "%s [-c conffile] profile...\n", progname);
		} else if (server) {
			fprintf (stderr, "%s [-c conffile] [-f] [profile...]\n", progname);
		} else {
			fprintf (stderr, "%s does not end in -connect, -disconnect or -server\n", progname);
		}
		exit (stxerr ? true : false);
	}
	//
	// Determine the name for the configuration file
	char *homedir = getenv ("HOME");
	if (homedir == NULL) {
		homedir = "/root";
	}
	char cfgfile [PATH_MAX + 3];
	if (explicit_cfgfile != NULL) {
		strncpy (cfgfile, explicit_cfgfile, PATH_MAX);
		cfgfile [PATH_MAX] = 0;
	} else {
		snprintf (cfgfile, PATH_MAX + 1, "%s/.config/0cpm/wgsip.conf", homedir);
		if ((access (cfgfile, F_OK) != 0) && (errno == ENOENT)) {
			strcpy (cfgfile, "/etc/0cpm/wgsip.conf");
		}
	}
	//
	// Load the configuration file, constrained by PROFILE...
	(void) ini_parse (cfgfile, config_cb, (void *) &confprofs);
	config_prune (client, server);
	//TODO// Test that all specified profiles were kept
	if (profiles == NULL) {
		fprintf (stderr, "%s: No usable profiles\n", progname);
		exit (1);
	}
	//
	// Fork now if this is a server not running in the foreground
	if (server && !foreground) {
		pid_t pid = fork ();
		switch (pid) {
		case -1:
			/* An error occurred */
			perror ("Failed to fork()");
			exit (1);
		case 0:
			/* Child process, continues */
			setsid ();
			break;
		default:
			/* Parent process, return successfully */
			//TODO// Save pidfile
			exit (0);
		}
	}
	//
	// Initialise libre
	assert (0 == libre_init ());
	//
	// Setup a DNS client
	struct sa nsv [32];
	uint32_t nsc = 32;
	assert (0 == dns_srv_get (NULL, 0, nsv, &nsc));
	struct dnsc *dnsc;
	assert (0 == dnsc_alloc (&dnsc, NULL, nsv, nsc));
	//
	// Setup a SIP stack, but *not* a full SIP dialog because
	// we want the Wireguard session to outlive us
	struct sip *stack = NULL;
	assert (0 == sip_alloc (&stack, dnsc, 32, 32, 32, "SIP for Wireguard",
				NULL /*exith*/, NULL));
	//
	// The server tries all ListenAddress declarations
	bool listening = false;
	if (server) {
		for (struct profile *prf = profiles; prf != NULL; prf = prf->next) {
			//
			// Not all profiles declare a ListenAddress
			if (prf->ListenAddress == NULL) {
				continue;
			}
			//
			// Parse the ListenAddress as [v6addr]:port
			struct sa lsa;
printf ("INTEND TO LISTEN ON %s\n", prf->ListenAddress);
			if (parse_sa (&lsa, prf->ListenAddress, "[::]:5060", prf->name, "ListenAddress") != 0) {
printf ("FAILED TO PARSE [v6addr]:port TO LSITEN ON: %s or [::]:5060\n", prf->ListenAddress);
				continue;
			}
			//
			// Setup a transport for UDP -- quietly accept failures for doubles
			if (sip_transp_add (stack, SIP_TRANSP_UDP, &lsa) != 0) {
printf ("FAILED TO ADD TRANSPORT FOR %s or [::]:5060\n", prf->ListenAddress);
				continue;
			}
			//
			// Setup the corresponding SIP message listener
			struct srvreq_data srd;
			srd.stack = stack;
			struct sip_lsnr *lsnr = NULL;
			if (sip_listen (&lsnr, stack, true, server_request, (void *) &srd) != 0) {
				fprintf (stderr, "%s: Failed to listen on %s\n",
						progname, prf->ListenAddress);
				continue;
			}
			//
			// We got through, and are now listening
			fprintf (stderr, "%s: Listening on %s\n",
						progname, prf->ListenAddress);
			listening = true;
		}
		if (!listening) {
			fprintf (stderr, "%s: No ListenAddress succeeded\n", progname);
			exit (1);
		}
	}
	//
	// For a client, run over the profiles
	unsigned active_clients = 0;
	if (client) {
		for (int i = 0; i < confprofs.profc; i++) {
			const struct profile *prf = config_find_named (confprofs.profv [i]);
			if (prf == NULL) {
				fprintf (stderr, "%s: Profile [%s] was not found, or was unusable\n",
						progname, confprofs.profv [i]);
			} else if (!start_client (prf, progname, stack, disconnect)) {
				fprintf (stderr, "%s: Profile [%s] failed\n",
						progname, confprofs.profv [i]);
			} else {
				active_clients++;
			}
		}
	}
	//
	// Main loop, re_main() runs until someone calls re_cancel()
	// ...re_main (...);
	if (listening || (active_clients > 0)) {
		re_main (signal_handler);
	}
#if 0
	//
	// Cancel the outstanding request TODO:ALLOFTHEM?
	if (client) {
		sip_request_cancel (sipreq);
	}
#endif
	//
	// Free the SIP stack, allowing any transactions to close
	// but that does not imply closing the Wireguard session
	sip_close (stack, false);
	//
	// Free libre state
	libre_close ();
	//
	// Drop all configuration data
	config_prune (false, false);
	//
	// Return success
	return 0;
}
