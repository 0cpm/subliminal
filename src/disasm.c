/* disasm.c -- Diasassembler for HDLC command seqeuences.
 *
 * The input is a binary byte sequence, with frames between FLAG 0x7e,
 * possibly avoiding their interpretation with an internal BREAK 0x7f,
 * and with byte-level escaping using 0x7d and ^0x40 in the next byte.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define SUBLIMINAL_HDLC_ASM_INTERNAL
#include <0cpm/subliminal.h>


int main (int argc, char *argv []) {
	uint8_t frame [2048];
	uint16_t frame_gotten = 0;
	bool frame_eof = false;
	bool in_a_frame = false;
	unsigned linenr = 0;
	uint8_t last_address = 0x00;
	//
	// Setup the output file in the sublime_hdlc_putframe() backend
	sublime_hdlcasm_swap_outfile (stdout);
	//
	// Read pieces from the input and find the frames
	while ((frame_gotten > 0) || !frame_eof) {
		//
		// Have enough input to match a HDLC frame
		if (frame_gotten < 512) {
			size_t rdlen = fread (frame + frame_gotten, 1, 1024, stdin);
			frame_eof = frame_eof || (rdlen == 0);
			frame_gotten += rdlen;
		}
		//
		// Chase the frame for a FLAG
		uint16_t framelen = 0;
		while ((framelen < frame_gotten) && (frame [framelen] != 0x7e)) {
			framelen++;
		}
		//
		// We are not in a frame if we did not find a FLAG
		if (framelen >= frame_gotten) {
			in_a_frame = false;
			framelen = frame_gotten - 1;
		}
		//
		// If this is a second FLAG then we have a frame to process
		bool cmd_break = false;
		if (in_a_frame) {
			//
			// Remove escapes and derive innerlen <= framelen
			uint16_t innerlen = 0;
			for (uint16_t i = 0; i < framelen; i++) {
				if (frame [i] == 0x7f) {
					cmd_break = true;
				} else if (frame [i] == 0x7d) {
					i++;
					frame [innerlen++] = frame [i] ^ 0x40;
				} else {
					frame [innerlen++] = frame [i]       ;
				}
			}
			//
			// Print the frame in assembler input format
			if (cmd_break) {
				//TODO// This is not triggered [for a trailing BREAK]
				printf ("\t.BREAK\n");
			} else {
				linenr++;
				uint8_t address = frame [0];
				uint8_t command = frame [1];
				sublime_hdlc_putframe (NULL, address, command, frame + 2, innerlen - 2 - 2);
			}

		}
		//
		// Whatever happened above, we found FLAG and are definitely in a frame
		in_a_frame = true;
		//
		// Remove the frame up to the FLAG from the buffer
		if (framelen + 1 < frame_gotten) {
			memcpy (frame, frame + framelen + 1, frame_gotten - framelen - 1);
		}
		frame_gotten -= framelen + 1;
		//
		// Now cycle back for more of the input / frame
		;
	}
	//
	// Remove the output file in the sublime_hdlc_putframe() backend
	(void) sublime_hdlcasm_swap_outfile (NULL);
	//
	// Done, and happy
	return 0;
}

