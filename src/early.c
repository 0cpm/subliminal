/* early.c -- Send or receive SIP Early Media.
 *
 * This tool can be used to pass data while a phone is ringing.  During
 * this phase, a ringback tone may optionally be passed back from the
 * callee to the caller, and the network tends to relay that as sound.
 *
 * When sending, insert subliminal messages between INVITE and CANCEL/BYE.
 * When receiving, receive data between 183 Session Progress and CANCEL/BYE.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#define _GNU_SOURCE	/* for strchrnul() */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <netinet/in.h>

#include <re/re.h>

#define SUBLIMINAL_HDLC_ASM_INTERNAL
#include <0cpm/subliminal.h>


#define ALAW_SAMPLES 160
#define ALAW_MSEC 20


/* The number of kilometers that fit in one second matches
 * roughtly the number of milliseconds in 5 minutes, which
 * is a good waiting time.  Yeah, nerd prank, I know...
 */
#ifndef TIMEOUT_5MIN
#define TIMEOUT_5MIN 299792
#endif


/* The number of seconds in an hour serves as registration time.
 * It can be overridden to allow faster test cycles.
 */
#ifndef TIMEOUT_60MIN
#define TIMEOUT_60MIN 3600
#endif



/* Argument iteration, starting from 0, separately for .hdlc and .al;
 * for (argi = arg0; argi < argc; argi++) { ...process...; }
 */
struct argit {
	char *ext;
	char **argv;
	int extl;
	int arg0;
	int argi;
	int argc;
	bool have;
	bool loop;
};


/* Send state, used to manage the isolated data for a call in send_main()..
 * This allows parallel handling of calls, which is also fine for libre.
 */
struct send_state {
	/* List element for this sender */
	struct le le;
	/* SIP stack */
	struct sip *stack;
	/* Call-ID and From: tag identify the caller */
	char *call_id;
	char *from_tag;
	/* A-law data, length and current index */
	off_t idx_alaw;
	/* Subliminal connection handler */
	struct sublime *subliminal_sender;
	/* RTP output buffer with PCMA payload pointer */
	struct mbuf *outbuf;
	uint8_t *payload;
	/* RTP connection and destination */
	struct rtp_sock *rtpsox;
	struct sa remote_sa;
	/* RTP timing information */
	uint32_t timestamp;
	uint64_t realtime;
	uint8_t realtime_mod5;
	/* RTP interval timer */
	struct tmr interval;
	/* RTP timeout after about 5 minutes */
	struct tmr early5min;
};


/* Some of the sending state is static, and therefore truly global.
 */
char *ss_data_alaw = NULL;
off_t ss_len_alaw = 0;
struct list ss_senders;


/* Debugging support for libre.
 */
int print4re (const char *str, size_t size, void *arg) {
	fwrite (str, 1, size, (FILE *) arg);
	return 0;
}
//
struct re_printf out4re = { print4re, NULL /* stdout setup in main() */ };
//
struct re_printf err4re = { print4re, NULL /* stderr setup in main() */ };


/* Next value for an iterator; intends to return an argument string.
 * Arguments are matched by their extensions.
 * Return NULL when nothing (more) is available.
 */
char *nextarg (struct argit *ai) {
	while (ai->have) {
		ai->argi++;
		if (ai->argi >= ai->argc) {
			if (!ai->loop) {
				ai->have = false;
				break;
			} else {
				ai->argi = ai->arg0;
			}
		}
		int arglen = strlen (ai->argv [ai->argi]);
		if (arglen > ai->extl) {
			if (0 == strcmp (ai->argv [ai->argi] + arglen - ai->extl, ai->ext)) {
				return ai->argv [ai->argi];
			}
		}
	}
	return NULL;
}


/* Clear a string before returning its memory.  This is called as part
 * of libre cleanup routines.
 */
void str_clear (void *str) {
	int len = strlen ((char *) str);
	memset (str, 0, len);
}


/* Fetch a secret using the secret-tool(1) program.  This gains access
 * to a secure backend, ultimately protected with a master/login password.
 * The returned value is a password string, which the caller must free().
 * The function returns NULL when it failed to retrieve a password.
 *
 * Several attributes are used, none of which is specific to this program.
 * The idea is that SIP passwords are generally useful, and should be set
 * in a single place.  It is up to the secret service to decide which
 * tools may access it, and which may not.
 *
 * app=sip-account
 * node=www or node=proxy to distinguish WWW-Auth from Proxy-Auth
 * mech=digest or other
 * realm=...taken-from-header...
 * addr=...set-to-sipid...
 */
char *sippassword (bool proxy, char *auth_mech, char *auth_realm, char *sipaddr) {
	//
	// While debugging, quit after a single attempt
	#ifdef DEBUG
	static bool once = true;
	if (once) {
		once = false;
	} else {
		return NULL;
	}
	#endif
	//
	// Form the command to query the password via secret-tool(1)
	char query [205];
	if (200 < snprintf (query, 202,
			"secret-tool lookup app sip-account node %s mech %s realm %s addr %s",
			proxy ? "proxy" : "www",
			auth_mech,
			auth_realm,
			sipaddr)) {
		fprintf (stderr, "Credential query command too long\n");
		return NULL;
	}
	//
	// Start the command via popen()
	FILE *cmd = popen (query, "r");
	bool ok = (cmd != NULL);
	//
	// Retrieve the output in a (large enough) buffer
	char pwd [205];
	ok = ok && (NULL != fgets (pwd, 202, cmd));
	pwd [202] = '\0';
	int pwdlen = strlen (pwd);
	ok = ok && (pwdlen > 6) && (pwdlen < 200);
	if (ok && (pwd [pwdlen-1] == '\n')) {
		pwdlen--;
	}
	if (cmd != NULL) {
		ok = (pclose (cmd) == 0) && ok;
	}
	char *rv = NULL;
	if (ok) {
		rv = mem_zalloc (pwdlen + 1, str_clear);
		ok = ok && (rv != NULL);
		if (ok) {
			memcpy (rv, pwd, pwdlen);
			rv [pwdlen] = '\0';
		}
		memset (pwd, 0, pwdlen + 1);
	} else {
		fprintf (stderr, "Failed to run the credential query: %s\n", query);
	}
	return rv;
}


/* Password callback for registration as a server.
 *
 * This derives the username from the localid and retrieves
 * the password from the backend store.  The values are set
 * in the output variables with str_dup().
 *
 * The return value is an error code, using 0 for success.
 */
int send_authcb (char **username, char **password, const char *realm, void *cbdata) {
fprintf (stderr, "Authentication callback for realm %s with cbdata %s\n", realm, (char *) cbdata);
	//
	// Parse the localid as a SIP address
	struct pl txt = { .p = cbdata, .l = strlen ((char *) cbdata) };
	struct sip_addr prs;
	int err = sip_addr_decode (&prs, &txt);
	//
	// Clone the user
	char *usr = mem_zalloc (prs.uri.user.l + 1, str_clear);
	if (usr == NULL) {
fprintf (stderr, "Authentication failed to allocate memory\n");
		return ENOMEM;
	}
	memcpy (usr, prs.uri.user.p, prs.uri.user.l);
	usr [prs.uri.user.l] = '\0';
	//
	// Clone the user@domain onto the stack
	int usrdomlen = (prs.uri.host.p - prs.uri.user.p) + prs.uri.host.l;
	char usrdom [ (prs.uri.host.p - prs.uri.user.p) + prs.uri.host.l ];
	memcpy (usrdom, prs.uri.user.p, usrdomlen);
	usrdom [usrdomlen] = '\0';
	//
	// Obtain the domain name for the user
	char dom [prs.uri.host.l + 1];
	memcpy (dom, prs.uri.host.p, prs.uri.host.l);
	dom [prs.uri.host.l] = '\0';
	//
	// Retrieve the password from the runtime environment
	#ifdef DEBUG
	fprintf (stderr, "Looking up digest password for %s in realm %s\n", usrdom, dom);
	#endif
	char *pwd = sippassword (false, "digest", dom, usrdom);
	if (pwd == NULL) {
fprintf (stderr, "Authentication of %s failed to locate a password\n", usr);
		mem_deref (usr);
		return EPERM;
	}
	#ifdef DEBUG
	fprintf (stderr, "Continuing with user \"%s\" and password #%d\n", usr, (int) strlen (pwd));
	#endif
	//
	// Setup the return values and return success
	*username = usr;
	*password = pwd;
fprintf (stderr, "Authentication returns successfully for user %s\n", usr);
	return 0;
}


/* Parse a [v6addr]:port pair into a struct sa.  Have a default.
 * Return 0 on OK, otherwise clear out_sa and return an errno value.
 */
int parse_sa (struct sa *out_sa, const char *in_str, const char *default_str) {
	int err = 0;
	if (in_str == NULL) {
		if (default_str == NULL) {
			fprintf (stderr, "Socket address has no default value\n");
			err = EINVAL;
			goto ret_error;
		}
		in_str = default_str;
	}
	struct pl addrport_str = { .p = in_str, .l = strlen (in_str) };
	struct pl port_str;
	struct pl host_str;
	err = uri_decode_hostport (&addrport_str, &host_str, &port_str);
	if (err != 0) {
		fprintf (stderr, "Socket address \"%s\" did not decode as host:port\n", in_str);
		goto ret_error;
	}
	uint32_t port_num = pl_u32 (&port_str);
	if ((port_num == 0) || (port_num > 65535)) {
		fprintf (stderr, "Socket address \"%s\" has an invalid port number\n", in_str);
		err = EINVAL;
		goto ret_error;
	}
	sa_init (out_sa, AF_INET6);
	err = sa_set (out_sa, &host_str, (uint16_t) port_num);
	if (err != 0) {
		fprintf (stderr, "Socket address \"%s\" did not work for an IPv6 socket address\n", in_str);
		goto ret_error;
	}
	return 0;
ret_error:
	memset (out_sa, 0, sizeof (*out_sa));
	return err;
}


/* Receive an A-law audio frame over RTP.  This also uses global variables.
 * TODO: Exploit the "arg" instead of global data.
 * TODO: Handle resending and order
 */
FILE *r_alawfile_opt = NULL;
struct sublime *subliminal_receiver = NULL;
//
void recv_pcma (const struct sa *src, const struct rtp_header *hdr, struct mbuf *mb, void *arg) {
	//
	// Pass the data through Sublime
#if 0
fprintf (stderr, "Received %d bytes of RTP data\n", mbuf_get_left (mb));
#endif
	sublime_recvmedia (subliminal_receiver, mbuf_buf (mb), mbuf_get_left (mb));
	//
	// Write the data to the A-law output file
	if (r_alawfile_opt != NULL) {
		fwrite (mbuf_buf (mb), 1, mbuf_get_left (mb), r_alawfile_opt);
#if 0
fprintf (stderr, "Wrote RTP data to file\n");
#else
putchar ('*'); fflush (stdout);
#endif
	}
}


/* Timeout trigger while receiving, after 5 minutes.
 * Since we are not really SIP participants, send no SIP message.
 */
void recv_timeout (void *_arg) {
	re_cancel ();
}


/* Timeout trigger while sending, after 5 minutes.
 * Since we are not really SIP participants, send no SIP message.
 */
void send_timeout (void *_arg) {
	re_cancel ();
}


/* Signal trigger from re_main().
 *
 * Possibly interesting options:
 *  - SIGHUP  to unregister, continue any RTP processes and exit when done
 *  - SIGSTOP to unregister so as to stop  responding to INVITE messages
 *  - SIGCONT to reregister so as to start responding to INVITE messages
 *
 * It would then be useful to send SIGHUP (with killall) to other instances
 * of the same side (sending or receiving) around the initial registration.
 */
void re_signal_handler (int sig) {
	switch (sig) {
	case SIGINT:
	case SIGQUIT:
	case SIGKILL:
	case SIGTERM:
		/* Quit the program */
		fprintf (stderr, "\n***signal %d***\n", sig);
		re_cancel ();
		break;
	default:
		/* Ignore this signal */
		break;
	}
}


/* Swapping callback for the HDLC input file, used when sending
 * runs into the end of a file, wondering if there is more to do.
 */
void swapcb_hdlc (void *data) {
	//
	// Translate the opaque data pointer to an HDLC argument sequence
	struct argit *argit_hdlc = (struct argit *) data;
	//
	// Grab the next HDLC file argument
	char *nextfn = nextarg (argit_hdlc);
	FILE *nextfh = NULL;
	if (nextfn != NULL) {
		nextfh = fopen (nextfn, "r");
	}
	FILE *prevfh = sublime_hdlcasm_swap_infile (nextfh);
	if (prevfh != NULL) {
		fclose (prevfh);
	}
}


/* Prepare for sending one payload sequence.  This is called before the timer
 * is setup, and then updated after every timer interrupt to send_chunk().
 */
void prep_chunk (struct send_state *ss) {
	//
	// Make the next contents to send from from the payload
	if (ss->idx_alaw + ALAW_SAMPLES <= ss_len_alaw) {
		//
		// Send a contiguous block of data as audio contents
		memcpy (ss->payload, ss_data_alaw + ss->idx_alaw, ALAW_SAMPLES);
		ss->idx_alaw += ALAW_SAMPLES;
		if (ss->idx_alaw >= ss_len_alaw) {
			ss->idx_alaw = 0;
		}
	} else {
		//
		// Send a split block of data as audio contents
		memcpy (ss->payload, ss_data_alaw + ss->idx_alaw, ss_len_alaw - ss->idx_alaw);
		ss->idx_alaw = ALAW_SAMPLES - (ss_len_alaw - ss->idx_alaw);
		memcpy (ss->payload, ss_data_alaw, ss->idx_alaw);
	}
	//
	// Inject subliminal messaging into the audio contents
	sublime_sendmedia (ss->subliminal_sender, ss->payload, ALAW_SAMPLES);
}


/* Send one sample set.  This is driven by an interval timer and must come from
 * global s_xxx variables (due to POSIX definitions).
 */
void send_chunk (void *cbdata) {
	//
	// Callback data is a struct send_state
	struct send_state *ss = (struct send_state *) cbdata;
	//
	// Give an optical hint of sending an RTP frame
	#ifdef DEBUG
	putc ('*', stdout);
	fflush (stdout);
	#endif
	//
	// Continue the interval timer without stacking timing errors
	tmr_continue (&ss->interval, 20, send_chunk, cbdata);
	//
	// Save the current thread state
	int errno_copy = errno;
	//
	// Send the memory buffer with next contents after the next RTP header
	ss->outbuf->pos = RTP_HEADER_SIZE;
	ss->outbuf->end = RTP_HEADER_SIZE + ALAW_SAMPLES;
	rtp_send (ss->rtpsox, &ss->remote_sa, false, false, 8, ss->timestamp, ss->realtime, ss->outbuf);
	//
	// Prepare contents for the next RTP frame
	prep_chunk (ss);
	//
	// Update RTP settings for the next RTP frame
	ss->timestamp += ALAW_SAMPLES;
	ss->realtime_mod5++;
	ss->realtime += (1L<<32) / ALAW_MSEC;
	if (ss->realtime_mod5 >= 5) {
		/* This implements the 0.8 fraction from 2^32 / 20 */
		ss->realtime--;
		ss->realtime_mod5 = 0;
	}
	//
	// Restore the original thread state
	errno = errno_copy;
}


/* Start a sending process for RTP streaming.
 *
 * Returns an enlisted send_state structure, or NULL on failure.
 */
struct send_state *send_start (struct sip *stack, struct argit *argit_hdlc_copy) {
	//
	// Allocate a self-destructing send_state structure
	void send_destroy (void *data);
	struct send_state *ss = (struct send_state *)
			mem_zalloc (sizeof (struct send_state), send_destroy);
	if (ss == NULL) {
		return NULL;
	}
	//
	// Allocate a memory buffer for RTP packets
	ss->outbuf = mbuf_alloc (RTP_HEADER_SIZE + ALAW_SAMPLES);
	if (ss->outbuf == NULL) {
		mem_deref (ss);
		return NULL;
	}
	ss->payload = mbuf_buf (ss->outbuf);
	ss->payload += RTP_HEADER_SIZE;
	//
	// Set the SIP stack
	ss->stack = stack;
	//
	// Prepare the unidirectional Sublime context
	ss->subliminal_sender = sublime_open (SUBLIME_SEND, "audio", "PCMA");
	if (ss->subliminal_sender == NULL) {
		perror ("Subliminal sender did not open");
	}
	sublime_detach (ss->subliminal_sender, true);
	//
	// Setup the input file swapper
	sublime_hdlcasm_setup_infile_swapper (argit_hdlc_copy, swapcb_hdlc);
	//
	// Determine the real time value to use in RTP frames
	struct timeval tv;
	gettimeofday (&tv, NULL);
	ss->realtime = tv.tv_sec;
	ss->realtime <<= 32;
	ss->realtime += lround (tv.tv_usec * 4294.9672960000 - 0.5);
	//
	// Setup the timers for RTP and the 5-minute timeout
	tmr_init (&ss->interval);
	tmr_init (&ss->early5min);
	memset (&ss->early5min, 0, sizeof (ss->early5min));
	//
	// Return the send-state
	return ss;
}


/* Stop a sending process by cancelling RTP, possibly abruptly.
 * Cleanup any resources that are still being used.
 *
 * Returns false to allow list_apply() to run to completion.
 */
bool send_stop (struct le *le, void *_cbdata) {
	struct send_state *ss = (struct send_state *) le->data;
	//
	// Stop the 5-minute timeout
	if (tmr_isrunning (&ss->early5min)) {
		tmr_cancel (&ss->early5min);
	}
	//
	// Stop the RTP chunk interval timer
	if (tmr_isrunning (&ss->interval)) {
		tmr_cancel (&ss->interval);
	}
	//
	// Cleanup the RTP socket
	if (ss->rtpsox != NULL) {
		mem_deref (ss->rtpsox);
		ss->rtpsox = NULL;
	}
	//
	// Cleanup the RTP output buffer
	if (ss->outbuf != NULL) {
		mem_deref (ss->outbuf);
		ss->outbuf = NULL;
	}
	//
	// Cleanup the Subliminal Messaging handler
	sublime_close (ss->subliminal_sender);
	ss->subliminal_sender = NULL;
	//
	// Allow list_apply() to run to completion (used during cleanup)
	return false;
}


/* Destroy memory that holds the send_state.  Includes cleanup.
 */
void send_destroy (void *data) {
	struct send_state *ss = (struct send_state *) data;
	//
	// Break off the RTP sending process
	send_stop (&ss->le, NULL);
	//
	// Take the process out of the list
	list_unlink (&ss->le);
}


/* See if two SIP messages match in terms of identities.
 * This is a list_apply_h, usable in list_apply().
 */
bool sip_msg_match (struct le *le, void *arg) {
	//
	// Reformulate the arguments
	struct send_state *ss  = (struct send_state *) le ;
	struct sip_msg    *msg = (struct sip_msg    *) arg;
	//
	// Return false if identities don't match
	if (pl_strcmp (&msg->callid, ss->call_id) != 0) {
		return false;
	}
	if (pl_strcmp (&msg->from.tag, ss->from_tag) != 0) {
		return false;
	}
	//
	// We found a match!
	return true;
}


/* Sender's callback for SIP requests.
 *
 * Upon INVITE, start sending early media for ~5 minutes.
 * Upon CANCEL, stop  sending early media.
 * Upon BYE,    stop  sending early media.
 */
struct send_request_data {
	struct sip *stack;
	struct argit argit_hdlc_copy;
};
//
bool send_request (const struct sip_msg *req, void *cbdata) {
	//
	// Callback data is a dedicated structure
	struct send_request_data *this = (struct send_request_data *) cbdata;
	//
	// Sanity checking for the request
	if (pl_strcmp (&req->ver, "SIP/2.0") != 0) {
		fprintf (stderr, "Wild version %.*s will be ignored\n",
				(int) req->ver.l, req->ver.p);
		return true;
	}
	if (!req->req) {
		fprintf (stderr, "Spurious response will be ignored\n");
		return true;
	}
	bool ok = true;
	//
	// Determine how to respond to the request
	#ifdef DEBUG
	fprintf (stderr, "%.*s request arrived\n", (int) req->met.l, req->met.p);
	#endif
	bool req_rtp_start = (0 == pl_strcmp (&req->met, "INVITE"));
	bool req_rtp_stop  = (0 == pl_strcmp (&req->met, "CANCEL"))
	                  || (0 == pl_strcmp (&req->met, "BYE"   ));
	//
	// Quietly accept anything else, including ACK, by ignoring it
	if (!(req_rtp_start || req_rtp_stop)) {
		return true;
	}
	//
	// Start sending RTP if so requested
	if (req_rtp_start) {
		struct sa ss_local_sa;
		struct sdp_session *sess = NULL;
		//
		// Set endpoint addresses
		ok = ok && (0 == net_if_getaddr (NULL, AF_INET6, &ss_local_sa));
		//
		// Allocate an RTP socket and bind it to a port
		struct rtp_sock *rtpsox = NULL;
		ok = ok && (0 == rtp_listen (&rtpsox,
					IPPROTO_UDP,
					&ss_local_sa,   /* address to bind to */
					49152, 65534, /* ephemeral port range */
					false,                     /* no RTCP */
					recv_pcma,         /* receive handler */
					NULL,             /* no RTCP callback */
					NULL       /* noarg for RTCP callback */
				));
		//
		// Get the local RTP port
		ok = ok && (0 == udp_local_get (rtp_sock (rtpsox), &ss_local_sa));
		uint16_t ss_local_port = sa_port (&ss_local_sa);
		ok = ok && (ss_local_port != 0);
		//
		// Parse the SDP body to the INVITE request
		ok = ok && msg_ctype_cmp (&req->ctyp, "application", "sdp");
		ok = ok && (0 == sdp_session_alloc (&sess, &ss_local_sa));
		struct sdp_media  *sdm = NULL;
		struct sdp_format *sdf = NULL;
		ok = ok && (0 == sdp_media_add (&sdm, sess, "audio", ss_local_port, "RTP/AVP"));
		if (ok) {
			sdp_media_set_ldir (sdm, SDP_SENDONLY);   /* void retval */
		}
		ok = ok && (0 == sdp_format_add (&sdf, sdm, false, "8", "PCMA", 8000, 0, NULL, NULL, NULL, false, NULL));
		ok = ok && (0 == sdp_decode (sess, req->mb, true));
		#ifdef DEBUG
		sdp_session_debug (&err4re, sess);
		#endif
		const struct sa *msa = ok
					? sdp_media_raddr (sdm)
					: NULL;
		ok = ok && (msa != NULL);
		struct send_state *ss = ok
				? send_start (this->stack, &this->argit_hdlc_copy)
				: NULL;
		ok = ok && (ss != NULL);
		//
		// Enlist the send_state for future closedown
		ok = ok && (0 == pl_strdup (&ss->call_id,  &req->callid  ));
		ok = ok && (0 == pl_strdup (&ss->from_tag, &req->from.tag));
		//
		// Decide whether to respond to the INVITE
		if (ok) {
			//
			// Copy data prepared above
			ss->rtpsox = rtpsox;
			rtpsox = NULL;
			memcpy (&ss->remote_sa, msa, sizeof (ss->remote_sa));
			//
			// Prepare the first PCMA chunk of data
			prep_chunk (ss);
			//
			// Invitations seems correct.  Send the response.
			struct mbuf *sdpbuf = NULL;
			#ifdef DEBUG
			sdp_session_debug (&err4re, sess);
			#endif
			ok = ok && (0 == sdp_encode (&sdpbuf, sess, false));
			#ifdef DEBUG
			fprintf (stderr, "<sdp>\n%.*s\n</sdp>\n", (int) (sdpbuf->size - sdpbuf->pos), sdpbuf->buf + sdpbuf->pos);
			#endif
			//
			// Reply with our "183 Session Progress" variation
			sip_replyf (this->stack, req,
					183, "Session Progress with Subliminal Messaging",
					"Content-Type: application/sdp\r\n"
					"Content-Length: %d\r\n"
					"\r\n"
					"%b",
					sdpbuf->end,
					sdpbuf->buf, sdpbuf->end);
			//
			// Start the RTP interval timer
			tmr_start (&ss->interval, 20, send_chunk, ss);
			//
			// Enlist the send_state for future cleanup
			list_append (&ss_senders, &ss->le, ss);
		} else {
			//
			// Failure -- cleanup resources allocated above
			if (rtpsox != NULL) {
				mem_deref (rtpsox);		/* Includes UDP socket close */
				rtpsox = NULL;
			}
			//
			// At this point, we should never find any send_state
			assert (ss == NULL);
		}
		//
		// Cleanup, regardless of success or failure
		if (sess != NULL) {
			mem_deref (sess);
			sess = NULL;
		}
	}
	//
	// Stop sending RTP if so requested
	// We'll ignore it if a leg is not found, as we're the quiet one
	if (req_rtp_stop) {
		//
		// Lookup the send_state for this message
		struct send_state *ss = (struct send_state *)
				list_apply (&ss_senders, true,
					sip_msg_match, (void *) req);
		//
		// Break the sending process
		if (ss != NULL) {
			send_stop (&ss->le, NULL);
			//TODO:SIGABRT// free (&ss->call_id );
			//TODO:SIGABRT// free (&ss->from_tag);
		}
		//
		// Respond to CANCEL regardless if we were transmitting RTP
		sip_reply (this->stack, req,
				200, "Termination of Subliminal Messaging");
	}
	//
	// We do not need to send ACK, as libre will do that
	;
	//
	// Accept anything that could go wrong; this is a best-effort operation
	return true;
}


/* Registration callback for sending.
 */
void send_regcb (int err, const struct sip_msg *resp, void *cbdata) {
	//
	// If we an error got reported to us, we should fail
	if (err != 0) {
		errno = err;
		perror ("REGISTER attempt failed");
		re_cancel ();
		return;
	}
	//
	// If we received an error response, we should fail
	if (resp->scode != 200) {
		fprintf (stderr, "REGISTER attempt returned %03d %.*s\n",
				resp->scode, (int) resp->reason.l, resp->reason.p);
		re_cancel ();
		return;
	}
	//
	// When debugging, report the good news
	#ifdef DEBUG
	fprintf (stderr, "REGISTER attempt succeeded\n");
	#endif
}


/* Main program for sublime-early-send.
 *
 * Register with an account to see INVITE, CANCEL and BYE messages.
 * Send RTP with Early Media, with timeout after about 5 minutes.
 *
 * TODO: A future extension might add useful behaviour once connected.
 *       A re-INVITE might pass the call but that would make it as
 *       annoying as a call center for the callee, and not acceptable.
 *       Also, this is supposed to be a simple demonstration utility.
 */
int main_send (struct sip *stack, char *localid, char *remotid, char *proxy, struct argit *argit_al, struct argit *argit_hdlc) {
	//
	// Initialise
	bool ok = true;
	list_init (&ss_senders);
	//
	// Start listening for SIP messages: INVITE, CANCEL, BYE
	struct sip_lsnr *lsnr = NULL;
	struct send_request_data srd;
	srd.stack = stack;
	memcpy ((void *) &srd.argit_hdlc_copy, argit_hdlc, sizeof (srd.argit_hdlc_copy));
	assert (0 == sip_listen (&lsnr, stack, true, send_request, &srd));
	//
	// The to_uri for registration is the localid with "sip:" prefix
	//TODO//BLUNT_USE_OF_ARG//
	const char *to_uri = localid;
	//
	// The registration uri is "sip:" plus the domain in remotid
	//TODO//BLUNT_USE_OF_ARG//
	const char *reg_uri = remotid;
	//
	// The contact username comes from the to_uri
	char contact [strlen (to_uri) + 1];
	int ofs = 0;
	if (strncmp (to_uri, "sip:", 4) == 0) {
		strcpy (contact, to_uri + 4);
	} else if (strncmp (to_uri, "sips:", 5) == 0) {
		strcpy (contact, to_uri + 5);
	} else {
		strcpy (contact, to_uri + 0);
	}
	* strchrnul (contact, '@') = '\0';
	//
	// The route is an array of length 0 or 1
	const char *route[1] = { proxy };
	const int   routelen = (proxy != NULL) ? 1 : 0;
	//
	// Construct the authentication callback data
#if 0
	struct mysipcreds *send_creds = ok
				? mem_zalloc (sizeof (struct mysipcreds), NULL /*TODO?OK?*/)
				: NULL;
#else
	void *send_creds = localid;
#endif
	ok = ok && (send_creds != NULL);
	//
	// Start registration as a client of the local domain
	struct sipreg *registration = NULL;
	assert (0 == sipreg_register (&registration, stack,
				reg_uri,        /* reg_uri, sip:regdomain.nep */
				to_uri,    /* to_uri, sip:meuser@mydomain.nep */
				NULL,      /* from_name, leave to real phones */
				to_uri,           /* from_uri, same as to_uri */
				TIMEOUT_60MIN,    /* for how long to register */
				contact,     /* contact username, from to_uri */
				route,           /* proxy as a routevectorray */
				routelen,        /* proxy as a routevectorlen */
				rand_u64 (),       /* regid, picked at random */
				send_authcb,       /* authentication callback */
				send_creds,         /* data for auth callback */
				false,          /* whether to deref authstate */
				send_regcb,      /* response handler callback */
				NULL,            /* response handler argument */
				NULL,      /* Contact-header params, not used */
				NULL           /* Extra SIP headers, not used */
				));
	//
	// Start Subliminal Messaging
	sublime_init ();
	//
	// Send data until timeout or SIP termination
	if (ok) {
		re_main (re_signal_handler);
	}
	//
	// Break off any remnants in ss_senders
	list_apply (&ss_senders, true, send_stop, NULL);
	list_clear (&ss_senders);
	//
	// Cleanup
	if (sipreg_registered (registration)) {
		//TODO// Unregistering does not seem to work
		sipreg_unregister (registration);
	}
	if (registration != NULL) {
		mem_deref (registration);
		registration = NULL;
	}
	//
	// Stop listening
	if (lsnr != NULL) {
		mem_deref (lsnr);
		lsnr = NULL;
	}
#if 0
	//
	// Cleanup any state stored in the sender's credentials
	if (send_creds != NULL) {
		mem_deref (send_creds);
		send_creds = NULL;
	}
#endif
	//
	// Finalisation
	sublime_fini ();
	//
	// Return success or failure
	return ok ? 0 : 1;
}


/* Receive a response ot the INVITE, and learn from its status code.
 */
struct client_resp_data {
	int status;
	struct sip *stack;
	struct sip_request *request;
	struct sip_auth *auth;
	uint32_t cseq_num;
	uint64_t from_tag;
	uint64_t call_id;
	struct pl to_tag;
	char *method;
	char *localid;
	char *remotid;
	char *call_dom;
	struct uri *opt_route;
	struct mbuf *sdpbuf;
};
//
void recv_bye_response (int err, const struct sip_msg *resp, void *arg) {
	re_cancel ();
}
//
void recv_invite_response (int err, const struct sip_msg *resp, void *arg) {
	//
	// Map the argument into a client_resp_data structure
	struct client_resp_data *crd = (struct client_resp_data *) arg;
	//
	// Is this an error being reported back to us?
	if (err != 0) {
		errno = err;
		perror ("SIP message caused an error");
		return;
	}
fprintf (stderr, "Response %03d received, possibly replacing %03d\n", resp->scode, crd->status);
	//
	// Was there a To: tag in the response that we were waiting for?
	if ((resp->scode > 100) && (resp->scode < 300) && (!pl_isset (&crd->to_tag)) && (pl_isset (&resp->to.tag))) {
		pl_dup (&crd->to_tag, &resp->to.tag);
fprintf (stderr, "Response set the To: tag=\"%.*s\"\n", (int) crd->to_tag.l, crd->to_tag.p);
	}
	//
	// Did we receive a 401 Unauthorized 407 Proxy Authentication Required?
	if ((resp->scode == 401) || (resp->scode == 407)) {
		//
		// Need to resend the request with authentication
		err = sip_auth_authenticate (crd->auth, resp);
		if (err != 0) {
			errno = err;
			perror ("Authentication failure");
			return;
		}
		//
		// Cancel the old request
		sip_request_cancel (crd->request);
		//
		// Test CSeq against authcseq, to make it step properly
		if (resp->cseq.num != crd->cseq_num) {
			errno = EBADR;
			perror ("Incorrect CSeq numbers");
			return;
		}
		crd->cseq_num++;
		//
		// Produce a follow-up request
		err = sip_requestf (
				&crd->request,          /* SIP request object */
				crd->stack,                      /* SIP stack */
				true,          /* Stateful client transaction */
				crd->method,             /* SIP method string */
				crd->remotid,           /* Request URI string */
				crd->opt_route,    /* Optional next-hop route */
				crd->auth,        /* SIP authentication state */
				NULL,                         /* Send handler */
				recv_invite_response,     /* Response handler */
				crd,                      /* Handler argument */
					   /* Formatted SIP headers and body: */
				"From: Subliminal Receiver <%s>; tag=%016llx\r\n"
				"To: Subliminal Sender <%s>\r\n"
				"Call-ID: <%016llx@%s>\r\n"
				"CSeq: %d %s\r\n"
				"Content-type: application/sdp\r\n"
				"Content-length: %d\r\n"
				"\r\n"
				"%b",
					crd->localid, crd->from_tag,
					crd->remotid,
					crd->call_id, crd->call_dom,
					crd->cseq_num, crd->method,
					crd->sdpbuf->end,
					crd->sdpbuf->buf, crd->sdpbuf->end);
	//
	// Otherwise take note of a risen status code
	} else if (resp->scode > crd->status) {
		crd->status = resp->scode;
	}
}


/* Main program for sublime-early-recv.
 *
 * Invite a remote account through INVITE, CANCEL and BYE messages
 * Receive RTP with Early Media, for up to about 5 minutes.
 *
 * TODO: A future extension might register to a trunk and process
 *       many users.  Not sure if this would be really helpful, as
 *       phone-by-phone listening is more controllable to users.
 *       Also, this is supposed to be a simple demonstration utility.
 */
int main_recv (struct sip *stack, char *localid, char *remotid, char *proxy, struct argit *argit_al, struct argit *argit_hdlc) {
	//
	// Initialise
	bool ok = true;
	//
	// Start Subliminal Messaging
	sublime_init ();
	//
	// Prepare the unidirectional Sublime context
	subliminal_receiver = sublime_open (SUBLIME_RECV, "audio", "PCMA");
	if (subliminal_receiver == NULL) {
		perror ("Subliminal receiver did not open");
	}
	sublime_detach (subliminal_receiver, true);
	//
	// There may be an .al file to record A-law data
	char *alawfn_opt = nextarg (argit_al);
	if (alawfn_opt != NULL) {
		r_alawfile_opt = fopen (alawfn_opt, "w");
		if (r_alawfile_opt == NULL) {
			fprintf (stderr, "Failed to open \"%s\" for A-law output\n", alawfn_opt);
			ok = false;
		}
	}
	//
	// There must be an .hdlc file to record HDLC data
	char *hdlcfn = nextarg (argit_hdlc);
	FILE *r_hdlcfile = fopen (hdlcfn, "w");
	if (r_hdlcfile == NULL) {
		fprintf (stderr, "Failed to open \"%s\" for HDLC output\n", hdlcfn);
		ok = false;
	}
	sublime_hdlcasm_swap_outfile (r_hdlcfile);
	//
	// Allocate a memory buffer for RTP packets
	struct mbuf *r_mbuf = mbuf_alloc (RTP_HEADER_SIZE + ALAW_SAMPLES);
	ok = ok && (r_mbuf != NULL);
	//
	// Setup the local endpoint as an [unbound] RTP socket
	struct sa r_local_sa;
	struct rtp_sock *r_rtpsox = NULL;
	ok = ok && (0 == net_if_getaddr (NULL, AF_INET6, &r_local_sa));
	ok = ok && (0 == rtp_listen (&r_rtpsox, IPPROTO_UDP, &r_local_sa, 8998, 8999, false, recv_pcma, NULL, NULL /*arg*/));
	//
	// Get the local RTP port
	ok = ok && (0 == udp_local_get (rtp_sock (r_rtpsox), &r_local_sa));
	uint16_t r_local_port = sa_port (&r_local_sa);
	ok = ok && (r_local_port != 0);
	//
	// Have a client response data object
	struct client_resp_data *crd = mem_zalloc (sizeof (struct client_resp_data), NULL);
	if (crd == NULL) {
		perror ("Failed to allocate client response data");
		return ENOMEM;;
	}
	crd->stack = stack;
	crd->localid = localid;
	crd->remotid = remotid;
	//
	// Prepare method, CSeq-number, From-tag, Call-ID with domain
	crd->method = "INVITE";
	crd->cseq_num = 1;
	crd->from_tag = rand_u64 ();
	crd->call_id = rand_u64 ();
	crd->call_dom = strrchr (localid, '@');
	crd->call_dom = (crd->call_dom == NULL) ? localid : 1 + crd->call_dom;
	//
	// Take note of the proxy setting, if nay
	struct uri route_uri;
	if (proxy != NULL) {
		struct pl route_uripl;
		pl_set_str (&route_uripl, proxy);
		if (0 == uri_decode (&route_uri, &route_uripl)) {
			crd->opt_route = &route_uri;
		} else {
			perror ("Proxy is not a URI");
		}
	}
	//
	// Prepare the SDP receiving side
	struct sdp_session *sess = NULL;
	struct sdp_media *sdm = NULL;
	struct sdp_format *sdf = NULL;
	ok = ok && (0 == sdp_session_alloc (&sess, &r_local_sa));
	ok = ok && (0 == sdp_media_add (&sdm, sess, "audio", r_local_port, "RTP/AVP"));
	if (ok) {
		sdp_media_set_ldir (sdm, SDP_RECVONLY);  /* void retval */
	}
	ok = ok && (0 == sdp_format_add (&sdf, sdm, false, "8", "PCMA", 8000, 0, NULL, NULL, NULL, false, NULL));
	ok = ok && (0 == sdp_encode (&crd->sdpbuf, sess, true));
	//
	// Reset the call status
	// update to the SIP status code (180..183, 200, ...)
	crd->status = 0;
	//
	// Build the authentication object
	//TODO// Assuming that send_authcb will work here too, with localid as data
	ok = ok && (0 == sip_auth_alloc (&crd->auth, send_authcb, localid, false));
fprintf (stderr, "Authentication structure 0x%p with ok=%d\n", crd->auth, ok);
	//
	// Send an INVITE to the remote
	crd->request = NULL;
	ok = ok && (0 == sip_requestf (
				&crd->request,          /* SIP request object */
				stack,                           /* SIP stack */
				true,          /* Stateful client transaction */
				crd->method,             /* SIP method string */
				remotid,                /* Request URI string */
				crd->opt_route,    /* Optional next-hop route */
				NULL,             /* SIP authentication state */
				NULL,                         /* Send handler */
				recv_invite_response,     /* Response handler */
				crd,                      /* Handler argument */
					   /* Formatted SIP headers and body: */
				"From: Subliminal Receiver <%s>; tag=%016llx\r\n"
				"To: Subliminal Sender <%s>\r\n"
				"Call-ID: <%016llx@%s>\r\n"
				"CSeq: %d %s\r\n"
				"Content-type: application/sdp\r\n"
				"Content-length: %d\r\n"
				"\r\n"
				"%b",
					crd->localid, crd->from_tag,
					crd->remotid,
					crd->call_id, crd->call_dom,
					crd->cseq_num, crd->method,
					crd->sdpbuf->end,
					crd->sdpbuf->buf, crd->sdpbuf->end));
	bool sent_invite = ok;
fprintf (stderr, "sent_invite = %d, cseq_num = %d\n", sent_invite, crd->cseq_num);
	//
	// Setup a timer for about 5 minutes.
	struct tmr early5min;
	memset (&early5min, 0, sizeof (early5min));
	//
	// Receive data until timeout or SIP termination
	if (ok) {
		tmr_start (&early5min, TIMEOUT_5MIN, recv_timeout, NULL);
		re_main (re_signal_handler);
		tmr_cancel (&early5min);
	}
	//
	// Send BYE or CANCEL to the remote (if necessary)
	// Note: CANCEL does not work reliably on the test case.
	//       That is awkward; what if the INVITE is branched to this UAS?
	mem_deref (crd->auth);
	crd->auth = NULL;
	crd->status = 0;
	//UNRELIABLE// crd->method = (crd->status >= 200) ? "BYE" : "CANCEL";
	crd->method = "BYE";
	struct sip_request *req_cancel = NULL;
	crd->cseq_num++;
fprintf (stderr, "Sending %s iff %d with CSeq %d\n", crd->method, sent_invite, crd->cseq_num);
	if (sent_invite) {
		int err = sip_requestf (
				&req_cancel,            /* SIP request object */
				stack,                           /* SIP stack */
				true,          /* Stateful client transaction */
				crd->method,             /* SIP method string */
				remotid,                /* Request URI string */
				crd->opt_route,    /* Optional next-hop route */
				NULL,             /* SIP authentication state */
				NULL,                         /* Send handler */
				recv_bye_response,        /* Response handler */
				crd,                      /* Handler argument */
					   /* Formatted SIP headers and body: */
				"From: Subliminal Receiver <%s>; tag=%016llx\r\n"
				"To: Subliminal Sender <%s>; tag=%b\r\n"
				"Call-ID: <%016llx@%s>\r\n"
				"CSeq: %d %s\r\n"
				"Content-type: application/sdp\r\n"
				"Content-length: 0\r\n"
				"\r\n",
				crd->localid, crd->from_tag,
				crd->remotid, crd->to_tag.p, crd->to_tag.l,
				crd->call_id, crd->call_dom,
				crd->cseq_num, crd->method);
fprintf (stderr, "After sending %s, err = %d\n", crd->method, err);
		ok = ok && (err == 0);
	}
	//
	// Receive data until timeout or SIP termination
	if (ok) {
		tmr_start (&early5min, TIMEOUT_5MIN, recv_timeout, NULL);
		re_main (re_signal_handler);
		tmr_cancel (&early5min);
	}
	//
	// Close the output files
	r_hdlcfile = sublime_hdlcasm_swap_outfile (NULL);
	if (r_hdlcfile != NULL) {
		fclose (r_hdlcfile);
		r_hdlcfile = NULL;
	}
	if (r_alawfile_opt != NULL) {
		fclose (r_alawfile_opt);
		r_alawfile_opt = NULL;
	}
	//
	// Cleanup
	if (r_rtpsox != NULL) {
		mem_deref (r_rtpsox);		/* Includes UDP socket close */
		r_rtpsox = NULL;
	}
	if (r_mbuf != NULL) {
		mem_deref (r_mbuf);
		r_mbuf = NULL;
	}
	sublime_close (subliminal_receiver);
	subliminal_receiver = NULL;
	sublime_fini ();
	//
	// Return success or failure
	return ok ? 0 : 1;
}


/* Main function, argument parsing and error reporting;
 * then calling either main_send() or main_recv();
 * use argit structures for separate .hdlc and .al iteration.
 */
int main (int argc, char *argv []) {
	//
	// Recognise the command
	char *program = strrchr (argv [0], '/');
	if (program != NULL) {
		program++;
	} else {
		program = argv [0];
	}
	bool recv_mode = (NULL != strstr (program, "-recv"));
	bool send_mode = !recv_mode;
	//
	// Process arguments
	int ch;
	bool help = false;
	bool error = false;
	char *localid = NULL;
	char *remotid = NULL;
	char *proxy = NULL;
	while (ch = getopt (argc, argv, "hl:r:p:"), ch != -1) {
		switch (ch) {
		case 'l':
			error = error || (localid != NULL);
			localid = optarg;
			break;
		case 'r':
			error = error || (remotid != NULL);
			remotid = optarg;
			break;
		case 'p':
			error = error || (proxy != NULL);
			proxy = optarg;
			break;
		case 'h':
			help = true;
			break;
		default:
			fprintf (stderr, "Unknown argument -%c\n", ch);
			error = true;
			break;
		}
	}
	//
	// Test if required arguments are present
	error = error || (localid == NULL) || ((remotid == NULL) && send_mode);
	//
	// Test the number of remaining arguments
	if (optind == argc) {
		error = true;
	}
	//
	// Count the occurrences of each extension and collect data
	//TODO// Try to use nextarg() instead!
	int num_hdlc = 0;
	int num_alaw = 0;
	for (int argi = optind; argi < argc; argi++) {
		int arglen = strlen (argv [argi]);
		struct stat st;
		if ((arglen > 5) && (strcmp (argv [argi] + arglen - 5, ".hdlc") == 0)) {
			if (recv_mode) {
				num_hdlc++;
			} else if (stat (argv [argi], &st) != 0) {
				perror ("Failed to open .hdlc file");
				error = true;
			} else if (st.st_size == 0) {
				fprintf (stderr, "Empty .hdlc file\n");
				error = true;
			} else {
				#ifdef DEBUG
				fprintf (stderr, "Got HDLC: %s\n", argv [argi]);
				#endif
				num_hdlc++;
			}
		} else if ((arglen > 3) && (strcmp (argv [argi] + arglen - 3, ".al") == 0)) {
			if (recv_mode) {
				num_alaw++;
			} else if (stat (argv [argi], &st) != 0) {
				perror ("Failed to open .al file");
				error = true;
			} else if (st.st_size == 0) {
				fprintf (stderr, "Empty .al file\n");
				error = true;
			} else {
				#ifdef DEBUG
				fprintf (stderr, "Got PCMA: %s\n", argv [argi]);
				#endif
				ss_data_alaw = realloc (ss_data_alaw, ss_len_alaw + st.st_size);
				if (ss_data_alaw == NULL) {
					perror ("No room for .al file");
					error = true;
				} else {
					FILE *f = fopen (argv [argi], "r");
					fread (ss_data_alaw + ss_len_alaw, 1, st.st_size, f);
					fclose (f);
				}
				ss_len_alaw += st.st_size;
				num_alaw++;
			}
		} else {
			fprintf (stderr, "Unrecognised argument: %s\n", argv [argi]);
			error = 1;
		}
	}
	//
	// Constrain arguments in -recv and -send differently
	if (recv_mode) {
fprintf (stderr, "num_hdlc = %d, num_alaw = %d\n", num_hdlc, num_alaw);
		if ((num_hdlc != 1) || (num_alaw > 1)) {
			fprintf (stderr, "%s expects one .hdlc and at most one .al file\n", program);
			error = true;
		}
	} else {
		if ((num_hdlc == 0) || (num_alaw == 0)) {
			fprintf (stderr, "%s expects at least one each of the .hdlc and .al files\n", program);
			error = true;
		}
	}
	//
	// Report instructions if needed, bail out if we must
	if (help || error) {
		fprintf (stderr, "Usage: %s [-h] -l localid %s-r remoteid%s [-p proxy] filename.al|filename.hdlc...\n", program, recv_mode?"[":"", recv_mode?"]":"");
		exit (error ? 1 : 0);
	}
	//
	// Setup the HDLC assembly file iterator
	struct argit argit_hdlc = {
		.argv = argv,
		.argc = argc,
		.arg0 = optind-1,
		.argi = optind-1,
		.ext  = ".hdlc",
		.extl = 5,
		.have = (num_hdlc > 0),
		.loop = send_mode,
	};
	//
	// Setup the A-law file iterator
	struct argit argit_al = {
		.argv = argv,
		.argc = argc,
		.arg0 = optind-1,
		.argi = optind-1,
		.ext  = ".al",
		.extl = 3,
		.have = (num_alaw > 0),
		.loop = (num_alaw > 0),
	};
	//
	// Initialise libre
	err4re.arg = (void *) stdout;
	err4re.arg = (void *) stderr;
	libre_init ();
	//
	// Setup a DNS client
	struct sa nsv [32];
	uint32_t nsc = 32;
	assert (0 == dns_srv_get (NULL, 0, nsv, &nsc));
	struct dnsc *dnsc = NULL;
	assert (0 == dnsc_alloc (&dnsc, NULL, nsv, nsc));
	//
	// Setup a SIP stack, but *not* a full SIP dialog because
	// we want to support multiple dialogs (on receiving)
	struct sip *stack = NULL;
	assert (0 == sip_alloc (&stack, dnsc, 32, 32, 32,
				"Subliminal Messaging in Early Media",
				NULL /* exith */, NULL));
	//
	// Add a local UDP transport (to exchange INVITE, CANCEL, BYE)
	struct sa mysa;
	assert (0 == net_if_getaddr (NULL, AF_INET6, &mysa));
	assert (0 == sip_transp_add (stack, SIP_TRANSP_UDP, &mysa));
	//
	// Split behaviour into -send and -recv modes
	// Return from these functions (we have non open resources).
	int rv = 1;
	if (send_mode) {
		rv = main_send (stack, localid, remotid, proxy, &argit_al, &argit_hdlc);
	} else {
		rv = main_recv (stack, localid, remotid, proxy, &argit_al, &argit_hdlc);
	}
	//
	// Cleanup resources
	sip_transp_flush (stack);
	mem_deref (dnsc);
	//
	// Close off and return the value in rv
	libre_close ();
	exit (rv);
}
