/* injext.c -- Inject HDLC into / Extract HDLC from -- A-law passing through
 *
 * These are filters that inject HDLC and extra HDLC while a flow of samples
 * passes throught.  By default, audio and PCMA is assumed, but this may be
 * overridden with -m <mediatype> and -c <codec> options.  Similar to the
 * src/sendrecv.c code for the sublime-{send,recv} utility.
 *
 * This program separates out the HDLC put/get logic and does not include the
 * default variants for those, but instead connects to the assembler as input
 * and to the disassembler as output interface, which in turn use a file.hdlc
 * named on the commandline to get HDLC input or put HDLC output.  (The normal
 * handling mechanisms for HDLC connect the codec to the backend after passing
 * through HDLC rituals.)
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>

#define SUBLIMINAL_HDLC_ASM_INTERNAL
#include <0cpm/subliminal.h>


int main (int argc, char *argv []) {
	char *progname = argv [0];
	//
	// Parse arguments
	int opt;
	char *mediatype = NULL;
	char *codec = NULL;
	char *keyfile = NULL;
	bool inject = false;
	bool extract = false;
	bool help = false;
	bool stxerr = false;
	char *progdash = strrchr (progname, '-');
	if (progdash != NULL) {
		if (strcmp (progdash, "-inject") == 0) {
			inject = true;
		}
		if (strcmp (progdash, "-extract") == 0) {
			extract = true;
		}
	}
	while (opt = getopt (argc, argv, "m:c:k:h"), opt != -1) {
		switch (opt) {
		case 'm':
			/* mediatype, other than audio */
			stxerr = stxerr || (mediatype != NULL);
			mediatype = optarg;
			break;
		case 'c':
			/* codec, other than PCMA */
			stxerr = stxerr || (codec != NULL);
			codec = optarg;
			break;
		case 'h':
			/* help wanted */
			help = true;
			break;
		case 'k':
			/* keyfile (including an algid) */
			stxerr = stxerr || (keyfile != NULL);
			keyfile = optarg;
			break;
		default:
			/* funny option */
			fprintf (stderr, "%s: Unknown option -%c\n", progname, opt);
			stxerr = true;
			break;
		}
	}
	if ((!inject) && (!extract)) {
		fprintf (stderr, "%s: You did not choose between -inject or -extract mode\n", progname);
		stxerr = true;
	}
	if (optind + 1 != argc) {
		fprintf (stderr, "Please supply a file to hold HDLC assembler (perhaps with .hdlc extension)\n");
		stxerr = true;
	}
	char *asmfile = argv [optind];
	if (stxerr || help) {
		fprintf (stderr, "Usage: %s [-m MEDIA] [-c CODEC] [-h] asmfile.hdlc\n", progname);
		exit (stxerr ? 1 : 0);
	}
	if (mediatype == NULL) {
		mediatype = "audio";
	}
	if (codec == NULL) {
		codec = "PCMA";
	}
	//
	// Marginally check the keyfile argument
	if (keyfile != NULL) {
		if (access (keyfile, R_OK | W_OK) != F_OK) {
			perror ("No access to the keyfile");
			exit (1);
		}
	}
	//
	// Initialise for Subliminal Messaging
	sublime_init ();
	struct sublime *recvfilt = NULL;
	struct sublime *sendfilt = NULL;
	struct sublime *actvfilt = NULL;
	struct sublime *pasvfilt = NULL;
	if (inject) {
		actvfilt = sendfilt = sublime_open (SUBLIME_SEND, mediatype, codec);
		sublime_detach (sendfilt, true);
	}
	if (extract) {
		actvfilt = recvfilt = sublime_open (SUBLIME_RECV, mediatype, codec);
		sublime_detach (recvfilt, true);
	}
	if (inject && extract) {
		sublime_attach (recvfilt, sendfilt);
	}
#ifdef DEBUG
	//
	// Debug mode: keyfile /dev/null triggers trivial debugging crypto
	if ((keyfile != NULL) && (0 == strcmp (keyfile, "/dev/null"))) {
		sublime_base_outercrypto_preset (actvfilt, 0xff, keyfile);
	}
#endif
	//
	// Preset the algorithm and keyfile
	if (keyfile != NULL) {
		sublime_base_outercrypto_preset (inject ? sendfilt : recvfilt, -666 /*TODO:DROPIT*/, keyfile);
	}
	//
	// Open the assembler file
	if (inject) {
		FILE *fin = fopen (asmfile, "r");
		if (fin == NULL) {
			fprintf (stderr, "Failed to open %s for assembler input\n", asmfile);
			exit (1);
		}
		sublime_hdlcasm_swap_infile (fin);
	}
	if (extract) {
		FILE *fout = fopen (asmfile, "w");
		if (fout == NULL) {
			fprintf (stderr, "Failed to open %s for disassembler output\n", asmfile);
			exit (1);
		}
		sublime_hdlcasm_swap_outfile (fout);
	}
	//
	// Read data and pass it through the applicable filters
	while (true) {
		uint8_t buf [1024];
		//
		// Read a buffer full of data
		ssize_t got = read (0, buf, sizeof (buf));
		if (got <= 0) {
			if (got < 0) {
				perror ("Error reading codec data");
			}
			break;
		}
		//
		// Modify the codec in passing
		sublime_recvmedia (recvfilt, buf, got);
		sublime_sendmedia (sendfilt, buf, got);
		//
		// Send the buffer full of data
		ssize_t sent = write (1, buf, got);
		if (sent < 0) {
			if (sent < 0) {
				perror ("Error sending codec data");
				break;
			}
		}
		if (sent != got) {
			fprintf (stderr, "%s: Only sent %zd out of %zd codec bytes\n", progname, sent, got);
		}
	}
	//
	// Close the assembler file
	if (inject) {
		fclose (sublime_hdlcasm_swap_infile (NULL));
	}
	if (extract) {
		fclose (sublime_hdlcasm_swap_outfile (NULL));
	}
	//
	// Cleanup after Subliminal Messaging
	sublime_close (sendfilt);
	sublime_close (recvfilt);
	sublime_fini ();
	//
	// Return neutral success
	return 0;
}
