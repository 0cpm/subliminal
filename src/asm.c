/* asm.c -- Ragel grammar for HDLC assembler code.
 *
 * This parser recognises an assembly line format for HDLC.
 * This makes it easy to parse files and process instructions
 * as a flow of frames to transmit.  Various dynamic forms of
 * data can be included herein.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define SUBLIMINAL_HDLC_ASM_INTERNAL
#include <0cpm/subliminal.h>



int main (int argc, char *argv []) {
	//
	// Setup the current input file
	sublime_hdlcasm_swap_infile (stdin);
	//
	// Fetch a HDLC frame
	sublime_hdlc_buffer_t hdlcbuf;
	bool seen_data = false;
	unsigned hdlclen;
	while (hdlclen = sublime_hdlc_getframe (NULL, hdlcbuf), hdlclen > 0) {
		//
		// Send a FLAG before the frame
		fputc (0x7e, stdout);
		//
		// Transmit the HDLC frame, escape bytes 0x7d, 0x7e, 0x7f
		for (unsigned ofs = 0; ofs < hdlclen; ofs++) {
			if ((hdlcbuf [ofs] >= 0x7d) && (hdlcbuf [ofs] <= 0x7f)) {
				fputc (0x7d, stdout);
				fputc (hdlcbuf [ofs] ^ 0x40, stdout);
			} else {
				fputc (hdlcbuf [ofs]       , stdout);
			}
		}
		//
		// Signal that FLAG and BREAK must be added
		seen_data = true;
	}
	//
	// Send a FLAG and BREAK if the last thing sent was data
	if (seen_data) {
		fputc (0x7e, stdout);
		fputc (0x7f, stdout);
	}
	//
	// Retract the current input file
	(void) sublime_hdlcasm_swap_infile (NULL);
}

