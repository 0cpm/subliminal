/* eXchange IDentification -- we a UUID to signal Subliminal Messaging.
 *
 * We should think of a mechanism to detect support or lack thereof on
 * the other end.  Like ISDN fax, we can use XID for that purpose.  It
 * is a positive signal, and we might use it without confirmation, such
 * as durig a one-directional ring tone, for opportunistic signals.
 *
 * A design alternative would have been to use RR or RNR, but that is
 * not sensitive to the application at hand, whereas XID can be setup
 * with a long enough code to warrent the application.
 *
 * I will not poison my head with information that I can only use in
 * limited forms, because it restricts my freedom to write open code.
 * For that reason, this is all based on open standards, of which too
 * little is known about the XID field.  WikiPedia suggests that a
 * first bit value 0 gives way to local formats.  And UUIDs are long
 * enough to steer clear from mishaps.
 *
 * The UUID transmitted in XID has a first byte with MSB "0",
 * 45e2b4e8-018b-4efd-8f05-137317273293
 * And exchange of data before this XID is sent or confirmed by the
 * other side counts as Subliminal Messaging
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


// NOTES: Some work referencing the XID format:
//
// RFC 1381 mentions XID forms as used in LAPB
//
