/* Networking over a Data Link layer of HDLC.
 *
 * HDLC may break a network packet into smaller frames, and send each in a
 * direct sequence.  Every HDLC frame is protected by a CRC and may be
 * resent upon failure.  HDLC maintains a window mechanism to allow this
 * without falling back to a lock-step exchange mechanism.
 *
 * The network side registers a sequence of applications by their address
 * as used over HDLC.  For each of these it sets an MTU and MRU to allow.
 *
 * The data link side can be implemented in many ways, with callbacks for
 * general HDLC events such as start of an information frame (allocating
 * space to write in), locking and unlocking data blocks, and various
 * state changes.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#define WINDEX_MASK 0x07

typedef uint8_t windex_t;

struct hdlc_app {
	uint8_t address;
	uint16_t mtu, mru;
};


struct hdlc_net {
	struct hdlc_app *appv;
	uint8_t appc;
	uint8_t app_up;
	uint8_t *win_up;
	windex_t win_up_hole1;
	windex_t win_up_empty;
	windex_t win_up_input;
	struct iovec up [7];
	uint32_t app_dn;
	uint8_t *win_dn;
	windex_t win_dn_hole1;
	windex_t win_dn_acked;
	windex_t win_dn_output;
	uint8_t *buf_input;
	struct iovec dn [7];
}


/**********  DATA LINK INTERFACE **********/


/* Allocate a portion of the window buffer for delivery of a frame.  Only a
 * maximum size can be given yet.  The memory is handed out again next time,
 * the contents are committed.  Prepare for reception with the window number
 * given, but do not yet assume that it will be a validated replacement.
 *
 * The space returned has at least 992 bytes free (highest MTU value in use)
 * and 5 bytes play room for CRC, flags, escapes.
 *
 * Return true on success.
 */
bool hdlc_up_getbuf (struct hdlc_net *net, windex_t windex, uint8_t **wbuf) {
	net->win_up_input = windex & WINDEX_MASK;
	*wbuf = win_up_input;
	return true;
}


/* Confirm that allocated memory was indeed written.  The next allocation
 * will overlap this portion.
 *
 * Return true on success.
 */
bool hdlc_up_bufused (struct hdlc_net *net, uint8_t *wstop) {
	intptr_t buflen = ((intptr_t) wstop) - ((intptr_t) net->win_up_input);
	if ((buflen < 0) || (buflen > 992)) {
		return false;
	}
	//TODO// Memorise the new window portion
	//TODO// Possibly remove old window portion
	net->win_empty = (net->win_up_input + 1) & WINDEX_MASK;
	buf_up = wstop;	//TODO// Cycle memory with window
}


/* Trigger the end of an MTU block, which possibly consisted of more than
 * one HDLC frame in immediate succession.  The return value true is an
 * indication of success, false indicates that a resend is required.  Any
 * such resends will then be flagged to make another attempt at sync.
 */
bool hdlc_up_sync (struct hdlc_net *net, windex_t windex) {
	if (net->win_up_hole1 != windex) {
		return false;
	}
}


/* Ask for the following window indexes:
 *  - The first slot to receive (either a hole or beyond last received)
 *  - The next unsent slot (regardless of a hole)
 *
 * When the values differ, send #1 in SREJ to request the hole.
 * When the values match,  send #2 in UA   to acknowledge reception.
 *
 * Piggybacked values should be #1 to avoid acknowledging unreasinably.
 *
 * After a hole is sent, reconsider if further holes need plugging.
 *
 * This function always succeeds, and always returns true.
 *
 * TODO: Consider managing this in "live" variables.
 */
#if 0
static inline bool hdlc_upbuf_acks (struct hdlc_net *net, windex_t *hole1, windex_t *empty) {
	*hole1 = net->win_hole1;
	*empty = net->win_empty;
	return true;
}
#endif

/* Test if the up (datalink to network) buffer is contiguous.
 */
#define hdlc_up_contigbuf(net) ((net)->win_hole1 == (net)->win_empty)

/* Find the first spot to write to, which can be used as piggyback acknowledge.
 */
#define hdlc_up_piggyack(net) ((net)->win_hole1 << 5)

/* Receive Ready and Receive Not Ready signaling (with nothing else to send).
 */
#define hdlc_up_RR(net)   (hdlc_upbuf_piggyack(net) | 0x01)
#define hdlc_up_RNR(net)  (hdlc_upbuf_piggyack(net) | 0x05)

/* REJect anything from the first hole; not the most efficient, not used here.
 */
#if 0
#define hdlc_up_REJ(net)  (hdlc_upbuf_piggyack(net) | 0x09)
#endif

/* Selective REJect of the first hole; possibly the next empty slot.
 */
#define hdlc_up_SREJ(net) (hdlc_upbuf_piggyack(net) | 0x0d)


/* Process a RSET command.  Anything not yet delivered is forgotten.
 *
 * This always returns true for success.
 */
bool hdlc_up_RSET (struct hdlc_net *net) {
	net->win_up_hole1 = net->win_up_empty = net->win_up_input = 0;
	//TODO// Wipe window buffers
	return true;
}


/* Process a SABM command.  Fails if the application is not accessible.
 *
 * This returns true for success.
 */
bool hdlc_up_SABM (struct hdlc_net *net, uint32_t app) {
	for (uint32_t appi = 0
}


/** CRC-16 utility.  Update the checksum with the bytes given.
 */
bool hdlc_crc16_update (uint16_t *crc, uint16_t poly, uint8_t *data, uint16_t len) {
	TODO;
}


/** CRC-32 utility.  Update the checksum with the bytes given.
 */
bool hdlc_crc32_update (uint32_t *crc, uint32_t poly, uint8_t *data, uint16_t len) {
	TODO;
}



/**********  NETWORK INTERFACE  **********/


/* I-frame with user information and the proper windowing information.
 */
#define hdlc_dn_INFO(net) (hdlc_upbuf_piggyack(net) | 0x01 | (net->win_dn_output << 1));

/* Set Asynchronous Balanced Mode, or "connect to an address".
 */
#define hdlc_dn_SABM(net) (0x2f)

/* DISConnect from an address.
 */
#define hdlc_dn_DISC(net) (0x23)

/* Disconnected Mode, rejection of communication attempt.  First use SABM.
 */
#define hdlc_dn_DM(net)   (0x0f)

/* ReSET the connection.  Drop any half-done network packet.
 */
#define hdlc_dn_RSET(net) (0x8f)

/* TEST the data link by asking for an echo.  Usable to test channel transparency?
 */
#define hdlc_dn_TEST(net) (0xe3)



