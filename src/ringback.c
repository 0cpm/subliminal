/* ringback.c -- Generate a ringback tone that can have HDLC added.
 *
 * This program produces a ringtone, compliant to ITU E.180 and RFC 4733.
 * For the US, it produces a mixture of 350 Hz and 440 Hz, while for
 * others it produces a single 425 Hz frequency.  On for 1s, off 4s.
 *
 * These parameters can be set within the ITU E.180 constraints.  These
 * are of some value, because devices depend on it.  The number of
 * ringback sounds created can be varied; it is usually better to
 * do any repeating after adding HDLC to it.  A-law files can simply
 * be concatenated.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>

#include <getopt.h>


int main (int argc, char *argv []) {
	//
	// Parse arguments
	bool error = false;
	bool help = false;
	int numrings = 1;
	int freqnr = 0;
	float freq [3];
	bool usfreq = false;
	char *outfn = NULL;
	char *progname = strrchr (argv [0], '-');
	if (progname == NULL) {
		progname = argv [0];
	}
printf ("Program ends in \"%s\"\n", progname);
	bool faxtone = (0 == strcmp (progname, "-faxtone"));
	float on_time  = faxtone ? 0.5 : 1.0;
	float off_time = faxtone ? 3.0 : 4.0;
	int opt = -1;
	while (opt = getopt (argc, argv, "un:f:d:s:"), opt != -1) {
		switch (opt) {
		case 'u':
			usfreq = true;;
			break;
		case 'n':
			numrings = atoi (optarg);
			break;
		case 'f':
			if (freqnr < 3) {
				freq [freqnr++] = atof (optarg);
			} else {
				error = true;
			}
			break;
		case 'd':
			on_time = atoi (optarg);
			break;
		case 's':
			off_time = atoi (optarg);
			break;
		case 'h':
			help = true;
			break;
		default:
			error = true;
			break;
		}
	}
	if (faxtone && (freqnr > 0)) {
		error = true;
	}
	if (usfreq && (freqnr > 0)) {
		error = true;
	}
	if (optind == argc) {
		outfn = NULL;
	} else if (optind + 1 == argc) {
		outfn = argv [argc - 1];
	} else {
		error = true;
	}
	// 
	// Report if an error was encountered
	if (help || error) {
		fprintf (stderr, "Usage: %s OPTIONS [output.al]\nDumps on stdout or the output.al file, with options: -u      US ringback cadence default\n -n NNN  Number of rings\n - F FF.F Frequency for (another, up to three) tone (in Hertz)\n -d TT.T duration of the ringback sound (in seconds)\n -s TT.T duration of silent periods (in seconds)\n -h      Help message\n", argv [0]);
		exit (error ? 1 : 0);
	}
	//
	// Setup default frequencies if none were supplied
	if (freqnr == 0) {
		if (faxtone) {
			freq [freqnr++] = 1100.0;
		} else if (usfreq) {
			freq [freqnr++] = 350.0;
			freq [freqnr++] = 480.0;
		} else {
			freq [freqnr++] = 425.0;
		}
	}
	//
	// Test the constraints set by ITU E.180
	if (numrings < 1) {
		fprintf (stderr, "You should set at least one ringtone, not -n %d\n", numrings);
		error = true;
	}
	if (faxtone) {
		//
		// Fax tones are already setup up
		if ((freq [0] < 1100-38) || (freq [0] > 1100+38)) {
			fprintf (stderr, "Frequencies must not deviate more than 38 Hz from the intended 1100 Hz pilot tone, failed on %f Hz\n", freq [0]);
			error = true;
		}
		if ((on_time < 0.425) || (on_time > 0.575)) {
			fprintf (stderr, "The fax tone must be ON for at least 425 ms and at most 575 ms to be detected, failed on %f ms\n", 1000 * on_time);
			error = true;
		}
		if ((off_time < 2.55) || (off_time > 3.45)) {
			fprintf (stderr, "The fax tone must not be OFF for at least 2.55 s and at most 3.45 s to be recognised, failed on %f s\n", off_time);
			error = true;
		}
	} else if (freqnr == 1) {
		//
		// Single-tone range
		if ((freq [0] < 400.0) || (freq [0] > 450.0)) {
			fprintf (stderr, "A single tone should be in the 400-450 Hz range, so %f Hz is wrong\n", freq [0]);
			error = true;
		}
	} else {
		//
		// Check tonal ranges -- TODO, ITU E.180 seems to be missing this?
		bool got_340_425 = false;
		bool got_400_480 = false;
		for (int i = 0; i < freqnr; i++) {
			if ((freq [i] >= 340.0) && (freq [i] <= 425.0)) {
				got_340_425 = true;
			}
			if ((freq [i] >= 400.0) && (freq [i] <= 480.0)) {
				got_400_480 = true;
			}
		}
		if (!got_340_425) {
			fprintf (stderr, "There should be one tone at 340 Hz to 425 Hz\n");
			error = true;
		}
		if (!got_400_480) {
			fprintf (stderr, "There should be one tone at 400 Hz to 480 Hz\n");
			error = true;
		}
		//
		// Tonal distance
		for (int i = 0; i < freqnr; i++) {
			for (int j = i + 1; j < freqnr; j++) {
				if (fabs (freq [i] - freq [j]) < 25.0) {
					fprintf (stderr, "Frequencies must differ at least 25 Hz, failed on %f Hz and %f Hz\n", freq [i], freq [j]);
					error = true;
				}
			}
		}
	}
	if (!faxtone) {
		if ((on_time < 0.67) || (on_time > 1.5)) {
			fprintf (stderr, "The ringback time should be 0.67s to 1.5s, not -d %f\n", on_time);
			error = true;
		}
		if ((off_time < 3.0) || (off_time > 5.0)) {
			fprintf (stderr, "The silence between ringback tones should be 3s to 5s, not -s %f\n", off_time);
			error = true;
		}
	}
	if (error) {
		fprintf (stderr, "Please modify your settings to be compliant with ITU %s\n", faxtone ? "T.30" : "E.180");
		exit (1);
	}
	//
	// Produce the ringing tone
	FILE *fout = (outfn != NULL) ? fopen (outfn, "w") : stdout;
	if (fout == NULL) {
		fprintf (stderr, "Failure to open output file %s for writing\n", outfn);
		exit (1);
	}
	int sample = 0;
	int sample_on  = roundl (on_time  * 8000.0);
	int sample_off = roundl (off_time * 8000.0);
	int sample_per = sample_on + sample_off;
	float dt = 1.0 / 8000.0;
	while (numrings-- > 0) {
		for (int i = 0; i < sample_per; i++) {
			uint8_t alaw, sgn, exp, mnt;
			if (i < sample_on) {
				//
				// Produce a sample for the ringback tone
				float total = 0.0;
				for (int f = 0; f < freqnr; f++) {
					total += sin (2 * M_PI * freq [f] * i * dt);
				}
				total /= (float) freqnr;
				total *= 15.0 / 16.0;
				// 
				// Map the sample to A-law (prior to wire ^ 0x55)
				if (total < 0.0) {
					total = - total;
					sgn = 0x80;
				} else {
					sgn = 0x00;
				}
				if (total < 0.125) {
					/* double range, covers 0x00 and 0x10 */
					exp = 0x00;
					mnt = (uint8_t) truncl ((total * 8 - 0.0) * 32);
				} else if (total < 0.25) {
					exp = 0x20;
					mnt = (uint8_t) truncl ((total * 4 - 0.5) * 32);
				} else if (total < 0.5) {
					exp = 0x30;
					mnt = (uint8_t) truncl ((total * 2 - 0.5) * 32);
				} else {
					exp = 0x40;
					mnt = (uint8_t) truncl ((total * 1 - 0.5) * 32);
				}
#ifdef DEBUG
				if (outfn != NULL) {
					printf ("A-law ^ 0x55 = 0x%02x | 0x%02x | 0x%02x   for float %8.4f\n", sgn, exp, mnt, total);
				}
#endif
				assert ((sgn & 0x7f) == 0x00);
				assert ((exp & 0x8f) == 0x00);
				if (exp > 0x10) {
					assert ((mnt & 0xf0) == 0x00);
				} else {
					assert ((mnt & 0xe0) == 0x00);
				}
				alaw = sgn | exp | mnt;
			} else {
				//
				// Produce silence
				alaw = 0;
			}
			fputc (alaw ^ 0x55, fout);
		}
	}
	if (outfn != NULL) {
		fclose (fout);
	}
	return 0;
}

