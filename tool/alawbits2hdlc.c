/* alawbits2hdlc.c -- Retrieve HDLC bits embedded in A-law media
 *
 * Retrieve the bit patterns that may occur in A-law media, and hope to
 * recognise them as HDLC frames.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



/* Flags for detected mangling (MANGLE_YES), detected non-mangling (MANGLE_NO)
 * and, because some samples are sent with an audio lower bit and do not allow
 * strict transmission of pre-mangled codes, ignored values that happen to be
 * mangle-output (MANGLE_IGN).  MANGLE_MASK allows only _YES and _NO.
 */
#define MANGLE_YES  0x01
#define MANGLE_NO   0x02
#define MANGLE_MASK 0x03

#define MANGLE_IGN  0x10


/* Array of mangled codes.  This is used before reading the LSB, to decide whether
 * the higher bits victimise the LSB to fixation in a A-μ-A translation process.
 * When this is the case, we choose to not get a data bit from it, but the value
 * sent may have been rewritten, so as a recipient we can detect whether it is.
 *
 * The index to this array is the un-flipped current byte, with either LSB bit
 * (so the mangled and unmangled values sit side by side) and the response is
 * MANGLE_YES or MANGLE_NO to determine that mangling happened, or did not happen
 * to a value where it would have been caused by a translation.
 *
 * There are very few MANGLE_YES, because A-law is so good to barely tamper with
 * data, and because we do not tamper with the audio signals to make them help
 * detect mangling (which is a choice that any transmitter may make).  Given that
 * we are conservative, it does no real harm to have few confirmations though;
 * we rather have many chances of getting convinced that mangling does not occur.
 * And this is present rather a lot in the table below.
 *
 * Negative and positive values look alike, except for their MSB.  This means
 * that the table lists the same 128 values in two series.
 */
uint8_t check_mangled [256] = {
	// 0x5.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0x4. -- ends with 0x4d<-0x4c, 0x4f<-0x4e, 0x49<-0x48, 0x4b<-0x4a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	// 0x7. -- ends with 0x79->0x78, 0x7b->0x7a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  ,
	// 0x6. -- ends with 0x6b->0x6a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_NO   , MANGLE_IGN  ,
	// 0x1. -- ends with 0x1b<-0x1a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_YES  , MANGLE_NO   ,
	// 0x0.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0x3.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0x2.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xD.  Copies 0x5.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xC.  Copies 0x4.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	// 0xF.  Copies 0x7.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  ,
	// 0xE.  Copies 0x6.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_NO   , MANGLE_IGN  ,
	// 0x9.  Copies 0x1.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_YES  , MANGLE_NO   ,
	// 0x8.  Copies 0x0.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xB.  Copies 0x3.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xA.  Copies 0x2.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
};


void putchar_output (char outbyte, int otf) {
	if (otf < 0) {
		otf = 2;
	}
	write (otf, &outbyte, 1);
}


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Parse arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s infile.al [hdlcfile...]\n"
				"Purpose: Extract HDLC frames that may be contained in A-law media.\n"
				, *argv);
		exit (1);
	}
	char *infn = argv [1];
	int argi = 2;
	uint8_t flip;
	flip = 0x00;
	if (strstr (infn, ".ul")) {
		// μ-law flips all  bits just before transmission
		flip ^= 0xff;
		fprintf (stderr, "%s: Beware that this program was designed for A-law\n", *argv);
	}
	if (strstr (infn, ".al")) {
		// A-law flips even bits just before transmission
		flip ^= 0x55;
	}
	if (flip == 0x00) {
		fprintf (stderr, "%s: Your input file must have .ul or .al in its name\n", *argv);
		exit (1);
	}
	if (flip == 0xaa) {
		fprintf (stderr, "%s: Your input file should not have both .ul and .al in its name\n", *argv);
		exit (1);
	}
	//
	// Prepare for measurements
	uint32_t total = 0;
	uint32_t data = 0;
	uint8_t mangle_hist = 0x00;
	//
	// Open files
	int otf = -1;
	int inf = open (infn, O_RDONLY);
	if (inf < 0) {
		fprintf (stderr, "%s: Failed to open %s for reading\n", *argv, infn);
		ok = false;
	}
	if (!ok) {
		goto fail;
	}
	//
	// Extract data from inf
	uint8_t buf [1000];
	ssize_t rdlen;
	int ones = 0;
	//
	// Shift buffer
	uint8_t outbyte = 0x00;
	uint8_t outmask = 0x01;
	//
	// Before FLAG and after BREAK, we enter a pause
	// While recognising these, probed may be true
	bool paused = true;
	bool probed = true;
	//
	// Loop over blocks read on the input file
	while (rdlen = read (inf, buf, sizeof (buf)), rdlen > 0) {
		//
		// Update the buffer with bits of noise (update stats)
		for (int i = 0; i < rdlen ; i++) {
			//
			// Count total data passing through
			total += 8;
			//
			// Fetch bits without transport bit flipping
			uint8_t bits = buf [i] ^ flip;
			//
			// Determine mask and the count the data carried
			uint8_t exp = bits & 0x70;
			uint8_t mask;
			switch (exp) {
			case 0x40:
				if (check_mangled [bits]) {
					/* Our single bit would be mangled */
					continue;
				}
				mask = 0x01;
				data += 1;
				break;
			case 0x30:
				mask = 0x02;
				data += 2;
				break;
			case 0x20:
				mask = 0x04;
				data += 3;
				break;
			case 0x10:
			case 0x00:
				mask = 0x08;
				data += 4;
				break;
			default:
				/* All bits are reserved for sound */
				continue;
			}
			//
			// Pausing continues as long as the low bit is 0
			if (paused) {
				if (check_mangled [bits]) {
					if ((bits & 0x02) == 0x02) {
						probed = true;
					}
				} else {
					if ((bits & 0x01) == 0x01) {
						probed = true;
					}
				}
				if (!probed) {
					//DEBUG// putchar ('#');
					continue;
				}
			}
			//
			// Not paused, so process the data
			//DEBUG// printf ("[%02x]", bits & (mask | (mask-1)));
			//
			// Iterate over the HDLC data bits
			for (uint8_t bitm = mask; bitm > 0x00; bitm >>= 1) {
				if (bitm == 0x01) {
					uint8_t mangle_flags = check_mangled [bits];
					if (mangle_flags) {
						//DEBUG// printf ("Un-flipped wire sample 0x%02x flags MANGLE_NO=%d, MANGLE_YES=%d, MANGLE_IGN=%d\n", bits, (mangle_flags & MANGLE_NO) ? 1: 0, (mangle_flags & MANGLE_YES) ? 1 : 0, (mangle_flags & MANGLE_IGN) ? 1 : 0);
						mangle_hist |= mangle_flags;
						continue;
					}
				}
				if ((bits & bitm) != 0x00) {
					//DEBUG// putchar ('-');
					//DEBUG// putchar (bitm + 'A' - 1);
					//DEFER// putchar ('1');
					//DEFER// outbyte |= outmask;
					ones++;
				} else {
					//DEBUG// putchar ('.');
					//DEBUG// putchar (bitm + 'M' - 1);
					//DEFER// putchar ('0');
					if (ones <= 5) {
						//
						// If we are paused, this is a failed probe
						if (paused) {
							probed = false;
							ones = 0;
							continue;
						}
						//
						// Take note if this is "0" bit stuffing
						bool bit_stuffed = (ones == 5);
						//
						// Shift in prior "1" bits (they are data)
						while (ones > 0) {
							//DEBUG// putchar ('1');
							outbyte |= outmask;
							if (outmask >= 0x80) {
								putchar_output (outbyte, otf);
								outbyte = 0x00;
								outmask = 0x01;
							} else {
								outmask += outmask;
							}
							ones--;
						}
						//
						// Skip if the current "0" is bit stuffing
						if (bit_stuffed) {
							//DEBUG// putchar ('s');
							continue;
						}
					} else if (ones == 6) {
						//
						// Handle FLAG in prior "1" bits (need prior "0")
						//DEBUG// putchar ('f');
						//DEBUG// printf ("\n===== NEW OUTPUT FILE AFTER %d BITS =====\n", data);
						outmask >>= 1;
						if ((outmask > 0x01) || (outbyte != 0x00)) {
							fprintf (stderr, " --> dropping remnant byte/mask 0x%02x/0x%02x\n", (int) outbyte, (int) outmask);
						}
						//
						// Close any output file that may currently be open
						if (otf >= 0) {
							close (otf);
							otf = -1;
						}
						//
						// Open the next output file, if any
						if (argi < argc) {
							char *otfn = argv [argi++];
							otf = open (otfn,
									O_WRONLY | O_CREAT | O_TRUNC,
									0644);
							if (otf < 0) {
								fprintf (stderr, "%s: Failed to open %s for writing\n", *argv, otfn);
								ok = false;
							}
						}
						//
						// Begin sampling fresh bytes
						outbyte = 0x00;
						outmask = 0x01;
						//
						// End any pause after probing successfully
						paused = false;
						probed = false;
						//
						// Skip the "0" and continue to the next bit
						ones = 0;
						continue;
					} else if (ones >= 7) {
						//
						// Handle BREAK in prior "1" bits
						//DEBUG// putchar ('b');
						//DEBUG// printf ("\n\n===== BREAK IN INPUT =====\n");
						outbyte = 0x00;
						outmask = 0x01;
						//
						// Start pausing after BREAK and before FLAG
						paused = true;
						probed = true;
						//
						// Skip the "0" and continue to the next bit
						ones = 0;
						continue;
					}
					//DEBUG// putchar ('0');
					if (outmask >= 0x80) {
						//
						// Complete data byte, print it
						putchar_output (outbyte, otf);
						outbyte = 0x00;
						outmask = 0x01;
					} else {
						outmask += outmask;
					}
				}
			}
			//DEBUG// putchar (' ');
			//
			// Zero the HDLC bits in the data stream
			bits &= ~mask;
			buf [i] = bits ^ flip;
		}
		//DEBUG// putchar ('\n');
	}
	if (rdlen < 0) {
		perror ("Error reading");
		ok = false;
		goto fail;
	}
	//
	// Reporting
	if (otf >= 0) {
		fprintf (stderr, "Incomplete recovery to file %s\n", argv [argi-1]);
	}
	while (argi < argc) {
		fprintf (stderr, "No data recovered for file %s\n", argv [argi++]);
	}
	if (mangle_hist & MANGLE_YES) {
		printf ("%s: There have been mangled samples\n", *argv);
	}
	if (mangle_hist & MANGLE_NO) {
		printf ("%s: There have been samples that escaped mangling\n", *argv);
	}
	if ((mangle_hist & MANGLE_MASK) == MANGLE_NO) {
		printf ("%s: Conservative estimates are IN FAVOUR OF BYTE-STEALING MODE\n", *argv);
	} else if (mangle_hist == 0x00) {
		printf ("%s: Conservative estimates are NOT CONVINCED OF BYTE-STEALING MODE\n", *argv);
	} else {
		printf ("%s: Conservative estimates are OPPOSED TO BYTE-STEALING MODE\n", *argv);
	}
	printf ("%s: HDLC in %d bits of %d bits, so %5.2f%% is data or %7.3f kb/s\n",
			*argv,
			data, total,
			(100.0 * data) / total,
			(data * 64.000) / total);
	//
	// Cleanup
fail:
	if (inf >= 0) {
		close (inf);
		inf = -1;
	}
	return ok ? 0 : 1;
}
