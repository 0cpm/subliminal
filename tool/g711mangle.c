/* mangle.c -- Translate between A-law and μ-law to see mangled bytes.
 *
 * This tool can mangle by mapping:
 *  - A-law to μ-law
 *  - μ-law to A-law
 *  - A-law to μ-law to A-law
 *  - μ-law to A-law to μ-law
 *
 * The mangling variant depends on filename extensions; if they are the
 * same then the other form is placed in the middle.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


//
// Translation tables, ul2al and al2ul (mapped back to 0..127 ranges)
//
// Notes from G.711 (which is rather confusing):
//  - A-law applies XOR 0x55 for transmission (maximising transitions)
//  - μ-law applies XOR 0x00 for transmission
//  - both set the high bit for positive, and reset it for negative
//  - both toggle that bit when changing through zero, quite a jump
//  - A-law toggles 1 == 0x80 to -1 == 0x00
//  - μ-law toggles 0 == 0xff to -0 == 0x7f
//  - A law peaks at 128 == 0xff and -128 == 0x7f
//  - μ-law peaks at 127 == 0x80 and -127 == 0x00
//
// The tables need something to be indexed on; we will generate them
// as a transport format mapping.

uint8_t ul2al [256];
uint8_t al2ul [256];


typedef int     wire2decout_t (uint8_t wire);
typedef uint8_t decout2wire_t (int   decout);

int alaw2decout (uint8_t wire) {
	wire ^= 0x55;
	if (wire & 0x80) {
		return + (int) (1 + (wire & 0x7f));
	} else {
		return - (int) (1 + (wire & 0x7f));
	}
}

int ulaw2decout (uint8_t wire) {
	if (wire & 0x80) {
		return + (int) (wire ^ 0xff);
	} else {
		return - (int) (wire ^ 0x7f);
	}
}

uint8_t decout2alaw (int decout) {
	if (decout > 0) {
		return (0x80 | (+decout - 1)) ^ 0x55;
	} else {
		return (0x00 | (-decout - 1)) ^ 0x55;
	}
}

uint8_t decout2ulaw (int decout) {
	if (decout > 0) {
		return 255 - decout;
	} else {
		return 127 + decout;
	}
}

/* Implement the translation table directly, and fill the translation
 * in a scatter-mode.
 */
void fill_al2ul (void) {
	//DEBUG// printf ("Filling al2ul: ");
	int newout;
	uint8_t al;
	uint8_t ul;
	for (int decout = 1; decout <= 128; decout++) {
		int decback = alaw2decout (decout2alaw (decout));
		if (decback != decout) {
			printf ("Warning: al2ul mapping %d != %d\n", decback, decout);
		}
		newout = ~decout;
		if (decout <= 8) {
			newout = decout * 2 - 1;
		} else if (decout <= 25) {
			newout = decout + 7;
		} else if (decout <= 32) {
			newout = (decout - 25) / 2 + 32;
		} else if (decout <= 44) {
			newout = decout + 3;
		} else if (decout <= 48) {
			newout = (decout - 45) / 2 + 48;
		} else if (decout <= 62) {
			newout = decout + 1;
		} else if (decout <= 64) {
			newout = 64;
		} else if (decout <= 78) {
			newout = decout;
		} else if (decout <= 80) {
			newout = 79;
		} else {
			newout = decout - 1;
		}
		//DEBUG// printf ("%d/%d, ", decout, newout);
		al = decout2alaw (+decout);
		ul = decout2ulaw (+newout);
		al2ul [al] = ul;
		// printf ("%02x/%02x, ", al, ul);
		al = decout2alaw (-decout);
		ul = decout2ulaw (-newout);
		al2ul [al] = ul;
		// printf ("%02x/%02x, ", al, ul);
	}
	//DEBUG// printf ("\n");
}

/* Implement the translation table directly, and fill the translation
 * in a scatter-mode.
 */
void fill_ul2al (void) {
	//DEBUG// printf ("Filling ul2al: ");
	int newout;
	uint8_t al;
	uint8_t ul;
	for (int decout = 0; decout <= 127; decout++) {
		int decback = ulaw2decout (decout2ulaw (decout));
		if (decback != decout) {
			printf ("Warning: ul2al mapping %d != %d\n", decback, decout);
		}
		if (decout <= 15) {
			newout = decout / 2 + 1;
		} else if (decout <= 32) {
			newout = decout - 7;
		} else if (decout <= 36) {
			newout = (decout - 33) * 2 + 27;
		} else if (decout <= 47) {
			newout = decout - 3;
		} else if (decout == 48) {
			newout = 46;
		} else if (decout <= 63) {
			newout = decout - 1;
		} else if (decout <= 79) {
			newout = decout;
		} else {
			newout = decout + 1;
		}
		//DEBUG// printf ("%d/%d, ", decout, newout);
		ul = decout2ulaw (+decout);
		al = decout2alaw (+newout);
		ul2al [ul] = al;
		// printf ("%02x/%02x, ", ul, al);
		ul = decout2ulaw (-decout);
		al = decout2alaw (-newout);
		ul2al [ul] = al;
		// printf ("%02x/%02x, ", ul, al);
	}
	//
	// Treat -0 as a mirror image of +0
	ul = decout2ulaw (0);
	al = decout2alaw (ul2al [0]);
	ul2al [ul ^ 0x80] = ul2al [ul] ^ 0x80;
	//DEBUG// printf ("\n");
}


#define FLIP_ALAW 0x55
#define FLIP_ULAW 0xff

uint8_t fn2flip (char *fn, char *progname) {
	uint8_t flip = 0x00;
	if (strstr (fn, ".ul")) {
		// μ-law flips all  bits just before transmission
		flip ^= FLIP_ULAW;
	}
	if (strstr (fn, ".al")) {
		// A-law flips even bits just before transmission
		flip ^= FLIP_ALAW;
	}
	if (flip == 0x00) {
		fprintf (stderr, "%s: Your file name must have .ul or .al in its name\n", progname);
		exit (1);
	}
	if (flip == (FLIP_ALAW ^ FLIP_ULAW)) {
		fprintf (stderr, "%s: Your file name should not have both .ul and .al in its name\n", progname);
		exit (1);
	}
	return flip;
}


int main (int argc, char *argv []) {
	//
	// Parse arguments
	if (argc != 3) {
		fprintf (stderr, "Usage: %s infile.al/.ul outfile.al/.ul\n"
				"Purpose: Mangle files by translation from/to A-law and μ-law codecs.\n"
				, *argv);
		exit (1);
	}
	//
	// Harvest information from .al and .ul in filenames
	char *infn = argv [1];
	char *otfn = argv [2];
	uint8_t inflip = fn2flip (infn, *argv);
	uint8_t otflip = fn2flip (otfn, *argv);
	//
	// Fill the tables that map between A-law and μ-law
	fill_ul2al ();
	fill_al2ul ();
	//
	// Announce the mapping that will be performed
	wire2decout_t *w2d;
	decout2wire_t *d2w;
	if (inflip == FLIP_ALAW) {
		w2d = alaw2decout;
		d2w = decout2ulaw;
		if (otflip == FLIP_ALAW) {
			printf ("Mapping A-law --> μ-law --> A-law\n");
		} else {
			printf ("Mapping A-law --> μ-law\n");
		}
	} else {
		w2d = ulaw2decout;
		d2w = decout2ulaw;
		if (otflip == FLIP_ULAW) {
			printf ("Mapping μ-law --> A-law --> μ-law\n");
		} else {
			printf ("Mapping μ-law --> A-law\n");
		}
	}
	//
	// Prepare for measurements
	uint32_t total = 0;
	uint32_t mods = 0;
	bool used [256];
	uint8_t map [256];
	memset (used, 0, sizeof (used));
	//
	// Open files
	bool ok = true;
	int inf = open (infn, O_RDONLY);
	if (inf < 0) {
		fprintf (stderr, "%s: Failed to open %s for reading\n", *argv, infn);
		ok = false;
	}
	int otf = open (otfn, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (otf < 0) {
		fprintf (stderr, "%s: Failed to open %s for writing\n", *argv, otfn);
		ok = false;
	}
	if (!ok) {
		goto fail;
	}
	//
	// Extract data from inf
	uint8_t buf [1000];
	ssize_t rdlen;
	int ones = 0;
	//
	// Loop over the data, flip bits, use al2ul[] / ul2al[] once or twice
	while (rdlen = read (inf, buf, sizeof (buf)), rdlen > 0) {
		//
		// Update the buffer by mapping each sample separately
		for (int i = 0; i < rdlen ; i++) {
			//
			// Take the wire input, including bit flipping
			uint8_t orig = buf [i];
			used [orig] = true;
			uint8_t bits;
			uint8_t back;
			//
			// Map once or twice between A-law and μ-law
			if (inflip == FLIP_ALAW) {
				bits = al2ul [orig];
				back = ul2al [bits];
				if (otflip == FLIP_ALAW) {
					bits = back;
				}
			} else {
				bits = ul2al [orig];
				back = al2ul [bits];
				if (otflip == FLIP_ULAW) {
					bits = back;
				}
			}
			//
			// Write the wire output, already with bit flipping
			buf [i] = bits;
			//
			// Count the total values and the number of modifications
			total += 1;
			map [orig] = back;
			if (orig != back) {
				mods += 1;
			}
		}
		//
		// Write out the buffer
		ssize_t wrlen = write (otf, buf, rdlen);
		if (wrlen < 0) {
			perror ("Error writing");
			ok = false;
			goto fail;
		} else if (wrlen < rdlen) {
			fprintf (stderr, "%s: Incomplete write\n", *argv);
			ok = false;
			goto fail;
		}
	}
	if (rdlen < 0) {
		perror ("Error reading");
		ok = false;
		goto fail;
	}
	//
	// Reporting
	for (int i = 0; i <= 0xff; i++) {
		if (used [i] && (map [i] != (uint8_t) i)) {
			printf ("Return to input codec mangled 0x%02x --> 0x%02x for %d --> %d\n",
					(uint8_t) i, map [i],
					w2d ((uint8_t) i), w2d (map [i]));
		}
	}
	printf ("%s: Code changed in %d out of %d samples, so %5.2f%% mangling\n",
			*argv,
			mods, total,
			(100.0 * mods) / total);
	//
	// Cleanup
fail:
	if (inf >= 0) {
		close (inf);
		inf = -1;
	}
	if (otf >= 0) {
		close (otf);
		otf = -1;
	}
	return ok ? 0 : 1;
}
