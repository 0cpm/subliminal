/* g711simunoise.c -- Add noise to u-Law or A-law, like in subliminal messaging.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Parse arguments
	if (argc != 3) {
		fprintf (stderr, "Usage: %s infile.al outfile.al\n"
				"Purpose: Insert noise at the audio level of Subliminal Messaging in an\n"
				"         A-law or μ-law media file, to test how that sounds.\n",
				*argv);
		exit (1);
	}
	char *infn = argv [1];
	char *otfn = argv [2];
	uint8_t flip;
	flip = 0x00;
	if (strstr (infn, ".ul")) {
		// μ-law flips all  bits just before transmission
		// (This is not true for negative values, but harmless in this program)
		flip ^= 0xff;
	}
	if (strstr (infn, ".al")) {
		// A-law flips even bits just before transmission
		flip ^= 0x55;
	}
	if (flip == 0x00) {
		fprintf (stderr, "%s: Your input file must have .ul or .al in its name\n", *argv);
		exit (1);
	}
	if (flip == 0xaa) {
		fprintf (stderr, "%s: Your input file should not have both .ul and .al in its name\n", *argv);
		exit (1);
	}
	//
	// Prepare for measurements
	uint32_t total = 0;
	uint32_t data = 0;
	//
	// Open source of noise
	int rnd = open ("/dev/urandom", O_RDONLY);
	if (rnd < 0) {
		fprintf (stderr, "%s: Failed to open noise source\n", *argv);
		ok = false;
	}
	//
	// Open files
	int inf = open (infn, O_RDONLY);
	if (inf < 0) {
		fprintf (stderr, "%s: Failed to open %s for reading\n", *argv, infn);
		ok = false;
	}
	int otf = open (otfn, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (otf < 0) {
		fprintf (stderr, "%s: Failed to open %s for writing\n", *argv, otfn);
		ok = false;
	}
	if (!ok) {
		goto fail;
	}
	//
	// Map content from inf to otf
	uint8_t buf [1000];
	uint8_t nox [1000];
	ssize_t rdlen;
	//
	// Loop over blocks read on the input file
	while (rdlen = read (inf, buf, sizeof (buf)), rdlen > 0) {
		//
		// Read the same amount of noise
		ssize_t nxlen = read (rnd, nox, rdlen);
		if (nxlen < 0) {
			perror ("Error reading noise");
			ok = false;
			goto fail;
		} else if (nxlen < rdlen) {
			fprintf (stderr, "%s: Ran out of noise\n", *argv);
			ok = false;
			goto fail;
		}
		//
		// Update the buffer with bits of noise (update stats)
		for (int i = 0; i < rdlen ; i++) {
			uint8_t mask;
			uint8_t exp = ((buf [i] ^ flip) & 0x70);
			switch (exp) {
			case 0x40:
				//MILD// mask = 0x00;
				//MILD// data += 0;
				mask = 0x01;
				data += 1;
				break;
			case 0x30:
				//MILD// mask = 0x01;
				//MILD// data += 1;
				mask = 0x03;
				data += 2;
				break;
			case 0x20:
				//MILD// mask = 0x03;
				//MILD// data += 2;
				mask = 0x07;
				data += 3;
				break;
			case 0x10:
			case 0x00:
				//MILD// mask = 0x07;
				//MILD// data += 3;
				mask = 0x0f;
				data += 4;
				break;
			default:
				mask = 0x00;
			}
			total += 8;
			//
			// Distort by XOR'ing the buffer with noise bits
			buf [i] ^= nox [i] & mask;
		}
		//
		// Write out the buffer
		ssize_t wrlen = write (otf, buf, rdlen);
		if (wrlen < 0) {
			perror ("Error writing");
			ok = false;
			goto fail;
		} else if (wrlen < rdlen) {
			fprintf (stderr, "%s: Incomplete write\n", *argv);
			ok = false;
			goto fail;
		}
	}
	if (rdlen < 0) {
		perror ("Error reading");
		ok = false;
		goto fail;
	}
	//
	// Reporting
	printf ("%s: Packed %d bits into %d bits, so %5.2f%% is data or %7.3f kb/s\n",
			*argv,
			data, total,
			(100.0 * data) / total,
			(data * 64.000) / total);
	//
	// Cleanup
fail:
	if (otf >= 0) {
		close (otf);
		otf = -1;
	}
	if (inf >= 0) {
		close (inf);
		inf = -1;
	}
	if (rnd >= 0) {
		close (rnd);
		rnd = -1;
	}
	return ok ? 0 : 1;
}
