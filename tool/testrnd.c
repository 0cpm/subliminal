/* testrnd.c -- Tool to check that our random model is somewhat reasonable.
 *
 * This random model is used in g711distort.c and is a lazy approximation
 * of a normal distribution (because it does not *really* matter, anything
 * about that shape will work).
 *
 * The samples are usually close to the average value (1000.0), but the
 * standard deviation needed a correction factor 20 which seems fine.
 * We did not go so far as to produce a Q-Q plot to test the distribution
 * because then we'd have to produce the normal distribution, which we
 * were trying to avoid in the first place :-) but a bar chart for the
 * numerically sorted samples looks good.
 *
 * This is only an internal program to validate the random model, it has
 * no uses in Subliminal Messaging, except for the normalish() call which
 * appears in g711distort.c too.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <math.h>

#include <time.h>


unsigned normalish (int avg, int stddev) {
        //
        // Sample random material
        long int base = random ();
	base -= RAND_MAX / 2;
        //
        // Compute the desired value
	float sample = (float) base / (float) RAND_MAX;
	int derv = (int) (20 * stddev * sample * sample * sample);
        derv += avg;
        if (derv < 0) {
                derv = 0;
        }
        return (unsigned) derv;
}


int main (int argc, char **argv) {
	int aimavg    = (argc > 1) ? atoi (argv [1]) : 1000;
	int aimstddev = (argc > 2) ? atoi (argv [2]) : 50;
	int rndseed   = (argc > 3) ? atoi (argv [3]) : time (NULL);
	srandom (rndseed);
	uint32_t sample [1000];
	uint32_t sum;
	for (int i = 0; i < 1000; i++) {
		sample [i] = normalish (aimavg, aimstddev);
		printf ("%5d\n", sample [i]);
		sum += sample [i];
	}
	float avg = ((float) sum) / 1000.0;
	float stddev = 0.0;
	for (int i = 0; i < 1000; i++) {
		float diff = sample [i] - avg;
		stddev += (diff * diff);
	}
	stddev = sqrt (stddev / 999.0);
	printf ("\naverage = %f, stddev = %f\n", avg, stddev);
}
