/* g711simupause.c -- Add a zero lowest bit to u-Law or A-law.
 *
 * Subliminal messaging works with HDLC flags, consisting of many "1" bits.
 * Setting the lowest-value bit to "0" avoids ever sending a flag as part
 * of voice data.  This is a good mode to fill gaps between data.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Parse arguments
	if (argc != 3) {
		fprintf (stderr, "Usage: %s infile.al outfile.al\n"
				"Purpose: Insert zero bits like during Subliminal Messaging pauses in an\n"
				"         A-law or μ-law media file, to test how that sounds.\n",
				*argv);
		exit (1);
	}
	char *infn = argv [1];
	char *otfn = argv [2];
	uint8_t flip;
	flip = 0x00;
	if (strstr (infn, ".ul")) {
		// μ-law flips all  bits just before transmission
		// (This is not true for negative values, but harmless in this program)
		flip ^= 0xff;
	}
	if (strstr (infn, ".al")) {
		// A-law flips even bits just before transmission
		flip ^= 0x55;
	}
	if (flip == 0x00) {
		fprintf (stderr, "%s: Your input file must have .ul or .al in its name\n", *argv);
		exit (1);
	}
	if (flip == 0xaa) {
		fprintf (stderr, "%s: Your input file should not have both .ul and .al in its name\n", *argv);
		exit (1);
	}
	//
	// Prepare for measurements
	uint32_t total = 0;
	uint32_t data = 0;
	//
	// Open files
	int inf = open (infn, O_RDONLY);
	if (inf < 0) {
		fprintf (stderr, "%s: Failed to open %s for reading\n", *argv, infn);
		ok = false;
	}
	int otf = open (otfn, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (otf < 0) {
		fprintf (stderr, "%s: Failed to open %s for writing\n", *argv, otfn);
		ok = false;
	}
	if (!ok) {
		goto fail;
	}
	//
	// Map content from inf to otf
	uint8_t buf [1000];
	ssize_t rdlen;
	//
	// Loop over blocks read on the input file
	while (rdlen = read (inf, buf, sizeof (buf)), rdlen > 0) {
		//
		// Update the buffer with low zero bits where data might be
		for (int i = 0; i < rdlen ; i++) {
			uint8_t bits = buf [i] ^ flip;
			uint8_t exp = bits & 0x70;
			if (exp <= 0x40) {
				/* A bit rough; sometimes we need 0xfd */
				bits &= 0xfe;
				buf [i] = bits ^ flip;
				data += 1;
			}
			total += 8;
		}
		//
		// Write out the buffer
		ssize_t wrlen = write (otf, buf, rdlen);
		if (wrlen < 0) {
			perror ("Error writing");
			ok = false;
			goto fail;
		} else if (wrlen < rdlen) {
			fprintf (stderr, "%s: Incomplete write\n", *argv);
			ok = false;
			goto fail;
		}
	}
	if (rdlen < 0) {
		perror ("Error reading");
		ok = false;
		goto fail;
	}
	//
	// Reporting
	printf ("%s: Zeroed %d bits in %d bits, so %5.2f%% is zeroed or %7.3f kb/s\n",
			*argv,
			data, total,
			(100.0 * data) / total,
			(data * 64.000) / total);
	//
	// Cleanup
fail:
	if (otf >= 0) {
		close (otf);
		otf = -1;
	}
	if (inf >= 0) {
		close (inf);
		inf = -1;
	}
	return ok ? 0 : 1;
}
