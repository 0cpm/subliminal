/* hdlc2alawbits.c -- Shift HDLC bits into A-law media passing through.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



/*
 * Modes distinguish what to do.  Minor modes count up, and may end in a next major mode.
 *
 * Major modes:
 *  - MODE_MAJOR_START, send break and flag signals.
 *     - Minors 0..15 send 0111111101111110
 *     - Minor 1 can be used for BREAK, 0-FLAG
 *     - Minor 8 can be used for 0-FLAG
 *     - Minor 9 can be used for FLAG after confirmed "0" bit
 *  - MODE_MAJOR_DATA, send bits of data
 *     - Minor 0 is after a "0" bit in the data
 *     - Minor 1..5 after 1..5 bits "1" in the data
 *     - Minor 6 to bit-stuff a "0" and continue to minor 0
 *  - MODE_MAJOR_END, send flag and break signals
 *     - Minor 0..14 send 011111101111111
 *     - Minor 0 sends an ending 0-FLAG
 *     - Minor 7 checks for new data, and sets it up for MODE_MAJOR_DATA, minor 0
 *     - Minor 8 can be a starting point for a new line without data
 *     - Minor 8..14 continue to transmit a final BREAK to stop from chasing data
 *     - Minor 15 makes a last check for new data, and may jump back to MODE_MAJOR_START, minor 7
 *  - MODE_MAJOR_SILENT, send no data at all (but zero low bits in A-law samples)
 */

#define MODE_MAJOR_MASK   0xf0
#define MODE_MAJOR_START  0x00
#define MODE_MAJOR_DATA   0x10
#define MODE_MAJOR_END    0x20
#define MODE_MAJOR_SILENT 0x30


typedef enum {
	SEND_ZERO    = 0,
	SEND_ONE     = 1,
	PASS_DATABIT = 2,
	STOP_SENDING = 3
} bitact_t;


/* Array of mangled codes.  This is used before writing the LSB, to decide whether
 * the higher bits victimise the LSB to fixation in a A-μ-A translation process.
 * When this is the case, we choose to not put a data bit in it, but we send
 * the value that would be rewritten, so the recipient can detect whether it is.
 * The index to this array is the un-flipped current byte, with either LSB bit
 * (so all entries occur twice) and the response is SEND_ZERO or SEND_ONE to
 * fixate the LSB, or PASS_DATABIT to allow a databit in this position.
 *
 * Negative and positive values look alike, except for their MSB.  This means
 * that the table lists the same 128 values in two series.
 */
bitact_t lsbact_mangling [256] = {
	// 0x5.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0x4. -- ends with 0x4d<-0x4c, 0x4f<-0x4e, 0x49<-0x48, 0x4b<-0x4a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	// 0x7. -- ends with 0x79->0x78, 0x7b->0x7a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ZERO   , SEND_ZERO   , SEND_ZERO   , SEND_ZERO   ,
	// 0x6. -- ends with 0x6b->0x6a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x1. -- ends with 0x1b<-0x1a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x0.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0x3.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0x2.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xD.  Copies 0x5.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xC.  Copies 0x4.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	// 0xF.  Copies 0x7.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ZERO   , SEND_ZERO   , SEND_ZERO   , SEND_ZERO   ,
	// 0xE.  Copies 0x6.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x9.  Copies 0x1.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x8.  Copies 0x0.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xB.  Copies 0x3.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xA.  Copies 0x2.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
};



/* Wrapper for the next bit to send.  The next bit may in fact indicate that
 * it is ready to STOP_SENDING, instead of just SEND_ZERO or SEND_ONE.  In
 * general, the wrap_nextbit() task is to manage its *mode and insert any
 * bits before or after the protocol for bit-level HDLC transmission.
 *
 * This is an inline function because it is mostly a logic decision tree for
 * the HDLC bit protocol; it assumes it will be inserted in one place only,
 * and can be juggled mercylessly by an optimising compiler.
 */
static inline bitact_t wrap_nextbit (uint8_t *mode, bitact_t nextbit) {
	uint8_t cur_mode = *mode;
	switch (cur_mode & MODE_MAJOR_MASK) {
	case MODE_MAJOR_START:
		/* Send a "0" or "1" bit as determined by BREAK and FLAG */
		*mode = cur_mode + 1;
		if ((cur_mode == MODE_MAJOR_START + 0x08) || (cur_mode == MODE_MAJOR_START + 0x0f)) {
			return SEND_ZERO;
		} else {
			return SEND_ONE;
		}
	case MODE_MAJOR_DATA:
		if (cur_mode == MODE_MAJOR_DATA + 5) {
			/* Stuff a "0" bit into the sequence */
			*mode = MODE_MAJOR_DATA;
			return SEND_ZERO;
		} else if (nextbit == STOP_SENDING) {
			/* Stop sending data, already send the initial "0" now */
			*mode = MODE_MAJOR_END + 1;
			return SEND_ZERO;
		} else {
			/* Send a data bit */
			if (nextbit == SEND_ZERO) {
				*mode = MODE_MAJOR_DATA;
			} else {
				*mode = cur_mode + 1;
			}
			return PASS_DATABIT;
		}
	case MODE_MAJOR_END:
		/* Send a pattern of "0" and "1" for FLAG and filler zeroes */
		if ((nextbit != STOP_SENDING) && (cur_mode < MODE_MAJOR_END + 6)) {
			/* rejuvenate the BREAK transmission into first FLAG */
			// Old: BREAK starts at MODE_MAJOR_END   + 0
			// New: FLAG  starts at MODE_MAJOR_START + 8
			*mode = cur_mode - (MODE_MAJOR_END + 0) + (MODE_MAJOR_START + 9);
			if (cur_mode == MODE_MAJOR_START) {
				return SEND_ZERO;
			} else {
				return SEND_ONE;
			}
		}
		if (cur_mode < MODE_MAJOR_END + 15) {
			/* Send the FLAG and BREAK bit sequence */
			*mode = cur_mode + 1;
			if ((cur_mode == MODE_MAJOR_END) || (cur_mode == MODE_MAJOR_END + 7)) {
				return SEND_ZERO;
			} else {
				return SEND_ONE;
			}
		} else if (nextbit != STOP_SENDING) {
			/* We have more data to send, after BREAK so first send FLAG */
			*mode = MODE_MAJOR_START + 9;
			return SEND_ZERO;
		} else {
			/* We have no more data to send, so we switch to silent / zeroing mode */
			*mode = MODE_MAJOR_SILENT;
			return STOP_SENDING;
		}
	case MODE_MAJOR_SILENT:
		/* We silenced HDLC bit transmission, but may be asked to restart it */
		if (nextbit != STOP_SENDING) {
			*mode = MODE_MAJOR_START + 9;
			return SEND_ZERO;
		}
		return STOP_SENDING;
	}
}


/* Map a single data byte to 8 bit actions.
 *
 * This inline function is used once and permits ruthless optimisation.
 */
static inline void data2bitacts (uint8_t *data, bitact_t *actions8) {
	uint16_t xmit = 0x0100 | (uint8_t) *data;
	while (xmit > 0x0001) {
		*actions8++ = (xmit & 0x0001) ? SEND_ONE : SEND_ZERO;
		xmit >>= 1;
	}
}


/* Fill the data byte with 8 STOP_SENDING bits.
 *
 * This inline function is used once and permits ruthless optimisation.
 */
static inline void nodata2bitstops (bitact_t *actions8) {
	for (int i = 8; i > 0; i--) {
		*actions8++ = STOP_SENDING;
	}
}


/* Fill the data byte with 8 bit actions based on available data.
 * This updates the data pointer and length.
 *
 * This inline function is used once and permits ruthless optimisation.
 */
static inline void data2actions (uint8_t **data, uint16_t *len, bitact_t *actions8) {
	if (*len == 0) {
		*data = NULL;
	}
	if (*data == NULL) {
		nodata2bitstops (actions8);
	} else {
		(*len)--;
		data2bitacts ((*data)++, actions8);
	}
}


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Parse arguments
	if (argc < 3) {
		fprintf (stderr, "Usage: %s infile.al outfile.al [hdlcframefile...]\n"
				"Purpose: Insert HDLC frames into an A-law media file.\n",
				*argv);
		exit (1);
	}
	char *infn = argv [1];
	char *otfn = argv [2];
	int argi = 3;
	uint8_t flip;
	flip = 0x00;
	if (strstr (infn, ".ul")) {
		// μ-law flips all  bits just before transmission
		flip ^= 0xff;
		fprintf (stderr, "%s: Beware that this program was designed for A-law\n", *argv);
	}
	if (strstr (infn, ".al")) {
		// A-law flips even bits just before transmission
		flip ^= 0x55;
	}
	if (flip == 0x00) {
		fprintf (stderr, "%s: Your input file must have .ul or .al in its name\n", *argv);
		exit (1);
	}
	if (flip == 0xaa) {
		fprintf (stderr, "%s: Your input file should not have both .ul and .al in its name\n", *argv);
		exit (1);
	}
	//
	// Prepare for measurements
	uint32_t total = 0;
	uint32_t data = 0;
	uint32_t zero = 0;
	//
	// Open files
	int inf = open (infn, O_RDONLY);
	if (inf < 0) {
		fprintf (stderr, "%s: Failed to open %s for reading\n", *argv, infn);
		ok = false;
	}
	int otf = open (otfn, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (otf < 0) {
		fprintf (stderr, "%s: Failed to open %s for writing\n", *argv, otfn);
		ok = false;
	}
	if (!ok) {
		goto fail;
	}
	//
	// Map content from inf to otf
	uint8_t buf [1000];
	ssize_t rdlen;
	uint8_t payloadbuf [65535];
	uint8_t *payload = NULL;
	uint16_t payloadlen = 0;
	bitact_t byteact [8];
	int nextbyteact = 99;
	uint8_t wrapmode = MODE_MAJOR_START + 1;
	bool done_sending = false;
	//
	// Loop over blocks read on the input file
	while (rdlen = read (inf, buf, sizeof (buf)), rdlen > 0) {
		//
		// Update the buffer with zero bits or actual data
		for (int i = 0; i < rdlen ; i++) {
			//
			// Choose between zeroing and filling in data bits
			if (done_sending) {
				//
				// Cheap-and-easy zero stuffing mode
				uint8_t bits = buf [i] ^ flip;
				uint8_t exp = bits & 0x70;
				if (exp <= 0x40) {
					//
					// Mark the LSB or, if needed, one up
					if (lsbact_mangling [bits] != PASS_DATABIT) {
						if (exp <= 0x30) {
							//DEBUG// putchar ('z');
							bits &= 0xfd;
						} else {
							//DEBUG// putchar ('X');
						}
					} else {
						//DEBUG// putchar ('Z');
						bits &= 0xfe;
					}
					//DEBUG// printf ("%x", bits & 0x0f);
					buf [i] = bits ^ flip;
					zero += 1;
				}
			} else {
				//
				// Determine mask and how many bits we can insert
				uint8_t mask;
				uint8_t bits = buf [i] ^ flip;
				uint8_t exp = (bits & 0x70);
				switch (exp) {
				case 0x40:
					mask = 0x01;
					break;
				case 0x30:
					mask = 0x02;
					break;
				case 0x20:
					mask = 0x04;
					break;
				case 0x10:
				case 0x00:
					mask = 0x08;
					break;
				default:
					mask = 0x00;
				}
				//
				// Collect the bits to send insert low bits into low bits
				while (mask > 0x00) {
					//
					// Be sure to have payload data -- probably sub-optimal here
					if ((nextbyteact < 8) && (byteact [nextbyteact] == STOP_SENDING) && (argi < argc)) {
						int plf = open (argv [argi++], O_RDONLY);
						if (plf < 0) {
							perror ("Payload unavailable");
							ok = false;
							goto fail;
						}
						payloadlen = read (plf, payloadbuf, sizeof (payloadbuf));
						payload = payloadbuf;
						close (plf);
						nextbyteact = 8;
					}
					//
					// Be prepared for byte actions
					if (nextbyteact >= 8) {
						data2actions (&payload, &payloadlen, byteact);
						nextbyteact = 0;
						//DEBUG// putchar ('\n');
					}
					//
					// Skip bit insertion if mangling may occur
					bitact_t todo = PASS_DATABIT;
					if (mask == 0x01) {
						todo = lsbact_mangling [bits];
						//DEBUG// if (todo != PASS_DATABIT) printf ("LSB=0x%02x.  Set todo=%d (SEND_ZERO=%d, SEND_ONE=%d, PASS_DATABIT=%d)\n", bits, todo, SEND_ZERO, SEND_ONE, PASS_DATABIT);
					}
					//
					// Poll how the protocol wraps the next byte action
					// but only when mangling is not imminent
					if (todo == PASS_DATABIT) {
						todo = wrap_nextbit (&wrapmode, byteact [nextbyteact]);
					} else {
						data -= 1;
					}
					//
					// Start zeroing at the end of the protocol
					if (todo == STOP_SENDING) {
						payload = NULL;
						todo = SEND_ZERO;
						zero += 1;
						data -= 1;
						payload = NULL;
						done_sending = (argi >= argc);
					//
					// When the protocol is ready, use the next databit
					} else if (todo == PASS_DATABIT) {
						todo = byteact [nextbyteact++];
					}
					//
					// Now either send a "1" or "0" bit
					if (todo == SEND_ONE) {
						//DEBUG// putchar ('1');
						//DEBUG// putchar (mask + 'A' - 1);
						bits |=  mask;
					} else {
						//DEBUG// putchar ('0');
						//DEBUG// putchar (mask + 'M' - 1);
						bits &= ~mask;
					}
					//
					// Count the data bit, modify the bit mask
					data += 1;
					mask >>= 1;
				}
				//DEBUG// putchar (' ');
				//
				// Distort by XOR'ing the buffer with noise bits
				buf [i] = bits ^ flip;
			}
			//
			// Count the total number of codec bits passed through
			total += 8;
		}
		//DEBUG// putchar ('\n');
		//
		// Write out the buffer
		ssize_t wrlen = write (otf, buf, rdlen);
		if (wrlen < 0) {
			perror ("Error writing");
			ok = false;
			goto fail;
		} else if (wrlen < rdlen) {
			fprintf (stderr, "%s: Incomplete write\n", *argv);
			ok = false;
			goto fail;
		}
	}
	if (rdlen < 0) {
		perror ("Error reading");
		ok = false;
		goto fail;
	}
	//
	// Reporting
	if (payload != NULL) {
		fprintf (stderr, "%s: Not all was sent from the file %s\n",
						*argv, argv [argi-1]);
	}
	while (argi < argc) {
		fprintf (stderr, "%s: Nothing was sent from the file %s\n",
						*argv, argv [argi++]);
	}
	printf ("%s: Transmitted %d bits and zeroed %d bits in %d bits, so %5.2f%% with data or %7.3f kb/s\n",
			*argv,
			data, zero, total,
			(100.0 * data) / total,
			(data * 64.000) / total);
	//
	// Cleanup
fail:
	if (otf >= 0) {
		close (otf);
		otf = -1;
	}
	if (inf >= 0) {
		close (inf);
		inf = -1;
	}
	return ok ? 0 : 1;
}
