/* g711distort.c -- Add random bit (sequence) disruptions to an ISDN channel
 *
 * The distorition can be random, or it may be initiated with a seed, to
 * allow reproductive testing.  The reason is that CRC error checking is
 * not 100% reliable, and making this fully random would introduce a risks
 * of failed tests.
 *
 * You can set parameters as follows:
 *  - input file
 *  - output file
 *  - average bit length between distortions (default 10000)
 *  - average bit length of disturtions (default 5)
 *  - bit length standard deviation between distortions (default 3000)
 *  - bit length standard deviation for burst (default 5)
 *  - random seed (default based on current time)
 *
 * Disruptions are set to override the current bits; that is, they need not
 * actually change the bits transported.  This is especially useful during
 * bursts.  The direction of the change (map to "0" or map to "1") is the
 * same for a burst, and will change some but not all bits.  The direction
 * is another random choice.  Bit errors may span across bytes, respecting
 * ISDN transmission order (MSB first), not HDLC order (LSB first).
 *
 * The statistical model is a bit arbitrary, and may change later:
 *  - Approximation of normal distribution with a square root
 *  - Sample a random value, modulo 4x the variance
 *  - Take its square root to end in the 2x stddev range
 *  - Sample a random value as a bit sign
 *  - Add the average value, if negative set to 0
 *  - Return as unsigned integer value
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


unsigned normalish (int avg, int stddev) {
	//
	// Sample random material
	long int base = random ();
	base -= RAND_MAX / 2;
	//
	// Compute the desired value
	float sample = (float) base / (float) RAND_MAX;
	int derv = (int) (20 * stddev * sample * sample * sample);
	derv += avg;
	if (derv < 0) {
		derv = 0;
	}
	return (unsigned) derv;
}


int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Parse arguments
	if ((argc < 3) || (argc > 8)) {
		fprintf (stderr, "Usage: %s infile.al outfile.al [avggood [avgbad [stdgood [stdbad [rndseed]]]]]\n"
				"Purpose: Distort the transmission line carrying audio and Subliminal Messaging in an\n"
				"         A-law or μ-law media file, to test error handling\n",
				*argv);
		exit (1);
	}
	char *infn = argv [1];
	char *otfn = argv [2];
	int avggood = (argc > 3) ? atoi (argv [3]) : 10000;
	int avgbad  = (argc > 4) ? atoi (argv [4]) : 5;
	int stdgood = (argc > 5) ? atoi (argv [5]) : 3000;
	int stdbad  = (argc > 6) ? atoi (argv [6]) : 5;
	int rndseed = (argc > 7) ? atoi (argv [7]) : (int) time (NULL);
	//
	// Determine the file names and their handling
	uint8_t flip;
	flip = 0x00;
	if (strstr (infn, ".ul")) {
		// μ-law flips all  bits just before transmission
		// (This is not true for negative values, but harmless in this program)
		flip ^= 0xff;
	}
	if (strstr (infn, ".al")) {
		// A-law flips even bits just before transmission
		flip ^= 0x55;
	}
	if (flip == 0x00) {
		fprintf (stderr, "%s: Your input file must have .ul or .al in its name\n", *argv);
		exit (1);
	}
	if (flip == 0xaa) {
		fprintf (stderr, "%s: Your input file should not have both .ul and .al in its name\n", *argv);
		exit (1);
	}
	//
	// Initialise the random source with the seed
	srandom (rndseed);
	avggood -= avgbad;
	unsigned skipgood = 0;
	unsigned makebad  = 0;
	bool burstbit = false;
	bool bursting = true;
	uint8_t burstbyte;
	//
	// Prepare for measurements
	uint32_t total = 0;
	uint32_t bitflips = 0;
	uint32_t bursts = 0;
	//
	// Open files
	int inf = open (infn, O_RDONLY);
	if (inf < 0) {
		fprintf (stderr, "%s: Failed to open %s for reading\n", *argv, infn);
		ok = false;
	}
	int otf = open (otfn, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (otf < 0) {
		fprintf (stderr, "%s: Failed to open %s for writing\n", *argv, otfn);
		ok = false;
	}
	if (!ok) {
		goto fail;
	}
	//
	// Map content from inf to otf
	uint8_t buf [1000];
	ssize_t rdlen;
	//
	// Loop over blocks read on the input file
	while (rdlen = read (inf, buf, sizeof (buf)), rdlen > 0) {
		//
		// Iterate over the buffer, bursting where desired
		for (int i = 0; i < rdlen ; i++) {
			//
			// Count the total input bits
			total += 8;
			//
			// Start a new skipgood period (init and after burst)
			if (bursting && (makebad == 0)) {
				skipgood = normalish (avggood, stdgood);
				//DEBUG// printf ("Skippging %d good bits\n", skipgood);
				bursting = false;
			}
			//
			// Test if this byte can be skipped
			if (skipgood >= 8) {
				skipgood -= 8;
				continue;
			}
			//
			// Skip any bits that should not be bursted
			burstbyte = 0x80;
			while (skipgood > 0) {
				burstbyte >>= 1;
				skipgood--;
			}
			//
			// Switch to bursting mode and setup parameters
			if (!bursting) {
				makebad = normalish (avgbad, stdbad);
				burstbit = (random () & 0x40000000) ? true : false;
				//DEBUG// printf ("Bursting to %d over %d bits\n", burstbit, makebad);
				bursting = true;
				bursts++;
			}
			//
			// Distort the bits for as long as the burst lasts
			while ((makebad > 0) && (burstbyte >= 0x01)) {
				uint8_t old = buf [i];
				if (burstbit) {
					buf [i] |=  burstbyte;
				} else {
					buf [i] &= ~burstbyte;
				}
				if (old != buf [i]) {
					bitflips++;
				}
				burstbyte >>= 1;
				makebad--;
			}
			//
			// Live on the edge :- leave the rest as extra good bits.
		}
		//
		// Write out the buffer
		ssize_t wrlen = write (otf, buf, rdlen);
		if (wrlen < 0) {
			perror ("Error writing");
			ok = false;
			goto fail;
		} else if (wrlen < rdlen) {
			fprintf (stderr, "%s: Incomplete write\n", *argv);
			ok = false;
			goto fail;
		}
	}
	if (rdlen < 0) {
		perror ("Error reading");
		ok = false;
		goto fail;
	}
	//
	// Reporting
	printf ("%s: Flipped %d bits in %d bursts, in a total of %d bits\n",
			*argv,
			bitflips, bursts, total);
	//
	// Cleanup
fail:
	if (otf >= 0) {
		close (otf);
		otf = -1;
	}
	if (inf >= 0) {
		close (inf);
		inf = -1;
	}
	return ok ? 0 : 1;
}
