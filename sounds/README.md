# Test sounds for Subliminal Messaging

Inputs are `.wav` recordings.  Mapped to other formats.

Produce μ-law and A-law:

```
sox quickfox.wav -c 1 -r 8000 -e u-law quickfox.ul
sox quickfox.wav -c 1 -r 8000 -e a-law quickfox.al

sox quickloudfox.wav -c 1 -r 8000 -e u-law quickloudfox.ul
sox quickloudfox.wav -c 1 -r 8000 -e a-law quickloudfox.al
```

There is some amateur poetry here (by Rick van Rein) that
you can use for testing.

