# TODO for Subliminal Messaging

**A-law and μ-law:**

+ Avoid byte codes that get mangled during A-μ-A translation
+ Consider deliberate use of mangled codes to learn about translation
+ Consider deliberate error bursts to learn about distortion and error capture
+ Consider byte-level direct A-law and μ-law TEST commands to check mangling

**HDLC:**

+ Add CRC-16 checksums for S-frames and U-frames
X Add CRC-32 checksums for I-frames -- and maybe for UI-frames?
- Windows with N(S) and N(R) counters
- Respond with checksum-based `RR` (or `RNR`) or perhaps `REJ` (or `SREJ`)
- Impose flow control with RR and RNR
- Negotiate MTU per address
X Handle Poll/Final flags to signal MTU combining
- Connect addresses with `SABM` and disconnect with `DISC` or `DM`
- Test for mangling with endpoints both in A-law or both in μ-law

**Service:**

- For configured TCP, bind and connect symmetrically in relation to `SABM`
- For configured UDP, bind and connect symmetrically to handle all frames
- For configured RTP, bind and connect symmetrically for SDP-agreed UI-frames

**Wireguard:**

+ Cleanup of tunnels, also remote (PinningKey, PublicKey in /var/lib/...?)
- PSK method identr:hello matchend with Identropy hello:<textblurb>
- PSK method for SASL key derivation (with Channel Binding for the keys)
