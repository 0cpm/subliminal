# User Interfaces for Secure Telephony

> *How can a user see if all is going well?  How does s/he respond?*

A few LEDs can be really helpful.  Possibly, sound signals too.

LEDs for data and voice can be:

  * Off when no security is active, or even tried
  * Green when both authentication and encryption are active
  * Red for failure; a flash would be typical for a failed MAC

It is possible to have separate LEDs for encryption and authentication,
but that is likely to be confusing.

A red flash can easily be handled with *what did you just say?* for
voice, and protocols can abort or report errors for data.

In bit-stealing mode, sound comes from the outer codec, so security
derives from outer crypto.  In byte-stealing mode, sound comes from
the inner codec, and  security matches inner crypto.

There may be different modes for incoming and outgoing codecs, since
each independently employs encryption and signing, and bit-stealing
or byte-stealing.

