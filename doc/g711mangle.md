# Mangling due to G.711 translation

> *The A-law and μ-law codecs are close, and may be translated automatically.
> They look quite different, but when translated back to the original form
> the damage is minimal.  There are a few codes to avoid, however.*

When translation is used, then A-μ-A is better than μ-A-μ because it has less
impact on the lower values that we mostly populate with data bits.  But when
a variant exists without the mangling of translation, then that prevails.

clearmode >= A-A == μ-μ > A-μ-A > μ-A-μ

So, we prefer A-law but if we know that both we and the others are using
μ-law with no mangling by A-law in the transit path, then that is better
yet.

## Mangling by A-μ-A translation

The following codes are mangled by the
[standard translation](https://www.itu.int/rec/T-REC-G.711):

| A-law| μ-law| A-law|Mangled |
|---|---|---|---|
|1|1|1||
|2|3|2||
|3|5|3||
|4|7|4||
|5|9|5||
|6|11|6||
|7|13|7||
|8|15|8||
|9..24|16..31|9..24||
|25,26|32|25|26|
|27,28|33|27|28|
|29,30|34|29|30|
|31,32|35|31|32|
|33..44|36..47|33..44||
|45,46|48|46|45|
|47,48|49|48|47|
|49..62|50..63|49..62||
|63,64|64|64|63|
|65..78|65..78|65..78||
|79,80|79|79|80|
|81..128|80..127|81..128||

So, mangling caused by the translation (bits after / may be changed, but x is mangled) is:

  * 26 --> 25 or in wire bytes 0x4c --> 0x4d and bits s100/110x
  * 28 --> 27 or in wire bytes 0x4e --> 0x4f and bits s100/111x
  * 30 --> 29 or in wire bytes 0x48 --> 0x49 and bits s100/100x
  * 32 --> 31 or in wire bytes 0x4a --> 0x4b and bits s100/101x
  * 45 --> 46 or in wire bytes 0x79 --> 0x78 and bits s1111/00x
  * 47 --> 48 or in wire bytes 0x7b --> 0x7a and bits s1111/01x
  * 63 --> 64 or in wire bytes 0x6b --> 0x6a and bits s11010/1x
  * 80 --> 79 or in wire bytes 0x1a --> 0x1b and bits s001101x/

It makes sense to send the values that are prone to mangling to detect whether
it is taking place.  The most conservative approach makes sense.  Note that
these mangled bits are not counted as bit errors in the checksum, because it
does not see them.  In packet mode, where bytes are sent directly, these
mangled bits do show up as bit errors in the checksum.

Based on the position of the x in the above:

  * We should insert data bits from high to low
  * We should check if the code will be mangled, and then skip the last bit
  * The last bit must also be skipped during FLAG and BREAK detection
  * Note that pausing sets only the low bit; this can still be done
  * Pausing may then start in the same sample that ends a FLAG or BREAK (which is fine)
  * Pausing does not occur in the same sample that starts a FLAG (which is excellent)

It is a good principle to send the codes that would be mangled (26, 28, .., 63, 80)
and test if they arrive.  Also, to test their counterparts and test those separately.
It does not matter how often each arrives, so we can keep a few flags that we update
depending on the incoming word.  Those flags can tell that the lower flag carries
data, and if not it indicates what the mangled or unmangled bit values are.

## Mangling by μ-A-μ translation

The following codes are mangled by the
[standard translation](https://www.itu.int/rec/T-REC-G.711):

| μ-law| A-law| μ-law|Mangled |
|---|---|---|---|
|0,1|1|1|0|
|2,3|2|3|2|
|4,5|3|5|4|
|6,7|4|7|6|
|8,9|5|9|8|
|10,11|6|11|10|
|12,13|7|13|12|
|14,15|8|15|14|
|16..32|9..25|16..32||
|33|27|33||
|34|29|34||
|35|31|35||
|36|33|36||
|37|34|37||
|38..47|35..44|38..47||
|48|46|48||
|49..63|48..62|49..63||
|64..79|64..79|64..79||
|80..127|81..128|80..127||

So, mangling caused by the translation is:

  * 0 --> 1
  * 2 --> 3
  * 4 --> 5
  * 6 --> 7
  * 8 --> 9
  * 10 --> 11
  * 12 --> 13
  * 14 --> 15

Mangling mostly occurs in low values, which usually represents half of a
conversation plus low samples while talking.  This is a majority.  It is
more opportunistic to avoid those, which explains the preference for A-law.
Of course, when no mangling is detected on μ-law, then that is preferred.

*Note:* Here, G.711 correctly states that only the low bit changes.

When inserting bits, there are a few conditions to avoid *and they are easy
to foresee in the order of bit insertion* namely the low bit for 0..15 must
be set to 0.  This, however, also impairs our intention to avoid audio as a
cause of spurious FLAG detection.

  * For exponent 0, we cannot distinguish odd/even values.

This is terrible for bit insertion performance, since most samples will
be soft; half of a phone call is spent listening and the other half may
not be spoken at full loudness.


## Avoidance of Mangling

In bit-insertion mode, the goal is to avoid mangling.  That is to say,
it is alright to send values bound to be mangled and learn whether
mangling takes place, but it is not good to assign different symbols
to values before and after mangling.

  * Values to send over A-law include pre-mangled code points
    26, 28, 30, 32, 45, 47, 63, 80.
  * Upon arrival of these values, increment an unmangled-counter.
  * Values to accept over A-law include post-mangled code points
    25, 27, 29 ,31, 46, 48, 64, 79
  * Upon arrival of these values, increment a mangled-counter.
  * As long as the mangled-counter is 0 and the unmangled-counter
    is positive, it seems conservative-yet-safe to assume a clear
    A-law channel.
  * There is no basis for assumption of an unmangled μ-law channel.


## Testing for Mangling

When not enough information is available about an unmangled mode
for a channel, then this can be tested.  The transmitted must then
support the alternative μ-law transmission mode, and use it to send
bytes.  The μ-law message includes its pre-mangled content, so a few
from 0, 2, 4, 6, 8, 10, 12, 14.  The message to send can be `TEST`,
which is the HDLC variant for ping.

Upon arrival, if the checksum fails, it may be caused by mangling.
Reverse that, and check the checksum again.  If this succeeds, then
mangling has taken place.  Send back the mangled form to inform the
sender.

Alternatively, a failed transmission may be rejected with `REJ`.
This is the normal response to a failed checksum.  It is not safe
to assume that mangling takes place, one or a few more resends
may help in that respect.  Hence the effort of checking with the
pre-mangled content to learn what to send back in the `TEST` reply.

A fully decisive procedure might alternate between the pre-mangled
input for μ-law and A-law to learn which is accepted without any
mangling; this is then the line discipline.

*Note:* All this is useless if the sender only has an A-law option
for sending.

