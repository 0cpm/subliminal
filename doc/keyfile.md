# Key Files for Subliminal Messaging

> *Key files start with a differentiating start, a header with version information
> and algorithm identifier, and then tags with binary values.*

This document defines the structure of keyfiles.  Every multi-byte integer is
stored in network byte order: every key may be needed on two hosts!

Keys are for uni-directional encryption.  For bi-directional security, a pair
of keys is required.  There is no strict requirement for these to be matched
in any way, but it is also not forbidden, as long as the unique IV is assured.

Key files are customarily stored with extension `.0key` to relate it to
the 0cpm project, home and inspiration of Subliminal Messaging.


## Magic code and Version

The start of a keyfile are the 10 bytes

```
00000000  53 75 62 6c 69 4d 65 31  2e 30     |SubliMe1.0|
```

The digits in the end identify that this is the v1.0 format for keys under
Subliminal Messaging.


### Algorithm Identifier

Keyfiles are dedicated to an algorithm with a given identifier.  This may sound like
a restriction, but it actually makes sense; good cryptographic practice holds that
a key must not be used in more than one application, for fear of creating combined
uses of algorithms that are beyond the scopes of each of the specifications.  It
follows that a keyfile may be dedicated to an algorithm without restriction to its
usefulness.

Right after the magic code an version string, an `int16_t` holds the value of the
algorithm identifier for the key.  Normally, the most significant byte is 0, but it
may be set to -1 to trigger debugging uses in a SubliMe stack compiled with debugging
options.


### Tag, Length, Value, [Pad] sequences

The following information contains zero or more structures consisting of:

  - **Tag** is a `uint16_t` to select a registered purpose.  The code will also
    use these values to find the desired information.

  - **Length** is a `uint16_t` to describe the number of bytes following.  The
    tag indicates how these are interpreted.

  - **Value** is a `uint8_t[]` sequence holding the bytes needed for the tagged
    interpretation.

  - **Padding** adds 1 to 4 bytes set to 0x00, to come to a file offset that is a
    multiple of 4.  Note that padding is always added, so there is always at least
    one zero byte following a value.


## Use of key files

The application with use `mmap()` to have a live version of the key file in memory.
The advantages are that it can be saved instantly, and that atomic operations can be
used on a shared instance, so that no mandatory locking is necessary; mandatory locks
are not well supported but they are vital to critical requirements to avoid IV repeat.

Once memory-mapped, loaders will use the tag to look for the binary elements they need,
and point to them.  Once this is done, the binary elements are available in any way the
crypto requires.  The memory mapping ensures continued availability.

When an IV is used, it needs to be updated atomically, and this is assured by the
continued availability under a memory map.  This does require the memory map to be
writeable.

Atomic operations have limited sizes.  An IV may hold a lock, or the IV might be
gray-coded, or some form of locking may be included.


### Algorithm-Only Key Files

To merely signal an algorithm identifier, a keyfile only holds the magic code, version
and the algorithm identifier.  This results in a short static string and no dependency
on random number generators.

An example invocation to this end could be:

```
sublime-keygen -a 0x01 -k /path/to/keyfile.0cpmkey
```

The `-a` option gives the algorithm identifier as negotiated over ISDN and the
`-k` option defines the storage path.  Implicitly, the hosting environment adds
its idea of endianness.

Expect the command to protect key files that already exist.  If you think a key
should go, you must delete it before creating a new one under the same filename.
