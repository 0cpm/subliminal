# Typical Examples

> *These are a few protocol flows for Subliminal Messaging.*

A few example use cases to show both similarity and variations.
In all of these examples, the carrier is a codec, so it does
not matter if the calls are made over a phone line or VoIP.

**Explanation:**

  * All protocols start by assuring support for Subliminal Messaging
    by sending a certain XID to HDLC Address 0x00.
  * Services also start negotiating their UUID with an XID command
    to another, arbitrary HDLC Address.  The UUID identifies what kind of
    protocol is being used.
  * SABM opens a connection, I or UI then send confirmed or unconfirmed data,
    and finally DISC ends the connection.
  * Think of I as TCP frames and UI as UDP frames.  Indeed, communication
    software could connect to services over TCP and UDP to relay the data
    for the service.  The address and port for the backend would depend on
    the UUID for the service.
  * The full spec is [`draft-vanrein-sublime`](https://datatracker.ietf.org/doc/draft-vanrein-sublime/).


## Text-supported calls

This procedure can be a great help to hearing-disabled persons.  A second
use case would be talking to police or emergency services without being heard.

 1. Make a call to the other station.
 2. Each end sends XID to Address 0x00 to indicate SubliMe support.
 3. The sending station tries a random Address 0xdf for RealTime Text
    and sends it a (plaintext) XID with the UUID for `itu-t140.sublime.0cpm.org`
    namely `6d7bf8a4-6054-318c-b3ab-0ebc41d54e3d`.
 4. The receiving station recognises the UUID, has no other use for 0xdf
    and responds with the XID holding the same UUID.
 5. Each side has now seen the XID for RealTime Text, so it is now active for
    Address 0xdf.
 6. The sending side sends SABM to Address 0xdf, without cryptographic Information
    field.
 7. The receiving end accepts and responds with SABM.
 8. The T.140 protocol is now started.  One or more of its 16-bit characters will
    be wrapped inside I commands, which the other side confirms with S-frames or,
    if typing is bidirectional, piggybacked on their I-frames.
 9. Text and voice can be used at the same time, either in one direction or both,
    just as the call develops.  While no text is passed, there is no audible
    impact on voice quality.  Hangup of the call terminates the text service.


## Sending a fax

This procedure sends a fax via bit-stealing mode.  It uses the T.38
procedure designed for networks that are not completely real-time,
so that tonal signaling is not appropriate.  Instead, T.38 focusses
on describing the tones in a digital form.

 1. Make a call to the other station.
 2. Each end sends XID to Address 0x00 to indicate SubliMe support.
 3. The sending station tries a random Address 0x3f for the fax service,
    and sends it a (plaintext) XID with the UUID for `itu-t38.sublime.0cpm.org`
    namely `b0725c13-01d7-358d-b099-19fd72233c53`.
 4. The receiving station recognises the UUID, has no other uses for 0x3f
    and responds with the XID holding the same UUID.
 5. Each side has now seen the XID for fax, so it is now active for Address 0x3f.
 6. The sending side sends SABM to Address 0x3f, without cryptographic Information
    field.
 7. The receiving side accepts and responds with SABM.
 8. The T.38 protocol is now started.  Its HDLC frames will be wrapped, one at a
    time, inside I commands which the other side confirms with return I commands
    and/or S-frames.
 9. When T.38 finishes, each station closes down the fax service with DISC.
10. During this procedure, vocal communication continues to be possible.
11. When all information has been exchanged, the two sides hangup.


## Sharing contact information in ringback tones

While phones are ringing, they send a ringback signal.  This may be produced
by the ringing phone, and is then sent back as unidirectional audio.  It is
possible to insert data in this phase, based on (1) SubliMe can transmit
data without confirmation, (2) there usually is time to retry and thereby
offer some fault tolerance.

 1. A caller makes the other end ring.  The other end sends a ringback tone.
 2. The ringback tone uses bit-stealing mode to send the SubliMe XID to
    Address 0x00.
 3. The ringback tone continues with the XID for `ietf-vcard.sublime.0cpm.org`,
    so the UUID is `b038dd65-9d02-373d-92a2-d99bd6f625bb`, to Address 0x2a.
 4. The ringback tone continues into a connection on Address 0x2a by sending
    SABM, the I-frame(s) for the vCard, and DISC.
 6. The ringback tone stops bit-stealing with BREAK, and avoids further FRAME
    until it wants to replicate the transmission.


## Organising a meeting with iTIP over the phone

The iTIP protocol uses the vCalendar format to exchange proposals about an
intended meeting.  These may be shown to callers, and their response may
be shared digitally.  This can be much faster than doing it vocally,
especially in a group meeting.

 1. Two people setup a phone call, setup bit-stealing and confirm SubleMe.
 2. One person picks a random Address 0x66 and requests it for iTIP by
    sending an XID with the `ietf-itip.sublime.0cpm.org` UUID,
    `793fad76-3725-3c23-af8b-eb0dbce103d6`.  The exchange SABM to open
    the connection.
 3. The participants in the call can now share FREE/BUSY schedules, see
    proposals being made and either select or reject them.  They can also
    propose alternatives, to which others may respond.
 4. Once settled, the iTIP procedure is closed off using DISC.
 5. Group variants to this scheme would appoint an organiser, who coordinates
    the various proposals and passes them to all participants.  This may
    well be a function of a phone meeting relay.


## Dialin to a remote desktop (or text interfaces)

Let's dial into a computer, like we did in the old days.  We will use a
VNC desktop, which means using the RFB protocol.  To avoid abuses of
the remote connection, we will use a security mode.

 1. Call the computer, and setup SubliMe with the AES128-SGCM security mode.
 2. Exchange the XID for `ietf-tls.sublime.0cpm.org`, so UUID
    `1c469ae6-fb6b-3516-9689-d953e0513880`, and run SABM without
    additional security Information.  Run a TLS handshake and extract a 128-bit
    key.  Do not end with DISC.
 3. Open another connection with SABM, this time adding a 96-bit random IV
    in the Information field.  The other side will do the same, sending the
    IV for its output.
 4. Setup cryptographic structures on both sides, for sending with the local IV
    and receiving with the remote IV.  Set the counter for I commands to 0 and
    the counter for UI/XID/UP commands to 4294967296 or 4294967295.
 5. We have not established a protocol yet.  Pick an address and send the XID
    for `ietf-rfb.sublime.0cpm.org` with UUID `081a98f4-883f-3042-adbc-661c8e14ecf1`.
    These XID frames are encrypted, so nobody knows that VNC/RFB is in use.
    The transmitted XID frame starts with a UI/XID counter, which is lowered
    enough to encrypt and sign the XID frame without counter-mode overlap.
 6. Once both sides agree, each side can send a chain of I commands holding the
    VNC/RFB content.  Each frame is verifiable with 32 bits only; for a 256-byte
    frame the risk of forgery lies around ~2^-24 and for a 2048-byte frame
    around 2^-21.
 7. When the connection closes with DISC, a full signature on the content of
    I commands is given.  This authenticates the preceding connection with
    128-bit security.
 8. Similar exchanges can be made with `arpa2-tty.sublime.0cpm.org`, which presents
    a textual flow of UTF-8 bytes with mulTTY extensions.  The use of mulTTY is
    that it can multiplex multiple programs, each with multiple own I/O streams,
    into a single byte flow.  This can be used for commandline shells, textual
    programs, and even an old-fashioned BBS.


## Encryption Before First Contact

When a Caller ID is known, it is theoretically possible to setup keys
before the call is answered.  This combines communication in ringback
tones with KIP for encryption.

 1. A caller makes the other end ring.  The other end sends a ringback tone.
 2. Use the Caller ID to find the ENUM domain of the caller, lookup the SRV
    record for _kip._tcp underneath and establish a KIP Document to relay a
    key accessible to any user under that domain.
 3. The ringback tone uses bit-stealing mode to send the SubliMe XID to
    Address 0x00.
 4. The ringback tone continues with the XID for `arpa2-kip.sublime.0cpm.org`,
    so the UUID is `fb3abc9a-bd42-3db0-9458-c1955ea6df5d`.
 5. The ringback tone continues with SABM, one or more I commands to relay
    the KIP Document, and a closing DISC.
 6. The ringback tone stops bit-stealing with BREAK, and avoids further FRAME
    until it wants to replicate the transmission.


## Sharing a secure document

Document transport can be secured with TLS key exchange.  An alternative
approach is to use documents that are themselves secure.  The KIP Document
structures allow signing and/or encryption in various ways, and may include
verifiable references.  KIP Documents can even form a stream, so it can be
seen as a continuous flow, to be passed in I commands without further need
for protection.

 1. Make a call to the other station.
 2. Each end sends XID to Address 0x00 to indicate SubliMe support.
 3. The sending station tries a random Address 0x99 for the fax service,
    and sends it a (plaintext) XID with the UUID for `arpa2-kipdoc.sublime.0cpm.org`
    namely `7cc50f02-4d3b-36f2-8991-b964b0a31c49`.
 4. The receiving station recognises the UUID, has no other uses for 0x99
    and responds with the XID holding the same UUID.
 5. Each side has now seen the XID for KIP Documents, so it is now active for
    Address 0x99.
 6. The sending side sends SABM to Address 0x99, without cryptographic Information
    field.
 7. The receiving side accepts and responds with SABM.
 8. The exchange of a KIP Document now starts.  It passes I commands to carry
    parts, forming a continnuous flow from one side to the other.  The flow
    may also be bidirectional.
 9. There is no requirement to read the document in transit, although it is
    possible if the receipient authenticates.
10. Neither on the wire nor in stored form would a KIP Document allow others
    to read it.  This end-to-end security goes well beyond transport security
    with protocols such as TLS and also beyond the cryptographic modes that
    SubliMe offers.

