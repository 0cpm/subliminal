.TH Subliminal 7 "September 2022" 0cpm "Linux Programmer's Manual"
.SH NAME
subliminal \- Data in noise levels of [telephone] codecs
.SH SYNOPSIS
.B Subliminal Messaging
injects structured data into the noise levels of common codecs,
allowing extra services to be negotiated and used.
.P
.SH DESCRIPTION
The integration of Internet and Telephony is imperfect.
If data is to travel over a phone line, it completely takes over
a communication channel, encodes data in timing-critical sounds
that are painful to listen to for a human.  Better integration
would allow the spontaneous addition of data as part of a phone
call, without disrupting the conversation.
.P
Telephone systems tend to be more rigid than Internet software,
and this makes it difficult to switch to a modern codec or to
initiate cryptographic enhancements such as encryption and
authentication.  In spite of codecs of superiour quality, the
involvement of a telephone switch usually imposes an ISDN
codec onto modern SIP telephony systems.
.P
Subliminal Messaging steps in by recognising that codecs used
in Telephony are reproduced on the other end with a high level
of reliability, and that some bits in these codecs are less
important than others, because they represent signals under a
tolerable noise floor.  Such bits can be used for alternate
purposes of data transport.
.P
Such data transport is normally opportunistic, and needs to
be detected and confirmed with good recognition for spurious
detection.  This is achieved by passing HDLC frames, with a
well-described structure, and including checksums.  A procedure
needs to be initiated, involving the excplicit negotiation of
Subliminal Messaging by sending a specific UUID to a fixed
address, to keep the risk of false positive detection very low.
.P
.SH HDLC
HDLC frames are sent to a one-byte Address, have a dedicated
one-byte Command and may incorporate an Information field of
up to 256 bytes (or 2048 bytes when long frames are mutually
accepted).  Then follows a 16-bit Check.  HDLC defines a set
of standard Commands and behaviours, and Subliminal Messaging
adopts a subset with small modifications (which is a very
common practice).
.TP
.B XID
Negotiates a service, identified by a UUID, at the Address to
which it is sent.  The UUID is usually derived from a DNS name.
For the initiation of the service, the DNS name is
.B sublime.0cpm.org
and the Address is \fB0x00\fR.  Other services use others UUIDs
and send it to any other Address; only \fB0x00\fR and \fB0xff\fR
are special and must not be used.
.TP
.B SABM
Connects a service at the targeted Address, and starts window counters
to achieve reliable communication due to acknowledgement and
resending of frames.
.TP
.B DISC
Disconnects a service at the targeted Address, as previously setup
with \fBSABM\fR.
.TP
.B UI
Sends an unconfirmed information frame to the other end, much
like UDP on an Internet connection.  This may in fact be how
the frame is relayed on an endpoint for Sublimal Messaging.
.TP
.B I
Sends a confirmed/windowed information frame to the other end,
much like TCP on an Internet connection.  This may in fact be
how the frame is relayed on an endpoint for Subliminal Messaging.
.TP
.B UP
Polls the other end to receive explicit information about the
window status on the other end.  This is sometimes used to enable
the flushing of a sender's window and make room for more data.
.TP
.B RR
Acknowledges reception up to a given point in the window, and
if flow control asked the sender to shut up for a while it also
enables sending again.
.TP
.B RNR
Acknowledges reception up to a given point in the window, and
imposes flow control by asking the sender to shut up for a
while.
.TP
.B REJ
Acknowledges reception up to a given point in the window, and
indicates that anything after this point has been rejected,
presumably due to a Check that failed (so due to line distortion).
This asks for frames to be resent from the rejected point onward.
.TP
.B SREJ
Acknowledges reception up to a given point in the window, and
indicates that a single frame after this point was rejected,
and asks for that frame to be resent.
.TP
.B DM
Responds when a connection-mode Command is sent to an Address
that is not currently connected.  Generally used to reject
various forms of negotiation.
.TP
.B TEST
Used to pass bytes of the communication channel and request that
the same bytes are returned.  This can be used to test the
pristine arrival of bytes that might be endangered with
mangling (such as when A-law converts to μ-law and back to A-law).
.P
It is possible to write a kind of "HDLC assembler" language that
explicitly communicates these commands and injects them into a
codec.  See
.IR sublime-asm (1)
and
.IR sublime-inject (1)
for details.  Likewise, HDLC frames may be extracted and dumped
with
.IR sublime-extract (1)
and
.IR sublime-disasm (1)
programs.
.SH CODECS
Generally, codecs start in a mode called bit-stealing, where they
leverage bits that fall under a noise floor.  It may be possible
to switch the entire connection over to Subliminal Messaging, and
dispense with voice completely, by switching to byte-stealing
mode.  Subsequently, voice can be transferred (in any desired codec)
using HDLC frames.
.P
The following telephony codecs offer space for data, and more may
be added at any time.
.TP
.BR A-law " or " PCMA
and the similar form
.BR μ-law " or " PCMU
are imposed by the Telephony backbone and, as a consequence, in the majority
of SIP calls.  The A-law variant is supported for slightly better technical
properties.
.RS
.P
Samples are represented as a floating-point number, with a mantisse
value that sounds as soft or as loud as the exponent indicates.  Bits
are cut off at a noise level; anything above is replicated faithfully
as audio data, the rest is used for data.  Some data bits are avoided
usable however, because they may be mangled when A-law converts to μ-law
and back to A-law.
.RE
.TP
.BR G.722
is a better-quality codec, but since it is not signaled by Telephone
switches it ends up being mostly ignored.  The lower 2 bits of each
sample are reserved for data, and that is what Subliminal Messaging
uses.
.TP
.BR CLEARMODE
is a mode for Telephone lines to pass data without any mangling.  This
allows the byte-serial transfer of HDLC messages, and is considered
the most pleasant manner of using Subliminal Messaging over the
Telephone network.  Other modes can switch to this mode (or a close
approximation) if the codec supports such changes.
.SH "EXAMPLE SERVICES"
The following is a list of imaginable services.  The identities
are deliberately based on DNS-names, which anyone can allocate
under their own domains, and mapped into a UUID that may also come
from other sources.  This makes it straightforward to add a
service for Subliminal Messaging without risk of clashing with
others.
.P
\fBGoal:\fR SubliMe opportunistic service signaling
.br
\fBName:\fR sublime.0cpm.org
.br
\fBUUID:\fR d14e63c6-3a3a-3b2b-8d9a-12140fa1b385
.br
\fBPver:\fR 1.0
.br
\fBUstx:\fR Full signatures on the most recent full second on the outer
.br
      codec; UI commands may be sent to Address 0x00 without active
.br
      connection.
.br
\fBParm:\fR Service Parameters bytes follow
.br
\fBInfo:\fR The outer codec is split into 1 second worth of samples, rounded
.br
      down to an integer.  This starts after the last BREAK.  Before
.br
      the next second, the signature must be transmitted to allow the
.br
      other side to verify outer codec integrity.
.P
\fBGoal:\fR Sharing contact information
.br
\fBName:\fR ietf-vcard.sublime.0cpm.org
.br
\fBUUID:\fR b038dd65-9d02-373d-92a2-d99bd6f625bb
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Textual, "vcard" grammar in Section 3.3. of RFC 6350.
.P
\fBGoal:\fR Calendaring actions
.br
\fBName:\fR ietf-itip.sublime.0cpm.org
.br
\fBUUID:\fR 793fad76-3725-3c23-af8b-eb0dbce103d6
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Textual, formatted as in Section 3 of RFC 5545.
.P
\fBGoal:\fR Facsimile transmission
.br
\fBName:\fR itu-t38.sublime.0cpm.org
.br
\fBUUID:\fR b0725c13-01d7-358d-b099-19fd72233c53
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR One HDLC frame as defined in ITU T.38
.P
\fBGoal:\fR Realtime text communication
.br
\fBName:\fR itu-t140.sublime.0cpm.org
.br
\fBUUID:\fR 6d7bf8a4-6054-318c-b3ab-0ebc41d54e3d
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Any number of whole characters as defined in ITU T.140
.P
\fBGoal:\fR Telephone-compliant Short Messaging
.br
\fBName:\fR org-smpp.sublime.0cpm.org
.br
\fBUUID:\fR 818212af-821e-36ac-a61b-7369b6a44c15
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Continuous flow following the SMPP protocol
.P
\fBGoal:\fR Telephone-compliant Multimedia Messaging
.br
\fBName:\fR etsi-mms-mm4.sublime.0cpm.org
.br
\fBUUID:\fR 269a5933-c9cf-3807-abf1-3af4804c2769
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR MM4 email as specified in ETSI TS 123 140, but every
.br
      dot on the start of a line is prefixed with another
.br
      dot, and the email is followed by a dot on a line of
.br
      its own (like for the SMTP DATA command).
.P
\fBGoal:\fR Realtime interaction with XMPP
.br
\fBName:\fR ietf-xmpp.sublime.0cpm.org
.br
\fBUUID:\fR 8affc68e-7819-3c18-864e-dcb888a26cbb
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR One XMPP stanza as defined in RFC 6120
.P
\fBGoal:\fR Remote database access
.br
\fBName:\fR ietf-ldap.sublime.0cpm.org
.br
\fBUUID:\fR a22ff2c0-b7f8-3f0c-897b-455fb14e8211
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR One LDAPMessage as defined in RFC4511
.br
\fBInfo:\fR Note that LDAP may be used bidirectionally
.P
\fBGoal:\fR Document sharing by name
.br
\fBName:\fR community-zmodem.sublime.0cpm.org
.br
\fBUUID:\fR c5375679-bc7a-3506-bcd3-f57fad341593
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Continuous flow adhering to Z-Modem specifications
.P
\fBGoal:\fR Remote text terminal
.br
\fBName:\fR arpa2-tty.sublime.0cpm.org
.br
\fBUUID:\fR 55f0fbe1-c230-3430-944e-d24b68b05c18
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Continuous flow of UTF-8 bytes with mulTTY extensions
.P
\fBGoal:\fR Remote desktop access with VNC/RFB
.br
\fBName:\fR ietf-rfb.sublime.0cpm.org
.br
\fBUUID:\fR 081a98f4-883f-3042-adbc-661c8e14ecf1
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR One RFB protocol message as defined in Section 7 of RFC 6143
.P
\fBGoal:\fR Remote device control over Modbus
.br
\fBName:\fR org-modbus.sublime.0cpm.org
.br
\fBUUID:\fR ecb81231-643a-39af-97bf-80f9b0f664e4
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR One frame of Modbus TCP
.P
\fBGoal:\fR KIP Document exchange
.br
\fBName:\fR arpa2-kipdoc.sublime.0cpm.org
.br
\fBUUID:\fR 7cc50f02-4d3b-36f2-8991-b964b0a31c49
.br
\fBPver:\fR 0.0
.br
\fBIstx:\fR Streaming content forming a KIP Document.
.P
Note that it is a matter of good style to document unambiguously what
the communicated data format is, and references to existing standards
documents are very suitable for doing that without much effort.  Do
aim to be precise, to avoid discouraging alternative implementations.
.SH AUTHOR
Written by Rick van Rein of OpenFortress.nl, for the 0cpm project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/0cpm/subliminal/
project page.  We use its issue tracker to track bugs and
feature requests.
.SH COPYRIGHT
Copyright \co 2022 Rick van Rein, 0cpm.
.PP
This project was kindly funded by NLnet Foundation.
.SH "SEE ALSO"
.IR sublime-silence "(1), " sublime-ringback "(1), "
.IR sublime-send "(1), " sublime-recv "(1), "
.IR sublime-inject "(1), " sublime-extract "(1), "
.IR sublime-asm "(1), " sublime-disasm "(1), "
.IR sublime (3).
