.TH Wireguard-SIP 1 "October 2022" 0cpm "User Commands"
.SH NAME
wgsip-connect, -disconnect, -server \- Wireguard setup with SIP
.SH SYNOPSIS
.B wgsip-connect "   \fR[\fB-c\fR \fIconffile\fR]       \fIprofile\fR..."
.br
.B wgsip-disconnect "\fR[\fB-c\fR \fIconffile\fR]       \fIprofile\fR..."
.br
.B wgsip-server "    \fR[\fB-c\fR \fIconffile\fR] [\fB-f\fR] [\fIprofile\fR...]"
.P
.SH DESCRIPTION
The tool
.B wgsip-connect
creates a Wireguard secure tunnel, using SIP and SDP to dynamically
negotiate parameters.  The remote end is assumed to run the
.B wgsip-server
or a comparable implementation; with \fB-f\fR the server runs in
the foreground and prints errors.  Connections can be torn down using
.B wgsip-disconnect
in collaboration with the server.
.P
In addition to the manual configuration option for Wireguard tunnels,
these utilities make it possible to create "dial-in" connections for
tunnels.  Given a flexible server configuration, it is even possible
to dialin to hitherto unknown networks.  Whether the server wants to
be so open is generally subject to access control configuration.
Flexibility can also help to support known clients which may have
dynamic addresses, such as roaming users.  To set a new address,
the client can simply run
.B wgsip-connect
again.
.P
The configuration details are usually obtained from one or more
\fIprofile\fR name.  The
.B wgsip-connect
and
.B wgsip-disconnect
must provide at least one \fIprofile\fR name to work on, while the
.B wgsip-server
defaults to support for all profiles it finds, with the exclusion of
any that are incomplete or syntactically wrong.
.P
.SH PROFILES
Profiles are loaded from the \fIconffile\fR, which may be specified with
the \fB-c\fR option on the commandline.  Without that, the default
\fIconffile\fR is
.B ~/.config/0cpm/wgsip.conf
or else
.BR /etc/0cpm/wgsip.conf .
The \fIconffile\fR is structured with INI file syntax, where the
section name matches against \fIprofile\fR names in a case-insensitive manner.
.P
Each section defines a number of profile-specific variables:
.TP
.BR LocalName " and " RemoteName
are used to populate and match the From: and To: headers in a SIP message.
When used in Subliminal Messaging they may alternatively be set to e.164
phone numbers.  When \fBRemoteName\fR is not specified it will match anything.
.TP
.BR TunnelIfName
is the name of the Wireguard tunnel interface to be managed by these programs.
.TP
.BR OuterAddress
is an IPv6 address that serves as the Wireguard encrypted service address.
The address (and port) may be dynamic, and need not be local to the host
setting up the connection over SIP/SDP.  There is no way to pin the remote
OuterAddress, because the security does not rely on such fixations.  The
most important reason to setup with SIP/SDP is to support such dynamicity.
This setting is optional; it defaults to a routable source address.
Setting it is useful on multi-homed networks.
.TP
.BR InnerPrefix
is used to configure the tunnel's IPv6 prefix.  The value is sent as part
of the SDP message from this profile.  IPv6 enables independent allocation
of prefixes without clash.
.TP
.BR PskMethod
can be set to a method name and possibly parameters for preshared key
negotiation.  The value is sent as part of the SDP message from this
profile.  The only value currently supported is
.BR none ,
to forego this process.
Future extensions will likely derive keys
from authentication exchanges, notably
.B sxover
for the SASL mechanism
.B SXOVER-PLUS
that implements Realm Crossover.
.TP
.BR Endpoint
is an IPv6 address and UDP port that serves as the Wireguard encrypted
service.  The address and port may be dynamic, and need not be local to
the host setting up the connection over SIP/SDP.  There is no way to
pin the remote \fBEndpoint\fR, because the security does not rely on
such fixations.  The most important reason to setup with SIP/SDP is to
support such dynamicity.
.br
Generally, IPv6 is the better protocol for peer-to-peer applications.
This definitely includes Wireguard, where the design protects privacy
by being silent when there is no traffic to be sent.  Doing similar
things on IPv4 is possible only with port forwarding in a NAT router.
If you need to clamp at the straws of IPv4 connectivity then you can
represent an IPv4 address in IPv6 by prefixing it with 96 zeroed bits.
In doing so, please realise that your remote will be forced into a
similar backward-compatibility trickbox, and that direct peering is 
truly supported by IPv6; acquiring an IPv6 tunnel is the better option,
because it imposes no constraints on remotes.
To keep an IPv4 or IPv6 firewall open, socketSYNergy may be used to
avoid manual configuration while avoiding pings along the full route.
.TP
.BR ListenAddress
is the IPv6 address and UDP port at which
.B wgsip-server
listens for incoming SIP/SDP requests.
.TP
.BR ProxyAddress
is the IPv6 address and UDP port for an optional outbound SIP proxy.
.P
.SH SCRIPTS
The Wireguard updates are processed using scripts.  These may be
easily adapted to a local situation, include user names and use of
.IR sudo (8).
The scripts are stored in the installation-prefixed directory
\fBshare/wgsip/\fR which holds the following scripts:
.TP
.B have-wg \fIv6addr\fR \fIinterface\fR \fIinneraddr\fR
.IP
should enter a context to which IPv6 address \fIv6addr\fR routes, and if the
\fIinterface\fR is not known there, run
.IR ip (8)
and
.IR wg (8)
as follows:
.IP
.BI "ip link add " interface " type wireguard"
.br
.BI "wg genkey | \e"
.br
.BI "wg set " interface " private-key /dev/stdin"
.br
.BI "ip -6 addr add " inneraddr " dev " interface
.br
.BI "ip link set up dev " interface
#	ip link set up dev $interface
.TP
.B show-wg \fIv6addr\fR \fIinterface\fR \fIvariable\fR
.IP
should enter a context to which IPv6 address \fIv6addr\fR routes, and run
.IR wg (8)
as follows:
.IP
.BI "wg show " interface " " variable
.IP
where \fIvariable\fR is a Wireguard variable name, such as \fBpublic-key\fR
or \fBlisten-port\fR.  The possibility that \fBshow-wg\fR re-runs without
setting a new key makes it more robust to extract information from this
command than to assume what would have been done.
.TP
.B drop-wg \fIv6addr\fR \fIinterface\fR
.IP
should enter a context to which IPv6 address \fIv6addr\fR routes, and if the
\fIinterface\fR is known there, run
.IR ip (8)
as follows:
.IP
.BI "ip link set down dev " interface
.br
.BI "ip link del " interface " type wireguard"
.TP
.B set-peer \fIv6addr\fR \fIinterface\fR \fIpinkey\fR \fIv6endpoint\fB \fIv6prefix\fR
.IP
should enter a context to which IPv6 address \fIv6addr\fR routes, and run
.IR wg (8)
and
.IR ip (8)
as follows:
.IP
.BI "wg set " interface " peer " pinkey " preshared-key /dev/stdin \e"
.br
.BI "       allowed-ips " v6prefix " endpoint " v6endpoint
.br
.BI "ip -6 route add " v6prefix " dev " interface
.TP
.B del-peer \fIv6addr\fR \fIinterface\fR \fIpinkey\fR
.IP
should enter a context to which IPv6 address \fIv6addr\fR routes, and run
.IR wg (8)
as follows:
.IP
.BI "wg set " interface " peer " pinkey " remove"
.P
.SH STORAGE
A few items will be stored in the
.BR /var/lib/wgsip
directory, using files whose name includes the profile.
.TP
.IB profile .pinkey
stores the last accepted public key.  This is used to detect changes,
both under trust-on-first-use policies and when the \fIprofile\fR
specifies a \fBPinningKey\fR variable.  This file is not
removed as a result of
.B wgsip-disconnect
processing.
.TP
.IB profile .dialog
stores the identity of a new dialog after its first successful
.B wgsip-connect
operation; this identity is then used for any subsequent
.B wgsip-connect
or
.B wgsip-disconnect
invocations.  After successful disconnection, it is removed.
.TP
.IB profile .profile
stores the profile information, possibly updated with dynamic
information for the \fBRemoteAddress\fR, for the last successful
.B wgsip-connect
invocation and until a successful
.B wgsip-disconnect
invocation.  This file may be specified after \fBwgsip-connect -c\fR
to trigger a reconnection, such as after a reboot, when an address
or port has changed, or when new keying is necessary (such as after
the loss of a preshared key after reboot).
.P
An example configuration file with extensive information is included
with the distribution.
.P
.SH SECURITY MODEL FOR REALM CROSSOVER
This work can be used to setup connections dynamically, even to
hitherto unknown peers.  The high level of dynamicity also works
well for known peers.  In both cases, tunnels are relatively
simple to setup.  Simplicity and dynamicity raise questions about
security, which is not sacrificed at all.
.P
The underlying model is one for Realm Crossover, as worked out
by the InternetWide.org project and summerised in an IETF document
.IR draft-vanrein-internetwide-realm-crossover .
The security hinges on SASL, with mechanics worked out in
.IR draft-vanrein-diameter-sasl
and specifically the
.BR SXOVER-PLUS
mechanism that implements Realm Crossover.
.IP
(TODO: Besides SXOVER-PLUS, Kerberos would also work, at least in theory,
if it uses Channel Binding and supports key export.  An approach for
Realm Crossover with Kerberos has been worked out already.  This would be
highly efficient and very difficult to combine in a Quantum Computing attack.)
.P
The negotiation with
.BR SXOVER-PLUS
can provide mutual authentication, which means that the
.BR LocalName " and " RemoteName
identities are validated on both ends.  The parties now know who
they are talking to, and may apply access control.  As a result,
the authentication exchange becomes a basis for secure decisions,
and it already involves cryptographic key material.
.P
Beyond the identities being authenticate, there is a facility for
Channel Binding in SASL, and it is supported (even required) in the
.BR SXOVER-PLUS
mechanism.  It sequences the Dialog ID (a combination of the
Call-ID header value, the From: tag and To: tag) as well as the
SDP offers from both ends to form a Channel Binding hash.  This
hash is part cryptographically added to the authentication, so
after that succeeds it is certain that those values cannot change.
That includes the public key material that is being setup, but
also the
.BR OuterAddress .
.P
Another option built into the
.BR SXOVER-PLUS
mechanism is key derivation.  This is used to derive a preshared
key for the tunnel.  Such keys are relevant in the mitigation of
Quantum Computer attacks when the material is based on symmetric
keys; they perturb the elegance of ECDH mechanisms beyond the
analytical facilities of such computers.  (TODO: There is a manual
option to identify additional entropy.)  The infrastructure for
.BR SXOVER-PLUS
is a symmetric-key infrastructure, but at some point it does rely
on TLS; the assumption is that this will move to Quantum Computer
relief on its own, and from that time on the new dynamic tunnel
would be protected.  For now, the protection rests with the need
to combine traffic from various sources to be able to decipher
the traffic; since the search space is at least quadratic it will
be very costly to do in the onset of Quantum Computers.
.P
In summary, the following assurances are made:
.TP
.B Authentication
The communicating parties validate the remote identity and can
use it for access control.  Unlike SIP over TLS, these are
end-to-end assurances.
.TP
.B Encryption
Uses Wireguard in the strongest possible mode, namely with a
preshared key.  This makes the mechanism an extension of the work
in Wireguard to mitigate Quantum Computer attacks.  Unlike SIP over
TLS, these are end-to-end assurances.
.P
.SH SIP/SDP profile
These tools use SIP and SDP as they are intended.  The session that is
initiated however, is not a media session but it uses generic UDP session
support using the media type \fBapplication/vnd.wireguard\fR.  This supplies
UDP address and port information to end points, which can then continue
transmitting UDP frames without RTP headers or RTCP control frames.
This is precisely what Wireguard uses.
.P
The SIP commands used are INVITE in
.B wgsip-connect
and BYE in
.BR wgsip-disconnect .
Authentication is normally requested with a 401 Unauthorized response
and successful setup is answered with 200 OK with an SDP body.  There
is room to delay the 200 OK and first send 183 Session Progress with
an SDP body, then use an authentication protocol such as 802.1x and
report the success as 200 OK.  Negative responses are also supported,
with 404 Not Found as a good example when attempting to connect to an
unknown \fBRemoteName\fR identity.
.P
SIP has two scopes in which messages belong together.  A dialog is
defined by the Call-ID: header, the From: tag and (once responded)
the To: tag.  These are long-living identities that need no active
programs and may even survive reboots thanks to stored information.
This is how it possible to call
.BR wgsip-connect ,
then reboot and continue working, until
.BR wgsip-disconnect
ends the tunnel.
Transactions are shorter lived message sequences, are include the
ever-increasing CSeq: header number as part of their identity.
A single INVITE request and its responses and final ACK are known
as one transaction; a single BYE is another transaction.  There
is no reason to keep a program running between transactions, and
a Wireguard tunnel typically does not accumulate calling minutes
so we can drop the guard.  It is however desirable if at least the
server detects a BYE request, which is why the
.B wgsip-server
normally keeps running, but it too can continue with settings that
were stored before a reboot (at least when its \fBPskMethod\fR
was \fBnone\fR).
.P
New about these SIP applications is their reliance on SIP for
authentication.  Specifically, the option of using this with the
SXOVER-PLUS mechanism for Realm Crossover, another element in
support of security for spontaneous connections.  This mechanism
can settle a preshared key for use in Wireguard when the profile
specifies \fBPskMethod = sxover\fR.
.\" .P
.\" .SH SUBLIMINAL MESSAGING
.\" TODO: It WILL BE possible to listen for traffic via Subliminal
.\" Messaging and exchange keys over another kind of connection.
.P
SIP generally follows the HTTP framework for authentication, but
it is much better organised because SIP messages can be related
with their contained identities.  In line with the Internet Draft
.IR draft-vanrein-httpauth-sasl
it is feasible to add advanced authentication methods to SIP,
which is what these tools do.
.P
.SH AUTHOR
Written by Rick van Rein of OpenFortress.nl, for the 0cpm project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/0cpm/subliminal/
project page.  We use its issue tracker to track bugs and
feature requests.
.SH COPYRIGHT
Copyright \co 2022 Rick van Rein, 0cpm.
.PP
This project was kindly funded by NLnet Foundation.
.SH "SEE ALSO"
.IR wg "(8), "
.IR subliminal "(7)."
.br
.IR https://gitlab.com/arpa2/socketSYNergy
.br
.IR https://datatracker.ietf.org/doc/draft-vanrein-internetwide-realm-crossover/
.br
.IR https://datatracker.ietf.org/doc/draft-vanrein-diameter-sasl/
.br
.IR https://datatracker.ietf.org/doc/draft-vanrein-httpauth-sasl/
