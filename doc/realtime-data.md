# Realtime Data with some Voice Suppression

> *The normal mode for Subliminal Messaging travels in minor bits
> of an audio signal.  In some cases, it may be interesting to
> also have an option to dampen the voice to allow more room
> for the data.  This may bring realtime performance, such as
> may be necessary for fax or other modem signals.*

Measurements on A-law codecs show:

  * Talk at the loudest volume allows about 17280 bits/sec raw data.

  * Talk at softer volumes allows about 28800 bits/sec raw data.

There are a few possible data rate improvements for A-law codecs:

  * Talk is usually half duplex, and no voice leaves room for up to
    32000 bits/sec raw data.

  * It might be possible to drive the sound level of voice up (for
    better signal/noise ratio) or down (for better throughput).

  * Every sample can carry a minimum number of bits if the sound
    does not go beyond the matching voice level.

  * A few sound values reduce the number of bits due to A-μ-A
    translation; knowing that this is absent makes the bits fully
    usable, or we may modify the audio sample to avoid these codes,
    at the expense of sound quality.

It should be possible to sustain a 14400 bits/sec rate with a fixed
number of bits per sample, in cases where this is required, such as
for realtime modem or fax transmission.  This would take an estimate
of 25% of the raw rate of an ISDN codec.  Such rates are commonly
available for G.722 (two lower bits are reserved) and having it for
G.711 or ISDN codecs could be very useful as a design alternative.

This leaves us with two approaches to transmission:

  * Opportunistic data insertion, using the mantissa when it is
    available and sacrificing symbol space as a result of possible
    A-μ-A translation.

  * Realtime data insertion, modifying the sound is such a way that
    enough room is available in every sample.

The choice between those alternatives can be made in the transmitter;
for the receiver it all looks the same.  (Even the translation risk
might be obviously circumvented with a good choice.)


