# Assembler for HDLC traffic

> *Is it an idea to write a simple "assembler" for HDLC commands?*

The general HDLC command consists of

```
+---------+---------+-----//-----+---//---+
| Address | Command | Information| Check  |
+---------+---------+-----//-----+---//---+
```

The Address is one hex byte only, and can be provided as a number, or the last
one might be repeated implicitly.  If there is no last, use `00` instead.

The Command has a mnemonic, and is split into three kinds of frames:

  * `I` is an I-frame for acknowledged data transfer
  * `RR`, `RNR`, `REJ`, `SREJ` are S-frames for supervisory data
    for ack/nak and flow control in the reverse direction of `I`
  * `UI`, `XID`, `SABM`, `DISC`, `I`, `TEST`, `UP` are U-frames
    which are sent rather blindly, may drop off the line, and may be
    resent when no response arrives in reasonable time

Commands may carry additional data, but we need not annotate that, ever:

  * `N(S)` is automatically added to I-frames
  * `N(R)` is automatically added to I-frames and S-frames
  * `Poll/Final` is not used in our version of HDLC

Variations on the Command could be Macros, such as

  * `.KEY` to setup Information as a key for the active Address *has an impact on new SABM/DISC pairs and intermediate frames using their Address*
  * `.UNIDIR` to transmit data without expectations of confirmation *has an impact on continued window counting*
  * `.BREAK` to insert a BREAK  intermediately, or at the end

Information is optional.  It may be supplied in various forms, but hex digits
make most sense.  Spaces, dots and dashes could be optionally inserted.  Strings
may be useful for debugging only.  Prefix hex bytes with `#` for implied form,
as in assembly, and allow `<` for the insertion of file data, possibly
auto-splitting if the size exceeds 256, and then auto-ending with a frame sized
255 or less.  Likewise, allow `:` for the insertion of TCP data from the port
number that follows (in decimal), at the `::1` address.  Also allow `"..."` for
strings.

Check is automatically computed by the CRC-16/6sub8 algorithm or, if crypto
is enabled, by the cryptographic routine.


## Output Format

The output format would be a *flow of HDLC frames* which essentially is what
a codec does.  If we wanted a representation it might be the CLEARMODE codec
in bit-stealing mode (which is the same as byte-streaming mode, but before
the explicit switch that would give).

Two tools can be used to manage HDLC assembly, namely `sublime-inject` as the
assembler and `HDLC-extract` as a disassembler.  They would work like `tee`
does, side-channeling the HDLC assembler file while passing A-law data from
`stdin` to `stdout` without change.

A feature-full disassembler might make sense of the contents of an XID command
by showing the UUID as a DNS name, explaining bits and MRU, and so on.  It could
also do clever things with Information fields, such as showing text.


## Examples of HDLC Assembler

Saying "Hello World" to a terminal client:

```
;
; Connect to the SubliMe service
00	XID	#03 #10 #0000 #d14e63c6-3a3a-3b2b-8d9a-12140fa1b385

;
; Connect to a Remote text terminal as a server / program
23	XID	#02 #00 #0000 #55f0fbe1-c230-3430-944e-d24b68b05c18
	SABM
;
; Send a data frame, like an echo statement on a server would
	I	"Hello World" #0a
;
; Disconnect from the Remote text terminal as a server / program
	DISC
```

Pushing a vCard down the caller's drain:

```
;
; Do not assume that acknowledgements will be sent back
	.UNIDIR
;
; Connect to the SubliMe service
00	XID	#03 #10 #0000 #d14e63c6-3a3a-3b2b-8d9a-12140fa1b385
;
; Send out a vCard, perhaps alongside ringback signaling
7a	XID	#02 #00 #0000 #b038dd65-9d02-373d-92a2-d99bd6f625bb
	SABM
	I	<moi.vcard
	DISC
```

