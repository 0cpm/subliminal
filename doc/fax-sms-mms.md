# Fax, SMS and MMS

> *The HDLC format welcomes many kinds of generic data flows.
> It can also incorporate a few things that now use modem signals
> and are terribly incompatible with VoIP transmissions.*


## SMS submits PDU frames

The format of
[PDU for SMS](https://www.gsmfavorites.com/documents/sms/pdutext/)
is just a sequence of bytes.  It can be transmitted to its own
HDLC address.

There is a widely accepted
[SMPP protocol](https://en.wikipedia.org/wiki/Short_Message_Peer-to-Peer)
with
[more detail here](https://smpp.org/#smppoverview)
to transfer SMS with SMSC but also different entitites.  This is
an open industry standard, later incorporated into ETSI standards.
An implementation might be
[OpenSMPPBox](https://github.com/pruiz/kannel-opensmppbox)
or as an alternative
[ksmppd](https://github.com/kneodev/ksmppd)
or one might consider
[kannel](https://kannel.org/)
as an SMSC, adding HDLC as an alternative carrier mode.

In all honesty, SMPP does not look too difficult and may be
easy enough to add if we need to.  It is a surprise that the
predictable idea of making SMPP available for online use via a
component for XMPP has not been built yet.  On top of the "mkroot"
contrib for a SIP Trunk, this could be an SMS facility connecting
to PSTN but quickly moving it into our own domain.

We may use our own number as the SMS Centre.  This constitutes an
improved level of freedom.  Messaging can be directly transported
between numbers, without intermediation by a telecom operator.
As a result, it can also be run over direct connections over the
Internet, such as between domains or to an ENUM recipient.

## MMS submits HTTP messages

The format of
[MMUS messaging over HTTP](https://www.25yearsofprogramming.com/series-60-platform/mms-protocol-data-unit-structure.html)
is a standard for a particular content type.  It could easily be
carried over other channels too.

## FAX runs over HDLC frames

The frames submitted for fax involve HDLC frames, and there are
signaling conditions based on sound to connect the faxes.  There
is a side-stepping procedure in T.37 for store-and-forward facsimile,
also known as fax-over-email.

The so-called Simple Mode in T.37 neither requires capabilities
exchange, nor confirmation of receipt (!) but leaves those open
as optional extras outside the T.37 scope.  This reduces the legal
value of this mode of operation.

The so-called Full Mode in T.37 added in 1999 incorporates email
[Delivery Status Notification](https://datatracker.ietf.org/doc/html/rfc1894)
and
[MDN](https://datatracker.ietf.org/doc/html/rfc3503) standards.
There is a list of RFCs related to Full Mode for Internet fax.

We can also map the fax HDLC format onto one of our streams.
It does not have to use the same address (255) as in T.30.
All we seem to have to do is setup the email variant.  The
difficulty here is to get legal status from delivery if it ends
up being a permissive guesture by the recipient.

It also seems possible to capture an entire fax exchange but
bypass the tonal troubles due to fax with/out T.38 protocol.
This is more likely to give immediate feedback, as part of the
transmission protocol, that the fax has properly landed at its
destination.  Whether it is read is not part of the information
shared, but that is a privacy matter; and it is the recipient's
responsibility after receiving the fax message.  All that a
sender cares about is usually that reception of the precise form
cannot be denied, and for that a successful transmission report
should suffice.  It may help if such transmissions are secure.

  * T.30 defines HDLC frames and the use of FLAG bytes between
    frames, as well as to train echo suppressors.  The HDLC frames
    are normally `UI` frames (written in reverse bit order) with
    internal structure.  HDLC addresses are always 0xff for use
    on PSTN, but we could digress.  The Poll/Final bit marks the
    last frame in a sequence.

  * Over analog lines, or ones acting like analog lines, there are
    procedures based on recognisable tone signals and switching to
    a V.34 standard modem at 300 bps speed (or a range of options).

  * See [analysing a basic fax call](https://www.ccexpert.us/voice-gateways/analyzing-a-basic-fax-call.html)
    for a more graphical demonstration and guide to T.30 fax.

  * The T.38 specification encodes tone signals as logic data, or
    more precisely there are `T30_INDICATOR` codes and `T30_DATA`
    forms, which then move over RTP or UDPTL.  This makes it all
    less criticial of timing (more than 1 ms jitter can devastate
    modem signals when they are passed as audio).

  * See [T.38 fax relay](https://www.ccexpert.us/voice-gateways/t38-fax-relay.html)
    for a more graphical demonstration and guide to T.38 fax.

  * For ISDN, addendum C defines the frame format for the B-channel.
    This eradicates the needs for tonal negotiation and modulation.
    It involves an `XID` mechanism defined in F/T.90.  This replaces
    the tone signaling used on analog/voice channels.  Indeed does
    T.90 specify `XID` bits to use, plus Appendix I for facsimile.
    X.25 in Appendix B is a counter-example of fax.  These seem to
    be run over D-channels, to setup the right type of connection.

  * Further fax signalling seems to sit within the `UI` field, for
    ISDN as well as for tonal calls.  It uses non-standard commands
    (sometimes with overlapping names and meaning, like `RNR`) but
    does not exploit the windowing capabilities of HDLC, which is
    in line with the use of `UI`.

  * In conclusion, HDLC is only beneficial to fax for determining
    a frame size and perhaps the CRC; beyond that, all the work
    is redone by fax protocols.  We can carry it but fax is quite
    complex, not trivial to add.  We should hope to add expertise
    from the fax community.  What we can still add there would be
    cryptographic assurances; this might improve/modernise the
    legal status of fax; it is valuable that this medium has no
    scripts, overlay tricks, colours or support for fine print.
    Images are indeed pretty good for contracts (as is ASCII).

  * In conclusion, T.38 would be a very good option to pass over
    the HDLC channels that we make.  The downside is just that our
    timing is unpredictable when talking is loud, as is the case
    with HDLC frames; but a common option is a 14k4 fax modem,
    which matches what Subliminal Messaging can do (on average).
    Resending is always possible, and is common for facsimile.
    Plus, there is always the option to stop talking during the
    fax transmission.  People learn such things with time.

Some tools of interest are:

  * [HylaFAX](https://www.hylafax.org/)
    is an old and established open source product for faxing.
    It has queues and assumes a modem pool.  Modems however,
    can be simulated over T.38 or even T.30 over a voice call.
    There is an extensive
    [handbook](https://www.hylafax.org/hylafax-handbook/),
    [receiving with `faxrecvd`](https://www.hylafax.org/hylafax-handbook/server-operation-2/receiving-faxes/),
    [sending with `faxsend`](https://www.hylafax.org/hylafax-handbook/server-operation-2/sending-faxes/)
    and generally the
    [`hfaxd`, `faxgetty` are general server components](https://www.hylafax.org/hylafax-handbook/server-operation-2/understanding-your-server/).

  * [t38modem for HylaFAX](https://github.com/hehol/t38modem)
    which simulates a modem that sends over SIP.
    It produces `/dev/ttyx123` devices that offer the
    Hayes AT commands for a fax modem.

  * [t38modem for SpanDSP, Asterisk](https://github.com/michaaa/t38modem)
    is another with the same (somewhat predictable) name.

  * [Gofax.IP](https://github.com/gonicus/gofaxip)
    is a Hylafax backend for Fax over IP.  Does T.38 and the
    more endangered T.30 over G.711.
