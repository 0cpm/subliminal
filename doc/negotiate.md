# Negotiation of Services for Subliminal Messaging

> *We may address a service without knowing it is there, and get feedback.
> Or we may negotiate to get the best, that is, more than the default.*

There are several things that may need some negotiation.

General things for all addresses/services:

  * Possibly a magic code in the 128-bit form of a UUID
  * Whether a service is offered in the role of client, server, both, or peer
  * Protocol versions (to allow compatibility)
  * Maximum info field size that can be sent
  * MRU of services, and whether they are restricted by the window
  * Whether fragment reassembly is used for the protocol

Default settings require no negotation and apply if XID is not used first:

  * HDLC frames with up to 256 bytes in the info field
  * Services can be tried at their defined addresses
  * Network frames are not reassembled from HDLC frames
  * MRU of services is assumed to be 7x256 = 1792 bytes
  * No services may be assumed on dynamic addresses 0x80..0xfe
  * Services respond to SABM with UA when available, or DM otherwise
  * Contextual SDP, if available, defines payload types for UI addresses

The assumptions are made for each service in isolation.


## Claiming Dynamic Addresses

It is possible to claim a dynamic address, in the 0x80..0xfe, with XID.
The response will confirm addresses that it can accept, or allow the
service at another address.  It may send MRU size 0 to reject.

An attempt to claim a particular address via XID is sent to that
address, possibly including acceptable alternative addresses.
If the other end agrees to the destination address or one of the
proposed alternatives, it will respond with XID to the address
of choice, thereby acknowledging the binding of the requested
service to the response's address.

Negative responses are possible tool.  Instead of replying with
XID, the response could be RD to indicate that none of the
proposed addresses was acceptable, or DM to indicate that the
service is not available.

When two such proposals crossover, they can be easily mistaken
for an answer and response XID.  The difference is that responses
do not offer alternative addresses, but are final about their
address of choice.  When a response give additional choices,
then the lowest overlapping value will be used in a final
response in both directions; each would find the same value.
When no overlap is found, RD is returned in both directions,
and the sides give up or wait a random time before retrying.


## Offering MRU and setting MTU

As part of the XID exchange, an MRU is setup as the total network
packet size that the XID-sending side can handle for this service.
It will usually be 7 times the window size, or less.  The MTU of
may be set lower than this, but will want to accommodate the
network layer above HDLC to transmit its frames in the window
size of 7 times the MTU.

It is not required that the MRU is the same in both directions,
but in most cases they will be locally constrained to the same
size before being setup as MTU.

By default, frames may carry up to 256 bytes in the information
field.  This allows MTU values up to 1792 bytes.  When larger
MTU values are configured, then this implies larger frames are
used.  The largest permitted information field size is 994
bytes.  This allows MTU values up to 6958 bytes in service of
the network layer above.

The maximum size is in the interest of the CRC quality.  The
Hamming distance is 4 up to and including 994 but drops for
higher values.  HDLC frames that carry up to 8 bytes in the
information field, including many of the management frames,
are even protected up to Hamming distance 6.

## Network Packets and Fragmentation for HDLC

The Hamming distance expresses the number of bit faults, but
message length influences the number of random errors that
may occur on the channel.  In that sense, smaller HDLC frames
are less likely to be corrupted without detection.  This is
desirable and forms a reason to send smaller frames.  It is
why the default frame size is set to 256 bytes and why the
small overhead of fragmentation is suggested; fragmentation
is not as costly as for IP because fragments are transmitted
in immediate succession; only during faults would there be
an out-of-order resend, to be handled by the window.

It is possible to send HDLC frames carrying less than the MTU,
even for fragments.  The only rule is that 7 HDLC frames can
be used to recollect the network packet.  An example of this
is transmission of a network packet of 1500 bytes when MTU
is 256.  It takes 6 frames, but sending the MTU causes the
last HDLC frame to be 220 bytes.  The CRC is now applied in
an unbalanced manner.  Instead, the breakup in 6 frames of
each size 250 would have given more even distribution.

Not all services need to offer reassembly.  If not, there
is a fallback option of "streaming" fragments to the service,
in the proper order but with possibly partial messages in the
receiver.  Well-coded TCP applications should not break on
this, but UDP and SCTP are sensitive to message boundaries.


## Initiation of Subliminal Messaging

The UUID for 0x00 is a static bitstring, and it is useful to check
that both ends are indeed speaking in terms of Subliminal Messaging.
It may be considered a way to pierce through the noise level in a
clear tone of voice.  For this reason, it is recommended to start
with XID using this UUID to address 0x00 before doing anything else.

When the other side is ready for Subliminal Messaging, and when it
is not currently sending frames, then it would be polite to respond
with its own XID if it hasn't recently sent it yet.  As long as the
other side has not sent their XID, especially for 0x00, it is not
certain that it is receiving Subliminal Messaging.  For all we know
we might not be heard; we may be no more than noise to our audience.

This uncertainty does not mean that we should sit and wait.  There
are opportunistic situations, or ones that repeat, such as in the
ringback sound from a called phone to its callee.  There may not
be a return channel, even if our ringing-phone sounds arrive in
good order.  Excellent for opportunistic sharing of information
such as key material and challenges for upcoming authentication,
or perhaps data such as a vCard or an SMS service address.

What we should do is direct the other side to sending XID to
bootstrap Subliminal Messaging.  This is done by starting in the
Initiation Mode for HDLC.  This implies responding to SIM with UA,
to any command having Poll set with DM having Final set, and
to any other request with a RIM response.  Once the XID for
address 0x00 is agreed, attempts to connect to services  This is
done for 0x00 as well as for any dynamic service.  An endpoint
receiving RIM in the middle of a connection, should conclude that
the other end has restarted, that any unconfirmed state is lost
and that no connections exist.


## When to include a UUID

The UUID is a unique identifier for a service, and must be used
if the server is not yet declared.  For 0x00 setup, the question
is open if the other side recognises our Subliminal Messaging
until they acknowledged that with their own statement.  So up
to that point the UUID would be included for 0x00 XID messages.
After service 0x00 is established, other services can commence.

For addresses 0x01..0x7f, which are statically assigned, there
is no question about the service identity, and so there is no
UUID defined for any of them.

Addresses 0x80..0xfe are dynamic, with no given UUID until
it has been acknowledged by XID from both ends.  Before that
time, and also after a remote restarts to its initial state,
any attempt to communicate will lead to DM responses.  The
SABM command may be sent, but this would fail with DM too,
until the XID handshake is performed (again).

The address 0xff is commonly used as a broadcast address.
It is not currently used by Subliminal Messaging.

In a one-directional channel such as a recorded message or
the ringback tone sent to a caller, there may not be a
chance to negotiate.  In that case, an opportunistic choice
may be made that any offers are accepted.  So, there is a
need to exchange XID, followed by SABM, but the response
may be assumed.  If these assumptions are wrong then the
data will not arrive.  Note that the commands may be sent
a few times, both handling synchronisation differences on
the endpoints and handling transmission errors.


## XID Information Field Contents

These are the contents of the Information Field.  Some parts are
optional, and depend on protocol progress, or the address used and
whether that address is "taken" on the receiving end.

The following byte formats are appended to form the Information Field
for an XID message:

  * One byte with flags understood by all services:

      - 2 bits with the XID sender's role (client, server, both, peer)

      - 1 bit to announce a UUID for this service

      - 1 bit to announce a different (from current/default) MRU

      - 1 bit to announce a version field for this service

      - 1 bit to announce one or more alternative addresses

      - The MSB is fixated to "0" for distinction from closed/other
        XID formats.

  * A byte with service-specific flags, as defined per service.

  * If announced: The service's UUID in 16 bytes.  This is the service
    that is requested to connect to the address of this HDLC frame.

  * If announced: One byte with the major version in the high nibble and
    the minor version in the low nibble.  These are protocol versions,
    not software versions, and so they should be slow-changing.

    The major version signifies incompatibility, though an implementation
    could offer more than one major version; care should be taken not to
    support old security problems .  Within the same major version, two
    implementations should agree on the lowest minor version on each end.

  * If announced: An unsigned 32-bit integer in network byte order,
    specifying the MRU (in terms of maximum info field sizes to)
    and the maximum size for the HDLC info field for the desired service,
    when transmitting to the sender of this XID service offer.
    This field is an unsigned 16-bit integer in network byte order.

    The maximum HDLC info field size occupies the top 2 bits of this
    field, where values 0..3 encode a size of 8, 64, 256 or 994 bytes.
    This number applies to traffic transmitted to the sender of this XID.

    TODO:RELOCATE: Info field sizes up to 7 are checked
    with HD(6) to resist 6.25% bit errors, while up to 994 bytes are
    checked with HD(4) to resist 0.05% acceptable bit error rate.
    (Even that last number is quite good; bit error rates are
     usually specified as negative powers of ten.)

    The MRU is encoded in the lower 30 bits as a number of bytes.

      - If fragmentation is not offered, then this cannot exceed the
        maximum HDLC info field size.  If it is higher, it will be
        capped to that maximum.

      - If fragmentation is offered, then this cannot exceed the
        maximum HDLC info field size times 7.  If it is higher, it will
        be capped to that maximum.

  * Further bytes, if any: Service-specific configuration data.


## XID for Management Service 0x00

The XID form for service address 0x00 is like the general form, except:

  * It MUST be sent to address 0x00 and specify no alternative address(es)
  * Each side SHOULD start by sending the fixed UUID for Subliminal
    Messaging, namely 45e2b4e8-018b-4efd-8f05-137317273293, to clarify
    protocol support
  * Each side MUST include the major and minor version bytes

The service-specific flags for 0x00 are defined as:

  * 1 bit to announce a list of packet modes that may be used.
  * 1 bit to announce a list of encryption modes
  * TODO: SPEC-MODIFIED

If announced: An additional list of one or more packet modes numbered 0..127,
stored with the MSB set in all but the last packet mode.
The following packet mode codes have been defined:

  * 0 for A-law (advised in Europe and all international connections)
  * 1 for μ-law (only for the national traffic of United States and of Japan)
  * TODO: SPEC-MODIFIED

If announced: An additional list of encryption modes terminated with the
meaningless value 0x00; each encryption mode has four flags in the lower
nibble and a version-specific encryption algorithm number in the higher
nibble.

  * 0xX1 to indicate support for audio encryption
  * 0xX2 to indicate support for audio signatures
  * 0xX4 to indicate support for data encryption
  * 0xX8 to indicate support for data signatures
  * 0x0X encryption with AES-128, signing with TODO:GHASH
  * TODO: SPEC-MODIFIED

In terms of CRC protection, note that the UUID is fixed for this message,
because it will be compared to the one expected value.  It cannot flip bits
without being detected.  So it does not matter for the hamming distance of
the CRC.

The data that may flip is the address, control and checksum and, in the
information field, the 2 sets of flag bits, 2 bytes MRU and 1 version byte,
plus 1 codec byte.
That is a total of 10 bytes.  Given CRC-16-6sub8, that is so small that
it falls under HD(6) with acceptable BER of 7.5% or 1 acceptable
bit error per 13.333 bits.

The certainty of not having run into a bit-flipped message is therefore
very high; the certainty of a literally correct UUID is much higher, at
a 128-bit level that even cryptographers consider indistuinguishable from
certainty.

