# Bit-mode HDLC in the A-law codec

> *Most phone lines use A-law (in Europe) or μ-law (in the US).  This is passed
> as a 64 kbit/sec digital signal.  We can insert bits, if we are careful about
> conversions between A-law and μ-law.*

The G.711 codecs are dated and low in quality, but they are the standard in
telephony circuits, which basically integrate this logic from ISDN.  Modern
VoIP telephony still often uses this codec, which is then transferred into
POTS.


## Sequences of Ones

The distinctive feature of bit-mode HDLC is how long the seuqence of
`1` bits are:

  * `(0)1111111` is a break signal.  It resets during a faulty transmission.
    It needs a `0`-prefixed flag before HDLC commences.
    This is a safe way to start a newly made connection, because it breaks
    off any HDLC frame in transmission.  It is also a good step when no
    data remains to be sent, to avoid processing follow-up information
    as actual data.
  * `(0)1111110` is a flag.  It does not occur in normal data flows.
  * `(0)11111` is an almost-flag.  If it occurs in data, a `0` is stuffed
    after this sequence, and during reception this `0` stuffing is removed,
    after being checked to distinguish from an actual flag occurring.
  * The flag is sent before and after a HDLC frame, and intermediate
    bytes are bit-stuffed with a `0` if necessary.
  * When no data is sent, the original voice signal may pass, except that
    the lowest bit of a potential data word are fixated to `0`, so they
    cannot become part of a flag.  Alternatives that interfere even less
    with the vocal data are possible, at the expense of more computation.

This means that a various states can be distinguished:

  * For being in non-data mode
  * While breaking, the number of break bits left
  * While flagging, the number of flag bits left, possibly including a `0` prefix
  * While on a HDLC frame, the number of `1` bits sent/received, from 0 to 6,
    and possibly working on `0` stuffing

In our implementation, we consider the following for sample interpretation:

  * `paused` mode means that we just started, or encountered a `BREAK`
    and are now chasing for `FLAG` while also open for another `BREAK`
  * `probed` mode means that samples are actively being investigated
  * we will start a `BREAK` on the lowest bit of a fresh sample, so in
    `paused` mode we only need to test the low bit to be `1` to enable
    `probed` mode.
  * when encountering `BREAK` in `probed` mode, we continue that mode
  * when encountering `BREAK` in data mode, we setup `paused` mode
  * when encountering `FLAG` in `probed` mode, we switch to data mode
    by disabling `paused` and `probed` mode

Note that aligning the start of `BREAK` to the start of a frame, and
promising to send `BREAK` before `FLAG` in paused mode, makes it easier
to recognise the transition.  It also helps to suppress noise when not
transmitting data, because only the low bit needs to be cleared.


## Escape Sequences

The bytes in a HDLC transmission can alternatively be ecaped at the byte
level.  The byte 0x7e would be seen as a flag in byte mode, and may be
escaped to 0x7d 0x5e where 0x7d is the escape character.

There is not much documentation about escaping, but it appears to XOR
a byte with 0x20, which is also a common approach to pull ASCII control
codes into the readable area.  Note that this is better than a prefix
character, where the prefix may need to be escaped too (shells...)

Note that this is not an issue in bit-mode HDLC but only in byte-mode HDLC.
Bit-mode uses `0` bit stuffing to avoid flags being raised by data.


## Codec translation

In general, HDLC will not pass through codec translations and must be taken
out before and re-inserted after the translation.  The translation between
A-law and μ-law however, is so straightforward and well-defined that we can
accommodate it.  This is good, because such translations are commonplace in
telephony networks.

The differences between the two variants of the codecs are not very big, but
a translation is required.  This translation gives completely different values
but when returning to the original codec only a few codes changed.  We select
A-law because it has the changes in the higher volume signals, which occur
less frequently (in a half duplex voice connection).

As long as A-law evades codes 26, 28, 30, 32, 45, 47, 63 and 80 it will not
see any changes due to A-μ-A codec translation; same for the negative variants.
These codes would map via 32, 33, 34, 35, 48, 49, 64 and 79 back to codes
25, 27, 29, 31, 46, 48, 64 and 79 so they are one off.  Not bad for audio,
but damaging for data.

These values can actually be used to test for a clear line, that is one that
is not translated:


THIS COULD WORK IN PACKET MODE:

  * Construct a `TEST` U-frame, and include 3 of the 8 codes that are
    sensitive to mapping.  Send it with P/F reset and expect a reply
    with P/F set.

  * This frame runs 5 bytes through the CRC-16 checksum
    [0xac9a](https://users.ece.cmu.edu/~koopman/crc/c16/0xac9a.txt)
    which means that up to 8 bit errors will be detected.  That
    includes a full mapping and may accommodate 3 transition errors.

  * When a `TEST` frame arrives with a bad CRC-16 checksum, try to
    map it back to the sensitive codes and recompute.  Accept it if
    that is the case, but learn that data is received via a mapping.

  * The reply is the payload as it arrived, with a fresh CRC-16
    checksum (the one that was compared to on input).

  * If the incoming reply has a proper CRC-16 checksum, then the data
    represents what the other side got.  If it does not match, try
    the alternative notation and if that works, learn that the data
    was received via a mapping.


THIS COULD WORK FOR BIT-SHIFTING:

  * Always transmit and receive A-law.  If required by any remote, use the
    [standard translation](https://www.itu.int/rec/T-REC-G.711-198811-I/en)
    to μ-law, but internally use A-law.

  * Treat the direct A-A codes and remapped A-μ-A codes as equivalent; do
    not submit different symbols through them.

  * Submit codes that are in danger of being mapped.  Whether they are
    mapped or not, they are considered as the same symbol.

  * Take note of any mapped code that is received.  It indicates that an
    A-μ-A mapping interferes with a literal data channel.  If it does not
    happen and the direct codes are received, then mapping is not taking
    place.

  * Conclusions on a clear channel should be made conservatively.  Since
    it is normal to use the same channel bidirectionally, the state may
    be shared between end points.

  * As soon as a side detects a mapped byte, it changes to the habit of
    sending symbols as mapped codes.  This will trigger the other side
    to learn about codec translation on the channel.

  * A conservative protocol to detect mapping involves having sent a
    byte that is prone to mapping, and win a reasonable time after that
    still receiving unmapped codes from the other side.

  * Once a mapped code is detected, the conncetion is locked into a
    state where it acknowledges mapping.  It will take the other side
    along with it, and they will keep each other locked in that
    conservative state.

  * As a precaution against glitches triggering this lock-down, it is
    possible to delay the change until a frame has its CRC confirmed.

  * A positive confirmation on a frame that holds a potentially mapped
    symbol is an indication that it was received without detectable
    glitches.  When a byte arrives that could have been mapped but is
    not, the conclusion must be that everything is fine.

**TODO:**  Codec rewrites are single-bit changes.  A CRC detects those well.
This may no longer be the case when it occurs in multiple places.
A test message may be constructed for explicit detection, with a CRC code
that maps directly to these codes.  There is a `TEST` command 

