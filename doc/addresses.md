# HDLC Address Registry

> *The meaning of addresses.*

There are a few kinds of information passed:

  * **I-frames** pass acknowledged information, to be relayed locally over
    TCP, UDP or SCTP.  Ports bind and connect symmetrically, so there is
    always just one remote.

    Addresses are allocated below.  The decisive source is the repository
    for Subliminal Messaging.

    Fragmentation is discussed below.  This matters for UDP and SCTP
    protocols, and would not harm TCP protocols.

  * **UI-frames** and **UIH-frames** pass non-acknowledged information, to
    be relayed over RTP.  The remote is usually negotiated over SDP and
    traffic is passed symmetrically to keep firewalls open.  NAT traversal
    is ignored at the HDLC level.

    This is a namespace for values up to 127, representing payload types.
    This suffices for the construction of a simple RTP header.  The only
    data in the UI-frame is the RTP payload.

    Audio frames are quietly dropped in bit-stealing mode, to avoid any
    confusion about audio sources at any time.  Note however that some
    of these frames may carry additional data, such as RealTime Text,
    which may be used as additional data.

    *Note:* SRTP authenticates the entire RTP header, so timing and
    counters are required.  It can be distinguished as `RTP/SAVP` profile
    instead of the plaintext `RTP/AVP` profile.

    *Note:* Certain payloads types are standardised.  This includes the
    A-law and μ-law codecs at payload types 8 and 0.  Others may be
    negotiated over SDP, over the corresponding HDLC payload.

**TODO:** UIH is silly if signing is performed on this data.

Note that S-frames `RR`, `RNR`, `REJ` and `SREJ` always reference a
connection with N(R) counter, so they relate to addresses as used in
I-frames.  Normal U-frames `DM`, `SABM`, `UA`, `RD`, `DISC` and `UP`
also reference a connection.  They only special cases with their own
address space related to RTP are `UI` and `UIH`.


## Addresses for I-frames, S-frames and most U-frames

These are used for sending as well as receiving, in a symmetric fashion.
See above for the exceptions for UI-frames and UIH-frames, which have
their own RTP-specific address space.

The selection is made to allow flexible connections to multiple backends,
such as switched by MIME-type, OID, path, command, ...

Several protocols are peer-to-peer; others are client-to-server, but
mark messages such that the direction can be inferred; yet others use
the channel to derive the client-to-server direction, and this last
class is either setup by sending SARM from client to server, or
encoded to allow both directions to pass over the same address.

### HDLC Management

  * 0x00 is reserved.

### Security

These may extract a key to facilitate cryptographic modes..

  * 0x10 Kerberos
  * 0x11 GSS-API wrapper
  * 0x12 SASL authentication
  * 0x13 Keyful Identity Protocol, KIP Documents (and wrappers)
  * 0x14 TLS extraction
  * HOW? OpenSSH wrapper
  * HOW? ZRTP media wrapper

### Call handling

  * 0x20 SIP for call updates
  * 0x21 SDP for codec change offers
  * 0x22 vCard for business card exchange
  * 0x23 vCalender for scheduling and planning

### PSTN Classics

  * 0x30 Modem ([Hayes AT commands?](https://github.com/wfd3/hayes) or a BBS?)
  * 0x31 Modem over IP ([V.150.1](https://www.itu.int/rec/T-REC-V.150.1-200301-I/en) or MoIP)
  * 0x31 Z-modem protocol (name-based files, like on Windows)
  * 0x32 Fax via T.38 mapped onto HDLC (see HDLC notes)
  * 0x33 RealTime Text via T.130
  * 0x34 SMS via SMPP, mapped directly onto HDLC
  * 0x35 MMS via MM4 (inter-MMSC, each frame is an email message, TODO: size)
  * 0x36 X.25 as the general ITU protocol (see HDLC notes)
  * 0x37 AX.25 as an Amateur Radio protocol (see HDLC and Callsign notes)
  * 0x38 Modbus TCP, one frame in each HDLC frame

**Notes on HDLC protocols:**

  - HDLC frames use their own addresses and control, specific to their service
  - HDLC frames are sent as pure data, not mixed with Subliminal Messaging
  - HDLC frames are stripped from flags, escapes and bit stuffing
  - HDLC frames retain their address, control, info and check fields
  - HDLC frames in this simplified form as carried in I-frame info fields
  - HDLC frames may span multiple I-frames if their size requires this

**Notes on Callsigns:**

  - Your address can be callsign, a short sequence of letters and digits
  - Callsigns are regulated for law enforcement in telecommunications
  - Callsigns are only available to licensed Radio Amateurs
  - Everyone is welcome to take an exam and become a proper Radio Amateur
  - Falsely claiming a callsign is an offense and may cause legal problems
  - If you are not a Radio Amateur there are many other protocols for you

### Applications

  * 0x40 Telnet with UTF-8 and mulTTY extensions
  * 0x41 OpenSSH shell
  * 0x42 OpenSSH SFTP
  * 0x43 Mosh as previously agreed
  * 0x44 Remote Framebuffer (VNC) [to be extended with SASL and Remote Wayland?]
  * 0x45 Remote X11
  * 0x46 Wireguard without its UDP carrier

Gemini? Gopher?

### Internet Enhancements

0x50..0x6f

  * 0x50 XMPP for instant messaging
  * 0x51 AMQP 1.0 for document queueing
  * 0x52 MQTT for subscription data
  * 0x53 LDAP for structured data exchange
  * 0x54 CoAP for loosely defined data exchange

### Payments

  * 0x7\_ *TODO*

### Reserved for Extensions

Dynamic address allocation with XID.

  * 0x00 is used as a "meta" address by Subliminal Messaging
  * 0x01..0x7f is allocated in the table above
  * 0xff is common for you-know-who (point-to-point) or for broadcasting
  * 0x80..0xfe may be requested with an XID-frame extension using a UUID

Details about [Dynamic Address Negotiation](negotiate.md).

## Address Switching and Payload Fragmentation

When the underlying protocol runs over UDP and SCTP, then payloads form
messages, whose boundaries need to be kept.  For TCP it does no harm
either.  Optimal use of the Frame Check Sequence limits HDLC message sizes,
so there is generally a need for fragmentation and reassembly.  Considering
that payload messages arrive as a whole at the transmitting terminal, it
is safe to assume sequentual fragment transmission without disruption, at
best disturbed by resends.

TODO: This is Normal Response Mode; we use Asynchronous Balanced/Response Mode!
The HDLC standard sets the Poll/Final bit to indicate its last frame; it is
called Poll in a command, and Final in a response.  This is the point where
the other side needs to provide feedback on success or failure receiving
the entire frame (earlier indications are possible, but optional).  It is
this Poll/Final bit that triggers sending of a complete frame over any of
UDP, SCTP or TCP.

Might the sending station continue after a half-completed frame, that is,
might it not be aware of having forgotten the Poll bit, then the receiver
can respond properly with SREJ or, ultimately, a complete rejection of
the sequence of HDLC frames.  Given the 8 frames and the maximum size of
each HDLC under optimal CRC usage, there would not usually be any data
that could accidentally be overlooked.  (Not that the sender should ever
assume that a message was sent until it received the acknowledgement.)


## Meta Services at Address 0x00

The address 0x00 is special.  It is used to describe the entire stream.

  * XID is used to negotiate options, such as the MRU and cryptographic
    capabilities.  For address 0x00 it also handles encryption and/or
    signing for voice and/or data.

  * SABM is used to switch the carrier entirely to Subliminal Messaging,
    usually in byte streaming mode.  DISC ends this state and returns to
    bit stealing mode.  The exact meaning depends on the underlying codec.

    Note that UI and UIH frames can be sent to relay audio in a codec of
    choice, including more compact ones.

  * TEST can be used to evaluate channel transparency.  This can be used
    to test if a channel is end-to-end A-law, or possibly has intermediate
    segments of μ-law that require escaping or switching back to the
    bit-stealing mode.

  * I-frames can be used to setup cryptographic options, such as salt,
    counters, message digests, encryption and/or authentication for audio
    and/or data aspects of the flow.  Each I-frame contains exactly one of
    these, with a prefix byte to select the upcoming facility.


### Cryptographic Salt, Counters and Keying

Both senders run their own cryptographic regimen, and inform the other
end about their whims.  The sender should refrain from attempts that
do not match with the XID that its peer sent.  It should not assume
that the peer can do anything before it sees the XID.

Encryption is an application that runs on top of HDLC, and so it does
not use HDLC signaling for negotiating.  The cryptographic operations
are however integrated with the HDLC logic.  TODO: This is confusing
and causes a lot of extra work.  Make an exception for 0x00 instead?

An I-frame has a first byte with four low bit values to indicate purposes,
in fact the same ones as used during negotiation:

  * 0xX1 for audio encryption
  * 0xX2 for audio signatures
  * 0xX4 for data encryption
  * 0xX8 for data signatures

TODO: Version-dependent encryption mode is also selected from a list!  Configure while setting up a key.

The higher 4 bits of this first byte form an action selector:

  * 0x0X to export a key from ...TODO:SECPROTO:MAYBELAST... but hold back on using it
  * 0x1X to derive a key as defined by the mechanism and start using it
      - Adding a salt, or setting an IV and/or counter
      - For AES-128 with TODO:GHASH, set the IV and start counting from 1
  * 0x2X to insert random information (by way of a salt)
  * 0x3X to transmit a signature
  * 0x4X through 0xfX are *reserved*

Following this is a data field if the action selector requires one.
For signatures, this is the MAC in the same number of bytes as the salt.

This is how encryption is done:

  * Counter mode provides a very long XOR pad.
  * These bytes are XORed to the audio bits that remain after bit stealing
    when audio encryption is selected.
  * These bytes are XORed to the other bits when data encryption is selected.
  * Bits in the XOR pad that were not selected for encryption are dropped.
  * UI and UIH frames with codec data are encrypted as part of data, not audio.
  * When switching between bit-stealing and byte-streaming modes, the same
    byte sequence will still be sent and the XOR pad continues running.

This is how data is signed:

  * The HDLC frame sequence is signed, including one intermediate FLAG byte.
  * UI and UIH frames with codec data are signed along with audio, not data.
  * Only the happy flow is signed; resends and requests to that end are not,
    so TODO: REJ, SREJ, RR, RNR, TODO: Poll/Final bits?
  * Append to the MAC one byte with the signature length in bytes
  * Signing starts at the beginning of the HDLC frame that starts signing.
    Any signature that follows runs over the sequence up to the beginning
    of the signing frame.  The hash continues forever, but may be dropped
    and replaced by a new start (at the expense of security).
  * To negotiate a fresh key, setup that key, then sign and start a new
    signing sequence, so the new key is approved by the old one.
  * Be mindful of suppression of signed content, as that may cause a denial
    of service to signatures.  TODO:HOW?
  * When switching between bit-stealing and byte-streaming modes, the HDLC
    frame sequence continues, albeit in another representation.  As before,
    a single FLAG byte sits between HDLC frames, but that is all.  Even
    unacknowledged traffic in one mode could be resent in the other.

This is how audio is signed:

  * The audio stream has the data bits zeroed, and the result is sequenced
    into the MAC algorithm.
  * UI and UIH frames with codec data are signed along with audio, not data,
    but they are only considered in byte streaming mode.
  * Append to the MAC one byte with the signature length in bytes
  * Signing TODO:starts... TODO:ends...
  * To negotiate a fresh key, setup that key, then sign and start a new
    signing sequence, so the new key is approved by the old one.
  * Be mindful of suppression of signed content, as that may cause a denial
    of service to signatures.  TODO:HOW?
  * When switching between bit-stealing and byte-streaming modes, there
    are untouched audio bits in bit-stealing mode and codec frames in the
    other.  As long as no overlap exists between these audio forms there
    is no lack of clarity about ordering the data.

*Note:* Codec data in UI and UIH frames is awkward; it is handled like data
for encryption, and like audio for signing.  TODO:REALLY?

This is how audio and data are signed together:

  * TODO: GHASH can multiply one add the other with XOR.

