# Cryptography for Telephony

> *Encryption can be used for privacy, and signing for integrity.
> Various mechanisms can be used to extract a key.  Various ciphers
> and cipher modes can be used.*

Our first cryptographic approach modifies AES-GCM from an
authenticated block cipher to an authenticated stream cipher.
See [NIST Special Publication 800-38D](https://csrc.nist.gov/publications/detail/sp/800-38d/final)
for Galois/Counter Mode (GCM) and GMAC documentation.

It is possible to use GMAC to authenticate data without also
encrypting it.  The definition in the above document is not
formal, but it is *assumed* to only need one counter value
to encrypt the final hash value, but otherwise hash only the
plaintext values with GHASH, including the encrypted form of 0.
This means that GMAC also provides keyed authentication.
See this [GMAC diagram](https://xilinx.github.io/Vitis_Libraries/security/2020.1/guide_L1/internals/gmac.html)
for only deriving the hash subkey to shuffle it in a manner
that depends on the key and derive one encoded IV/counter block
to protect the final GHASH outcome on plaintext blocks with
the cipher key.

TODO: HDLC header encryption?  S-frame encryption?  U-frame encryption?
This has an impact on the ability to translate the codec with or without
end-to-end security for HDLC.  Proxies need HDLC flow control, and they
cannot translate encrypted sound.


## Null Encryption

When no keys are used, HDLC is treated in a default mode.

  * This is not a counter mode, and it uses no salt or
    initialisation vector.

  * Encryption is void, that is, a XOR pad of all zero bytes
    is used.  Plaintext then equals the "encrypted" text.

  * Signing is based on a fixed key of all zero bytes.  It
    uses the GMAC mode with AES-128.  Signatures are still
    added, but their guarantees drop from origin integrity
    to transport integrity (because everyone knows the key).

  * The mode of operation is GCM as a Stream Cipher, defined
    below.  This ends with multiplication to spread even the
    last byte's entropy thoroughly and gain maximum error
    detection potential.

  * This mode does not protect from men in the middle, neither
    listening nor actively modifying the traffic in transit.


## Galois Counter Mode as a Stream Cipher

We boldly vary on AES-GCM to make it suitable for streaming:

  * Counter mode can easily be used as a XOR pad in a byte stream,
    so stream encryption is not difficiult in itself.

  * The block operation encrypts one block, XORs it to the
    prior value and multiplies the result by the 128-bit
    hash subkey (an encrypted zero block).  It should also be
    possible to use the XOR pad encrypt one byte at a time,
    apply that to the previous output and multiply that by
    the matching 8 bits in the hash subkey.

    The last byte added is not likely to spread until it has
    influenced, on average, half the bits in the output.  As
    a result, the hash is more vulnerable in the last bytes.
    The resolution to this is using a full GHASH round, by
    simply adding 16 more 0x00 bytes to finalise GHASH after
    the last data byte.  (AES-GCM also makes another full
    multiplication, but it is less apparent because it operates
    in block mode anyway.)  The hash subkey, which is a constant
    derived from the cipher key, is unknown to adversaries and
    bit distribution cannot be predicted to help forge data.

  * The authentication tag is encrypted, which is done with a
    first block in AES-GCM, but we can allocate the first four
    bytes from the XOR pad for the same purpose, and get the
    next after finishing an output block.

  * The number of authenticated and encrypted bytes is normally
    annotated to simplify padding, but since we do not complete a
    block and instead use bytes in the XOR pad differently, we
    should be fine without padding.

  * It may or may not be useful to sign prior data as additional
    data, and when we add it we may need to start with its length
    encoded in bytes.

  * We may choose to sign with shorter blocks, such as 32 bits
    for I-frames, because we continue with the run of data and
    will therefore collect 128 bit security after 4 such short
    codes.  The UIH command may be used to relay a full
    signature at any desirable point in the HDLC flow.


## Inner and Outer Cryptography

Sometimes the media path calls for codec translation, for instance
when switching between PSTN, GSM and DECT.  This is impossible
when the sound data is encrypted, and signing it is also useless.
Outer cryptography is therefore defined as encryption and signing
such that translation is not possible without breaking end-to-end
security.  The bits that offer this are passed in the plaintext
initiation of SubliMe, and can be modified by the translator.

Translators may also detect opportunistic attempts to initiate
SubliMe, and extract the data from the incoming codec and inject
it back into the outgoing codec.  Since the bit streams may flow
at different rates, such a translator needs to have access to
HDLC flow control via S-frames RR and RNR.  The other S-frames
are REJ and SREJ, which it cannot produce in lieu of end-to-end
key material, and all I-frames and U-frames are also private.
The XID for opportunistic setup of SubliMe cannot be encrypted,
so that can be detected in the translator.  This creates a model
for inner cryptography, which is protects HDLC frames only,
but not on the sound data carried in the codec.

TODO: Restrict to address 0x00? But what if that is encrypted?

The FLAG and BREAK signals are never encrypted, so framing is
inhereted in the encrypted content.  Encryption and signatures
start and end with the byte that holds the last bit of the FLAG
or, informally put, with the last byte of the FLAG.  Since the
FLAG may end somewhere in the middle of this byte, this byte
overlaps between signed and encrypted forms.  The byte is not
repeated if signatures or encryption run over them to address
a consecutive sequence of HDLC frames.

The RR and RNR commands are recognised as a sequence of just an
Address, Command and Check, without Information field.  This can
be recognised in both keyed and null encryption.  Which
bits in the Command are encrypted depends on its contents.

<figure><artwork><![CDATA[
Command  | Encrypt  | Meaning
---------+----------+-------------------    Legend:
xxxxxxx0 | yyyyyyy0 | I-frame               x - arbitrary bit
xxxx0x01 | yyyy0n01 | S-frames RR, RNR      y - encrypt this bit
xxxx1x01 | yyyy1y01 | S-frames REJ, SREJ    n - retain plain bit
xxxxxx11 | yyyyyy11 | U-frame
]]></artwork></figure>

TODO: Maybe reveal N(R) values as a hint towards a key?  But then also reveal the address...

TODO: Maybe reserve counter values for REJ, SREJ

This encryption masking is applied to the Command for inner
encryption.  Matched bits 0 and 1 are copied, so the other
side can employ the same recognition mechanism.

Address fields are always encrypted, but a translator would
choose Address 0x00 to indicate that it wants to suspend all
HDLC data, and also set 0 for N(R) and the Poll flag,
so their RR and RNR frames are fixed.  After "encryption"
and signing with Null Encryption, the result is fixed and
must be compared before normal frame checking.  The risk of
overlap with the key is very low, and the only impact would
be reduced communication speeds due to slowness at the
remote.  The RR and RNR codes cannot be confused because the
bit that makes the difference is not encrypted.

Since the FLAG byte 0x7e is part of the encrypted flow, the
need for escaping also applies to the encrypted flow.  Any
0x7e or 0x7d bytes are unmodified in the plaintext flow, but
when they occur in the encrypted flow they will be escaped.

When translators inject RR and RNR commands in the HDLC flow
they cannot use the end-to-end key for signing.  Instead, they
use their own.  Endpoints may choose to accept a risk of a
denial of service by ignoring signatures on RR and RNR, or
they may explicitly configure keys for codec translators
and restrict the acceptable set of RR/RNR signers.  SubliMe
is opportunistic, so it is always at risk of denial of service.
Only when its use is enforced, presumably with known peers,
then translation issues may call for this level of care.


## Prepared Responses

The protocol uses S-frames to acknowledge or reject frames,
as well as for flow control.  These may be sent at any time,
but they are the result of some other activity and may have
their counter space "allocated" beforehand, and a dictionary
may be used to lookup a translation.  (TODO: Consider small
systems, recomputation versus storage overhead.)

The following graphs represent follow-up activities after
each command, along with their byte sizes:

```
RR@0(2)

RNR@0(2)

UI(n)

XID(n) -+-> #XID(n)
        +-> #XID(2)  # failure

SABM(2) -+-> UA(2)
         +-> DM(2)
         +-> FRMR(7)

DISC(2) -+-> UA(2)
         +-> DM(2)

TEST(n) ---> #TEST(n)

I(n) -+-> RNR(2) --> RR(2) -+-> SREJ(2)
      +----------<----------+-> REJ(2)

# TODO: Add UP ???
UP(2) -+-> SREJ(2)
       +-> REJ(2)

# TODO: FRMR (flag W) ???
```

Basically, the S-frames RR(2) and RNR(2) for address 0x00 can be
allocated in the beginning; they are constant for as long as the
key is.  They occur only without Poll/Final bit and N(R)=0.
Note that they could also be signed with the Null Key, with only
a mild denial-of-service risk.

After SABM(2), only U-frames may occur, namely UA(2) or DM(2),
and possible DISC(2) followed by UA(2) or DM(2).  It does not
matter if both sides send the same DISC(2).  These commands
have no variations due to N(R) reporting.  The DISC(2) comes
in a variant with and without Poll bit, the others follow
the matching request.  TODO: Treat DISC(2) separately, and
have side allocate its own key material for it, with its own
UA(2) or DM(2).  Do we need its history?  I don't think so.

After I(2), all S-frames may occur, but only one of them will
happen.  The Final bit depends on the I-frame, and N(S) is set
to match the I-frame.  The same applies to the RNR(2) and RR(2)
frames; RNR(2) may occur with Poll if I(2) has it, and always
without.  SO there are 3 commands to cover per I-frame, and up
to 8 of those can be in flight for the connection address (but
a regimen with more Poll bits can reduce that).

The load on storage therefore is:

  * Address 0x00 allocates 2 commands sized 2;
  * Other connections allocate 4 commands sized 2, where
    DISC(2) has two forms;
  * In-flight I-frames each allocate 4 commands sized 2,
    namely two forms of RNR(2), all for each of the 8 window
    positions, so 32 commands sized 2.
  * XID(n) and TEST(n) responses can be allocated by the key on
    the other side, as they appear logically.  The may however
    trigger REJ(2) or SREJ(2), so each allocates 2 bytes.
  * Generally, the data sizes are only somewhat large for
    connections, but they also need a largish window size.

A question to answer where to get the key material from.  Basically,
each side shows initiative.  The pre-allocation of counter values
above is on this initiating side.  On each side, the counter
values are stepped independently, for instance one side odd and
the other even, or even safer, under different IVs so they know
the quality of its allocation.

The final question that remains is how the receiver can stay in sync
with the sender.  Any sender will resend HDLC frames literally
when they receive no response, so transmission errors will be
caught.  Literal resending is even safe out-of-order.  The one
thing that does not work is the UI(n) frame, which is quietly
dropped in case of errors.  Except of course, that it cannot be
decrypted in every situation, so in general it might trigger
SREJ(2) and that could be turned into general behaviour.

A detail about SREJ(2) is that it needs to know what to respond
to, and even REJ(2) needs the Address to complain to...

## Encryption of Voice and/or Data

TODO: This mode can only work without translation of the byte flow.  Consider distinction between inner/outer encryption, that is end-to-end without translation and end-to-end for HDLC frames but with audio translation.
TODO: Consider a translation-friendly alternative for the HDLC flow, pass S-frames with RR/RNR flow control in plaintext but not the S-frames REJ/SREJ.  U-frames and I-frames are always private; initial XID is needed for opportunistic detection, including in translators, but this stage will still be plaintext.  The pattern of HDLC header bits to trigger plaintext passing of RR/RNR is xxxx0x01 in the Command byte and no Information byte; this can be recognised in phases, I-frames use xxxxxxx0, U-frames use xxxxxx11, S-frames REJ/SJRE use xxxx1x01 and each can be encrypted to the highest possible level.  Think about the Check, this should not involve a key or be quietly ignored.  It may be sensible to also pass FLAG bytes in plaintext, to recognise frame separation and Command words.

Bit-stealing mode is used to initialise the call, but it can also
be useful for framing efficiency (no headers/trailers).  Whether
the voice part, so the original codec minus the stolen bits,
gets encrypted is a choice.  It can be enforced by masking of the
continuous XOR pad.  The counter mode is used per byte, and then
applied to the voice data per byte.

The data in bit-stealing mode can be separately selected for
encryption, which would use the remaining bits of the continuous
XOR pad.  In byte-streaming mode, this happens to apply to all
8 bits.

Note that voice carried in frames counts as data, not voice.  It
is for this reason that such voice data is not played.  The logic
for its handling is easier to understand when thinking about the
RTP codec for RealTime Text, which actually is a data stream.
Any other treatment would cause terrible overhead and may not even
work because it requires looking ahead in the byte flow; all this
is highly unpractical and undesirable.

So, the only supported distinction is between bit-stealing voice
and the data in either all the other bits in bit-stealing mode or
all bits in byte-streaming mode.


## Signing Data

For data signing, it does not matter if it is transmitted in
bit-stealing mode or byte-streaming mode.  In general, the
data consists of a HDLC frame sequence, started, separated and
terminated with one FLAG byte 0x7e.  Not all HDLC frames are
taking into account; only numbered command frames matter, so
that limits the choice to I-frames only; S-frames only mention
the number from other frames and U-frames are not numbered.

The data flow is only counted in one direction, but it equally
applies to all connections.

TODO: WHEN REJECTED, THE COUNTER MAY BE OFF, AND RESENDING CAN
INTRODUCE A RISK OF REUSING THE COUNTER VALUE, AND THE OTHER
END MAY NOT KNOW HOW MANY BYTES WERE TRANSMITTED.  BUT WE DO
HAVE MRU/MTU VALUES THAT MAY PROVE HELPFUL.  BE VERY CAREFUL!

Signatures are the lower 32 bits of the GHASH value after the
HDLC frame sequence with the last one up to and including the
Information field.  Since the HDLC frame sequence continues
afterwards, it is possible to await four GHASH values to
obtain 128-bit security.  Higher values are unlikely to be of
use, due to the 128-bit internal state of AES.


## Signing Voice

In bit-stealing mode, the non-stolen bits count as voice data.
The rest is filled with zero bits to overlay any data or fine
detail of the vocal data.  The resulting bytes can be signed,
for which purpose it will be referred to as bit-mode voice.
TODO: Timing of start/stop of the signed time range.

In either bit-stealing mode or byte-streaming mode, there can
be UI-frames that hold audio data, or other RTP streams such
as for RealTime Text that will be treated like audio data.
For these, the Information field will be concatatenated to
form what will be referred to as byte-mode voice for the
purposes of signing.  TODO: Frame boundaries may matter, and
codec switching certainly does.

Signatures on bit-mode voice is transmitted in UIH-frames,
wheres signatures on byte-mode voice is attached to the
UI-frames.  In both cases, the signature applies only to
the isolated frame (TODO: Counter value match/increment)
and the full 128-bit GHASH multiplication is performed at
the end of the frame before applying the XOR pad that was
loaded TODO:before encrypting the data.

TODO: Separate frame signatures, so not chained, means that
the security is less whole and there is no way to combine
smaller signatures to achieve 128-bit certainty.

TODO: Sending to UIH would be a waste of the address byte,
since it always applies to the channel's "outer" codec.


## Initialisation Vectors

It is vitally important that IVs are never used again, or
with overlap.  For that reason, allocate carefully.  The IV
is not secret; it is submitted to the other side where its
effects can be reproduced.

The IV for AES-GCM is a 128-bit value, that consists of:

  * a 32-bit value that identifies a unique key storage
    handle; this can be the UNIX timestamp for writing the
    key/IV file on disk, under a reasonable assumption that
    keys do not spread very fast.

  * a 32-bit byte range, covering transmission bytes.

      - In outer mode, counting bytes starting with 0 for
        the first byte covered by the mode;
      - In inner mode, counting up from 0 for SAMB, I and
        DISC;
      - In inner mode, counting down from the end for UI,
        XID and UP;
      - Wrap-around must be avoided, if needed by a forced
        switch to garbled output or brutally stopping.

  * a 64-bit session counter; this is incremented on the side
    that sends crypto (and IV) before it uses the key; this may
    be done with a memory-mapped file and an atomic increment,
    or it could be done while the file is locked.

