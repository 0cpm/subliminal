# Frame Check Sequence (FCS)

> *HDLC uses a Frame Check Sequence which is a CRC-16 or CRC-32.*

There is a lot of information on this topic... and only a little really helps:

  * [Gentle yet Thorough introduction](http://www.ross.net/crc/download/crc_v3.txt), by Ross N. Williams
  * [Best CRCs has high HD given msglen](https://users.ece.cmu.edu/~koopman/crc/index.html), by Koopman
  * [Seven Deadly Sins](https://betterembsw.blogspot.com/2013/06/seven-deadly-sins-of-crcs-and-checksums.html)

A useful public document is [RFC 1662 on PPP](https://datatracker.ietf.org/doc/html/rfc1662#appendix-C) details CRC-16 and CRC-32 code along with the generators and tables

HDLC uses the following checksums,

  * A reversed scheme, because HDLC sends LSB first
  * Initialise with ~0 to allow initial 0x00 bytes
  * FCS inverts the CRC and sums it on reception, for a fixed good value
  * The CRC-16 variant used is CRC-16-CCITT or 0x1021     with good value 0xF0B8
  * The CRC-32 variant may use CRC-32-IEEE  or 0x04C11DB7 with good value 0xDEBB20E3

There are also implementations,

  * [yahdc](https://github.com/bang-olufsen/yahdlc/tree/master/C) under MIT license by Bang & Oluffsen
  * [minihdlc](https://github.com/mengguang/minihdlc) under Apache license
  * [hdlcd](https://github.com/Strunzdesign/hdlcd/blob/master/LICENSE) under **GPLv3**


## Purposes

There are two reasons to use a checksum in Subliminal Messaging

  * To detect noisy disruptions on the line
  * To reject plain audio as non-HDLC transmissions

Security is specifically not achievable with a CRC-16 or CRC-32 checksum.
Their computational properties make it relatively simple to manipulate
data protected with any of those.

### Detection of noisy disruptions

This mostly comes down to a large hamming distance.  This is standard
CRC knowledge, mostly due to systematic research by Phillip Koopman.
The optimal profile also relates to actual message lengths.

### Detection of HDLC in an Audio Stream

This is not standard knowledge.  We need to consider a noisy channel
and decide whether it is HDLC or not.

The CRC-16 checksum matches with probability 2^-16 and CRC-32 with 2^-32.
A few of these combine to a "certainty" of 2^-128.

One concern is that long-lasting noise could be considered a long sequence
of attempts to recognise a frame.  This seriously reduces the chance of
proper recognition.  It may be helpful to exchange a 128-bit identifier,
such as a UUID in an XID-frame, before processing data.  Alternatively,
it would be possible to count up on good frames and down on bad ones,
and thereby drop so far negative on a noisy channel that it should never
become positive for long enough.  Initial noise may however disturb the
ability to bootstrap Subliminal Messaging.


## FCS performance

The analysis files list very long lines.  To cut them down to a given HW value, download them and use the following to mark non-zero decimal words,

```
cat 0x8810.txt | cut -f 1-$((1+${HW:-10})) | less '+/\<[1-9][0-9]*\>'
```

### 16-bit checksums

Following Philip Koopman, the performance of [CRC-16-CCITT](https://users.ece.cmu.edu/~koopman/crc/c16/0x8810.txt) is constant over a long range of messages sizes:

  * HW(4) is the lowest non-zero value up to 32751 bit messages (address, control, info up to 4089 bytes, checksum)

In comparison, the performance of [0xac9a or CRC-16F/5](https://users.ece.cmu.edu/~koopman/crc/c16/0xac9a.txt) is:

  * HW(6) is the lowest non-zero value up to 35 bit msglen (address, control, checksum)
  * HW(5) is the lowest non-zero value up to 241 bit msglen (info up to 26 bytes)
  * HW(4) is never the lowest non-zero value
  * HW(3) is never the lowest non-zero value

In comparison, the performance of [0x8fdb or CRC-16F/8.2](https://users.ece.cmu.edu/~koopman/crc/c16/0x8fdb.txt) is:

  * HW(7) and up are not usable for HDLC
  * HW(6) is the lowest non-zero value up to 51 bits (info up to 2 bytes)
  * HW(4) is the lowest non-zero value up to 32751 bits (info up to 4089 bytes)

In conclusion,

  * CRC-16F/8.2 outperforms CRC-16-CCITT for short messages, as are common in HDLC, and never underperforms
  * CRC-16F/5 reaches HW(6) for small messages, but is beaten by CRC-16F/8.2

### 32-bit checksums

Checksum notation makes comparison difficult, but the [CRC-32-IEEE for 802.3](https://users.ece.cmu.edu/~koopman/crc/c32/0x82608edb.txt) according to Koopman perfoms as follows:

  * HW(9) and up are not usable for HDLC frames with a CRC-32
  * HW(8) for messages up to 57 bits (info up to 1 byte)
  * HW(7) for messages up to 91 bits (info up to 5 bytes)
  * HW(6) for messages up to 171 bits (info up to 15 bytes)
  * HW(5) for messages up to 268 bits (info up to 27 bytes)
  * HW(4) for messages up to 2974 bits (info up to 365 bytes)
  * HW(3) for messages up to 91607 bits (info up to 11445 bytes)

This offers hihger HW() than the CRC-16 checksum 0x8fdb

This is not a very useful distribution, it would be pleasant to allow somewhat longer frames at higher HW() values, with an animal from the [CRC-32 zoo](https://users.ece.cmu.edu/~koopman/crc/crc32.html).

I considered [0xf8c9140a](https://users.ece.cmu.edu/~koopman/crc/c32/0xf8c9140a.txt) before, but that was under the assumption that lengths were in bytes, not bits, but in reality it performas worse than the standard one.  For about 900 byte frame sizes, we need about 7000-ish bit messages.

  * [0x8f6e37a0 or CRC-32C from iSCSI and SCTP](https://users.ece.cmu.edu/~koopman/crc/c32/0x8f6e37a0.txt) does 5243 bits or 649 info bytes at HW(6) and 117 bits or 8 info bytes at HW(8)
  * [0x9d9947fd or CRC-32K](https://users.ece.cmu.edu/~koopman/crc/c32/0x9d9947fd.txt) does 8162 bits or 1014 info bytes at HW(5) and 361 bits or 39 info bytes at HW(6) and 56 bits or 1 info byte at HW(9).
  * [0xba0dc66b or CRC-32K/6.1](https://users.ece.cmu.edu/~koopman/crc/c32/0xba0dc66b.txt) does 16360 bits or 2039 info bytes at HW(6).
  * [0x9960034c or CRC-32K/6.4](https://users.ece.cmu.edu/~koopman/crc/c32/0x9960034c.txt) does 32738 bits or 4086 info bytes at HW(6) and 193 bits or 18 info bytes at HW(8).
  * [0xfa567d89 or CRC-32/6 corrected](https://users.ece.cmu.edu/~koopman/crc/c32/0xfa567d89.txt) does 32736 bits or 4086 info bytes at HW(6) and 274 bits or 28 info bytes at HW(8).

In conclusion,

  * The benefit of CRC-32 over CRC-16 is modest, but for larger frames so is its cost
  * CRC-16 0x8fdb does HW(6) up to 2 info bytes, HW(4) up to 4089 info bytes
  * CRC-32 may add HW(8) for small messages, but then adds overhead for low-risk messages
  * CRC-32 may add HW(6) for longer messages, up to 4086 info bytes
  * We could store a common MTU plus tunnel headers in one message

### Switching between 16-bit and 32-bit checksums

The choice *could* be made on grounds of message type; I-frames and UI-frames could use CRC-32,
and possibly XID commands.  Alternatively, we could use the message length if we know it before
choosing this.  And yes, we do; this will not likely to ever be implemented in hardware, and if
so it would implement two CRC engines running in parallel on the same data.

  * Use CRC-16F/8.2 for HDLC frames up to 6 bytes:
      - Frame bytes up to address-1, control-1, info-[0..2], crc-2
      - Reaches HW(6) in HDLC frames with up to 2 bytes of data:
  * Use CRC-32/6 corrected for HDLC frames from 8 up to 4092 bytes
      - Frame bytes up to address-1, control-1, info-[3..4086], crc-4
      - HW(8) up to 28 info bytes
      - HW(6) up to 4086 info bytes
  * Received messages of 0, 1, 2, 3, 7 or 8 bytes are not acceptable.
  * Strongly advise against more than 1700 bytes transmission.
  * Request MRU on the other end to establish MTU.

## Impact of Message Fragmentation

We might also choose a sufficiently low frame, and have more CRC-16 checksums applied
to it.  Given the smaller size, a suitable checksum can then be found.

If we aim for 256 bytes in an information field, add 4 bytes for address, control and
checksum, we end up with 260 bytes or 2080 bits of data.  In 7 frames, we can capture
1792 bytes, a high-enough MTU to capture the customary MTU at 1500 and tunnel headers.

Back to the [zoo](https://users.ece.cmu.edu/~koopman/crc/crc16.html)...
what is the highest attainable hamming distance for a CRC-16 with messages up to 2080 bits?

  * HD(4) is achievable with quite a few checksums
  * Of these, which offer HD(max) with [non-data] messages of 32 bits?
  * Of these, which offer HD(5) with the longest messages?
  * [CRC-16F/4.2](https://users.ece.cmu.edu/~koopman/crc/c16/0xd175.txt) does {32751,32751,93,93,...unusable}
  * [CRC-16/6sub8](https://users.ece.cmu.edu/~koopman/crc/c16/0x808d.txt) does {28642,28642,99,99}
  * [CRC-16K/4](https://users.ece.cmu.edu/~koopman/crc/c16/0xbaad.txt) does {7985,7985,108,20,...unusable}
  * Notation: {HD(3),HD(4),...} where ...unusable drops below 32 bits of minimum HDLC length
  * These all perform well on 256 byte frames
  * 93/99/108 bits is for 11/12/13 bytes, barely distinguishable sizes
  * 32751/28642/7985 bits goes up to 4089/3576/994 info bytes, 32
  * Drop CRC-16K/4 because it "only" achieves HD(5) up to this 11/12/13 range
  * Drop CRC-16K/4 because it excludes common MTU sizes (plus tunnel headers) sent at once
  * Prefer longer frames with higher HD(5),HD(6) values

The winner is... **CRC-16/6sub8**, also known as 0x808d.

  * Default to frames with up to 256 info bytes, and allow combining up to 7 of these to 1792.
  * Negotiation of larger MTU sizes (perhaps with CRC-32) can be supported up to (28642-48)/8=3574 bytes, 7 of which combine to 25019 bytes.
  * Submitting 1500 bytes in 256-byte frames takes 6 frames; spread that over 250 bytes each, so frames of 254 bytes or 2032 bits, achieving HD(4).  The overhead is 24 bytes or 1.6%, instead of 6 bytes or 0.4% in one frame with a CRC-32 checksum.
  * Under HD(6) from CRC-16/6sub8 for up to 8 info bytes, max.BER = 6.25000%
  * Under HD(4) from CRC-16/6sub8 for up to 256 info bytes, max.BER = 0.19230%
  * Under HD(4) from CRC-16/6sub8 for up to 994 info bytes, max.BER = 0.05010% (negotiable?)
  * Under HD(8) from CRC-32/6-corrected for up to 28 info bytes, max.BER = 2.94117% (not used)
  * Under HD(6) from CRC-32/6-corrected for up to 256 info bytes, max.BER = 0.28625% (not used)
  * Under HD(6) from CRC-32/6-corrected for up to 4086 info bytes, max.BER = 0.018328% (negotiable?)
  * Under HD(4) from CRC-16-CCITT for up to 256 info bytes, max.BER = 0.19230% (not used)
  * Under HD(4) from CRC-16-CCITT for up to 4086 info bytes, max.BER = 0.012224% (not used)

The BER varies, but not dramatic enough to choose both CRC-16 and CRC-32 on that alone.
Another reason to like CRC-32 is to have larger frames, not up to 994 info bytes but up to
4086 info bytes, or in 7 frames, not going up to 6958 info bytes but up to 28502 info bytes.
UDP frames can go well beyond 1500, but are not reached by either, and it mostly has a
backup in TCP for non-tunneling uses, where octet streaming is the norm and frame sizes
do not matter.  Also, UDP headers themselves indicate their total length, and reassembly is
possible on those grounds alone.  The one other case would be SCTP, which can have user
messages of higher size, that may be fragmented into chunks.

In short, there is no real need for going up to 64k frames.  We do not need to exceed the
994 byte frame, ever, and can therefore stick with **CRC-16/6sub8 for everything** as long
as user messages do not go over 994 bytes and use windowing to collect a negotiable MTU:

  * By default, max. 256 info bytes per frame; need 6 frames to get to 1500, max.BER = 0.19230%
  * Negotiable, max. 994 info bytes per frame; need 2 frames to get to 1500, max.BER = 0.05010%
  * For small frames up to 8 info bytes, high HD(6) noise tolerance, or max.BER = 6.25000%
  * Without negotiation, but error rates up to 0.19230% are detected properly
  * Vital command frames (without data) withstand **1-in-16 bit errors**
  * Any frame under standard MTU can withstand up to **1-in-520 bit errors**
  * Any frame can withstand up to **1-in-2000 bit errors**

Glad we got that worked out.  No convincing reasons to use a CRC-32, then, but a better one
than standard is available.


## Nesting of Checksums

When protocols are forwarded over Subliminal Messaging, it may happen that they carry
their own CRC codes.  It is generally better to check and remove them if possible:

  * Nested CRCs are checked between end points.  Though easier for a relay, it adds
    opportunities to catch bit errors, as it travels over multiple links.
  * The CRC adds to the message size, it increases the risk of catching bit errors.
  * If resends are necessary, then it is best done over the fewest legs in a route.

It is far better to capture errors on the shortest possible links.  So, upon arrival,
check a CRC and remove it.  And when forwarding, add a fresh one back in.
Examples of this approach are:

  * When HDLC frames are encapsulated into Subliminal Messages, they retain their
    own address and control fields, but not their FCS.
  * When AX.25 or even FX.25 is relayed via Subliminal Messages, they are sent as
    the AX.25 address, control and information fields.
  * When Modbus frames are forwarded, then it is done as for Modbus TCP.  Not only
    does that add more dispatching options, but it also avoids the RTU checksum.
    It is however a binary protocol, and therefore reasonably compact.

