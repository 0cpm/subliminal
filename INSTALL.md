# Inslallation for Subliminal Messaging

> *Build libsublime from source.*


## Dependencies

We look for the `pkg_config` files of

  * `libinih` for handling .INI configuration files
  * `libuuid` for handling UUIDs of backend services

On Debian or Ubuntu, you can install them with

```
apt-get install libini-dev uuid-dev
```


## Building

We use CMake and CTest.

```
git clone https://gitlab.com/0cpm/subliminal
cd subliminal
mkdir build
cd build
cmake ..
make
ctest
make install
```

