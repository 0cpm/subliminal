/* Subliminal Messaging API to filter and update codecs.
 *
 * To support Subliminal Messaging, the data bytes for a codec must pass
 * through a filter.  There are independent filters to extract data from
 * incoming codecs and to add data to outgoing codecs.
 *
 * In most practical cases, reliability is required, and that means that
 * the two directions of filtering must be coupled.  This is not always
 * required or desired, however; a ringback sound may opportunistically
 * send information such as a vCard or the beginnings of key agreement.
 *
 * Once setup, all that needs to be done is to pass codec data through
 * the filters.  Any contained data will be injected on the output
 * codec and extracted from the input codec.
 *
 * This API is designed to be straightforward to incorporate anywhere.
 * Coupling to backends is automatically configured from an INI file.
 * Since the protocol is opportunistic, most errors are quietly ignored.
 * You can even pass in NULL codecs to quickly skip a function.
 * 
 * The library reads configuration from ~/.config/0cpm/subliminal.conf
 * or /etc/0cpm/subliminal.conf -- with backend service information.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#ifndef NOCPM_SUBLIMINAL
#define NOCPM_SUBLIMINAL


#include <stdbool.h>
#include <stdint.h>



/* Data structure to capture codec state for Subliminal Messaging.
 * Some of this is generic, but parts will be dedicated to a codec,
 * and they may differ between the sending and receiving side.
 */
struct sublime;



/* Initialisation for the Subliminal Messaging library.
 *
 * This involves reading the configuration file into globals.
 *
 * Returns true on success, false with errno set on failure.
 * It is not an error to ignore such errors; calls may still
 * be made but will not have any effect.
 */
bool sublime_init (void);


/* Finalise the Subliminal Messaging library.
 *
 * Calling this function will clear global memory used by the
 * library.  Before this call you should close all open codecs.
 */
void sublime_fini (void);


/* Create a context for an incoming codec.   Example calls would
.* be for mediatype "audio" and subtype "PCMA".  The full
 * list is maintained by IANA on
 * https://www.iana.org/assignments/rtp-parameters/rtp-parameters.xhtml
 * The customary continuations in SDP are permitted (spaces, slashes).
 *
 * Set recv_not_send to true for receiving, false for sending.
 * Or use the alias SUBLIME_RECV or SUBLIME_SEND.  In normal use,
 * these are each called separately, and the directions coupled
 * with sublime_attach().  If the intention is unidirectional
 * traffic, use sublime_detach() to correct this assumption.
 *
 * Returns a context structure, or NULL with errno set on failure.
 * It is not an error to pass NULL into any later calls.
 */
struct sublime *sublime_open (bool recv_not_send, const char *mediatype, const char *subtype);
#define SUBLIME_RECV true
#define SUBLIME_SEND false


/* Close a context for an incoming or outgoing codec.
 *
 * If the codec is attached, it will first be detached.
 */
void sublime_close (struct sublime *sbl);


/* Pair an incoming and outgoing context.  This allows input data
 * to improve the output data.  The output data will depend on the
 * feedback, and it stops working in unidirectional mode.
 *
 * The attachment helps to pass window indexes N(S) and N(R) for
 * both directionns.  And it help can detect XID conflicts.
 *
 * If the codec was already attached, then it will first be detached.
 *
 * If either codec is NULL, then nothing is done.  Except detaching
 * the other if it is not NULL.
 */
void sublime_attach (struct sublime *recver, struct sublime *sender);


/* Decouple an incoming and outgoing context by passing one.
 * The other will also be decoupled.
 *
 * It is harmless to call this function on a codec that is not
 * detached yet, or anymore.
 *
 * The unidir flag can be set to allow unidirectional communication.
 * Implied detachment always reset this mode.  For a new sublime
 * object, it is reset, and this function may be called to set it
 * explicitly -- even without preceding sublime_attach() call.
 *
 * If the codec is NULL, then nothing is done.
 */
void sublime_detach (struct sublime *either, bool unidir);


/* Find the attached codec.  Doing this twice returns the original
 * one, provided it was attached.
 *
 * Returns the codec in the attached other direction, or NULL
 * for a detached codec.  Note that you can safely pass NULL
 * into any call in this API.
 */
struct sublime *sublime_other (struct sublime *either);


/* Before sending codec data, pass it through this output filter.
 * The codec data may be modified by this call, but the number of
 * bytes remains unchanged and it will still be codicly correct.
 *
 * If the sender is NULL, then the filter makes no changes.
 */
void sublime_sendmedia (struct sublime *sender, uint8_t *data, unsigned size);


/* After receiving codec data, pass it through this input filter.
 * The codec data may be modified by this call, but the number of
 * bytes remains unchanged and it will still be codicly correct.
 *
 * If the sender is NULL, then the filter makes no changes.
 */
void sublime_recvmedia (struct sublime *recver, uint8_t *data, unsigned size);



#ifdef  SUBLIMINAL_CODEC_INTERNAL
#define SUBLIMINAL_BASE_INTERNAL
#endif

#ifdef  SUBLIMINAL_HDLC_ASM_INTERNAL
#define SUBLIMINAL_HDLC_INTERNAL
#endif

#ifdef  SUBLIMINAL_HDLC_INTERNAL
#define SUBLIMINAL_BASE_INTERNAL
#endif

#ifdef  SUBLIMINAL_BACK_INTERNAL
#define SUBLIMINAL_BASE_INTERNAL
#endif

#ifdef  SUBLIMINAL_CRYPTO_INTERNAL
#define SUBLIMINAL_BASE_INTERNAL
#endif


#ifdef SUBLIMINAL_BASE_INTERNAL
//
// CRYPTO EXTENSIONS
//
// These define the maximum sizes available for cryptographic modes.
// Such modes should test for this if they are to work.
//
// SABM may carry Information with a 256-bit key and 256-bit signature
// DISC Check may be extended from 32 bits to a 512-bit checksum
// U-frames may prefix Information with a 64-bit counter value
// 	*inside of* a secure connection; not SABM and DISC though
//
// These values can only be assumed for compiled-in components of
// Sublime; external entities cannot automatically rely on using
// a library with the same function.  It is only needed internally
// anyway.
//
#define SUBLIME_CRYPTMODE_MAXTEND_SABM		64
#define SUBLIME_CRYPTMODE_MAXTEND_DISC		60
#define SUBLIME_CRYPTMODE_MAXTEND_UFRAME	 8
//
#define SUBLIME_CRYPTMODE_MAXTEND_ALL		64
//
#ifdef SUBLIMINAL_LARGE_FRAMES
#define SUBLIME_INFOSIZE 2048
#define SUBLIME_INFOSIZE 2048
#else
#define SUBLIME_INFOSIZE  256
#endif
//
typedef uint8_t sublime_hdlc_buffer_t [1+1+SUBLIME_INFOSIZE+4+SUBLIME_CRYPTMODE_MAXTEND_ALL];



/* THIS IS AN UPCALL FROM HDLC TO THE SUBLIME BASE.
 *
 * Allocate the current window frame and return its index
 * for later freeing with sublime_base_freeframe().
 *
 * The return value may be a failure.  It should be tested with
 * SUBLIME_WINDOW_ENTRY_OK() if the context does not guarantee
 * the availability of the current (firstfree) frame.
 */
uint8_t sublime_base_allocframe (struct sublime *sbl);

/* THIS IS AN UPCALL FROM HDLC TO THE SUBLIME BASE.
 *
 * Free the window index that was previously allocated with
 * sublime_base_allocframe().
 */
void sublime_base_freeframe (struct sublime *sbl, uint8_t allocframe);


/* THIS IS AN UPCALL FROM ANYWHERE TO THE SUBLIME BASE.
 *
 * Obtain the backend for a given address and must, must_not
 * constraints to the address_flags.  Return NULL on failure.
 */
struct sublime_back *sublime_base_match_backend (struct sublime *sbl, uint8_t address, uint8_t must, uint8_t must_not);

/* Add a Check field at the indicated location.
 * Take keys and crypgraphic modes into account.
 * Return the number of Check bytes added.
 */
unsigned sublime_base_makecheck (struct sublime *sbl, uint8_t *frame, unsigned prechecklen, uint8_t *check);

/* Verify the Check field at the end of the frame.
 * Return the number of bytes of the remaining message,
 * or 0 in case of any failure, including with the Check
 * field value.
 */
unsigned sublime_base_testcheck (struct sublime *sbl, uint8_t *frame, unsigned framelen);


/* THIS IS AN UPCALL FROM ANYWHERE TO THE SUBLIME BASE.
 *
 * Return whether the reverse is in ready order, that is,
 * either it is set or we are in unidirectional mode.
 *
 * If the output parameter optout_reverse is not NULL, then
 * set the currently attached sublime structure into it,
 * setting it to NULL if we are in unidirectional mode.
 */
bool sublime_base_reverse (struct sublime *sbl, struct sublime **optout_reverse);


/* Preset the outer crypto to use after the next BREAK.  The code is
 * usually exchanged during HDLC traffic and the keyfile indicates
 * where the key material (and IV material) can be found.  The key
 * is quiet until the next BREAK, and could even be overwritten in
 * the mean time.  See sublime_base_outercrypto_sync() for details.
 *
 * The crypto direction follows the underlying sublime structure.
 * The sending crypto is used to produce encryption and signatures.
 * Receiving   crypto is used for decryption and verification.
 *
 * The buffer, which started as a xorm buffer (encryption pad), is
 * assumed to change, one byte at a time, into a hash input.  When
 * this call is made, this original buffer is called hash_xorm and
 * assumed to consist of hashsz bytes of hash input and xormsz of
 * encryption pad.  This call:
 *  - adds [0..hashsz-1] to the old crypto signature;
 *  - computes the old crypto signature and feeds into the next;
 *  - wipes [0..hashsz-1] as a security measure;
 *  - rewrites [hashsz..hashsz+xormsz] with the new crypto xor pad.
 *
 * Failure to locate the new crypto will not take a risk of fallback
 * to null encryption; it sticks to the old crypto, safe and simple.
 * And may it be a lesson to anyone ignorant of proper negotiations.
 */
void sublime_base_outercrypto_preset (struct sublime *our, int crypto_algcode, const char *keyfile);


/* Synchronise outer crypto, possibly replacing the crypto and also
 * substituting the key for bytes after the sync.
 *
 * The assumption is that the desired outer crypto has been setup
 * prior to this call, using sublime_base_outercrypto_preset();
 *
 * When a next BREAK arrives, it is detected by the codec layer.
 * To make the switch, the base layer needs to switch crypto, in
 * the middle of a frame being processed, and deliver an XOR mask
 * for the remaining codec bytes.  The codec does not need to test
 * if hashsz includes more than one old crypto algorithm; this is
 * the responsibility of the base layer; the codec just counts up
 * the hash bytes within a frame and continues doing so after this
 * _sync() call.
 *
 * If there is no next crypto, this will be ignored, so it does no
 * harm to call this function for every BREAK; the XOR mask is then
 * simply returned without change.
 *
 * Do however be accurate about the timing for this call when used
 * with outer encryption:
 *  - in bit-stealing mode, the last byte taken before the switch
 *    must apply to the byte that holds the "0" bit immediately
 *    following a BREAK;
 *  - in byte-stealing mode, the last byte taken should be the
 *    byte that holds the BREAK.
 */
void sublime_base_outercrypto_sync (struct sublime *our, uint8_t *hash_xorm, unsigned hashsz, unsigned xormsz);


#define HDLC_I(N_R,N_S)	(0x00 | (((N_R) & 0x07) << 5) | (((N_S) & 0x07) << 1))
#define HDLC_RR(N_R)	(0x01 | (((N_R) & 0x07) << 5))
#define HDLC_RNR(N_R)	(0x05 | (((N_R) & 0x07) << 5))
#define HDLC_REJ(N_R)	(0x09 | (((N_R) & 0x07) << 5))
#define HDLC_SREJ(N_R)	(0x0d | (((N_R) & 0x07) << 5))
#define HDLC_UI()	 0x03
#define HDLC_XID()	 0xaf
#define HDLC_SABM()	 0x2f
#define HDLC_DISC()	 0x43
#define HDLC_DM()	 0x0f
#define HDLC_UP()	 0x23
#define HDLC_TEST()	 0xe3


/* A structure for the API between libsublime and its codecs.
 */
struct sublime_codec;


/* A structure for the API between libsublime and its crypto.
 */
struct sublime_crypto;


/* A structure for the API between libsublime and its backends.
 */
struct sublime_back;


#endif /* SUBLIMINAL_BASE_INTERNAL */



#ifdef SUBLIMINAL_CODEC_INTERNAL
//
// These are definitions for internal use by Subliminal Messaging.
// They are used to connect a codec implementation to base Sublime.
//
// Do not call them from a user program.  Do not include this part
// other than for codec definitions for use by libsublime.
//


/* The number of window entries may be configured in a range 8..100
 */
#ifndef SUBLIME_WINDOW_ENTRIES
#define SUBLIME_WINDOW_ENTRIES 24
#endif

#define SUBLIME_WINDOW_ENTRY_NIL ((uint8_t) 255)
#define SUBLIME_WINDOW_ENTRY_OK(idx) ((idx) < SUBLIME_WINDOW_ENTRIES)


/* The abstract data type structure passed to the user.
 */
struct sublime {
	//
	// Point to the reverse direction, if attached
	struct sublime *reverse;
	//
	// Static fields from the codec
	const struct sublime_codec *codec;
	//
	// Fields from the current (and optional next) outer crypto
	// in both sending and receiving directions
	struct sublime_crypto *outercrypto_curr;
	struct sublime_crypto *outercrypto_next;
	//
	// The list of backends, connected or not
	struct sublime_back *backends;
	//
	// The backends that are related to an address
	// Though present, 0x00 and 0xff are not allocated
	struct sublime_back *addr2back [256];
	//
	// Input/output buffers for HDLC, usually in the window
	unsigned hdlclen;
	uint8_t *hdlcptr;
	//
	// A flag to explicitly permit unidirectional transfer
	bool unidir;
	//
	// Whether this codec is setup for sending or receiving
	bool recv_not_send;
	//
	// Already-signed offset in the currently translating frame
	unsigned signskip;
	//
	// Window with a free elements list (ends in high index)
	sublime_hdlc_buffer_t window [SUBLIME_WINDOW_ENTRIES];
	uint8_t firstfree;
	uint8_t nextfree [SUBLIME_WINDOW_ENTRIES];
};


/* Backend functions implemented by codecs.  These are used to
 * implement codec-specific calls:
 *  - Create a new codec handler structure
 *  - Destroy a    codec handler structure
 *  - Filter codec data before  sending
 *  - Filter codec data after receiving
 *
 *  At some point, these modules may come from loadable modules.
 */
typedef struct sublime *sublime_codec_create_fn (bool recv_not_send, const char *codec);
typedef void            sublime_codec_destroy_fn (struct sublime *goner);
typedef void            sublime_codec_sendmedia_fn (struct sublime *sender, uint8_t *data, uint8_t *xorm, unsigned size);
typedef void            sublime_codec_recvmedia_fn (struct sublime *sender, uint8_t *data, uint8_t *xorm, unsigned size);


/* The codec defines a structure of functions.
 */
#define SUBLIME_CODEC_VERSION 1
#define SUBLIME_CODEC_FUNCTIONS 4
struct sublime_codec {
	unsigned version;
	unsigned functions;
	sublime_codec_create_fn    *create   ;
	sublime_codec_destroy_fn   *destroy  ;
	sublime_codec_sendmedia_fn *sendmedia;
	sublime_codec_recvmedia_fn *recvmedia;
};


/* Hard-linked A-law codec.
 */
extern struct sublime_codec sublime_codec_alaw;


/* THIS IS AN UPCALL FROM CODEC TO THE SUBLIME BASE.
 *
 * Ask to fill the hdlcbuf/len/ptr, and return true if data is
 * made available, or false if no content is available.  This
 * call must be called by the codec when it starts, as there
 * will be no initial HDLC content standing by.
 *
 * If true was returned, then the following will be setup:
 *  - hdlclen   is the length of the HDLC frame
 *  - hdlcptr   is at the start of the HDLC frame
 *
 * If false was returned, then the following can be found:
 *  - hdlclen   is 0
 *  - hdlcptr   is NULL
 *
 * The caller may update these variables as desired, but not the
 * buffer, because that might be a constant string.
 */
bool sublime_base_fill_hdlc_buffer (struct sublime *sender);


/* THIS IS AN UPCALL FROM CODEC TO THE SUBLIME BASE.
 *
 * Prepare the head of the free list for HDLC frame reception.
 * A separate call to sublime_base_deliver_hdlc_buffer() can
 * be used if the HDLC frame arrives in good order, otherwise
 * this call simply resets the free list for another attempt.
 *
 * If true was returned, then the following will be setup:
 *  - hdlclen	is the length of the HDLC frame buffer
 *  - hdlcptr	is at the start of the HDLC buffer
 *
 * If false was returned, then the following can be found:
 *  - hdlclen	is 0
 *  - hdlcptr	is NULL
 *
 * The caller may update these variables and write at most
 * hdlclen bytes to the indicated pointer.  The logic that
 * works well is hdlcptr++ / hdlclen-- paired operations,
 * but this must be avoided once hdlclen drops to 0.  The
 * hdlcptr may be reset to NULL to avoid future delivery of
 * the frame.
 */
bool sublime_base_setup_hdlc_buffer (struct sublime *recver);


/* THIS IS AN UPCALL FROM CODEC TO THE SUBLIME BASE.
 *
 * Invalidate a previously received buffer from a call to
 * sublime_base_setup_hdlc_buffer().  This ensures that
 * future calls to sublime_based_deliver_hdlc_buffer()
 * will process the [partially] delivered result.  This
 * may be useful when HDLC reception fails to transport
 * correctly over a codec.
 */
static inline void sublime_base_reset_hdlc_buffer (struct sublime *recver) {
	recver->hdlclen = 0;
	recver->hdlcptr = NULL;
}


/* THIS IS AN UPCALL FROM CODEC TO THE SUBLIME BASE.
 *
 * Deliver a single character to the HDLC frame, if it was
 * allocated properly.  Return true on success.  On failure,
 * use sublime_base_reset_hdlc_buffer() and return false.
 */
static inline bool sublime_base_putc_hdlc_buffer (struct sublime *recver, uint8_t ch) {
	if (recver->hdlclen > 0) {
		*recver->hdlcptr++ = ch;
		recver->hdlclen--;
	} else {
		//
		// Frame too long, fail for now
		sublime_base_reset_hdlc_buffer (recver);
		return false;
	}
}


/* THIS IS AN UPCALL FROM CODEC TO THE SUBLIME BASE.
 *
 * Deliver the head of the free list, as previously assigned
 * in a true-return call to sublime_base_setup_hdlc_buffer().
 * During this call, hdlclen is supposed to indicate the
 * number of unused bytes in the originally returned buffer.
 * In addition, hdlcptr + hdlclen must be at the end of the
 * originally returned buffer.  If those conditions are not
 * me, the delivery will be quietly rejected.
 *
 * This routine will process the HDLC frame, and may well
 * allocate the head of the free list for processing, which
 * would cause the next sublime_base_setup_hdlc_buffer() call
 * to deliver another element, or fail.
 *
 * The task of processing HDLC is hereby handed off to the
 * Sublime base, and will include many tasks:
 *  - compare the Check to the expected value
 *  - rejecting any damaged HDLC frames
 *  - process N(R) and N(S) fields in the Command
 *  - compose I and UI commands to the backend MRU sizes
 *  - actual delivery of frames to the backend
 *  - negotiating, opening and closing service backends
 */
void sublime_base_deliver_hdlc_buffer (struct sublime *recver);


#endif /* SUBLIMINAL_CODEC_INTERNAL */



#ifdef SUBLIMINAL_HDLC_INTERNAL

/* Put a HDLC frame after it has been received.  This works on
 * the current window index (firstfree) and will allocate it if
 * it needs to use it over a long time, as is the case for an
 * I-frame that is adopted by a backend.  In that case future
 * freeing of the allocated window frame will follow.  These
 * use sublime_base_allocframe() and sublime_base_freeframe().
 */
void sublime_hdlc_putframe (struct sublime *recver, uint8_t address, uint8_t command, uint8_t *info, unsigned infolen);

/* Get a HDLC frame to send.  This may return false if nothing
 * is available yet, including when a conccurrent attempt is
 * being undertaken to retrieve data from a backend.  Since
 * codec handling should be done in realtime, it is possible
 *
 * This function not a mirror image of sublime_hdlc_putframe()
 * because it may poll backends to determine the backend from
 * which the frame is taken.
 *
 * Return the number of meaningful bytes in the hdlcbuf, for
 * Address, Command, Information and Check.  Return 0 if no
 * frame is available.
 *
 * If and only if an I-frame is returned will the hdlcbuf have
 * been allocated with sublime_base_allocframe() and will it
 * require freeing after delivery with sublime_hdlc_freeframe().
 * When S-frames and U-frames are returned, they are handled
 * immediately and need no such treatment.  TODO:CONCURRENCY?
 */
unsigned sublime_hdlc_getframe(struct sublime *sender, sublime_hdlc_buffer_t hdlcbuf);



#ifdef SUBLIMINAL_HDLC_ASM_INTERNAL

#include <stdio.h>

/* Setup the output file for sublime_hdlc_putframe().
 * This will be filled with disassembled HDLC lines.
 */
FILE *sublime_hdlcasm_swap_outfile (FILE *);

/* Setup the input file for sublime_hdlc_getframe().
 * This will read from a HDLC assembler file.
 */
FILE *sublime_hdlcasm_swap_infile (FILE *);

/* A callback function that can be requested to install
 * another sublime_hdlcasm_swap_infile() or _outfile().
 * The data points will be passed from callback setup.
 */
typedef void sublime_swapper_callback_t (void *data);

#if 0
/* Setup a callback that to be made when a new call to
 * sublime_hdlc_swap_outfile() is desired.  To stop the
 * callbacks, pass NULL for cbfun.
 *
 * TODO: This appears to be useless; one file is enough.
 */
void sublime_hdlcasm_setup_outfile_swapper (void *data, sublime_swapper_callback_t cbfun);
#endif

/* Setup a callback that to be made when a new call to
 * sublime_hdlc_swap_infile() is desired.  To stop the
 * callbacks, pass NULL for cbfun.
 */
void sublime_hdlcasm_setup_infile_swapper (void *data, sublime_swapper_callback_t cbfun);


#endif /* SUBLIMINAL_HDLC_ASM_INTERNAL */

#endif /* SUBLIMINAL_HDLC_INTERNAL */


#ifdef SUBLIMINAL_BACK_INTERNAL
//
// These are definitions for internal use by Subliminal Messaging.
// They are used to connect a HDLC backend to base Sublime.
//
// Do not call them from a user program.  Do not include this part
// other than for codec definitions for use by libsublime.
//

#include <uuid/uuid.h>

/* The address state indicates the state of negotiation for
 * a backend service UUID, and gives clues on how the address
 * may be used for it.
 *
 * _UP    --> sent to the remote
 * _DOWN  --> received from the remote
 * _BOTH  --> exchanged with the remote
 *
 * _XID_  --> XID frame for service negotiation
 * _SABM_ --> SABM frame for service connection
 * _DISC_ --> DISC frame for service disconnect
 *
 * _DM_INSECURE --> Service was rejected in insecure mode
 * _DM_SECURE   --> Service was rejected in   secure mode
 */
#define SUBLIME_BACK_ADDRESS_FRESH		0x00
#define SUBLIME_BACK_ADDRESS_XID_UP		0x01
#define SUBLIME_BACK_ADDRESS_XID_DOWN		0x02
#define SUBLIME_BACK_ADDRESS_XID_BOTH		0x03
#define SUBLIME_BACK_ADDRESS_DM_INSECURE	0x04
#define SUBLIME_BACK_ADDRESS_DM_SECURE		0x08
#define SUBLIME_BACK_ADDRESS_SABM_UP		0x10
#define SUBLIME_BACK_ADDRESS_SABM_DOWN		0x20
#define SUBLIME_BACK_ADDRESS_SABM_BOTH		0x30
#define SUBLIME_BACK_ADDRESS_DISC_UP		0x40
#define SUBLIME_BACK_ADDRESS_DISC_DOWN		0x80
#define SUBLIME_BACK_ADDRESS_DISC_BOTH		0xc0


/* The structure to describe the activity on a connection, and in
 * what stage of development it has ended.
 */
struct sublime_back {
	//
	// The formal identification and optional name
	uuid_t back_uuid;
	char *back_name;
	//
	// A linked list of known services, connected or not
	struct sublime_back *next;
	//
	// The HDLC Address in the range 0..255 and
	// the SUBLIME_ADDRESS_xxx flags defined above
	uint8_t address;
	uint8_t address_flags;
	//
	// Window offset N(S) is updated when sending HDLC
	// Window offset N(R) is updated when acknowledging
	// These count locally in 8 bits but are transmitted
	// over HDLC in their (mod 8) value.  The value of
	// extra local bits is to subtract and quickly see
	// what the difference N(S)-N(R) is.
	//
	// sender: N_S = "what we sent", N_R = "what other got"
	// recver: N_S = "what other sent", N_R = "what we got"
	// Also see the SUBLIME_BACK_WINDOW_HASROOM() macro.
	int8_t N_S;
	int8_t N_R;
};


/* Check that room exists in the backend's window; this would
 * allow it to send another I-frame.
 */
#define SUBLIME_BACK_WINDOW_HASROOM(be) (((uint8_t) (be)->N_S - (be)->N_R) < 7)


/* Initialise the backend of whicever plumage.  Return success.
 */
bool back_init (void);

/* Finalise the backend of whichever plumage.
 */
void back_fini (void);


/* Open a Sublime backend, for the secure or insecure state.
 * Return NULL if it is not available (at this security level).
 * Non-NULL results must end in sublime_back_close().
 *
 * This is an initiative from the remote peer being passed down.
 * See sublime_base_connect_active() for the reverse direction.
 */
struct sublime_back *sublime_back_connect_passive (uuid_t back_uuid, bool secure);

/* THIS IS AN UPCALL FROM BACKEND TO THE SUBLIME BASE.
 *
 * The backend requests a Sublime connection, perhaps because
 * a service just came online.  The secure flag indicates a
 * requirement for secure service only.
 *
 * Returns true on success.
 *
 * This is an initiative from the local endpoint being passed up.
 * See sublime_back_connect_passive() for the reverse direction.
 */
bool sublime_base_connect_active (struct sublime_back *back, bool secure);

/* Close a Sublime backend that was opened with either
 * sublime_back_connect_passive() or sublime_base_connect_active().
 */
void sublime_back_close (struct sublime_back *);

/* Pass the payload of an I frame to a backend.
 * On success, feed N_S back into the Sublime framework,
 * but this may only be expected when true is returned.
 */
bool sublime_back_send_I  (struct sublime_back *, const uint8_t *data, unsigned size, uint8_t N_S);

/* Pass the payload of a UI frame to a backend.
 * An attempt to delivery may only be expected when true
 * is returned.
 */
void sublime_back_send_UI (struct sublime_back *, const uint8_t *data, unsigned size);

/* THIS IS AN UPCALL FROM BACKEND TO THE SUBLIME BASE.
 *
 * Inform the base about the successful delivery of an I-frame.
 * The N_R value is taken from the N_S parameter to an earlier
 * sublime_back_send_I downcall.
 */
void sublime_base_delivered_I (struct sublime_back *, uint8_t N_R);

/* Poll the backend for an I frame, to fit in a buffer of a
 * given data location and maximum *size.
 *
 * When an I frame is available, return the corresponding
 * backend object and replace the maximum size with the
 * actual *size.  TODO: Protocol for frame-end.
 *
 * When no I frame is available, return NULL and do not
 * alter the maximum size.
 */
struct sublime_back *sublime_back_poll_I  (uint8_t *data, unsigned *size);

/* Poll the backend for a UI frame, to fit in a buffer of a
 * given data location and maximum *size.
 *
 * When a UI frame is available, return the corresponding
 * backend object and replace the maximum size with the
 * actual *size.  TODO: Protocol for frame-end.
 *
 * When no UI frame is available, return NULL and do not
 * alter the maximum size.
 */
struct sublime_back *sublime_back_poll_UI (uint8_t *data, unsigned *size);

/* THIS IS AN UPCALL FROM BACKEND TO THE SUBLIME BASE.
 *
 * Set symmetric key material for upcoming SABM setups.
 * This replace any current key data as a result of a successful
 * key exchange procedure.
 *
 * Poll for the desired size by setting keydata to non-NULL and
 * size to 0 -- this does not erase key material but refuses
 * the new offer due to a smaller size than required.
 *
 * Set keydata to NULL and size to 0 to remove all key material.
 *
 * Returns 0 on success, or a desired size if it is too small.
 */
unsigned sublime_base_set_keydata (uint8_t *keydata, unsigned size);

/* THIS IS A CALL FROM ONE SUBLIME BASE TO ANOTHER.
 *
 * Indicate that a command was received that carries an N(R)
 * value to show what it expects the next frame number to be.
 * Any value up to N(R)-1 is thereby acknowledged, counting
 * (mod 8) from the current N(R) stored.  Afterwards, the
 * given N(R) value will be the stored N(R).
 *
 * Any command can be supplied, but U-frames will be ignored
 * because they carry no N(R) acknowledgement field, contrary
 * to I-frames and S-frames.
 */
void sublime_base_acknowledge (struct sublime *sender, uint8_t address, uint8_t command);


#endif /* SUBLIMINAL_BACK_INTERNAL */


#ifdef SUBLIMINAL_CRYPTO_INTERNAL
//
// These are definitions for internal use by Subliminal Messaging.
// They are used to connect a crypto algorithm to base Sublime.
//
// Do not call them from a user program.  Do not include this part
// other than for codec definitions for use by libsublime.
//


/** Version number for the API version.
 */
#define SUBLIME_CRYPTO_VERSION 1



/** Data structure between Subliminal Messaging and cryptography.
 *
 * This holds buffers with XOR padding, both at the start and end of
 * the range.  It also holds a buffer into which signature data can
 * collect.  Finally, it has callback functions that are used to
 * update any values.
 *
 * The structure holds callbacks to functions for updates of the
 * various buffers included:
 *  - crypto_open()   prepares the state of the crypto
 *    [but is in reality external to this structure]
 *  - crypto_mkpad()  requests updates to the XOR pad buffers
 *  - crypto_udpate() adds data to an upcoming signature
 *  - crypto_sign()   requests a signature
 *  - crypto_close()  clears out anything sensitive
 * Note the definition of SUBLIMINAL_CRYPTO_FUNCTIONS is 4.
 *
 * The structure may hold private parts not shown to SubliMe.
 * There is no reason to mistrust other parts of the program, so
 * this serves no security purpose, but one of data abstraction.
 */
struct sublime_crypto {
	//
	// The range of free byte numbers in the XOR pad.
	// There can never be overlap between bytes using these numbers.
	// These values are initialised during crypto_reset(), then
	// updated by SubliMe, and read during crypto_xorpad().
	uint64_t iv_free_first;
	uint64_t iv_free_last;
	//
	// A series of bytes, up   to 256, of XOR pad covering start % 256
	uint8_t xorpad_first [256];
	//
	// A series of bytes, down to 256, of XOR pad covering last  % 256
	uint8_t xorpad_last  [256];
	//
	// The ultimate values of iv_free_ available in the xorpad,
	// and the call for action to crypto_xorpad().
	// These values are written by the crypto and read by SubliMe.
	uint64_t iv_endpad_first;
	uint64_t iv_endpad_last;
	//
	// Callback to assure having some XOR padding bytes;
	// this is initially needed after crypto_init() because
	// the same functions may be used for inner or outer crypto.
	// provide SUBLIME_XORPAD_xxx flagging for what to check.
	void (*crypto_mkpad) (struct sublime_crypto *sc, int uses);
	//
	// Gather signature bytes from an input buffer (to the signature)
	void (*crypto_update) (struct sublime_crypto *sc,
			uint8_t *inbuf, uint16_t inlen);
	//
	// Callback to request a signature hash code
	void (*crypto_sign) (struct sublime_crypto *sc, bool reset,
			uint8_t **outbuf, uint16_t *outlen);
	//
	// Callback to request a safe cleanup and memory free
	void (*crypto_close) (struct sublime_crypto *sc);
	//
	// The number of crypto functions in this structure
	#define SUBLIME_CRYPTO_FUNCTIONS 4
};


/** The options for the XOR pad inquiries, one or both (or none).
 */
#define SUBLIME_XORPAD_FIRST  0x01
#define SUBLIME_XORPAD_LAST   0x02
#define SUBLIME_XORPAD_BOTH   0x03


/** The type of a "callback" to initialise a sublime_crypto structure.
 *
 * Though this feels like a callback in the sublime_crypto API, it
 * must precede it because it helps to setup the data structure.
 * For that reason, it will be available as a separate name.
 *
 * The keyfile names where the key material can be found, if needed.
 * NULL may be used to ask for a default key, dependent on the crypto.
 */
typedef struct sublime_crypto *sublime_crypto_open (const char *keyfile);


/** Cryptographic algorithms, as negotiated.
 *
 * The _SCRAMBLE value cannot occur on the wire, and must be considered
 * a last resort in case things go wrong.  It is used when the IV values
 * cross, for instance, so the programmer does not actually need to be
 * on guard for this kind of problem.
 */
#define SUBLIME_CRYPTO_SCRAMBLE		-1
#define SUBLIME_CRYPTO_NULL		0x00
//TODO:IMPLEMENT// #define SUBLIME_CRYPTO_AES128GCM	0x01


/** Initialised functions, to be called with a cryptographic algorithm.
 *
 * These each open a specific cryptographic algorithm with supporting
 * data structure.
 *
 * When DEBUG is defined, then sublime_debugcrypto_open() is available
 * for reproducible random XOR padding and fixed signatures 0x12 0x34.
 */
struct sublime_crypto *sublime_nullcrypto_open     (const char *keyfile);
struct sublime_crypto *sublime_scramblecrypto_open (const char *keyfile);
#ifdef DEBUG
struct sublime_crypto *sublime_debugcrypto_open    (const char *keyfile);
#endif


#endif /* SUBLIMINAL_CRYPTO_INTERNAL */

#endif /* NOCPM_SUBLIMINAL */
