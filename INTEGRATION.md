# Integration of Subliminal Messaging

> *This library provides an abstract framework that can find its way
> into a variety  of applications.  How?*

This library is not an end-applicaation, but merely a framework with
demonstration programs that show how ISDN and VoIP software can be
modified to use the mechanisms.


## Is this interesting at all?

**Why subliminal?**
This work passes data in the sound range that is heard as noise.  It
can rely on that for data because the sound is passed in digital
form, as was first introduced with ISDN telephony.

**Can it go beyond PCMA alone?**
This code was written on the well-defined A-law codec, also known as
PCMA, which was the original ISDN codec.  There are many more codecs,
and this framework can easily be extended with new codec modules.
Most codecs can be tweaked to let go of some sound quality in return
for data.

**ISDN is ancient history?**
It is both history and present.  The telephony backbone radically
enforces it, and VoIP phones quietly adapt to that norm.  When
you are making a VoIP call that is not bypassed over the Internet,
then you are making an ISDN call.  The mobile network is a
(rather closed) alternative that is also adopting codec variation.

**Why not use VoIP?**
If you can use VoIP and reliably find your peer, go right ahead!
The thing is, we still have no such guarantees and need to fallback
on a telephony backbone.

**Why not use ZRTP?**
When you use ZRTP, you need extra bandwidth to carry signatures to
validate integrity.  This problem is solves in 0cpm Subliminal
Messaging by carrying signatures in the data channel, which steals
bandwidth from an audio codec.  The audio codec can continue as it
was.


## How can I use this?

This is a proof-of-concept.  The `sublime-inject` and
`sublime-extract` tools demonstrate how encryption can be applied
to traffic in transit, as well as injecting and extracting data
as part of the codec flow.

The tools demonstrate quite clearly how this is done.  The API is
well-designed are great care was put into rock-solid implementation.
So if you want to add this to your codec flow, you should find that
it is relatively easy.  After setup, you basically apply a filter
to your codec data:

```
sublime_recvmedia (recvfilt, buf, buflen);
```

or

```
sublime_sendmedia (sendfilt, buf, buflen);
```

There are no errors, if it fails it will quietly pickup the pieces
and recover.

Additional tools to look into are the `sublime-asm` and `sublime-disasm`
that change textual HDLC frames into the binary data form and vice versa.
This is used to feed the filters.


## What remains to be done?

  * We did not integrate with desktop tooling yet, but prepared structures
    for such backcalls.

  * We implemented only the A-law or PCMA codec.

  * The current crypto needs to roll from a key exchange.
