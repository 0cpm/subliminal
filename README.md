# Subliminal Messaging

> *Voice communication can be used to carry additional data.  Use cases
> range from simply sharing a URL during a phone call to a complete
> change of codec, possibly to a full VPN connection.  Dial-in computer
> networks are back!*

Telephone networks are no longer analog; they carry signals in a digital
form, encoded in a certain *codec*.  Most of these codecs offer some
room to sneak in a few bits of data.  This can carry a data signal at a
low bitrate.  This can be used as-is, but it is also possible to
negotiate a (more) complete use of the available bandwidth for digital
communication, and cease vocal communication altogether.

This approach requires setting apart noise from actual data.  This is
done with three levels, and it falls back at any point of failure:

  * Depending on the codec, bits are filtered out.  Frames are always
    surrounded by a pair of flags, each of which is a bit sequences.
  * Every frame ends with a checksum that is highly unlikely to match
    when the signal is in reality just noise.  The unlikelyhood of a
    spurious match drops exponentially with the number of frames.
  * There is a need for a handshake before actual communication is
    possible.  This establishes the presence of Subliminal Messageing.
  * For some functions, authentication (and possibly encryption) can
    be started; both are highly protective against deliberate attempts
    to forge traffic.

This is implemented as follows:

  * Bit insertion into noisy bits of a voice signal.  For instance,
    bits in low-volume G.711 samples or the data-ready lower 2 bits
    of a G.722 sample.
  * Alternatively, a voice signal that completely switched to data
    can transmit bytes, but some may have to be escaped and flags
    must still be distinguished from data bytes.  The flags are
    important, because they define the size of a data frame.
  * The format used is HDLC.  This separates data frames by flags,
    and each frame consists of an address, command, optional data
    and a CRC checksum.  Especially the protocol negotiation is
    very sensitive to bit errors.
  * The address indicates an application between end points.  Except
    for an initial address, none is available without asking via
    
  * Commands take care of the data link layer, such as setting up a
    connection, passing data, and handling resend windows when an
    error is detected.

Endpoints and proxies can work with this as follows:

  * Use the voice channel as a data link layer.
  * Send data by inserting it into the HDLC layer and letting it
    pass the data into the voice channel at the pace that it will
    permit.  HDLC handles in-order reliable transmission for an
    address-determined application.
  * When voice data is translated between codecs, it is necessary
    to extract the HDLC data from the input, and insert it into
    the output stream.  There is no need to understand anything
    about the application, though that could be used to decide
    whether the effort has any use.
  * Receive data from the HDLC layer on the other end and relay
    it to a service end point for the address.
  * Responses are sent back in the opposite direction, using the
    same mechanism.

Several addresses are predefined:

  * Management: Protocol Negotiation, set/reset complete data mode.
  * Security wrappers: ZRTP, TLS, OpenSSH, Kerberos, GSS-API, SASL, KIP.
  * Entropy building: Key exchange mechanisms.
  * Call handling: URI passing, SIP, SDP, MSRP, XMPP, CoAP, AMQP 1.0, MQTT.
  * Sessions: store-and-forward Fax, SMPP for SMS/MMS, Teletype with mulTTY, Mosh, X-Windows, RFB/VNC, Wireguard.

These are all pretty generic; they may be used in many different
ways depending on what traffic flows over them.


## Building

We build using CMake, so the usual sequence applies:

```
git clone https://gitlab.com/0cpm/subliminal
cd                                subliminal
mkdir build
cd    build
cmake ..
make
```


## Simulations

A few simulation sounds can be built quickly with

```
make test
```

They are located in the `sounds` subdirectory:

  * `quickfox.*` use an audio recording at normal speaking level
  * `quickloudfox.*` use an audio recording at an amplified speaking level
  * `*.al` are A-law recordings
  * `*.ul` are μ-law recordings
  * `*.noise.*` inserts random bits in the data bits for Subliminal Messaging
  * `*.filter.*` clears the data bits that are used for Subliminal Messaging
  * `*.pause.*` inserts zero bits like when data is paused for Subliminal Messaging

The conclusions are:

  * The noise level is noticable, but bearable under a conversation
  * The paused sounds is not detectably differet from the original sound
  * The filtered sounds are still noisy, and sound more restless
  * Open for investigation: Reconstruction filtering, perhaps with the z-Transform
  * Hearing-impaired people can avoid mmixint sound with data, and benefit from undisturbed sound during tranmissions pauses; but, especially to them, the ability to mix in content other than audio can bring relief

The simulations are made with these tools:

  * `g711simnoise` makes the `*.noise.*` files
  * `g711simufilter` makes the `*.filter.*` files
  * `g711simupause` makes the `*.pause.*` files

Two simulations that do not make test files, but are useful to verify the
performance on realistic networks:

  * `g711mangle` simulates sample mangling due to A-μ, μ-A, A-μ-A and
    μ-A-μ translations in the ISDN backbone, especially between the
    European and American/Japanese networks; Subliminal Messaging must
    make it through any sequence that starts and ends with A-law
  * `g711distort` simulate line distortions (including small burts) one
    transmission lines; the checksums in Subliminal Messaging should
    detect many of these problems with a high likelihood.


## Playing

*Note: At the time of this writing, the file is included raw, with only
`BREAK` and `FLAG` separators and "0" bit stuffing to suppress spurious
`FLAG` separators in data.*

You can now add files to an audio flow sampled in A-law, using

```
./hdlc2alawbits ../sounds/quickloudfox.al vocaldata.al infile1 infile2 infile3
```

The bitrate is quite high, more than a kilobyte per second.  So if you want to hear the
noise you may need to use longer data.  Tip: Add `/dev/urandom` after your input files.
You will be ticked off that not all of this infinite stream packs into the finite
voice sample, of course.

Now you can play the input and output files with your favourite audio tool,

```
play ../sounds/quickloudfox.al vocaldata.al
```

You will hear that a modest noise floor has been added.

You can also convert the A-law encoded files to a WAV at the same quality level,

```
sox -c 1 -r 8000 vocaldata.al vocaldata.wav
```

Now you have heard the noise-enhanced vocal, you will want to extract the data from it,

```
./alawbits2hdlc vocaldata.al outfile1 outfile2 outfile3
md5sum infile? outfile? | sort
```

Be sure to try this with very little data.  With no data to send, the sound is
still mangled somewhat, to create an explicit pause in data transmission.  This
is more subtle in effect and you are likely to not hear the mangling at all.
In other words, this paused mode would not quickly bother anyone.

Note, the `quickloudfox.ai` was stored in A-law with the highest possible volume.
This has two effects.  First, the noise floor is lower than in the more realistic
level of `../sounds/quickfox.al`.  Second, perhaps surprisingly, there is less
room for data.  You can pack 17280 bits/second in `quickloudfox.al` but as much
as 28800 bits/second into `quickfox.al` due to more low-valued samples.  During
silence, you should be able to go as high as 32000 bits/second with this method.

Note that the bitrates are among the highest from modem days.  We achieve that
at mere noise noise levels with ongoing pleasant communication!


## Overhead relative to Raw Data Rates

We expect to loose maybe 10% on the raw data rates.  So the example of a raw
rate of 17280 bits/second at maximum loudness would drop to a real rate of
about 17107 bits/second.

**HDLC adds bytes to each frame.**
The bit rates mentioned are raw bit rates, given `BREAK` and `FLAG` and in the
data flow we avoid spurious occurrences of `FLAG` separators with "0" bit
stuffing.  But when we transmit HDLC it will add a few bytes of overhead per
frame (address, command, checksum).  The files transmitted about would be those
HDLC frames, but they are now treated as raw data passed.  There is also a
feedback mechanism in HDLC, though that may piggyback on data transmission.

**The toll of A-μ-A translation.**
Not the entire world uses A-law, and so translations may mangle certain symbols.
We need to avoid those, resulting in slightly fewer values than the raw bit
capacity shown here without any such translation.  (Like, not transmitting
16 symbols in 4 bits but only 14 symbols, depending on the sound sample.)

