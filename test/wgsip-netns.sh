#!/bin/bash
#
# Test the wgsip programs between two network namespaces.
# This test is specifically designed for Linux systems.
#
# TODO: Incorporate CMake variables to reference programs.
#
# From: Rick van Rein <rick@openfortress.nl>


if [ ! -x ./wgsip-server -o ! -x ./wgsip-connect -o ! -x ./wgsip-disconnect ]
then
	echo >&2 'Please run from the build directory.  We need to run ./wgsip-*'
	exit 1
fi

if [ $UID != 0 ]
then
	echo >&2 'Please run as root.  We need ip-netns.'
	exit 1
fi


CLINS=test-wgsip-client
SRVNS=test-wgsip-server

echo 'Creating fresh test namespaces'
ip netns del $CLINS 2>/dev/null || true
ip netns del $SRVNS 2>/dev/null || true
ip netns add $CLINS
ip netns add $SRVNS
ip netns exec $CLINS ip link set up dev lo
ip netns exec $SRVNS ip link set up dev lo
echo

echo 'Connecting test namespaces'
# ip link add cli2srv netns $CLINS type veth peer srv2cli netns $SRVNS
ip link add srv2cli netns $SRVNS type veth peer cli2srv netns $CLINS
ip netns exec $CLINS ip link set cli2srv up
ip netns exec $SRVNS ip link set srv2cli up
ip netns exec $CLINS ip addr  add 2001:db8:abba::cddc/64 dev cli2srv
ip netns exec $SRVNS ip addr  add 2001:db8:12::34/64     dev srv2cli
ip netns exec $CLINS ip route add 2001:db8:12::/48       dev cli2srv
ip netns exec $SRVNS ip route add 2001:db8:abba::/48     dev srv2cli
# ip netns exec $CLINS ip link show
# ip netns exec $CLINS ip addr show
# ip netns exec $SRVNS ip link show
# ip netns exec $SRVNS ip addr show
echo

sleep 2
ip netns exec $CLINS ip -6 addr  show | sed 's/^/cli-addr: /'
ip netns exec $SRVNS ip -6 addr  show | sed 's/^/srv-addr: /'
ip netns exec $CLINS ip -6 route show | sed 's/^/cli-rout: /'
ip netns exec $SRVNS ip -6 route show | sed 's/^/srv-rout: /'
# ip netns exec $SRVNS tshark -ni any &
echo 'Starting wgsip server'
sleep 2
ip netns exec $SRVNS ./wgsip-server -f -c ../etc/wgsip.conf &
# ip netns exec $SRVNS nc -u -l -s :: -p 5070 &
DAEMON="$!"
# ip netns exec $SRVNS /bin/bash
# ip netns exec $SRVNS nc -u -l -s 2001:db8:12::34 -p 5070 &
# ip netns exec $SRVNS ss -l
echo

#USELESS# sleep 2
#USELESS# echo 'Testing the connection between client and server (THIS SHOULD FAIL)'
#USELESS# ip netns exec $SRVNS ping6 -n -c 2 2001:db8:456:1::1        2>&1 | sed 's/^/srv2srv: /'
#USELESS# ip netns exec $CLINS ping6 -n -c 2 2001:db8:456:34a2::12:13 2>&1 | sed 's/^/srv2cli: /'
#USELESS# ip netns exec $CLINS ping6 -n -c 2 2001:db8:456:1::1        2>&1 | sed 's/^/cli2srv: /'
#USELESS# ip netns exec $SRVNS ping6 -n -c 2 2001:db8:456:34a2::12:13 2>&1 | sed 's/^/cli2cli: /'

sleep 2
echo 'Connecting with the wgsip client'
ip netns exec $CLINS ./wgsip-connect -c ../etc/wgsip.conf john-cook &
CLIENT="$!"
sleep 5
kill $CLIENT 2>/dev/null
sleep 2
ps ax | grep $CLIENT | grep -v grep
kill -0 $CLIENT 2>/dev/null && kill -9 $CLIENT
sleep 1
echo

sleep 2
echo 'Testing the connection between client and server (should now be established)'
ip netns exec $SRVNS ping6 -n -c 2 2001:db8:456:1::1        2>&1 | sed 's/^/srv2srv: /'
ip netns exec $CLINS ping6 -n -c 2 2001:db8:456:34a2::12:13 2>&1 | sed 's/^/srv2cli: /'
ip netns exec $CLINS ping6 -n -c 2 2001:db8:456:1::1        2>&1 | sed 's/^/cli2srv: /'
ip netns exec $SRVNS ping6 -n -c 2 2001:db8:456:34a2::12:13 2>&1 | sed 's/^/cli2cli: /'

sleep 2
echo 'Disconnecting with the wgsip client'
ip netns exec $CLINS ./wgsip-disconnect -c ../etc/wgsip.conf john-cook &
CLIENT="$!"
sleep 5
kill $CLIENT 2>/dev/null
sleep 2
ps ax | grep $CLIENT | grep -v grep
kill -0 $CLIENT 2>/dev/null && kill -9 $CLIENT
sleep 1
echo

sleep 5
echo 'Testing the connection between client and server (THIS SHOULD FAIL)'
echo 'srv2srv: (skipped, may still be up)' #MAYBEUP# ip netns exec $SRVNS ping6 -n -c 2 2001:db8:456:1::1        2>&1 | sed 's/^/srv2srv: /'
ip netns exec $CLINS ping6 -n -c 2 2001:db8:456:34a2::12:13 2>&1 | sed 's/^/srv2cli: /'
ip netns exec $CLINS ping6 -n -c 2 2001:db8:456:1::1        2>&1 | sed 's/^/cli2srv: /'
echo 'srv2srv: (skipped, may still be up)' #MAYBEUP# ip netns exec $SRVNS ping6 -n -c 2 2001:db8:456:34a2::12:13 2>&1 | sed 's/^/cli2cli: /'

sleep 2
echo 'Killing wgsip server'
kill $DAEMON
sleep 2
ps ax | grep $DAEMON | grep -v grep
kill -0 $DAEMON 2>/dev/null && kill -9 $DAEMON
echo

echo 'Cleaning up test namespaces'
ip netns del $CLINS
ip netns del $SRVNS
echo

echo 'Done.  We hope you enjoyed our flight.'

