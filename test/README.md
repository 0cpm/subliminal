# Test techniques

Use the scripts in this directory to test.  They will log output in a test directory, which includes a version hash, so you can try multiple versions to track down an error.

Note that changes to a file can be traced back with things like

```
git blame ../lib/alaw.c | grep 2024 | cut -f 1,5-6 -d\  | sort | uniq
```

Don't forget to rebuild your historic checkout ;-)

