#!/bin/sh
#
# Take a text, inject it into a sound, extract it and compare.
# Print any error messages or differences and exit properly.
#
# Parameter=default:
#  - SRCROOT=.. is the source root directory
#  - BLDROOT=.  is the build  root directory
#  - SNDFILE    is the $SRCROOT sound file to play =sounds/quickfox.al
#  - ASMFILE    is the $SRCROOT HDLC  file to send =sounds/vlokje.hdlc
#
# Designed as CTest plugin and development tool.
#
# From: Rick van Rein <rick@openfortress.nl>


# Setup for stdout-only with given directories
#
EXEFILE=$(basename "$0")
SRCROOT=$(realpath ${1:-..})
BLDROOT=$(realpath ${2:-.})
SNDFILE=${3:-sounds/quickfox.al}
ASMFILE=${4:-sounds/vlokje.hdlc}
GITHASH=$(git -C "$SRCROOT" log -n 1 --format=%h)
TESTDIR="$BLDROOT/test/$GITHASH"
mkdir -p "$TESTDIR"
echo "GITHASH=$GITHASH TESTDIR=test/$GITHASH SRCROOT=$SRCROOT BLDROOT=$BLDROOT"
exec >"$TESTDIR/$EXEFILE.log" 2>&1

# Inject vlokje.hdlc and extract it
#
"$BLDROOT/sublime-inject" "$SRCROOT/$ASMFILE" \
	< "$SRCROOT/$SNDFILE" \
	> "$TESTDIR/quickfox-vlokje.al" \
	|| exit 1

# Hope to extract vlokje.hdlc
#
"$BLDROOT/sublime-extract" "$TESTDIR/quickfox-vlokje.hdlc" \
	< "$TESTDIR/quickfox-vlokje.al" \
	> "$TESTDIR/quickfox.al" \
	|| exit 1

# Assemble and disassemble vlokje.hdlc
#
"$BLDROOT/sublime-asm" \
	< "$SRCROOT/sounds/vlokje.hdlc" \
	> "$TESTDIR/vlokje-asm.bin" \
	|| exit 1
"$BLDROOT/sublime-disasm" \
	< "$TESTDIR/vlokje-asm.bin" \
	> "$TESTDIR/vlokje-asm-disasm.hdlc" \
	|| exit 1

# Final comparison between old and new ASCII text
#
diff "$TESTDIR/vlokje-asm-disasm.hdlc" "$TESTDIR/quickfox-vlokje.hdlc"
EXITVAL=$?

# Proper exit
#
echo
echo "exit($EXITVAL)"
exit $EXITVAL
