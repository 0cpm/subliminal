/* hdlc-asmin.rl -- Ragel grammar for HDLC assembler code.
 *
 * This parser recognises an assembly line format for HDLC.
 * This makes it easy to parse files and process instructions
 * as a flow of frames to transmit.  Various dynamic forms of
 * data can be included herein.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define SUBLIMINAL_BASE_INTERNAL
#define SUBLIMINAL_HDLC_ASM_INTERNAL
#include <0cpm/subliminal.h>



%%{

machine HDLC;

comment = [;] . [^\r\n]*
	;

spaces = [ \t]+
	;

byte = [0-9a-fA-F]{2}
		${ uint8_t dig = (*p | 0x20) - '0'; if (dig > 9) dig -= 39;
		   tmpbyte = (tmpbyte << 4) | dig; }
	;

address = byte
		@{ hdlcbuf[0] = tmpbyte; }
	;

# Allowing ZX Spectrum double quotes, so "...""..."
string =  (
	  ["]
		${ if (infoffset >= sizeof (sublime_hdlc_buffer_t)) fbreak;
		   if (use_quote) hdlcbuf [infoffset++] = *p; }
	. [^"\r\n\\]*
		${ if (infoffset >= sizeof (sublime_hdlc_buffer_t)) fbreak;
		   hdlcbuf [infoffset++] = *p; }
	. ["]
		${ use_quote = true; }
	)+
		>{ use_quote = false; }
	;

hex = [#] .
	(
	  byte
		@{ if (infoffset >= sizeof (sublime_hdlc_buffer_t)) fbreak;
		   hdlcbuf [infoffset++] = tmpbyte; }
	)+
	;

information = (
	  string . spaces?
	| hex . spaces?
	)+
	;

command = "I"
		@{ hdlcbuf[1] = HDLC_I(0,0); }
	  . spaces . information
	| "UI"
		@{ hdlcbuf[1] = HDLC_UI(); }
	  . spaces . information
	| "XID"
		@{ hdlcbuf[1] = HDLC_XID(); }
	  . spaces . information
	| "SABM"
		@{ hdlcbuf[1] = HDLC_SABM(); }
	| "DISC"
		@{ hdlcbuf[1] = HDLC_DISC(); }
	| "TEST"
		@{ hdlcbuf[1] = HDLC_TEST(); }
	  . spaces . information
	| "UP"
		@{ hdlcbuf[1] = HDLC_UP(); }
	| "DM"
		@{ hdlcbuf[1] = HDLC_DM(); }
	| "RR"
		@{ hdlcbuf[1] = HDLC_RR(0); }
	| "RNR"
		@{ hdlcbuf[1] = HDLC_RNR(0); }
	| "REJ"
		@{ hdlcbuf[1] = HDLC_REJ(0); }
	| "SREJ"
		@{ hdlcbuf[1] = HDLC_SREJ(0); }
	;

macro = "BREAK"
		@{ hdlcbreak = true; }
	# | "KEY"
	#   . spaces . information
	# | "UNIDIR"
	;

instruction =
	  address?
	. spaces
	. ( command | "." . macro )
	. spaces? . comment
	;

line := ( ""
	| spaces
	| comment
	| instruction
	) . "\r"? . "\n"
	;

}%%


%% write data;


static void *swapcb_data = NULL;

static sublime_swapper_callback_t *swapcb_cbfun = NULL;

static FILE *asmin  = NULL;

static unsigned line_in  = 0;

static uint8_t   lastaddr_in  = 0x00;

static uint8_t N_S = 0;

void sublime_hdlcasm_setup_infile_swapper (void *data, sublime_swapper_callback_t *cbfun) {
	swapcb_data  = data;
	swapcb_cbfun = cbfun;
}

FILE *sublime_hdlcasm_swap_infile (FILE *newin) {
	FILE *retval = asmin;
	asmin = newin;
	line_in = 0;
	lastaddr_in = 0x00;
	N_S = 0;
	return retval;
}


/* Parse a single line of HDLC assembly code.
 *
 * Place the command in the hdlcbuf, and return the infoend, with a few
 * impossible values interpreted differently:
 *
 *  - Size 0 indicates that no command is returned
 *  - Size 1 indicates a BREAK
 *  - Size ~0 and other high values are dropped as errors,
 *            including Information fields in excess of 256 bytes
 *
 * At calling time, infoffset is provided as 2, but it may be set higher
 * when so desired.
 *
 * At calling time, the last address (or 0x00 if there was none) has been
 * setup in hdlcbuf[0].  This allows us to not set it and still get the
 * desired result.
 */
static unsigned parseline (unsigned linenr, char *txtptr, unsigned txtlen, unsigned infoffset, sublime_hdlc_buffer_t hdlcbuf) {
	//
	// Data fields for the parsed instruction
	unsigned infoffset_orig = infoffset;
	bool hdlcbreak = false;
	bool error = false;
	uint8_t tmpbyte = 0;
	bool use_quote = false;
	//
	// Parser configuration
	int cs;
	char *p   = txtptr;
	char *pe  = txtptr + txtlen;
	char *eof = pe;
	//
	// Instantiate the HDLC machine and call it
	%%{
		machine HDLC;
		write init;
		write exec;
	}%%
	//
	// Return a BREAK signal if requested
	if (hdlcbreak) {
		return 1;
	}
	//
	// Test if the resulting frame is not too large
	if (infoffset - infoffset_orig > 256) {
		fprintf (stderr, "%4d %.*sInformation field contains %d > 256 bytes\n", linenr, txtlen, txtptr, infoffset - infoffset_orig);
		return ~0;
	}
	//
	// Test if the machine exited correctly
#ifdef ALLOW_WONTWORK
	if ((cs <= HDLC_first_final) || (p < pe)) {
printf ("%d <= %d || %p < %p\n", cs, HDLC_first_final, p, pe);
		fprintf (stderr, "%4d %.*sTrailing garbage: \"%.*s\"\n", linenr, txtlen, txtptr, (int) (pe - p), p);
		return ~0;
	}
#endif
	//
	// Return the actual length of the HDLC frame
	return infoffset;
}


unsigned sublime_hdlc_getframe (struct sublime *sender, sublime_hdlc_buffer_t hdlcbuf) {
	//
	// Only continue when we have an input file
retry:
	if (asmin == NULL) {
		if (swapcb_cbfun != NULL) {
			swapcb_cbfun (swapcb_data);
		}
		if (asmin == NULL) {
			fprintf (stderr, "Please configure HDLC assembler input file\n");
			return 0;
		}
	}
	//
	// Read a single line at a time
	char linebuf [1024];
	while (fgets (linebuf, 1024, asmin) != NULL) {
		//
		// We found another line, so increment the lineno counter
		line_in++;
		//
		// Setup the buffer for the line assembler
		hdlcbuf [0] = lastaddr_in;
		//
		// Parse the line as assembly code
		unsigned linelen = strlen (linebuf);
		unsigned infoffset = 2;
		unsigned precheck = parseline (line_in, linebuf, linelen, infoffset, hdlcbuf);
		if (precheck == 0) {
			//
			// No line found, continue parsing the next line
			continue;
		} else if (precheck == 1) {
			//
			// Send FLAG and/or BREAK, whatever the caller deems worthy
			return 0;
		} else if (precheck > 2 + 256) {
			//
			// Ignore this non-HDLC line (which may have printed an error)
			// and continue parsing the next line
			continue;
		} else {
			//
			// Proper frame from assembler, produce output for it
			//
			if ((hdlcbuf [1] & 0x01) == 0x00) {
				hdlcbuf [1] = (hdlcbuf [1] & 0xf1) | ((N_S++ << 1) & 0x0e);
			}
			//
			// Set the Check field with the CRC-16/6sub8 operation
			unsigned checklen = sublime_base_makecheck (sender, hdlcbuf, precheck, &hdlcbuf [precheck]);
			//
			// Only learn the new default address from a proper frame
			lastaddr_in = hdlcbuf [0];
			//
			// Return the frame as we have it now
			return (precheck + checklen);
		}
	}
	//
	// At EOF, attempt to switch to restart with another file
	if (feof (asmin)) {
		if (swapcb_cbfun != NULL) {
			line_in++;
			swapcb_cbfun (swapcb_data);
			if (line_in == 0) {
				goto retry;
			}
		}
	}
	//
	// No (more) frames available
	return 0;
}

