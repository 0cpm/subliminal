
#line 1 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
/* assembler.rl -- Ragel grammar for HDLC assembler code.
 *
 * This parser recognises an assembly line format for HDLC.
 * This makes it easy to parse files and process instructions
 * as a flow of frames to transmit.  Various dynamic forms of
 * data can be included herein.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define SUBLIMINAL_BASE_INTERNAL
#include <0cpm/subliminal.h>




#line 121 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"




#line 32 "/home/vanrein/Git/sublime/build/hdlc-asmin.c"
static const char _HDLC_actions[] = {
	0, 1, 0, 1, 3, 1, 4, 1, 
	7, 1, 8, 1, 9, 1, 10, 1, 
	11, 1, 12, 1, 13, 1, 14, 1, 
	15, 1, 16, 1, 17, 1, 18, 1, 
	19, 2, 0, 1, 2, 0, 6, 2, 
	2, 5, 2, 5, 2
};

static const char _HDLC_key_offsets[] = {
	0, 0, 11, 23, 24, 25, 26, 27, 
	28, 29, 32, 34, 36, 37, 38, 40, 
	44, 48, 53, 58, 64, 70, 81, 84, 
	85, 86, 88, 89, 90, 91, 92, 93, 
	94, 95, 97, 98, 99, 105, 107, 117
};

static const char _HDLC_trans_keys[] = {
	9, 10, 13, 32, 59, 48, 57, 65, 
	70, 97, 102, 9, 10, 13, 32, 46, 
	68, 73, 82, 83, 84, 85, 88, 10, 
	66, 82, 69, 65, 75, 9, 32, 59, 
	10, 13, 73, 77, 83, 67, 9, 32, 
	9, 32, 34, 35, 10, 13, 34, 92, 
	9, 32, 34, 35, 59, 9, 32, 34, 
	35, 59, 48, 57, 65, 70, 97, 102, 
	48, 57, 65, 70, 97, 102, 9, 32, 
	34, 35, 59, 48, 57, 65, 70, 97, 
	102, 69, 78, 82, 74, 82, 65, 82, 
	66, 77, 69, 74, 69, 83, 84, 73, 
	80, 73, 68, 48, 57, 65, 70, 97, 
	102, 9, 32, 9, 32, 46, 68, 73, 
	82, 83, 84, 85, 88, 0
};

static const char _HDLC_single_lengths[] = {
	0, 5, 12, 1, 1, 1, 1, 1, 
	1, 3, 2, 2, 1, 1, 2, 4, 
	4, 5, 5, 0, 0, 5, 3, 1, 
	1, 2, 1, 1, 1, 1, 1, 1, 
	1, 2, 1, 1, 0, 2, 10, 0
};

static const char _HDLC_range_lengths[] = {
	0, 3, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 3, 3, 3, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 3, 0, 0, 0
};

static const unsigned char _HDLC_index_offsets[] = {
	0, 0, 9, 22, 24, 26, 28, 30, 
	32, 34, 38, 41, 44, 46, 48, 51, 
	56, 61, 67, 73, 77, 81, 90, 94, 
	96, 98, 101, 103, 105, 107, 109, 111, 
	113, 115, 118, 120, 122, 126, 129, 140
};

static const char _HDLC_indicies[] = {
	0, 2, 3, 0, 5, 4, 4, 4, 
	1, 0, 2, 3, 0, 6, 7, 8, 
	9, 10, 11, 12, 13, 1, 2, 1, 
	14, 1, 15, 1, 16, 1, 17, 1, 
	18, 1, 19, 19, 5, 1, 2, 3, 
	5, 20, 21, 1, 22, 1, 23, 1, 
	24, 24, 1, 24, 24, 25, 26, 1, 
	1, 1, 28, 1, 27, 29, 29, 30, 
	26, 5, 1, 29, 29, 25, 26, 5, 
	1, 31, 31, 31, 1, 32, 32, 32, 
	1, 29, 29, 25, 26, 5, 31, 31, 
	31, 1, 33, 34, 35, 1, 36, 1, 
	37, 1, 38, 39, 1, 40, 1, 41, 
	1, 42, 1, 43, 1, 44, 1, 45, 
	1, 46, 1, 47, 48, 1, 49, 1, 
	50, 1, 51, 51, 51, 1, 52, 52, 
	1, 52, 52, 6, 7, 8, 9, 10, 
	11, 12, 13, 1, 1, 0
};

static const char _HDLC_trans_targs[] = {
	2, 0, 39, 3, 36, 10, 4, 11, 
	14, 22, 25, 30, 33, 34, 5, 6, 
	7, 8, 9, 9, 12, 9, 13, 9, 
	15, 16, 19, 16, 17, 18, 16, 20, 
	21, 23, 24, 9, 9, 9, 26, 28, 
	27, 9, 29, 9, 31, 32, 14, 14, 
	9, 35, 14, 37, 38
};

static const char _HDLC_trans_actions[] = {
	0, 0, 0, 0, 1, 0, 0, 0, 
	7, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 31, 0, 0, 21, 0, 15, 
	0, 42, 0, 3, 5, 0, 39, 1, 
	36, 0, 0, 23, 27, 25, 0, 0, 
	0, 13, 0, 29, 0, 0, 17, 9, 
	19, 0, 11, 33, 0
};

static const int HDLC_start = 1;
static const int HDLC_first_final = 39;
static const int HDLC_error = 0;

static const int HDLC_en_line = 1;


#line 125 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"


static FILE *asmin  = NULL;

static unsigned line_in  = 0;

static uint8_t   lastaddr_in  = 0x00;

static uint8_t N_S = 0;

FILE *sublime_hdlcasm_swap_infile (FILE *newin) {
	FILE *retval = asmin;
	asmin = newin;
	line_in = 0;
	lastaddr_in = 0x00;
	N_S = 0;
	return retval;
}


/* Parse a single line of HDLC assembly code.
 *
 * Place the command in the hdlcbuf, and return the infoend, with a few
 * impossible values interpreted differently:
 *
 *  - Size 0 indicates that no command is returned
 *  - Size 1 indicates a BREAK
 *  - Size ~0 and other high values are dropped as errors,
 *            including Information fields in excess of 256 bytes
 *
 * At calling time, infoffset is provided as 2, but it may be set higher
 * when so desired.
 *
 * At calling time, the last address (or 0x00 if there was none) has been
 * setup in hdlcbuf[0].  This allows us to not set it and still get the
 * desired result.
 */
static unsigned parseline (unsigned linenr, char *txtptr, unsigned txtlen, unsigned infoffset, sublime_hdlc_buffer_t hdlcbuf) {
	//
	// Data fields for the parsed instruction
	unsigned infoffset_orig = infoffset;
	bool hdlcbreak = false;
	bool error = false;
	uint8_t tmpbyte = 0;
	bool use_quote = false;
	//
	// Parser configuration
	int cs;
	char *p   = txtptr;
	char *pe  = txtptr + txtlen;
	char *eof = pe;
	//
	// Instantiate the HDLC machine and call it
	
#line 195 "/home/vanrein/Git/sublime/build/hdlc-asmin.c"
	{
	cs = HDLC_start;
	}

#line 200 "/home/vanrein/Git/sublime/build/hdlc-asmin.c"
	{
	int _klen;
	unsigned int _trans;
	const char *_acts;
	unsigned int _nacts;
	const char *_keys;

	if ( p == pe )
		goto _test_eof;
	if ( cs == 0 )
		goto _out;
_resume:
	_keys = _HDLC_trans_keys + _HDLC_key_offsets[cs];
	_trans = _HDLC_index_offsets[cs];

	_klen = _HDLC_single_lengths[cs];
	if ( _klen > 0 ) {
		const char *_lower = _keys;
		const char *_mid;
		const char *_upper = _keys + _klen - 1;
		while (1) {
			if ( _upper < _lower )
				break;

			_mid = _lower + ((_upper-_lower) >> 1);
			if ( (*p) < *_mid )
				_upper = _mid - 1;
			else if ( (*p) > *_mid )
				_lower = _mid + 1;
			else {
				_trans += (unsigned int)(_mid - _keys);
				goto _match;
			}
		}
		_keys += _klen;
		_trans += _klen;
	}

	_klen = _HDLC_range_lengths[cs];
	if ( _klen > 0 ) {
		const char *_lower = _keys;
		const char *_mid;
		const char *_upper = _keys + (_klen<<1) - 2;
		while (1) {
			if ( _upper < _lower )
				break;

			_mid = _lower + (((_upper-_lower) >> 1) & ~1);
			if ( (*p) < _mid[0] )
				_upper = _mid - 2;
			else if ( (*p) > _mid[1] )
				_lower = _mid + 2;
			else {
				_trans += (unsigned int)((_mid - _keys)>>1);
				goto _match;
			}
		}
		_trans += _klen;
	}

_match:
	_trans = _HDLC_indicies[_trans];
	cs = _HDLC_trans_targs[_trans];

	if ( _HDLC_trans_actions[_trans] == 0 )
		goto _again;

	_acts = _HDLC_actions + _HDLC_trans_actions[_trans];
	_nacts = (unsigned int) *_acts++;
	while ( _nacts-- > 0 )
	{
		switch ( *_acts++ )
		{
	case 0:
#line 34 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ uint8_t dig = (*p | 0x20) - '0'; if (dig > 9) dig -= 39;
		   tmpbyte = (tmpbyte << 4) | dig; }
	break;
	case 1:
#line 39 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[0] = tmpbyte; }
	break;
	case 2:
#line 45 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ if (infoffset >= sizeof (sublime_hdlc_buffer_t)) {p++; goto _out; }
		   if (use_quote) hdlcbuf [infoffset++] = *p; }
	break;
	case 3:
#line 48 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ if (infoffset >= sizeof (sublime_hdlc_buffer_t)) {p++; goto _out; }
		   hdlcbuf [infoffset++] = *p; }
	break;
	case 4:
#line 51 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ use_quote = true; }
	break;
	case 5:
#line 53 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ use_quote = false; }
	break;
	case 6:
#line 59 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ if (infoffset >= sizeof (sublime_hdlc_buffer_t)) {p++; goto _out; }
		   hdlcbuf [infoffset++] = tmpbyte; }
	break;
	case 7:
#line 71 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_I(0,0); }
	break;
	case 8:
#line 74 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_UI(); }
	break;
	case 9:
#line 77 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_XID(); }
	break;
	case 10:
#line 80 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_SABM(); }
	break;
	case 11:
#line 82 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_DISC(); }
	break;
	case 12:
#line 84 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_TEST(); }
	break;
	case 13:
#line 87 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_UP(); }
	break;
	case 14:
#line 89 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_DM(); }
	break;
	case 15:
#line 91 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_RR(0); }
	break;
	case 16:
#line 93 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_RNR(0); }
	break;
	case 17:
#line 95 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_REJ(0); }
	break;
	case 18:
#line 97 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbuf[1] = HDLC_SREJ(0); }
	break;
	case 19:
#line 101 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"
	{ hdlcbreak = true; }
	break;
#line 358 "/home/vanrein/Git/sublime/build/hdlc-asmin.c"
		}
	}

_again:
	if ( cs == 0 )
		goto _out;
	if ( ++p != pe )
		goto _resume;
	_test_eof: {}
	_out: {}
	}

#line 182 "/home/vanrein/Git/sublime/lib/hdlc-asmin.rl"

	//
	// Return a BREAK signal if requested
	if (hdlcbreak) {
		return 1;
	}
	//
	// Test if the resulting frame is not too large
	if (infoffset - infoffset_orig > 256) {
		fprintf (stderr, "%4d %.*sInformation field contains %d > 256 bytes\n", linenr, txtlen, txtptr, infoffset - infoffset_orig);
		return ~0;
	}
	//
	// Test if the machine exited correctly
#ifdef ALLOW_WONTWORK
	if ((cs <= HDLC_first_final) || (p < pe)) {
printf ("%d <= %d || %p < %p\n", cs, HDLC_first_final, p, pe);
		fprintf (stderr, "%4d %.*sTrailing garbage: \"%.*s\"\n", linenr, txtlen, txtptr, (int) (pe - p), p);
		return ~0;
	}
#endif
	//
	// Return the actual length of the HDLC frame
	return infoffset;
}


unsigned sublime_hdlc_getframe (struct sublime *sender, sublime_hdlc_buffer_t hdlcbuf) {
	//
	// Only continue when we have an input file
	if (asmin == NULL) {
		fprintf (stderr, "Please configure HDLC assembler input file\n");
		return 0;
	}
	//
	// Read a single line at a time
	char linebuf [1024];
	while (fgets (linebuf, 1024, asmin) != NULL) {
		//
		// We found another line, so increment the lineno counter
		line_in++;
		//
		// Setup the buffer for the line assembler
		hdlcbuf [0] = lastaddr_in;
		//
		// Parse the line as assembly code
		unsigned linelen = strlen (linebuf);
		unsigned infoffset = 2;
		unsigned precheck = parseline (line_in, linebuf, linelen, infoffset, hdlcbuf);
		if (precheck == 0) {
			//
			// No line found, continue parsing the next line
			continue;
		} else if (precheck == 1) {
			//
			// Send FLAG and/or BREAK, whatever the caller deems worthy
			return 0;
		} else if (precheck > 2 + 256) {
			//
			// Ignore this non-HDLC line (which may have printed an error)
			// and continue parsing the next line
			continue;
		} else {
			//
			// Proper frame from assembler, produce output for it
			//
			if ((hdlcbuf [1] & 0x01) == 0x00) {
				hdlcbuf [1] = (hdlcbuf [1] & 0xf1) | ((N_S++ << 1) & 0x0e);
			}
			//
			// Set the Check field with the CRC-16/6sub8 operation
			unsigned checklen = sublime_base_makecheck (sender, hdlcbuf, precheck, &hdlcbuf [precheck]);
			//
			// Only learn the new default address from a proper frame
			lastaddr_in = hdlcbuf [0];
			//
			// Return the frame as we have it now
			return (precheck + checklen);
		}
	}
	//
	// No (more) frames available
	return 0;
}

