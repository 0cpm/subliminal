/* crypto.c -- The Subliminal Messaging support for cryptography.
 *
 * Cryptographic operations generally mangle content to make it
 * unreadable, and sign it to avoid modification of content.
 *
 * Subliminal Messaging generally relies on counter mode, which
 * means that encryption bytes are pre-generated.  It collects
 * data for signing.  Both are used independently of inner or
 * outer mode.  Callbacks to cryptographic frameworks enable
 * updates when a buffer runs empty or full; and when the data
 * stream needs such a callback.  The API between Sublime and
 * the crypto framework is intended to be general, in support of
 * updates of crypto mechanisms.
 *
 * The functions below serve as plugins to libsublime.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#ifdef DEBUG
#include <stdio.h>
#endif

#include <assert.h>

#include <sys/random.h>


#define SUBLIMINAL_CRYPTO_INTERNAL
#include <0cpm/subliminal.h>



#if SUBLIME_CRYPTO_VERSION != 1
#  error "This crypto framework was written for sublime crypto API version 1"
#endif

#if SUBLIME_CRYPTO_FUNCTIONS != 4
#  error "This crypto framework defines 4 functions in sublime crypto API"
#endif


/* The initialisation of a scramblecrypto structure can be used to overwrite
 * an existing crypto algorithm if it goes awry.  This is always possible due
 * to the minimum size used here.  The API calls will be overwritten.
 */
static void sublime_scramblecrypto_init (struct sublime_crypto *sc);



/********** NULL CRYPTOGRAPHY **********/



/** Null cryptography creates trivial encryption and signing.
 *
 * Encryption uses all-zero XOR pads, that is, it does not change the data.
 * Signing uses CRC16-6sub8, so a plain and short checksum without keying.
 */
struct sublime_nullcrypto {
	struct sublime_crypto std;
	uint16_t crc;
	uint8_t out [2];
};


static void sublime_nullcrypto_close (struct sublime_crypto *sc) {
	//
	// Nothing to erase, only deallocate
	if (sc != NULL) {
		free (sc);
	}
}


static void sublime_nullcrypto_mkpad (struct sublime_crypto *sc, int uses) {
	//
	// Generate XOR padding for the first and/or last parts
	if ((uses & SUBLIME_XORPAD_FIRST) && (sc->iv_free_first == sc->iv_endpad_first)) {
		sc->iv_endpad_first += 256;
	}
	if ((uses & SUBLIME_XORPAD_LAST ) && (sc->iv_free_last  == sc->iv_endpad_last )) {
		sc->iv_endpad_last -= 256;
	}
	//
	// Protect against IV vectors crossing sometime soon
	// in which case we will overwrite ourselves
	if (sc->iv_endpad_first > sc->iv_endpad_last) {
		//TODO// fprintf (stderr, "IV vector outage is looming.  Panic.  Crossing over to scrambler mode.\n");
		(void) sublime_scramblecrypto_init (sc);
	}
}


static void sublime_nullcrypto_update (struct sublime_crypto *sc, uint8_t *inbuf, uint16_t inlen) {
	extern uint16_t crc16_6sub8 [];
	struct sublime_nullcrypto *nc = (struct sublime_nullcrypto *) sc;
	//
	// Compute the crc16-6sub8 algorithm with the extra bytes
	uint16_t crc = nc->crc;
	while (inlen-- > 0) {
		crc = (crc >> 8) ^ crc16_6sub8 [0xff & (crc ^ *inbuf)];
		inbuf++;
	}
}


static void sublime_nullcrypto_sign (struct sublime_crypto *sc, bool reset, uint8_t **outbuf, uint16_t *outlen) {
	struct sublime_nullcrypto *nc = (struct sublime_nullcrypto *) sc;
	//
	// Export the signature via sc->out
	nc->out [0] = (nc->crc >> 8);
	nc->out [1] = (nc->crc & 0xff);
	*outbuf = nc->out;
	*outlen = 2;
	//
	// Reset the signature if that is desired
	if (reset) {
		nc->crc = 0xffff;
	}
}


struct sublime_crypto *sublime_nullcrypto_open (const char *keyfile) {
	//
	// Allocate a zeroed structure
	struct sublime_nullcrypto *nc = calloc (sizeof (struct sublime_nullcrypto), 1);
	if (nc != NULL) {
		//
		// Initialise the CRC and the last free
		nc->std.iv_free_last = 0xffffffff;
		nc->crc = 0xffff;
		//
		// Initialise the null crypto with initial non-zero settings
		nc->std.iv_endpad_first = 0x00000100;
		nc->std.iv_endpad_last  = 0xfffffeff;
		nc->std.iv_free_last    = 0xffffffff;
		//
		// Setup the API calls for null crypto
		nc->std.crypto_mkpad  = sublime_nullcrypto_mkpad;
		nc->std.crypto_update = sublime_nullcrypto_update;
		nc->std.crypto_sign   = sublime_nullcrypto_sign;
		nc->std.crypto_close  = sublime_nullcrypto_close;
	}
	//
	// Return the value even if it is NULL
	return (struct sublime_crypto *) nc;
}




#ifdef DEBUG

/********** PREDICTABLE DEBUG CRYPTOGRAPHY **********/




/** Debugging crypto is deliberately predictable, independent of keys.
 *
 * The idea is to have a good test medium that does erratic things,
 * which would easily trigger any bugs, but in a reproducible manner.
 *
 * When defined, the value SUBLIME_CRYPTO_DEBUG_SEED is used to prime
 * the generation.  The _first seed is this value, the _last seed is
 * the value XOR 0x7fffffff.  Signatures are fixed to 0x12, 0x34.
 */
struct sublime_debugcrypto {
	struct sublime_crypto std;
	struct random_data rndbuf_first;
	struct random_data rndbuf_last;
	uint8_t rndstate64_first [64];
	uint8_t rndstate64_last  [64];
	uint8_t sig [2];
};


#ifndef SUBLIME_CRYPTO_DEBUG_SEED
#define SUBLIME_CRYPTO_DEBUG_SEED 0x31415926
#endif


static void sublime_debugcrypto_close (struct sublime_crypto *sc) {
	//
	// Nothing to clear, only deallocate
	if (sc != NULL) {
		free (sc);
	}
}


static void sublime_debugcrypto_mkpad (struct sublime_crypto *sc, int uses) {
	struct sublime_debugcrypto *dc = (struct sublime_debugcrypto *) sc;
	//
	// Generate XOR padding for the first and/or last parts
	if ((uses & SUBLIME_XORPAD_FIRST) && ((sc->iv_endpad_first & 0x00) == 0x00)) {
		int i = 0;
		while (true) {
			int32_t pad31 = 0xffffff;
			random_r (&dc->rndbuf_first, &pad31);
			dc->std.xorpad_first [i++] = (pad31 >> 16) & 0xff;
			if (i == 256) {
				break;
			}
			assert (i <= 256-3);
			dc->std.xorpad_first [i++] = (pad31 >>  8) & 0xff;
			dc->std.xorpad_first [i++] = (pad31      ) & 0xff;
		}
		sc->iv_endpad_first += 256;
	}
	if ((uses & SUBLIME_XORPAD_LAST ) && ((sc->iv_endpad_last  & 0xff) == 0xff)) {
		int i = 0;
		while (true) {
			int32_t pad31 = 0xffffff;
			random_r (&dc->rndbuf_first, &pad31);
			dc->std.xorpad_last  [i++] = (pad31 >> 16) & 0xff;
			if (i == 256) {
				break;
			}
			assert (i <= 256-3);
			dc->std.xorpad_last  [i++] = (pad31 >>  8) & 0xff;
			dc->std.xorpad_last  [i++] = (pad31      ) & 0xff;
		}
		sc->iv_endpad_last -= 256;
	}
	//
	// Protect against IV vectors crossing sometime soon
	// in which case we will overwrite ourselves
	if (sc->iv_endpad_first > sc->iv_endpad_last) {
		//TODO// fprintf (stderr, "IV vector outage is looming.  Panic.  Crossing over to scrambler mode.\n");
		(void) sublime_scramblecrypto_init (sc);
	}
}


static void sublime_debugcrypto_update (struct sublime_crypto *sc, uint8_t *inbuf, uint16_t inlen) {
	//
	// Trivially ignore the data, because our signatures are fixed
	;
}


static void sublime_debugcrypto_sign (struct sublime_crypto *sc, bool reset, uint8_t **outbuf, uint16_t *outlen) {
	struct sublime_debugcrypto *dc = (struct sublime_debugcrypto *) sc;
	//
	// Export the signature via dc->sig
	*outbuf = dc->sig;
	*outlen = 2;
	//
	// Reset the signature if that is desired
	;
}


struct sublime_crypto *sublime_debugcrypto_open (const char *keyfile) {
	//
	// Clear the entire structure
	struct sublime_debugcrypto *dc = calloc (sizeof (struct sublime_debugcrypto), 1);
	if (dc != NULL) {
		//
		// Set the only non-zero counters, namely the _last area
		dc->std.iv_free_last =
		dc->std.iv_endpad_last = 0xffffffff;
		//
		// Setup a trivial signature
		dc->sig [0] = 0x12;
		dc->sig [1] = 0x34;
		//
		// Setup the random generators
		initstate_r (SUBLIME_CRYPTO_DEBUG_SEED,
				dc->rndstate64_first, 64,
				&dc->rndbuf_first);
		initstate_r (SUBLIME_CRYPTO_DEBUG_SEED ^ 0x7fffffff,
				dc->rndstate64_last,  64,
				&dc->rndbuf_last );
		//
		// Setup the API calls for debug crypto
		dc->std.crypto_mkpad  = sublime_debugcrypto_mkpad;
		dc->std.crypto_update = sublime_debugcrypto_update;
		dc->std.crypto_sign   = sublime_debugcrypto_sign;
		dc->std.crypto_close  = sublime_debugcrypto_close;
	}
	//
	// Return the pointer, even if it is NULL
	return (struct sublime_crypto *) dc;
}

#endif /* DEBUG */




/********** DESTRUCTIVE SCRAMBLER CRYPTOGRAPHY **********/



/* Scrambler crypto makes data unreadable, and signs unverifiably.
 * It will bring you a lot of trouble and very little data.
 * This can be used as a fallback when you goofed up.
 */



static void sublime_scramblecrypto_close (struct sublime_crypto *sc) {
	//
	// Nothing to clear, only deallocate
	if (sc != NULL) {
		free (sc);
	}
}


static void sublime_scramblecrypto_mkpad (struct sublime_crypto *sc, int uses) {
	//
	// Generate XOR padding for the first and/or last parts
	// and set the indexes appropriately
	if ((uses & SUBLIME_XORPAD_FIRST) && ((sc->iv_endpad_first & 0xff) == 0x00)) {
		assert (256 == getrandom (sc->xorpad_first, 256, 0));
		sc->iv_endpad_first += 256;
	}
	if ((uses & SUBLIME_XORPAD_LAST ) && ((sc->iv_endpad_last  & 0xff) == 0xff)) {
		assert (256 == getrandom (sc->xorpad_last , 256, 0));
		sc->iv_endpad_last -= 256;
	}
}


static void sublime_scramblecrypto_update (struct sublime_crypto *sc, uint8_t *inbuf, uint16_t inlen) {
	//
	// Bluntly ignored request to produce solid evidence
	;
}


static void sublime_scramblecrypto_sign (struct sublime_crypto *sc, bool reset, uint8_t **outbuf, uint16_t *outlen) {
	//
	// Do not produce infinite amounts of non-sense
	uint16_t rndlen = *outlen;
	if (rndlen > 16) {
		rndlen = 16;
	}
	//
	// Produce random nonsense
	if (rndlen > 0) {
		rndlen = getrandom (outbuf, *outlen, 0);
	}
	//
	// Report how many bytes we scrambled
	*outlen = rndlen;
}


static void sublime_scramblecrypto_init (struct sublime_crypto *sc) {
	//
	// Clear the entire structure, and open a random device
	memset (sc, 0, sizeof (struct sublime_crypto));
	sc->iv_free_last = 0xffffffff;
	//
	// Setup the API calls for null crypto
	sc->crypto_mkpad  = sublime_scramblecrypto_mkpad;
	sc->crypto_update = sublime_scramblecrypto_update;
	sc->crypto_sign   = sublime_scramblecrypto_sign;
	sc->crypto_close  = sublime_scramblecrypto_close;
}


struct sublime_crypto *sublime_scramblecrypto_open (const char *keyfile) {
	//
	// Allocate a scrambler crypto structure
	struct sublime_crypto *sc = calloc (sizeof (struct sublime_crypto), 1);
	//
	// Initialise data and API
	if (sc != NULL) {
		sublime_scramblecrypto_init (sc);
	}
	//
	// Return the return, even if it is NULL
	return sc;
}
