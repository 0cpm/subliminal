/* lib/sublime.c -- API functions to handle Subliminal Messaging.
 *
 * Filtering to inject data into a codec, or extract data from it.
 *
 * Software dependencies:
 *  - inih to parse .INI files
 *  - uuid to derive UUIDs from domain names
 *  - utlist.h (static) to manage the list of backends
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define SUBLIMINAL_CODEC_INTERNAL
#define SUBLIMINAL_CRYPTO_INTERNAL
#define SUBLIMINAL_HDLC_INTERNAL
#define SUBLIMINAL_BACK_INTERNAL
#include <0cpm/subliminal.h>

#include "utlist.h"



/* Nesting/repeat counter for sublime_init() --> sublime_fini()
 */
static unsigned _initctr = 0;



/********** INTERNAL UTILITY FUNCTIONS **********/



/* Add a Check field at the indicated location.
 * Take keys and crypgraphic modes into account.
 * Return the number of Check bytes added.
 */
unsigned sublime_base_makecheck (struct sublime *sbl, uint8_t *frame, unsigned prechecklen, uint8_t *check) {
	extern uint16_t crc16_6sub8 [];
	if (check == NULL) {
		check = &frame[prechecklen];
	}
	//
	// Run through the CRC-16/6sub8 computation
	uint16_t crc = 0xffff;
	while (prechecklen-- > 0) {
		crc = (crc << 8) ^ crc16_6sub8 [(crc >> 8) ^ (*frame++)];
	}
	check [0] = (crc >> 8);
	check [1] = (crc & 0xff);
	return 2;
}


/* Verify the Check field at the end of the frame.
 * Return the number of bytes of the remaining message,
 * or 0 in case of any failure, including with the Check
 * field value.
 */
unsigned sublime_base_testcheck (struct sublime *sbl, uint8_t *frame, unsigned framelen) {
	unsigned checklen = 2;
	if (framelen <= checklen) {
		return 0;
	}
	uint8_t proper_check [checklen];
	unsigned checklen_actual = sublime_base_makecheck (sbl, frame, framelen - checklen, proper_check);
	if (checklen_actual != checklen) {
		return 0;
	}
	if (memcmp (proper_check, frame + framelen - checklen, checklen) != 0) {
		return 0;
	}
	return framelen - checklen;
}


/* Obtain the backend for a given address and must, must_not
 * constraints to the address_flags.  Return NULL on failure.
 */
struct sublime_back *sublime_base_match_backend (struct sublime *sbl, uint8_t address, uint8_t must, uint8_t must_not) {
	if (sbl == NULL) {
		return NULL;
	}
	struct sublime_back *retval = sbl->addr2back [address];
	if (retval != NULL) {
		if ((retval->address_flags & (must | must_not)) != must) {
			retval = NULL;
		}
	}
	return retval;
}



/********** SUBLIME BASE API FOR CODECS, HDLC AND BACKENDS **********/



bool sublime_base_reverse (struct sublime *sbl, struct sublime **optout_reverse) {
	if (optout_reverse != NULL) {
		*optout_reverse = sbl->reverse;
	}
	return (sbl->unidir) || (sbl->reverse != NULL);
}


bool sublime_base_fill_hdlc_buffer (struct sublime *sender) {
	//
	// Do not transmit any frames until we are attached to a
	// reverse channel or are setup for unidirection sending.
	if ((sender->reverse == NULL) && !sender->unidir) {
		return false;
	}
	//
	// Consider the next free slot; it will later be allocated
	// for an I-frame, or bluntly used for S- and U-frames.
	if (! SUBLIME_WINDOW_ENTRY_OK (sender->firstfree)) {
		return false;
	}
	//
	// Fetch the HDLC content for the buffer and return
	// its success; it will allocate the firstfree for an
	// I-frame, or bluntly use it without allocation fro
	// S-frames and U-frames
	sender->hdlclen = sublime_hdlc_getframe (sender, sender->window [sender->firstfree]);
	sender->hdlcptr = (sender->hdlclen == 0) ? NULL : sender->window [sender->firstfree];
	return (sender->hdlcptr != NULL);
}


void sublime_base_acknowledge (struct sublime *sender, uint8_t address, uint8_t command) {
	//
	// Weed out any U-frames, as they carry no N(R)
	if (command & 0x03 == 0x03) {
		return;
	}
	//
	// Find the backend that is communicating on this address
	struct sublime_back *back = sublime_base_match_backend (sender, address, SUBLIME_BACK_ADDRESS_SABM_UP, SUBLIME_BACK_ADDRESS_DISC_UP);
	if (back == NULL) {
		return;
	}
	//
	// Find the current N(R) and find the acknowledged range
	uint8_t new_N_R = command >> 5;
	uint8_t cur_N_R = back->N_R;
	//
	// Return now if nothing would be found
	if (cur_N_R == new_N_R) {
		return;
	}
	//
	// Match flags for the low 4 fields of an I command rrrPsss0
	// These test that (1) it is an I-frame, (2) N(S) == sss
	bool match [16];
	memset (match, 0, sizeof (match));
	while (((new_N_R - cur_N_R) & 0x07) != 0x00) {
		match [2 * (cur_N_R & 0x07)] = true;
		cur_N_R++;
	}
	//
	// Store the new N(R) value for future runs
	back->N_R = cur_N_R;
	//
	// Run over the window and remove 
	for (uint8_t widx = 0; widx < SUBLIME_WINDOW_ENTRIES; widx++) {
		//
		// Skip other addresses
		if (sender->window [widx] [0] != address) {
			continue;
		}
		//
		// Skip if the command does not fit the command
		if (! match [sender->window [widx] [1] & 0x04]) {
			continue;
		}
		//
		// Found one!  Disable its future find, and make free
		sender->window [widx] [0] =
		sender->window [widx] [1] = 0xff;
		sender->nextfree [widx] = sender->firstfree;
		sender->firstfree = widx;
	}
}


void sublime_base_delivered_I (struct sublime_back *back, uint8_t N_R) {
	//
	// This is called on the receiver's end and represents what
	// was received from the remote sender, so their window state,
	// which will be reported back in our I-frames and S-frames
	//
	// It is fully assumed that backend calls were made in order,
	// and so are these callbacks.  This can be achieved when the
	// back->N_S is used to track which I-frame to deliver next
	// and N_R to track which I-frame has been delivered.
	back->N_R = N_R;
	//TODO// FREE THE WINDOW FROM THE DELIVERED ELEMENT(S)
}


bool sublime_base_setup_hdlc_buffer (struct sublime *recver) {
	//
	// Consider the next free slot; fail if none is free
	if (! SUBLIME_WINDOW_ENTRY_OK (recver->firstfree)) {
		return false;
	}
	//
	// Setup the first frame without allocating it yet
	recver->hdlcptr = recver->window [recver->firstfree];
	recver->hdlclen = sizeof (sublime_hdlc_buffer_t);
	//
	// Return success
	return true;
}


void sublime_base_deliver_hdlc_buffer (struct sublime *recver) {
	//
	// Reproduce the originally returned hdlcbuf and hdlclen
	// and reduce the latter by the remaining size
	uint8_t *hdlcptr = recver->window [recver->firstfree];
	unsigned hdlclen = sizeof (sublime_hdlc_buffer_t) - recver->hdlclen;
	//
	// Return quietly when the frame is incorrect
	// This includes detection of NULL in hdlcptr
	if (recver->hdlcptr != &hdlcptr [hdlclen]) {
		return;
	}
	//
	// Compare the Check field to the expectations
	// and return if an error occurs
	hdlclen = sublime_base_testcheck (recver, hdlcptr, hdlclen);
	if (hdlclen == 0) {
		return;
	}
	//
	// Print the frame contents
	uint8_t address = hdlcptr [0];
	uint8_t command = hdlcptr [1];
#ifdef DEBUG
	fprintf (stderr, "Address 0x%02x, Command 0x%02x, Data %.*s, Check 0x%02x%02x\n",
			address,
			command,
			hdlclen - 2, hdlcptr + 2,
			hdlcptr [hdlclen+0],
			hdlcptr [hdlclen+1]);
#endif
	//
	// Produce an acknowledgement in the reverse (ignored for U-frames)
	sublime_base_acknowledge (recver->reverse, address, command);
	//
	// Delegate the frame to the HDLC implementation
	sublime_hdlc_putframe (recver, address, command, hdlcptr + 2, hdlclen - 2);
	//
	// Reset the buffer
	sublime_base_reset_hdlc_buffer (recver);
}


uint8_t sublime_base_allocframe (struct sublime *sbl) {
	uint8_t oldfirst = sbl->firstfree;
	sbl->firstfree = sbl->nextfree [oldfirst];
	return oldfirst;
}


void sublime_base_freeframe (struct sublime *sbl, uint8_t allocframe) {
	sbl->nextfree [allocframe] = sbl->firstfree;
	sbl->firstfree = allocframe;
}


static struct sublime_crypto *sublime_crypto_factory (int crypto_algcode, const char *keyfile) {
	switch (crypto_algcode) {
#ifdef SUBLIME_CRYPTO_NULL
	case SUBLIME_CRYPTO_NULL:
		return sublime_nullcrypto_open (keyfile);
#endif
#ifdef SUBLIME_CRYPTO_AES128GCM
	case SUBLIME_CRYPTO_AES128GCM:
		"TODO:IMPLEMENT";
		return sublime_aes128gcmcrypto_open (keyfile);
#endif
#ifdef SUBLIME_CRYPTO_SCRAMBLE
	case SUBLIME_CRYPTO_SCRAMBLE:
		return sublime_scramblecrypto_open (keyfile);
#endif
	default:
#ifdef DEBUG
	if ((crypto_algcode & 0x0f) == 0x0f) {
		/* Switch to DEBUG crypto */
		return sublime_debugcrypto_open (keyfile);
	}
#endif
	/* Do not switch crypto */
	return NULL;
	}
}


void sublime_base_outercrypto_preset (struct sublime *our, int crypto_algcode, const char *keyfile) {
	//
	// Tolerate NULL pointers
	if (our == NULL) {
		return;
	}
	//
	// Allocate new next crypto
	struct sublime_crypto *new_crypto = sublime_crypto_factory (crypto_algcode, keyfile);
	//
	// Skip a missing crypto attempt
	if (new_crypto == NULL) {
		return;
	}
	//
	// Clear any outstanding next crypto (as they were overruled)
	if (our->outercrypto_next != NULL) {
		our->outercrypto_next->crypto_close (our->outercrypto_next);
	}
	//
	// Install the newly created crypto as the intended next
	our->outercrypto_next = new_crypto;
}


static void sublime_base_fill_crypto (struct sublime_crypto *crypt, uint8_t *xorm, unsigned size) {
	//
	// Loop to collect the required number of bytes from buffered XOR pad backcalls
	while (true) {
		//
		// Check how much is available now
		int64_t avail = crypt->iv_endpad_first - crypt->iv_free_first;
		//
		// We should not have overdrawn our account
		assert (avail >= 0);
		//
		// We should never find more than 256 buffered bytes
		assert (avail <= 256);
		//
		// Copy as much as is available, inasfar as we need it
		if (avail > 0) {
			if (avail > size) {
				avail = size;
			}
#ifdef DEBUG
			// fprintf (stderr, "copying %3d --- ", (int) avail);
#endif
			memcpy (xorm, crypt->xorpad_first + (crypt->iv_free_first & 0x000000ff), avail);
			crypt->iv_free_first += avail;
			//
			// We should not have overdrawn our account
			assert (crypt->iv_free_first <= crypt->iv_endpad_first);
		}
		//
		// Remove the size that was available
#ifdef DEBUG
		// fprintf (stderr, "size =%6d, done%6d --- ", (int) size, (int) avail);
#endif
		xorm += avail;
		size -= avail;
		//
		// Maybe we are done eating
		if (size == 0) {
			return;
		}
		//
		// If we are still hungry, we need to iterate
		crypt->crypto_mkpad (crypt, SUBLIME_XORPAD_FIRST);
#ifdef DEBUG
		// fprintf (stderr, "free_first = %ld, endpad_first = %ld\n", crypt->iv_free_first, crypt->iv_endpad_first);
#endif
	}
}


void sublime_base_outercrypto_sync (struct sublime *our, uint8_t *hash_xorm, unsigned hashsz, unsigned xormsz) {
	//
	// Tolerate NULL pointers
	if ((our == NULL) || (hash_xorm == NULL)) {
		return;
	}
	//
	// Tolerate not having anything to do
	if (our->outercrypto_next == NULL) {
		return;
	}
	//
	// Use hash_xorm [0..hashsz-1] as old-signature input
	uint8_t *old_hash = "";
	uint16_t old_hashsz = 0;
	struct sublime_crypto *old_crypto = our->outercrypto_curr;
	old_crypto->crypto_update (old_crypto, hash_xorm + our->signskip, hashsz - our->signskip);
	old_crypto->crypto_sign (old_crypto, true, &old_hash, &old_hashsz);
	our->signskip = hashsz;
	//
	// Cleanup the possibly sensitive hashed information
	memset (hash_xorm, 0, hashsz);
	//
	// Make the actual switch
	our->outercrypto_curr = our->outercrypto_next;
	our->outercrypto_next = NULL;
	//
	// Send the old signature to the new crypto signer
	our->outercrypto_curr->crypto_update (our->outercrypto_curr, old_hash, old_hashsz);
	//
	// Cleanup the old crypto structure
	old_crypto->crypto_close (old_crypto);
	//
	// Fill the requested size with encryption bytes
	sublime_base_fill_crypto (our->outercrypto_curr, hash_xorm + hashsz, xormsz);
}



/********** SUBLIME API FUNCTIONS **********/



bool sublime_init (void) {
	bool ok = true;
	//
	// Manage nesting/repeat counter
	if (_initctr++ > 0) {
		goto conclude;
	}
	//
	// Initialise the backend
	ok = ok && back_init ();
	//
	// Indicate if any backends were configured
conclude:
	return ok;
}

void sublime_fini (void) {
	//
	// Lower nesting/repeat counter
	if (--_initctr > 0) {
		return;
	}
	//
	// Cleanup
	back_fini ();
}

struct sublime *sublime_open (bool recv_not_send, const char *mediatype, const char *subtype) {
	struct sublime *new = NULL;
	//
	// Test the mediatype (TODO, we only process "audio")
	//TODO// Continuations of the word?
	if (strncmp (mediatype, "audio", 5) != 0) {
		fprintf (stderr, "Passing non-audio mediatyype ...%s\n", mediatype);
		return NULL;
	}
	//
	// Test the subtype (TODO, we only process "PCMA")
	//TODO// Continuations of the word?
	if (strncmp (subtype, "PCMA", 4) != 0) {
		fprintf (stderr, "Passing non-PCMA media subtype ...%s\n", subtype);
		return NULL;
	}
	struct sublime_codec *codec = &sublime_codec_alaw;
	if ((codec->version != SUBLIME_CODEC_VERSION) || ( codec->functions < SUBLIME_CODEC_FUNCTIONS)) {
		fprintf (stderr, "Incompatible codec for audio, PCMA\n");
		return NULL;
	}
	//
	// Allocate a management structure; quietly accept any failure
	if (codec->create != NULL) {
		new = codec->create (recv_not_send, subtype);
	} else {
		new = calloc (1, sizeof (struct sublime));
	}
	if (new != NULL) {
		//
		// Initialise the structure inasfar as the backend did not do that
		new->reverse = NULL;
		new->unidir = false;
		new->recv_not_send = recv_not_send;
		//
		// Initialise the codec reference (if it was not set more specifically)
		if (new->codec == NULL) {
			new->codec = codec;
		}
		//
		// Initialise an empty HDLC buffer
		new->hdlcptr = NULL;
		new->hdlclen = 0;
		//
		// Free the window entries
		new->firstfree = 0;
		for (uint8_t i = 0; i < SUBLIME_WINDOW_ENTRIES; i++) {
			new->window [i][0] =
			new->window [i][1] = 0xff;
			new->nextfree [i] = i + 1;
		}
		new->nextfree [SUBLIME_WINDOW_ENTRIES-1] = SUBLIME_WINDOW_ENTRY_NIL;
		//TODO// TEST BACKEND ENTRIES
		extern struct sublime_back test_back_aa;
		extern struct sublime_back test_back_bb;
		new->addr2back [0xaa] = &test_back_aa;
		new->addr2back [0xbb] = &test_back_bb;
	}
	//
	// Return the newly created sublime abstract data type
	return new;
}

void sublime_close (struct sublime *sbl) {
	//
	// Ensure detachment
	sublime_detach (sbl, false);
	//
	// Tolerate NULL pointers
	if (sbl == NULL) {
		return;
	}
	//
	// Cleanup any outstanding crypto with their own secure wipe
	if (sbl->outercrypto_curr != NULL) {
		sbl->outercrypto_curr->crypto_close (sbl->outercrypto_curr);
	}
	if (sbl->outercrypto_next != NULL) {
		sbl->outercrypto_next->crypto_close (sbl->outercrypto_curr);
	}
	//
	// Cleanup data
	if (sbl->codec->destroy != NULL) {
		sbl->codec->destroy (sbl);
	} else {
		free (sbl);
	}
}

void sublime_attach (struct sublime *recver, struct sublime *sender) {
	//
	// Ensure detachment
	sublime_detach (recver, false);
	sublime_detach (sender, false);
	//
	// Tolerate NULL pointers
	if ((recver == NULL) || (sender == NULL)) {
		return;
	}
	if ((recver->codec == NULL) || (sender->codec == NULL)) {
		return;
	}
	if ((recver->codec->recvmedia == NULL) || (sender->codec->sendmedia == NULL)) {
		return;
	}
	//
	// Make it happen
	recver->reverse = sender;
	sender->reverse = recver;
}

void sublime_detach (struct sublime *either, bool unidir) {
	//
	// Tolerate NULL pointers
	if (either == NULL) {
		return;
	}
	//
	// Take note of unidirectional mode
	either->unidir = unidir;
	//
	// Make it happen
	if (either->reverse != NULL) {
		either->reverse->reverse = NULL;
		either->reverse          = NULL;
	}
}

struct sublime *sublime_other (struct sublime *either) {
	//
	// Tolerate NULL pointers
	if (either == NULL) {
		return NULL;
	}
	//
	// Lookup the other, which may be NULL
	return either->reverse;
}

void sublime_sendmedia (struct sublime *sender, uint8_t *data, unsigned size) {
	//
	// Tolerate NULL pointers
	if ((sender == NULL) || (sender->codec == NULL) || (sender->codec->sendmedia == NULL)) {
		return;
	}
	//
	// Quietly ignore the wrong direction
	if (sender->recv_not_send == SUBLIME_RECV) {
		return;
	}
	//
	// Require that at least NULL crypto is considered
	if (sender->outercrypto_curr == NULL) {
		sender->outercrypto_curr = sublime_nullcrypto_open (NULL);
		if (sender->outercrypto_curr == NULL) {
			return;
		}
	}
	//
	// Have a reusable stack buffer for the XOR mask
#if 0
//TODO// Problematic on re-entry
	static uint8_t *xorm = NULL;
	static uinsigned xormlen = 0;
	if (size > xormlen) {
		xorm = realloc (xorm, size + 256);
		if (xorm == NULL) {
			perror ("PANIC: Encryption failure");
			exit (1);
		}
	}
#endif
//TODO// Problematic with large buffers and code optimisation
	uint8_t xorm [size];
	//
	// Fill the XOR mask buffer with just the right size
	sublime_base_fill_crypto (sender->outercrypto_curr, xorm, size);
	//
	// Make it happen
	sender->signskip = 0;
	sender->codec->sendmedia (sender, data, xorm, size);
	//
	// Process the former XOR mask as a signature hash buffer
	sender->outercrypto_curr->crypto_update (sender->outercrypto_curr, xorm + sender->signskip, size - sender->signskip);
	//
	// Clear the XOR mask stack buffer
	//TODO// Problematic if this is conceaved as dead code
	memset (xorm, 0, size);
}

void sublime_recvmedia (struct sublime *recver, uint8_t *data, unsigned size) {
	//
	// Tolerate NULL pointers
	if ((recver == NULL) || (recver->codec == NULL) || (recver->codec->recvmedia == NULL)) {
		return;
	}
	//
	// Quietly ignore the wrong direction
	if (recver->recv_not_send == SUBLIME_SEND) {
		return;
	}
	//
	// Require that at least NULL crypto is considered
	if (recver->outercrypto_curr == NULL) {
		recver->outercrypto_curr = sublime_nullcrypto_open (NULL);
		if (recver->outercrypto_curr == NULL) {
			return;
		}
	}
	//
	// Have a reusable stack buffer for the XOR mask
#if 0
//TODO// Problematic on re-entry
	static uint8_t *xorm = NULL;
	static uinsigned xormlen = 0;
	if (size > xormlen) {
		xorm = realloc (xorm, size + 256);
		if (xorm == NULL) {
			perror ("PANIC: Encryption failure");
			exit (1);
		}
	}
#endif
//TODO// Problematic with large buffers and code optimisation
	uint8_t xorm [size];
	//
	// Fill the XOR mask buffer with just the right size
	sublime_base_fill_crypto (recver->outercrypto_curr, xorm, size);
	//
	// Make it happen
	recver->signskip = 0;
	recver->codec->recvmedia (recver, data, xorm, size);
	//
	// Process the former XOR mask as a signature hash buffer
	recver->outercrypto_curr->crypto_update (recver->outercrypto_curr, xorm + recver->signskip, size - recver->signskip);
	//
	// Clear the XOR mask stack buffer
	//TODO// Problematic if this is conceaved as dead code
	memset (xorm, 0, size);
}
