/* hdlc.c -- HDLC handling to drive the linked backend
 *
 * The HDLC layer contains protocol knowledge, and sits between the
 * Sublime base and the backend.  It is aware of the difference
 * between I-frame and the various S-frames and U-frames, and does
 * the hard work in processing them semantically.
 *
 * Concurrency is also organised at the HDLC level, because it helps
 * to determine whether fillup with FLAG or even BREAK are needed,
 * versus having data available for delivery.  Concurrency decouples
 * backend timing from frontend responsiveness; we are dealing with
 * codecs after all, and they need to be processed with low jitter.
 *
 * Core functions sublime_hdlc_putframe() and sublime_hdlc_getframe()
 * are used from the Sublime base, and they can make callbacks for
 * such things as window managament with sublime_base_allocframe() and
 * sublime_base_freeframe().
 *
 * Alternative implementations of the HDLC functions are possible,
 * namely for text input and output via the assembler and disassembler.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define SUBLIMINAL_HDLC_INTERNAL
#define SUBLIMINAL_BACK_INTERNAL
#include <0cpm/subliminal.h>



void sublime_hdlc_putframe (struct sublime *recver, uint8_t address, uint8_t command, uint8_t *info, unsigned infolen) {
	//
	// Handle in a manner depending on the frame type
	if ((command & 0x01) == 0x00) {
		//
		// Allocate the I-frame in the window, to be faster than immediate delivery
		uint8_t I_frame = sublime_base_allocframe (recver);
		//
		// Process the received I-frame
		//TODO// Enforce strict N(S) ordering
		uint8_t N_S = (command & 0xe0) >> 1;
		//
		// Process the backend for the targeted address
		struct sublime_back *back = sublime_base_match_backend (recver, address, SUBLIME_BACK_ADDRESS_SABM_DOWN, SUBLIME_BACK_ADDRESS_DISC_DOWN);
		if (back == NULL) {
			fprintf (stderr, "No connected backend service for  I command to address 0x%02x\n", address);
		} else if (!sublime_back_send_I (back, info, infolen, N_S)) {
			fprintf (stderr, "Backend did not swallow I-frame\n");
			//
			// Recover the window entry because it is neither processed nor freed
			sublime_base_freeframe (recver, I_frame);
		}
	} else if ((command & 0x03) == 0x01) {
		//
		// Process the received S-frame
		//TODO// RR, RNR, REJ, SREJ -- call for action
		fprintf (stderr, "Received S-frame but processing the request is a TODO\n");
	} else {
		//
		// Process the received U-frame
		switch (command & 0xec) {
		case HDLC_UI() & 0xec:
			{ ; }
			struct sublime_back *back = sublime_base_match_backend (recver, address, SUBLIME_BACK_ADDRESS_SABM_DOWN, SUBLIME_BACK_ADDRESS_DISC_DOWN);
			if (back != NULL) {
				sublime_back_send_UI (back, info, infolen);
			} else {
				fprintf (stderr, "No connected backend service for UI command to address 0x%02x\n", address);
			}
			break;
		case HDLC_XID() & 0xec:
			fprintf (stderr, "TODO: Handle service negotiation for address 0x%02x\n", address);
			break;
		case HDLC_SABM() & 0xec:
			fprintf (stderr, "TODO: Handle connection setup for address 0x%02x\n", address);
			break;
		case HDLC_DM() & 0xec:
			fprintf (stderr, "TODO: Handle failed connection for address 0x%02x\n", address);
			break;
		case HDLC_UP() & 0xec:
			fprintf (stderr, "TODO: Handle polling request by responding with RR for address 0x%02x\n", address);
			break;
		case HDLC_TEST() & 0xec:
			fprintf (stderr, "TODO: Handle test by sending back the same bytes for address 0x%02x\n", address);
			break;
		default:
			fprintf (stderr, "Dropping unsupported U-frame command 0x%02x to address 0x%02x\n", command, address);
			break;
		}
	}
}


unsigned sublime_hdlc_getframe (struct sublime *sender, sublime_hdlc_buffer_t hdlcbuf) {
	//
	// Try S-frames first; they are tiny and cut down on work
	struct sublime_back *to_send;
	unsigned size = SUBLIME_INFOSIZE;
	unsigned cksz = 2;
	if (false /* TODO: Possibly construct an S-frame */ ) {
		//
		// Do not claim the sender buffer; it will not
		// change until the next call to this function
		;
		//
		// Populate the Address and Command fields
		hdlcbuf [0] = to_send->address;
		//TODO// hdlcbuf [1] = ...(to_send->N_R);
		//
		// Populate the Check field
		cksz = sublime_base_makecheck (sender, hdlcbuf, 2 + size, NULL);
		//
		// Tell the caller to send this frame
		return 2 + size + cksz;
	}
	//
	// Try UI before I because that could loose traffic
	to_send = sublime_back_poll_UI (&hdlcbuf[2], &size);
	if (to_send != NULL) {
		//
		// Do not claim the sender buffer; it will not
		// change until the next call to this function
		;
		//
		// Populate the Address and Command fields
		hdlcbuf [0] = to_send->address;
		hdlcbuf [1] = HDLC_UI();
		//
		// Populate the Check field
		cksz = sublime_base_makecheck (sender, hdlcbuf, 2 + size, NULL);
		//
		// Tell the caller to send this frame
		return 2 + size + cksz;
	}
	//
	// Try I; most backends will arrange flow control,
	// so this is the least of our throughput concerns
	to_send = sublime_back_poll_I (&hdlcbuf[2], &size);
	if (to_send != NULL) {
		//
		// Claim the sender buffer
		struct sublime *reverse;
		bool unidir = sublime_base_reverse (sender, &reverse);
		if (!unidir) {
			uint8_t I_frame = sublime_base_allocframe (sender);
		}
		//
		// Derive the N_R field to send, namely what the
		// remote end has been able to deliver to the
		// attached receiver
		uint8_t remote_N_R;
		struct sublime_back *otherback = sublime_base_match_backend (reverse, to_send->address, SUBLIME_BACK_ADDRESS_SABM_UP, SUBLIME_BACK_ADDRESS_DISC_UP);
		if (otherback != NULL) {
			remote_N_R = otherback->N_R;
		} else {
			remote_N_R = 0x00;
		}
		//
		// Populate the Address and Command fields,
		// adopt the frame by advancing N_S in the backend
		// N_R represents the remote's sender window!
		hdlcbuf [0] = to_send->address;
		hdlcbuf [1] = HDLC_I(remote_N_R, to_send->N_S++);
		if (unidir) {
			to_send->N_R++;
		}
		//
		// Populate the Check field
		cksz = sublime_base_makecheck (sender, hdlcbuf, 2 + size, NULL);
		//
		// Tell the caller to send this frame
		return 2 + size + cksz;
	}
	//
	// No data pending, tell the caller to stop sending frames
	return 0;
}

