/* alaw.c -- The Subliminal Messaging support for the A-law codec
 *
 * A-law is the European ISDN codec, and it is also common in VoIP.
 * It is used in preference over μ-law because that has poor quality
 * for lower volumes, causing far less data to be transported.
 *
 * The functions below serve as plugins to libsublime.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <string.h>

#define SUBLIMINAL_CODEC_INTERNAL
#include <0cpm/subliminal.h>


//DEBUG// #include <stdio.h>


#if SUBLIME_CODEC_VERSION != 1
#  error "This codec was written for sublime codec API version 1"
#endif

#if SUBLIME_CODEC_FUNCTIONS != 4
#  error "This codec defines 4 functions for sublime codec API"
#endif



/********** SETTINGS FOR ALAW_SENDMEDIA **********/



/*
 * Modes distinguish what to do.  Minor modes count up, and may end in a next major mode.
 *
 * Major modes:
 *  - MODE_MAJOR_START, send break and flag signals.
 *     - Minors 0..15 send 0111111101111110
 *     - Minor 1 can be used for BREAK, 0-FLAG
 *     - Minor 8 can be used for 0-FLAG
 *     - Minor 9 can be used for FLAG after confirmed "0" bit
 *  - MODE_MAJOR_DATA, send bits of data
 *     - Minor 0 is after a "0" bit in the data
 *     - Minor 1..5 after 1..5 bits "1" in the data
 *     - Minor 6 to bit-stuff a "0" and continue to minor 0
 *  - MODE_MAJOR_END, send flag and break signals
 *     - Minor 0..14 send 011111101111111
 *     - Minor 0 sends an ending 0-FLAG
 *     - Minor 7 checks for new data, and sets it up for MODE_MAJOR_DATA, minor 0
 *     - Minor 8 can be a starting point for a new line without data
 *     - Minor 8..14 continue to transmit a final BREAK to stop from chasing data
 *     - Minor 15 makes a last check for new data, and may jump back to MODE_MAJOR_START, minor 7
 *  - MODE_MAJOR_SILENT, send no data at all (but zero low bits in A-law samples)
 */
#define MODE_MAJOR_START  0x00
#define MODE_MAJOR_DATA   0x10
#define MODE_MAJOR_END    0x20
#define MODE_MAJOR_SILENT 0x30
#define MODE_MAJOR_MASK   0xf0


typedef enum {
	SEND_ZERO      = 0,
	SEND_ONE       = 1,
	PASS_DATABIT   = 2,
	STOP_SENDING   = 3,
	SEND_ZERO_SYNC = 4
} bitact_t;


/* Array of mangled codes.  This is used before writing the LSB, to decide whether
 * the higher bits victimise the LSB to fixation in a A-μ-A translation process.
 * When this is the case, we choose to not put a data bit in it, but we send
 * the value that would be rewritten, so the recipient can detect whether it is.
 * The index to this array is the un-flipped current byte, with either LSB bit
 * (so all entries occur twice) and the response is SEND_ZERO or SEND_ONE to
 * fixate the LSB, or PASS_DATABIT to allow a databit in this position.
 *
 * Negative and positive values look alike, except for their MSB.  This means
 * that the table lists the same 128 values in two series.
 */
static bitact_t lsbact_mangling [256] = {
	// 0x5.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0x4. -- ends with 0x4d<-0x4c, 0x4f<-0x4e, 0x49<-0x48, 0x4b<-0x4a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	// 0x7. -- ends with 0x79->0x78, 0x7b->0x7a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ZERO   , SEND_ZERO   , SEND_ZERO   , SEND_ZERO   ,
	// 0x6. -- ends with 0x6b->0x6a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x1. -- ends with 0x1b<-0x1a
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x0.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0x3.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0x2.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xD.  Copies 0x5.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xC.  Copies 0x4.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	SEND_ONE    , SEND_ONE    , SEND_ONE    , SEND_ONE    ,
	// 0xF.  Copies 0x7.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	SEND_ZERO   , SEND_ZERO   , SEND_ZERO   , SEND_ZERO   ,
	// 0xE.  Copies 0x6.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x9.  Copies 0x1.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, SEND_ZERO   , SEND_ZERO   ,
	// 0x8.  Copies 0x0.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xB.  Copies 0x3.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	// 0xA.  Copies 0x2.
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
	PASS_DATABIT, PASS_DATABIT, PASS_DATABIT, PASS_DATABIT,
};


/* Wrapper for the next bit to send.  The next bit may in fact indicate that
 * it is ready to STOP_SENDING, instead of just SEND_ZERO or SEND_ONE.  In
 * general, the wrap_nextbit() task is to manage its *mode and insert any
 * bits before or after the protocol for bit-level HDLC transmission.
 *
 * This is an inline function because it is mostly a logic decision tree for
 * the HDLC bit protocol; it assumes it will be inserted in one place only,
 * and can be juggled mercylessly by an optimising compiler.
 */
static inline bitact_t wrap_nextbit (uint8_t *mode, bitact_t nextbit) {
	uint8_t cur_mode = *mode;
	switch (cur_mode & MODE_MAJOR_MASK) {
	case MODE_MAJOR_START:
		/* Send a "0" or "1" bit as determined by BREAK and FLAG */
		*mode = cur_mode + 1;
		if (cur_mode == MODE_MAJOR_START + 0x08) {
			return SEND_ZERO_SYNC;
		} else if (cur_mode == MODE_MAJOR_START + 0x0f) {
			return SEND_ZERO;
		} else {
			return SEND_ONE;
		}
	case MODE_MAJOR_DATA:
		if (cur_mode == MODE_MAJOR_DATA + 5) {
			/* Stuff a "0" bit into the sequence */
			*mode = MODE_MAJOR_DATA;
			return SEND_ZERO;
		} else if (nextbit == STOP_SENDING) {
			/* Stop sending data, already send the initial "0" now */
			*mode = MODE_MAJOR_END + 1;
			return SEND_ZERO;
		} else {
			/* Send a data bit */
			if (nextbit == SEND_ZERO) {
				*mode = MODE_MAJOR_DATA;
			} else {
				*mode = cur_mode + 1;
			}
			return PASS_DATABIT;
		}
	case MODE_MAJOR_END:
		/* Send a pattern of "0" and "1" for FLAG and filler zeroes */
		if ((nextbit != STOP_SENDING) && (cur_mode < MODE_MAJOR_END + 6)) {
			/* rejuvenate the BREAK transmission into first FLAG */
			// Old: BREAK starts at MODE_MAJOR_END   + 0
			// New: FLAG  starts at MODE_MAJOR_START + 8
			*mode = cur_mode - (MODE_MAJOR_END + 0) + (MODE_MAJOR_START + 9);
			if (cur_mode == MODE_MAJOR_START) {
				return SEND_ZERO;
			} else {
				return SEND_ONE;
			}
		}
		if (cur_mode < MODE_MAJOR_END + 15) {
			/* Send the FLAG and BREAK bit sequence */
			*mode = cur_mode + 1;
			if ((cur_mode == MODE_MAJOR_END) || (cur_mode == MODE_MAJOR_END + 7)) {
				return SEND_ZERO;
			} else {
				return SEND_ONE;
			}
		} else if (nextbit != STOP_SENDING) {
			/* We have more data to send, after BREAK so first send FLAG */
			*mode = MODE_MAJOR_START + 9;
			return SEND_ZERO;
		} else {
			/* We have no more data to send, so we switch to silent / zeroing mode */
			*mode = MODE_MAJOR_SILENT;
			return STOP_SENDING;
		}
	case MODE_MAJOR_SILENT:
		/* We silenced HDLC bit transmission, but may be asked to restart it */
		if (nextbit != STOP_SENDING) {
			*mode = MODE_MAJOR_START + 9;
			return SEND_ZERO;
		}
		return STOP_SENDING;
	}
}


/* Map a single data byte to 8 bit actions.
 *
 * This inline function is used once and permits ruthless optimisation.
 */
static inline void data2bitacts (uint8_t *data, bitact_t *actions8) {
	uint16_t xmit = 0x0100 | (uint8_t) *data;
	while (xmit > 0x0001) {
		*actions8++ = (xmit & 0x0001) ? SEND_ONE : SEND_ZERO;
		xmit >>= 1;
	}
}

/* Fill the data byte with 8 STOP_SENDING bits.
 *
 * This inline function is used once and permits ruthless optimisation.
 */
static inline void nodata2bitstops (bitact_t *actions8) {
	for (int i = 8; i > 0; i--) {
		*actions8++ = STOP_SENDING;
	}
}

/* Fill the data byte with 8 bit actions based on available data.
 * This updates the data pointer and length.
 *
 * This inline function is used once and permits ruthless optimisation.
 */
static inline void data2actions (uint8_t **data, unsigned *len, bitact_t *actions8) {
	if (*len == 0) {
		*data = NULL;
	}
	if (*data == NULL) {
		nodata2bitstops (actions8);
	} else {
		(*len)--;
		data2bitacts ((*data)++, actions8);
	}
}



/********** SETTINGS FOR ALAW_RECVMEDIA **********/



/* Flags for detected mangling (MANGLE_YES), detected non-mangling (MANGLE_NO)
 * and, because some samples are sent with an audio lower bit and do not allow
 * strict transmission of pre-mangled codes, ignored values that happen to be
 * mangle-output (MANGLE_IGN).  MANGLE_MASK allows only _YES and _NO.
 */
#define MANGLE_YES  0x01
#define MANGLE_NO   0x02
#define MANGLE_MASK 0x03

#define MANGLE_IGN  0x10


/* Array of mangled codes.  This is used before reading the LSB, to decide whether
 * the higher bits victimise the LSB to fixation in a A-μ-A translation process.
 * When this is the case, we choose to not get a data bit from it, but the value
 * sent may have been rewritten, so as a recipient we can detect whether it is.
 *
 * The index to this array is the un-flipped current byte, with either LSB bit
 * (so the mangled and unmangled values sit side by side) and the response is
 * MANGLE_YES or MANGLE_NO to determine that mangling happened, or did not happen
 * to a value where it would have been caused by a translation.
 *
 * There are very few MANGLE_YES, because A-law is so good to barely tamper with
 * data, and because we do not tamper with the audio signals to make them help
 * detect mangling (which is a choice that any transmitter may make).  Given that
 * we are conservative, it does no real harm to have few confirmations though;
 * we rather have many chances of getting convinced that mangling does not occur.
 * And this is present rather a lot in the table below.
 *
 * Negative and positive values look alike, except for their MSB.  This means
 * that the table lists the same 128 values in two series.
 */
uint8_t check_mangled [256] = {
	// 0x5.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0x4. -- ends with 0x4d<-0x4c, 0x4f<-0x4e, 0x49<-0x48, 0x4b<-0x4a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	// 0x7. -- ends with 0x79->0x78, 0x7b->0x7a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  ,
	// 0x6. -- ends with 0x6b->0x6a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_NO   , MANGLE_IGN  ,
	// 0x1. -- ends with 0x1b<-0x1a
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_YES  , MANGLE_NO   ,
	// 0x0.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0x3.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0x2.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xD.  Copies 0x5.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xC.  Copies 0x4.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   ,
	// 0xF.  Copies 0x7.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	MANGLE_NO   , MANGLE_IGN  , MANGLE_NO   , MANGLE_IGN  ,
	// 0xE.  Copies 0x6.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_NO   , MANGLE_IGN  ,
	// 0x9.  Copies 0x1.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , MANGLE_YES  , MANGLE_NO   ,
	// 0x8.  Copies 0x0.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xB.  Copies 0x3.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	// 0xA.  Copies 0x2.
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
	0           , 0           , 0           , 0           ,
};



/********** SETTINGS FOR OUTER ENCRYPTION IN THE ALAW CODEC **********/


/* We need to survive passing through A-law / uLaw transitions.
 *
 * This means that we need to count on bits that may get flipped,
 * for instance by masking out such bits from signatures.
 *
 * Equally important, the transitions are applied to the transport
 * bytes, not the original audio data.  Because of that, we must
 * first encrypt the audio part before we can tell which bits can
 * be overwritten.
 *
 * After the bits to encrypt are known, the bits can be set, either
 * in their encrypted variation or in plaintext for future repeated
 * encryption.  Such repeated encryption may be applied to the byte
 * as a whole, which is easier and more elegant than splitting the
 * encryption step.
 *
 * After using the encryption byte passed down from the base and
 * its connection to the XOR pad generator, we shall overwrite its
 * position with a signature byte.  This byte consists of the
 * cleartext (so pre-encryption) value of the byte in transit,
 * with any possibly-changed bits due to A-law / uLaw translation
 * set to zero, thus causing proper decoding.  It is however not
 * the intention to send one codec and receive the other; we do
 * assume that any uLaw is translated to A-law before it is
 * processed for Subliminal Messaging (because that improves the
 * bandwidth at virtually no cost).  We shall not consider the
 * transition encoding by XOR'ing with 0x55 in the signed data.
 *
 * The codecs are designed for bulk handling of audio frames, and
 * will treat the data more piecewise and even inject it byte by
 * byte if necessary.  The cryptographic handling however, also
 * has better efficiency with more bulk operations.  These are
 * part of the byte-by-byte handling in the codec, but they are
 * passed in and out of the codec as arrays and can easily span
 * several cryptographic block buffers, up to chunks of 256 bytes.
 * This helps the system to a fairly good efficiency, in spite of
 * the differences in chunk sizes and even dynamically changing
 * conditions when new forms are negotiated.
 */



/********** SUBLIME API FOR THE ALAW CODEC **********/



/* The management structure for this codec.
 */
struct sublime_alaw {
	//
	// Generic structures
	struct sublime base;
	//
	// ALAW_SENDMEDIA
	//
	// Actions for sending the next byte
	bitact_t byteact [8];
	int nextbyteact;
	uint8_t wrapmode;
	//
	// ALAW_RECVMEDIA
	//
	// Prepare for measurements
	uint8_t mangle_hist;
	//
	// Before FLAG and after BREAK, we enter a pause
	// While recognising these, probed may be true
	bool paused;
	bool probed;
	//
	// Count the number of consecutive 1 bits
	int ones;
	//
	// Shift buffer
	uint8_t outbyte;
	uint8_t outmask;
};

#define ALAW(x) ((struct sublime_alaw *) (x))
#define BASE(x) ((struct sublime      *) (x))


/* Create a new A-law object for sending or receiving.
 */
static struct sublime *alaw_create (bool recv_not_send, const char *codec) {
	//
	// Allocate the new structure
	struct sublime_alaw *new = calloc (1, sizeof (struct sublime_alaw));
	if (new == NULL) {
		return NULL;
	}
	//
	// Initialise the structure
	if (recv_not_send) {
		//
		// Initialise for receiving
		new->mangle_hist = 0x00;
		new->ones = 0;
		new->outbyte = 0x00;
		new->outmask = 0x01;
		new->paused = true;
		new->probed = true;
	} else {
		//
		// Initialise for sending
		new->nextbyteact = 99;
		new->wrapmode = MODE_MAJOR_START + 1;
	}
	//
	// Return the initialised structure
	return BASE(new);
}


/* Cleanup and destroy an A-law object.
 */
static void alaw_destroy (struct sublime *goner) {
	memset (goner, 0, sizeof (*goner));
	free (goner);
}


/* Filter A-law media before sending.
 */
static void alaw_sendmedia (struct sublime *sender, uint8_t *data, uint8_t *xorm, unsigned size) {
	struct sublime_alaw *alaw = ALAW (sender);
	//
	// A-law flips even bits
	const uint8_t flip = 0x55;
	//
	// The hdlcptr is only NULL after STOP_SENDING is actually processed,
	// and then it persists only when no new frame exists.
	bool done_sending = (sender->hdlcptr == NULL);
	if (done_sending && sublime_base_fill_hdlc_buffer (sender)) {
		done_sending = false;
		alaw->nextbyteact = 8;
	}
	//
	// Prepare for measurements
	//
	// Update the buffer with zero bits or actual data
	// Code taken from tool/hdlc2alawbits.c
	const unsigned orig_size = size;
	while (size-- > 0) {
		//
		// Choose between zeroing and filling in data bits
		//
		// Note: We apply encryption to the exponent to see
		//       how the network handles its exponents and,
		//       more importantly, A-law <--> muLaw changes.
		//       Further down we actually encrypt the sample.
		//
		uint8_t encr = *xorm;
		uint8_t bits = *data ^ flip;	/* Note: unflip input audio */
		uint8_t blib =  bits ^ encr;
		uint8_t exp  =  bits & 0x70;
		bool mangled = false;
		//
		// If we have nothing to send, we emit "0" bits as data
		if (done_sending) {
			//
			// Cheap-and-easy zero stuffing mode on data bits
			// Check if we should have data bits (behold mangling)
			if (exp <= 0x40) {
				//
				// We have at least one bit reserved for data
				//TODO// This yields false mangled-bit reports?
				if (lsbact_mangling [blib] != PASS_DATABIT) {
					//
					// Mangling with >1 bit for data
					if (exp <= 0x30) {
						//DEBUG// putchar ('z');
						bits &= 0xfd;
					//
					// Mangling with =1 bit for data
					} else {
						//DEBUG// putchar ('X');
					}
				//
				// Mangling does not apply, clear the LSB
				// as cheap reception only looks at that
				} else {
					//DEBUG// putchar ('Z');
					bits &= 0xfe;
				}
			}
		//
		// If we have things to send, we need to aim more properly
		} else {
			//
			// Determine mask and how many bits we can insert
			uint8_t mask;
			switch (exp) {
			case 0x40:
				mask = 0x01;
				break;
			case 0x30:
				mask = 0x02;
				break;
			case 0x20:
				mask = 0x04;
				break;
			case 0x10:
			case 0x00:
				mask = 0x08;
				break;
			default:
				mask = 0x00;
			}
			//
			// Collect the bits to send insert low bits into low bits
			while (mask > 0x00) {
				//
				// Be sure to have payload data -- probably sub-optimal here
				if ((alaw->nextbyteact < 8) && (alaw->byteact [alaw->nextbyteact] == STOP_SENDING)) {
				       if (sublime_base_fill_hdlc_buffer (sender)) {
						//
						// Setup for reading the payload bytes now, with
						// preparation for byte actions in the next check
						// to cause the removal of the conditions above.
						alaw->nextbyteact = 8;
				       } else {
					       //
					       // No change, meaning that STOP_SENDING will prevail
					       ;
				       }
				}
				//
				// Be prepared for byte actions
				if (alaw->nextbyteact >= 8) {
					data2actions (&sender->hdlcptr, &sender->hdlclen, alaw->byteact);
					alaw->nextbyteact = 0;
					//DEBUG// putchar ('\n');
				}
				//
				// Skip bit insertion if mangling may occur
				bitact_t todo = PASS_DATABIT;
				if (mask == 0x01) {
					blib = bits ^ encr;
					todo = lsbact_mangling [blib];
					mangled = todo != PASS_DATABIT;
					//DEBUG// if (todo != PASS_DATABIT) printf ("BLIB=0x%02x.  Set todo=%d (SEND_ZERO=%d, SEND_ONE=%d, PASS_DATABIT=%d)\n", blib, todo, SEND_ZERO, SEND_ONE, PASS_DATABIT);
				}
				//
				// Poll how the protocol wraps the next byte action
				// but only when mangling is not imminent; when it is,
				// better not encrypt the fun out of the lowest bit
				if (mangled) {
					encr &= 0xfe;
				} else {
					todo = wrap_nextbit (&alaw->wrapmode, alaw->byteact [alaw->nextbyteact]);
				}
				//
				// Start zeroing at the end of the protocol
				if (todo == STOP_SENDING) {
					sender->hdlcptr = NULL;
					todo = SEND_ZERO;
					done_sending = !sublime_base_fill_hdlc_buffer (sender);
				//
				// When the protocol is ready, use the next databit
				} else if (todo == PASS_DATABIT) {
					todo = alaw->byteact [alaw->nextbyteact++];
				}
				//
				// Now either send a "1" or "0" bit
				if (todo == SEND_ONE) {
					//
					// We shall send SEND_ONE
					//DEBUG// putchar ('1');
					//DEBUG// putchar (mask + 'A' - 1);
					bits |=  mask;
				} else {
					//
					// We shall SEND_ZERO or SEND_ZERO_SYNC
					//DEBUG// putchar ('0');
					//DEBUG// putchar (mask + 'M' - 1);
					bits &= ~mask;
					//
					// Now check if we completed BREAK-0
					if (todo == SEND_ZERO_SYNC) {
						//
						// We must sync outer crypto settings
						blib = bits ^ encr;
						if (check_mangled [blib]) {
							*xorm = bits & 0xfe;
						} else {
							*xorm = bits;
						}
						sublime_base_outercrypto_sync (
								sender,
								xorm + 1,
								orig_size - size,
								size);
					}
				}
				//
				// Count the data bit, modify the bit mask
				mask >>= 1;
			}
			//DEBUG// putchar (' ');
		}
		//
		// Take note of the data bytes, without the line flip,
		// but clear the lowest bit if it could get mangled
		*xorm++ = mangled ? (bits & 0xfe) : bits;
		//
		// Distort by XOR'ing the buffer with noise bits
		// as well as XOR'ing the xorm padding bits
		//DEBUG// printf ("%x", bits & 0x0f);
		blib    = bits ^ encr;
		*data++ = blib ^ flip;
	}
}


/* Filter A-law media after receiving.
 */
static void alaw_recvmedia (struct sublime *recver, uint8_t *data, uint8_t *xorm, unsigned size) {
	struct sublime_alaw *alaw = ALAW (recver);
	//
	// A-law flips even bits
	const uint8_t flip = 0x55;
	//
	// Update the buffer with bits of noise (update stats)
	// Code taken from tool/alawbits2hdlc.c
	const unsigned orig_size = size;
	while (size-- > 0) {
		//
		// Fetch bits without transport bit flipping
		// (blib, that is a small blob) and remove any
		// encryption to find the audio/data bits
		uint8_t encr = *xorm;
		uint8_t blib = *data ^ flip;	/* Note: unflip input audio */
		uint8_t bits =  blib ^ encr;
		//
		// Determine mask and the count the data carried
		// This interpretation follows from bits, except
		// that mangling is based on the blib value.
		uint8_t exp = bits & 0x70;
		uint8_t mask;
		switch (exp) {
		case 0x40:
			if (check_mangled [blib]) {
				/* Our single bit may have been mangled */
				/* TODO: Short-cut missing out on learning? */
				mask = 0x00;
				*xorm++ = bits;
				*data++ = bits ^ flip;
				continue;
			}
			mask = 0x01;
			break;
		case 0x30:
			mask = 0x02;
			break;
		case 0x20:
			mask = 0x04;
			break;
		case 0x10:
		case 0x00:
			mask = 0x08;
			break;
		default:
			/* All bits are reserved for audio */
			mask = 0x00;
			*xorm++ = bits;
			*data++ = bits ^ flip;
			continue;
		}
		//
		// Pausing continues as long as the low bit is 0
		// which is kept low for long stretches that
		// easily include the few bits per byte
		if (alaw->paused) {
			if (check_mangled [blib]) {
				/* Check one up from the LSB, if it is data */
				if (((bits & 0x02) == 0x02) && (mask >= 0x02)) {
					alaw->probed = true;
				}
			} else {
				/* Check the LSB, if it is data */
				if (((bits & 0x01) == 0x01) && (mask >= 0x01)) {
					alaw->probed = true;
				}
			}
			if (!alaw->probed) {
				//DEBUG// putchar ('#');
				*xorm++ = bits;
				*data++ = bits ^ flip;
				continue;
			}
		}
		//
		// Not paused, so process the data
		//DEBUG// printf ("[%02x]", bits & (mask | (mask-1)));
		//
		// Iterate over the HDLC data bits
		for (uint8_t bitm = mask; bitm > 0x00; bitm >>= 1) {
			if (bitm == 0x01) {
				uint8_t mangle_flags = check_mangled [blib];
				if (mangle_flags) {
					//DEBUG// printf ("BLIB=0x%02x flags MANGLE_NO=%d, MANGLE_YES=%d, MANGLE_IGN=%d\n", blib, (mangle_flags & MANGLE_NO) ? 1: 0, (mangle_flags & MANGLE_YES) ? 1 : 0, (mangle_flags & MANGLE_IGN) ? 1 : 0);
					alaw->mangle_hist |= mangle_flags;
					//
					// Remove encryption from the lowest bit
					bits = bits ^ (encr & 0x01);
					//
					// Loop: shift bitm to 0x00 and finish
					continue;
				}
			}
			//
			// Handle reception of a "1" data bit,
			// counting a sequence to see if it might
			// be a FLAG or BREAK signal
			if ((bits & bitm) != 0x00) {
				//DEBUG// putchar ('-');
				//DEBUG// putchar (bitm + 'A' - 1);
				//DEFER// putchar ('1');
				//DEFER// outbyte |= outmask;
				alaw->ones++;
			//
			// Handle reception of a "0" data bit,
			// including detection of FLAG and BREAK
			} else {
				//DEBUG// putchar ('.');
				//DEBUG// putchar (bitm + 'M' - 1);
				//DEFER// putchar ('0');
				//
				// Handle short "1" sequences up to 11111#5
				// where 11111#5 itself counts as an escape
				if (alaw->ones <= 5) {
					//
					// If we are paused, this is a failed probe
					if (alaw->paused) {
						alaw->probed = false;
						alaw->ones = 0;
						continue;
					}
					//
					// Take note if this is "0" bit stuffing
					bool bit_stuffed = (alaw->ones == 5);
					//
					// Shift in prior "1" bits (they are data)
					while (alaw->ones > 0) {
						//DEBUG// putchar ('1');
						alaw->outbyte |= alaw->outmask;
						if (alaw->outmask >= 0x80) {
							alaw->paused = ! sublime_base_putc_hdlc_buffer (recver, alaw->outbyte);
							alaw->outbyte = 0x00;
							alaw->outmask = 0x01;
						} else {
							alaw->outmask += alaw->outmask;
						}
						alaw->ones--;
					}
					//
					// Skip if the current "0" is bit stuffing
					if (bit_stuffed) {
						//DEBUG// putchar ('s');
						continue;
					}
				//
				// Handle 111111#6 as the FLAG sequence
				} else if (alaw->ones == 6) {
					//
					// Handle FLAG in prior "1" bits (need prior "0")
					//DEBUG// putchar ('f');
					alaw->outmask >>= 1;
					if ((alaw->outmask > 0x01) || (alaw->outbyte != 0x00)) {
						//DEBUG// fprintf (stderr, "FLAG --> dropping bits/mask 0x%02x/0x%02x forming an incomplete byte\n", (int) alaw->outbyte, (int) alaw->outmask);
					}
					//
					// Stop any existing output, and start a fresh buffer
					sublime_base_deliver_hdlc_buffer (recver);
					//DEBUG// printf ("\n\n===== START OF NEW HDLC FRAME =====\n");
					sublime_base_setup_hdlc_buffer (recver);
					//
					// Begin sampling fresh bytes
					alaw->outbyte = 0x00;
					alaw->outmask = 0x01;
					//
					// End any pause after probing successfully
					alaw->paused = false;
					alaw->probed = false;
					//
					// Skip the "0" and loop around for the next bit
					alaw->ones = 0;
					continue;
				//
				// Handle 1111111#7 as the BREAK sequence
				} else if (alaw->ones >= 7) {
					//
					// Handle BREAK in prior "1" bits
					//DEBUG// putchar ('b');
					//DEBUG// printf ("\n\n===== BREAK IN INPUT =====\n");
					alaw->outbyte = 0x00;
					alaw->outmask = 0x01;
					//
					// Start pausing and probing between BREAK and FLAG
					alaw->paused = true;
					alaw->probed = true;
					//
					// If new outer crypto was setup, it should start at
					// the byte following the one holding the "0" after a
					// BREAK.  Since we drop the "0" that's the next byte!
					if (check_mangled [blib]) {
						*xorm = bits & 0xfe;
					} else {
						*xorm = bits;
					}
					sublime_base_outercrypto_sync (
							recver,
							xorm + 1,
							orig_size - size,
							size);
					//
					// Skip the "0" and continue to the next bit
					alaw->ones = 0;
					continue;
				}
				//
				// The received "0" is just a boring data bit
				//DEBUG// putchar ('0');
				if (alaw->outmask >= 0x80) {
					//
					// Complete data byte, print it; on errors,
					// quietly fall back to paused mode
					alaw->paused = ! sublime_base_putc_hdlc_buffer (recver, alaw->outbyte);
					//DEBUG// printf ("<%02x>", (int) alaw->outbyte);
					//
					// Reset the output byte collector
					alaw->outbyte = 0x00;
					alaw->outmask = 0x01;
				} else {
					alaw->outmask += alaw->outmask;
				}
			}
		}
		//DEBUG// putchar (' ');
		//
		// Take note of the clear data, with encryption removal
		// for mangled bits, and take note of it for signatures
		*xorm++ = bits;
#if 0
		//
		// Zero the HDLC bits in the audio stream
		//TODO// Why?  Let's just output the audio with data
		bits &= ~(mask-1);
#endif
		*data++ = bits ^ flip;
	}
}


/* The exported structure to connect this codec to libsublime.
 */
struct sublime_codec sublime_codec_alaw = {
	.version   = SUBLIME_CODEC_VERSION,
	.functions = SUBLIME_CODEC_FUNCTIONS,
	.create    = alaw_create   ,
	.destroy   = alaw_destroy  ,
	.sendmedia = alaw_sendmedia,
	.recvmedia = alaw_recvmedia,
};

