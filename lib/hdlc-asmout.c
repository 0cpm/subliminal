/* hdlc-asmfile.c -- HDLC alternative for assembler file I/O.
 *
 * These definitions read HDLC frames from assembler files, or write
 * HDLC frames to a (dis)assembler file.  To the Subliminal base, they
 * form an alternative to the hdlc.c file for linking to a backend.
 *
 * The files being handled are global.  They handle all input and all
 * output, until they are replaced.  The files start as NULL and should
 * end as NULL.  Use hdlcasm_swap_{in,out}file() too swap it.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define SUBLIMINAL_HDLC_INTERNAL
#define SUBLIMINAL_HDLC_ASM_INTERNAL
#include <0cpm/subliminal.h>



/********** FILE MANAGEMENT **********/


static FILE *asmout = NULL;

static unsigned line_out = 0;

static uint8_t   lastaddr_out = 0x00;
static bool have_lastaddr_out = false;

FILE *sublime_hdlcasm_swap_outfile (FILE *newout) {
	if ((asmout != NULL) && (line_out > 0)) {
		fprintf (asmout, "\t.BREAK\n");
	}
	FILE *retval = asmout;
	asmout = newout;
	line_out = 0;
	have_lastaddr_out = false;
	lastaddr_out = 0x00;
	return retval;
}



/********** DISASSEMBLER **********/



void sublime_hdlc_putframe (struct sublime *recver, uint8_t address, uint8_t command, uint8_t *info, unsigned infolen) {
	//
	// Advance to the next line number
	line_out++;
	//
	// Print the address
	char addrbuf [10];
	const char *addrstr = addrbuf;
	if (have_lastaddr_out && (address == lastaddr_out)) {
		addrbuf [0] = '\0';
	} else {
		sprintf (addrbuf, "%02x", address);
		lastaddr_out = address;
		have_lastaddr_out = true;
	}
	//
	// Print the command
	char cmdhex [10];
	const char *cmd = NULL;
	if ((command & 0x01) == 0x00) {
		//
		// I-frame (so the I command)
		cmd = "I";
		;
	} else if ((command & 0x03) == 0x01) {
		//
		// S-frame
		const char *s_frames [4] = { "RR", "RNR", "REJ", "SREJ" };
		cmd = s_frames [(command >> 2) & 0x03];
		;
	} else {
		//
		// U-frame
		switch (command & 0xee) {
		case HDLC_UI() & 0xee:
			cmd = "UI";
			break;
		case HDLC_XID() & 0xee:
			cmd = "XID";
			break;
		case HDLC_SABM() & 0xee:
			cmd = "SABM";
			break;
		case HDLC_DISC() & 0xee:
			cmd = "DISC";
			break;
		case HDLC_DM() & 0xee:
			cmd = "DM";
			break;
		case HDLC_UP() & 0xee:
			cmd = "UP";
			break;
		case HDLC_TEST() & 0xee:
			cmd = "TEST";
			break;
		default:
			sprintf (cmdhex, "0x%02x", command);
			cmd = cmdhex;
			break;
		}
	}
	//
	// Print the Information field (
	char infield [4500];
	unsigned infieldlen = 0;
	if (infolen > 0) {
		infield [infieldlen++] = '\t';
		infield [infieldlen++] = '#';
		while (infolen-- > 0) {
			sprintf (infield + infieldlen, "%02x", *info++);
			infieldlen += 2;
		}
	}
	infield [infieldlen] = '\0';
	//
	// Print the HDLC assmbler line
	FILE *fout = (asmout != NULL) ? asmout : stderr;
	// fprintf (fout, "%05d\t%s\t%02x%s\n", line_out, addrstr, cmd, infield);
	fprintf (fout, "%s\t%s%s\n", addrstr, cmd, infield);
}

