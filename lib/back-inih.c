/* back-inih.c -- HDLC backend code for .INI parser inih
 *
 * HDLC backends are the I/O handlers for HDLC I and UI frames.
 * This backend parses .INI files from a standard location to
 * learn what UDP or TCP ports to connect to.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <ini.h>
#include <uuid/uuid.h>

#include "utlist.h"


#define SUBLIMINAL_BACK_INTERNAL
#include <0cpm/subliminal.h>



/* RFC 4122, Name string is a fully-qualified domain name,
 * identified by 6ba7b810-9dad-11d1-80b4-00c04fd430c8
 */
UUID_DEFINE (_namespace_dns,
		0x6b, 0xa7, 0xb8, 0x10,
		0x9d, 0xad,
		0x11, 0xd1,
		0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 );

/* Configuration variable names.
 */
enum configvarnums {
	CFGVAR_vmajor,
	CFGVAR_vminor,
	CFGVAR_host,
	CFGVAR_tcp,
	CFGVAR_udp,
	CFGVAR_active,
	CFGVAR_mru,
	// CFGVAR_encrypt,
	// CFGVAR_authenticata,
	// CFGVAR_authorise,
	//TODO// role
	//TODO// svcparams
	NUM_CFGVARS
};
const char *cfgvars [NUM_CFGVARS] = {
	"vmajor",
	"vminor",
	"host",
	"tcp",
	"udp",
	"active",
	"mru",
	// "encrypt",
	// "authenticate",
	// "authorise",
	//TODO// role
	//TODO// svcparams
};


/* A backend describes redirections to/from TCP and/or UDP.
 */
struct backend {
	//
	// The section_uuid is the key, and starts the structure
	uuid_t section_uuid;
	//
	// The section name as given first in the configuration file
	const char *section_name;
	//
	// Connective tissue for utlist.h
	struct backend *next;
	//
	// Configuration variable strings are set here, after strdup()
	const char *config [NUM_CFGVARS];
	//
	// Whether this is an active backend
	bool active;
	//
	// Whether this backend supports I frames / TCP
	bool does_I;
	//
	// Whether this backend supports UI frames / UDP
	bool does_UI;
	//
	// The MRU for this service
	uint16_t mru;
	//
	// The version byte
	uint8_t version;
};

/* The list of backends loaded from configuration files
 */
static struct backend *_backends = NULL;



/********** CONFIGURATION FILE LOADING (INTERNAL) **********/



static int config_cmp (const struct backend *a, const struct backend *b) {
	return memcmp (a->section_uuid, b->section_uuid, 16);
}

static int config_cb (void *user, const char *section, const char *name, const char *value) {
	//
	// We may be called for a new section
#if INI_CALL_HANDLER_ON_NEW_SECTION
	if ((name == NULL) || (value == NULL)) {
		return 1;
	}
#endif
	//
	// Debug
#ifdef DEBUG
	fprintf (stderr, "DEBUG: Callback from .ini file: [%s].%s = %s\n", section, name, value);
#endif
	//
	// Find the name as a cfgvar index
	int cfgvarnum = 0;
	while (strcasecmp (cfgvars [cfgvarnum], name) != 0) {
		if (++cfgvarnum >= NUM_CFGVARS) {
			//
			// Report and return an error
			fprintf (stderr, "Ignoring uknown name [%s].%s\n", section, name);
			return 0;
		}
	}
	//
	// Determine the section name (UUID or domain name)
	uuid_t cur_uuid;
	if ((_backends != NULL) && (strcmp (_backends->section_name, section) == 0)) {
		//
		// section name as added last
		uuid_copy (cur_uuid, _backends->section_uuid);
	} else if (strchr (section, '.') != NULL) {
		//
		// Domain name
		uuid_generate_md5 (cur_uuid, _namespace_dns, section, strlen (section));
	} else {
		//
		// UUID
		if (uuid_parse (section, cur_uuid) == 1) {
			//
			// Print and return an error
			fprintf (stderr, "Use FQDN or UUID in [%s]\n", section);
			return 0;
		}
	}
#ifdef DEBUG
	char _str [40];
	uuid_unparse_lower (cur_uuid, _str);
	fprintf (stderr, "DEBUG: Found UUID \"%s\" for [%s]\n", _str, section);
#endif
	//
	// Look for the UUID
	struct backend *cur_backend;
	struct backend *target = (struct backend *) cur_uuid;
	LL_SEARCH (_backends, cur_backend, target, config_cmp);
	//
	// If not found, add a section with this UUID
	if (cur_backend == NULL) {
		cur_backend = calloc (1, sizeof (struct backend));
		if (cur_backend == NULL) {
			return 0;
		}
		memcpy (cur_backend->section_uuid, cur_uuid, 16);
		cur_backend->section_name = strdup (section);
		LL_PREPEND (_backends, cur_backend);
	}
	//
	// Setup the name and value in the section
	if (cur_backend->config [cfgvarnum] != NULL) {
		//
		// It is already set; stubbornly refuse the new value
		fprintf (stderr, "No override for [%s].%s\n", section, name);
		return 1;
	}
	cur_backend->config [cfgvarnum] = strdup (value);
	//
	// Return OK
	return 1;
}

static bool config_bool (const char *value, bool fallback) {
	if (value == NULL) {
		return fallback;
	}
	switch (*value) {
		case 'y':
		case 'Y':
		case '1':
		case 't':
		case 'T':
			return true;
		case 'n':
		case 'N':
		case '0':
		case 'f':
		case 'F':
			return false;
		default:
			return fallback;
	}
}

static int config_int (const char *value, int min, int max, int fallback) {
	if (value == NULL) {
		return fallback;
	}
	char *endptr = NULL;
	unsigned long intval = strtol (value, &endptr, 10);
	if ((endptr == NULL) || (*endptr != '\0')) {
		fprintf (stderr, "Ignoring invalid integer: %s\n", value);
		return fallback;
	}
	if ((intval < min) || (intval > max)) {
		fprintf (stderr, "Integer %ld out of range %d..%d\n", intval, min, max);
		if (intval < min) {
			return min;
		} else {
			return max;
		}
	} else {
		return intval;
	}
}

void back_fini (void) {
	struct backend *tmp;
	struct backend *it;
	LL_FOREACH_SAFE (_backends, it, tmp) {
		free ((void *) it->section_name);
		for (int cfgvarnum = 0; cfgvarnum < NUM_CFGVARS; cfgvarnum++) {
			if (it->config [cfgvarnum] != NULL) {
				free ((void *) it->config [cfgvarnum]);
			}
		}
		free (it);
	}
	_backends = NULL;
}

bool back_init (void) {
	//
	// Form the configuration filename (personal or system-central)
	char *homedir = getenv ("HOME");
	if (homedir == NULL) {
		homedir = "/root";
	}
	char cfgfile [PATH_MAX + 3];
	snprintf (cfgfile, PATH_MAX + 1, "%s/.config/0cpm/subliminal.conf", homedir);
	if ((access (cfgfile, F_OK) != 0) && (errno == ENOENT)) {
		strcpy (cfgfile, "/etc/0cpm/subliminal.conf");
	}
	//
	// Open the personal file (if it exists) or else try the system file
	void *user = NULL;
	if (ini_parse (cfgfile, config_cb, user) == -1) {
		//
		// Ignore the output, take what we can get, only return failure if nothing
	}
	//
	// Parse configuration variables from strings to normal data
	struct backend *it;
	LL_FOREACH (_backends, it) {
		//
		// Parse the configuration variables
		it->does_I  = (it->config [CFGVAR_tcp] != NULL);
		it->does_UI = (it->config [CFGVAR_udp] != NULL);
		it->active = config_bool (it->config [CFGVAR_active], false);
		it->mru = config_int (it->config [CFGVAR_mru], 0, 65535, 1500);
		it->version = (config_int (it->config [CFGVAR_vmajor], 0, 15, 0) << 4)
		            | (config_int (it->config [CFGVAR_vminor], 0, 15, 0)     );
		// it->encrypt = config_bool (..., false);
		// tt->authorise = config_bool (..., false);
		// tt->authenticate = config_bool (... , tt->authorise);
	}
	//
	// Return success (that is, some backends are available)
	return (_backends != NULL);
}



/********** Actual structures for the .INI file backend **********/



/* The Sublime backend structure for inih
 */
struct sublime_back_inih {
	//
	// The base functions of Sublime
	struct sublime_back base;
	//
	// Socket for the stream and/or packet connection, -1 for none
	int sox_stream;
	int sox_packet;
	//TODO// int sox_keydata; // TCP; send size, recv keydata
	//
	//
};


//TODO// None of this is implemented yet, API design is in scaffolding



#include <stdio.h>

//TODO// TEST ENTRIES
struct sublime_back test_back_aa = {
	.address = 0xaa,
	.address_flags = SUBLIME_BACK_ADDRESS_XID_BOTH | SUBLIME_BACK_ADDRESS_SABM_BOTH
};
struct sublime_back test_back_bb = {
	.address = 0xbb,
	.address_flags = SUBLIME_BACK_ADDRESS_XID_BOTH | SUBLIME_BACK_ADDRESS_SABM_BOTH
};

struct sublime_back *sublime_back_poll_UI (uint8_t *data, unsigned *size) {
	static int i = 0;
	if (++i < 3) {
		*size = snprintf (data, *size, "UI #%d", i);
		return &test_back_aa;
	} else {
		return NULL;
	}
}

struct sublime_back *sublime_back_poll_I (uint8_t *data, unsigned *size) {
	static int i = 0;
	if (++i < 3) {
		*size = snprintf (data, *size, "I-frame #%d", i);
		return &test_back_bb;
	} else {
		return NULL;
	}
}

void sublime_back_send_UI (struct sublime_back *back, const uint8_t *data, unsigned size) {
	printf ("Delivering UI command frame, \"%.*s\"\n", size, data);
}

bool sublime_back_send_I (struct sublime_back *back, const uint8_t *data, unsigned size, uint8_t N_S) {
	printf ("Delivering  I command frame, \"%.*s\"\n", size, data);
	sublime_base_delivered_I (back, N_S);
	return true;
}



/********** OPTIONAL TEST PROGRAM **********/



#ifdef SUBLIME_CONFIGTEST_MAIN
int SUBLIME_CONFIGTEST_MAIN (int argc, char *argv []) {
	sublime_init ();
	struct backend *it;
	LL_FOREACH (_backends, it) {
		char _str [40];
		uuid_unparse_lower (it->section_uuid, _str);
		printf ("%s[%s] == [%s]\n", (it != _backends) ? "\n" : "", it->section_name, _str);
		for (int varnum = 0; varnum < NUM_CFGVARS; varnum++) {
			const char *curval = it->config [varnum];
			if (curval == NULL) {
				curval = "?";
			}
			printf ("%s = %s\n", cfgvars [varnum], curval);
		}
	}
	sublime_fini ();
}
#endif

